define("BaseGridDetailV2", ["terrasoft", "ConfigurationItemGenerator", "css!TcmSummaryCSS", "ConfigurationGridUtilities"],
	function(Terrasoft) {
		return {
			messages: {
				"DetailSummaryConfig": {
					mode: this.Terrasoft.MessageMode.PTP,
					direction: this.Terrasoft.MessageDirectionType.PUBLISH
				}
			},
			mixins: {
				ConfigurationGridUtilities: "Terrasoft.ConfigurationGridUtilities"
			},
			methods: {

				
				initData: function(callback, scope) {
					this.callParent([function() {
						Terrasoft.chain(
							function(next){
								this.initSummaryCollection();
								this.initSummaryConfigFromProfile();
								this.initGridChangedEvents();
								next();
							},
							function(next){
								this.loadSummaryConfigData(next);
							},
							function(next) {
								this.publishSummaryConfigMessages(next);
							},
							function() {
								callback.call(scope);
							},
							this);
					}, this]);
				},

				
				publishSummaryConfigMessages: function(callback) {
					var collection= this.get("SummaryCollection");
					var data = [];
					collection.each(function(item) {
						data.push({
							name: item.get("Caption").replace(":", ""),
							value: Terrasoft.decode(item.get("Value")),
							column: item.get("ColumnName"),
							aggregationType: item.get("AggregationType")
						});
					}, this);
					
					this.sandbox.publish("DetailSummaryConfig", data, [this.sandbox.id]);
					
					if (callback) {
						callback.call(this);
					}
				},

				
				initGridChangedEvents: function() {
					this.on("change:GridSettingsChanged", function(model, value, event) {
						if(value) {
							this.reInitSummaryColumnsButtons();
						}
					}, this);
				},

				
				delSummItemFromView: function(elId) {
					//remove from collection
					var collection = this.get("SummaryCollection");
					var item = collection.getItems().find(p=> p.get("Id") === elId);
					collection.removeByKey(elId);
					
					//remove from profile
					var conf = this.get("SummaryConfig");
					var delName = item.get("ColumnName")+"|"+item.get("AggregationType");
					delete conf[delName];
					this.saveSummaryConfigInUserProfile();
				},

				
				reInitSummaryColumnsButtons: function() {
					var collection = this.get("SummaryColumns");
					collection.clear();
					this.decorateSummaryColumnsCollection(collection);
					this.set("SummaryColumns", collection);
					this.updateSummaryColumnsCaptions(this.get("Profile"));
				},

				
				decorateSummaryColumnsCollection: function(collection) {
					var gridColumns = this.mixins.GridUtilities.getProfileColumns.call(this);
					var availableTypes = this.getAvailableDataTypes();
					this.getCountAllColumnConfig(collection);
					this.Terrasoft.each(gridColumns, function(column, columnName) {
						var columnType = column.dataValueType || (this.entitySchema.columns[column.path] ? this.entitySchema.columns[column.path].dataValueType : 1);
						if (availableTypes.includes(columnType)) {
							collection.addItem(this.getButtonMenuItem({
								Caption: {bindTo: this.name + columnName + "_SummaryColumnCaption"},
								Items: this.getItemsForColumn(columnName)
							}));
						}
					}, this);
				},

				
				getCountAllColumnConfig: function(collection) {
					collection.addItem(this.getButtonMenuItem({
						Caption: {bindTo: "Resources.Strings.AllCountColumnCaption"},
						Tag: {columnTag: "Id", type: Terrasoft.AggregationType.COUNT},
						Click: {bindTo: "addSummaryItem"}
					}));
				},

				
				saveRowChanges: function(row, callback, scope) {
					scope = scope || this;
					callback = this.loadSummaryConfigData.bind(this, callback || Terrasoft.emptyFn, scope);
					this.mixins.ConfigurationGridUtilities.saveRowChanges.call(this, row, callback, scope);
				},

				
				initSummaryCollection: function() {
					this.set("SummaryCollection", Ext.create("Terrasoft.BaseViewModelCollection"));
				},

				clearSummaryCollection: function() {
					this.get("SummaryCollection").clear();
				},

				
				setSummaryDataFromProfile: function() {
					var data = this.get("SummaryConfig");
					if (data.length > 0) {
						var collection = this.get("SummaryCollection");
						data.forEach(function(item){
							
							var addedItem = this.getSummaryItemContainerConfig(item);
							collection.add(addedItem.get("Id"), addedItem);
						}, this);
						this.set("SummaryCollection", collection);
					}
				},

				
				getSummaryItemContainerConfig: function(item) {
					var scope = this;
					return Ext.create("Terrasoft.BaseViewModel", {
						values: {
							Id: Terrasoft.generateGUID(),
							Caption: item.Caption,
							Value: item.Value,
							ColumnName: item.ColumnName,
							AggregationType: item.AggregationType
						},
						methods: {
							onDelSummClick: function() {
								scope.delSummItemFromView(this.get("Id"));
							}
						},
						Ext: this.Ext,
						Terrasoft: this.Terrasoft,
						sandbox: this.sandbox
					});
				},

				
				getSummaryItemViewConfig: function(view) {
					var container = {
						id: "mainContainer",
						className: "Terrasoft.Container",
						classes: {
							wrapClassName: ["main-summ-container"]
						},
						items: [
							{
								className: "Terrasoft.Container",
								classes: {
									wrapClassName: ["inside-summ-container-label"]
								},
								items: [{
									className: "Terrasoft.Button",
									caption: {bindTo: "Caption"},
									style: "transparent",
									enabled: false,
									classes: {
										textClass: "summ-label-class"
									}
								}]
							},
							{
								className: "Terrasoft.Container",
								classes: {
									wrapClassName: ["inside-summ-container-value"]
								},
								items: [{
									className: "Terrasoft.Button",
									caption: {bindTo: "Value"},
									style: "transparent",
									enabled: false,
									classes: {
										textClass: "summ-label-value-class"
									}
								}]
							},
							{
								className: "Terrasoft.Container",
								classes: {
									wrapClassName: ["inside-summ-container-del"]
								},
								items: [{
									className: "Terrasoft.Button",
									//caption: {bindTo: "Value"},
									labelConfig: {visible: false},
									imageConfig: this.get("Resources.Images.DelSummImage"),
									style: "transparent",
									click: {bindTo: "onDelSummClick"},
									classes: {
										wrapperClass: ["del-button-wrapper"]
									}
								}]
							}
						]
					};
					view.config = container;
				},

				
				onNewSummaryRowAdded: function(item, sendMessage) {
					var collection = this.get("SummaryCollection");
					var addedItem = this.getSummaryItemContainerConfig(item);
					collection.add(addedItem.get("Id"), addedItem);
					
					if (sendMessage) {
						this.publishSummaryConfigMessages();
					}
				},

				
				initToolsButtonMenu: function() {
					this.initSummaryColumns();
					this.callParent(arguments);
				},

				initSummaryConfigFromProfile: function() {
					var profile = this.getProfile();
					var summConf = !Ext.isEmpty(profile.summConf) ? profile.summConf : "{}";
					this.set("SummaryConfig", Terrasoft.decode(summConf));
					this.setSummaryDataFromProfile();
				},

				resetConf: function() {
					this.set("SummaryConfig", {});
					this.saveSummaryConfigInUserProfile();
					this.clearSummaryCollection();
					this.publishSummaryConfigMessages();
				},

				loadSummaryConfigData: function(callback, scope) {
					var config = this.get("SummaryConfig");
					if (config && Object.keys(config).length > 0) {
						var esq = Ext.create("Terrasoft.EntitySchemaQuery", {rootSchemaName: this.entitySchemaName});
						Terrasoft.each(config, function(item, tag) {
							esq.addAggregationSchemaColumn(item.column, item.aggType, tag);
						}, this);
						esq.filters.addItem(this.getFilters());
						esq.getEntityCollection(function(response) {
							var mainItem = response.collection.getByIndex(0);
							
							this.clearSummaryCollection();
							this.updateSummaryColumnsCaptions(this.get("Profile"));
							
							Terrasoft.each(mainItem.columns, function(mainItem, item, tag) {
								var column = item.columnPath;
								var value = mainItem.get(tag);
								var aggType = this.get("SummaryConfig")[tag].aggType;
								var columnCaption = this.getMyColumnCaption(column);
								if (columnCaption) {
									var allStr = this.getFullColumnCaption(aggType, columnCaption);
									this.onNewSummaryRowAdded({
										Caption: allStr,
										Value: value.toString(),
										ColumnName: column,
										AggregationType: aggType
									}, false);
								}
							}.bind(this, mainItem));
							
							if(callback) {
								callback.call(scope);
							}
						}, this);
					} else {
						if(callback) {
							callback.call(scope);
						}
					}
				},

				
				saveSummaryConfigInUserProfile: function() {
					var profile = this.getProfile();
					var key = this.getProfileKey();
					if (profile && key) {
						profile.summConf = JSON.stringify(this.get("SummaryConfig"));
						this.Terrasoft.utils.saveUserProfile(key, profile, false);
					}
				},

				
				addGridOperationsMenuItems: function(toolsButtonMenu) {
					this.callParent(arguments);
					var gridSummaryMenuItem = this.getGridSummaryMenuItem();
					if (gridSummaryMenuItem) {
						toolsButtonMenu.addItem(this.getButtonMenuSeparator());
						toolsButtonMenu.addItem(gridSummaryMenuItem);
					}
					var resetMenuItem = this.getResetMenuItem();
					if (resetMenuItem) {
						toolsButtonMenu.addItem(resetMenuItem);
					}
				},

				getResetMenuItem: function() {
					return this.getButtonMenuItem({
						Caption: {bindTo: "Resources.Strings.ResetButtonCaption"},
						Tag: "reset",
						Click: {bindTo: "resetConf"}
					});
				},

				
				getGridSummaryMenuItem: function() {
					return this.getButtonMenuItem({
						Caption: {"bindTo": "Resources.Strings.SummaryMenuCaption"},
						Items: this.get("SummaryColumns")
					});
				},

				getItemsForColumn: function(columnTag) {
					var collection = this.Ext.create("Terrasoft.BaseViewModelCollection");
					var evalTypes = this.getAvailableAggTypes();
					evalTypes.forEach(function(type) {
						collection.addItem(this.getButtonMenuItem({
							Caption: this.getCaptionBindOnType(type),
							Tag: {columnTag: columnTag, type: type},
							Click: {bindTo: "addSummaryItem"}
						}));
					}, this);
					return collection;
				},

				
				getCaptionBindOnType: function(type) {
					switch (type) {
						case Terrasoft.AggregationType.SUM:
							return {"bindTo": "Resources.Strings.sum"};
							break;
						case Terrasoft.AggregationType.AVG:
							return {"bindTo": "Resources.Strings.avg"};
							break;
						case Terrasoft.AggregationType.MAX:
							return {"bindTo": "Resources.Strings.max"};
							break;
						case Terrasoft.AggregationType.MIN:
							return {"bindTo": "Resources.Strings.min"};
							break;
						default:
							return "";
							break;
					}
				},

				
				getAvailableAggTypes:function() {
					return [Terrasoft.AggregationType.SUM, Terrasoft.AggregationType.AVG, Terrasoft.AggregationType.MAX, Terrasoft.AggregationType.MIN];
				},

				
				initSummaryColumns: function() {
					var collection = this.Ext.create("Terrasoft.BaseViewModelCollection");
					this.decorateSummaryColumnsCollection(collection);
					this.set("SummaryColumns", collection);
					this.updateSummaryColumnsCaptions(this.get("Profile"));
				},

				
				updateSummaryColumnsCaptions: function(columnsSettingsProfile) {
					var propertyName = this.getDataGridName();
					columnsSettingsProfile = propertyName ? columnsSettingsProfile[propertyName] : columnsSettingsProfile;
					columnsSettingsProfile = columnsSettingsProfile || {};
					var gridsColumnsConfig = columnsSettingsProfile.isTiled ?
						columnsSettingsProfile.tiledConfig :
						columnsSettingsProfile.listedConfig;
					if (gridsColumnsConfig) {
						var columnsConfig = this.Ext.decode(gridsColumnsConfig);
						for (var i = 0; i < columnsConfig.items.length; i++) {
							var cell = columnsConfig.items[i];
							var columnKey = cell.bindTo;
							var caption = this.getProfileColumnCaption(cell);
							this.set(this.name + columnKey + "_SummaryColumnCaption", caption);
						}
					}
				},

				
				getAvailableDataTypes: function() {
					return [Terrasoft.DataValueType.MONEY, Terrasoft.DataValueType.INTEGER, Terrasoft.DataValueType.FLOAT];
				},

				addSummaryItem: function(tag) {
					var column = tag.columnTag;
					var aggType = tag.type;
					var config = this.get("SummaryConfig");
					var tagName = column+"|"+aggType; //С - совместимость
					if (!config[tagName] || (config[tagName] && config[tagName].aggType !== aggType)) {
						this.getDataForSummary(column, aggType);
						var config = this.get("SummaryConfig");
						config[tagName]={aggType: aggType, column: column};
						this.saveSummaryConfigInUserProfile();
					} else {
						this.showInformationDialog(this.get("Resources.Strings.SummItemInListWarning"));
					}
				},

				getDataForSummary: function(column, aggType) {
					var esq = Ext.create("Terrasoft.EntitySchemaQuery", {rootSchemaName: this.entitySchemaName});
					esq.addAggregationSchemaColumn(column, aggType, column);
					esq.filters.addItem(this.getFilters());
					esq.getEntityCollection(function(column, aggType, response) {
						var item = response.collection.getByIndex(0);
						var value = item.get(column);
						var columnCaption = this.getMyColumnCaption(column);
						var allStr = this.getFullColumnCaption(aggType, columnCaption);
						this.onNewSummaryRowAdded({
							Caption: allStr,
							Value: value.toString(),
							ColumnName: column,
							AggregationType: aggType
						}, true);
					}.bind(this, column, aggType));
				},

				
				getMyColumnCaption: function(column) {
					return column === "Id" ? 
							this.get("Resources.Strings.AllCountColumnCaption") :
							this.get(this.name + column + "_SummaryColumnCaption");
				},

				
				getFullColumnCaption: function(aggType, columnCaption) {
					return aggType === Terrasoft.AggregationType.COUNT ?
						(columnCaption+":") :
						(this.get(this.getCaptionBindOnType(aggType).bindTo) + this.get("Resources.Strings.onColumnStr") + columnCaption+":");
				},

				onDataChanged: function() {
					this.loadSummaryConfigData();
					this.callParent();
				}


			},
			diff: /**SCHEMA_DIFF*/[
				{
					"operation": "insert",
					"name": "SummaryConfigCL",
					"values": {
						"generateId": true,
						"generator": "ConfigurationItemGenerator.generateContainerList",
						"idProperty": "Id",
						"onGetItemConfig": "getSummaryItemViewConfig",
						"collection": "SummaryCollection"
					},
					"parentName": "Detail",
					"propertyName": "tools"
				}
			]/**SCHEMA_DIFF*/
		};
	}
);
