define("FileDetailV2", [],
	function() {
	return {
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "move",
				"name": "SetListedModeButton",
				"parentName": "Detail",
				"propertyName": "tools",
				"index": 3
			},
			{
				"operation": "move",
				"name": "SetTiledModeButton",
				"parentName": "Detail",
				"propertyName": "tools",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "SummaryConfigCL",
				"parentName": "Detail",
				"propertyName": "tools",
				"index": 5
			}
		]
		/**SCHEMA_DIFF*/
	};
});
