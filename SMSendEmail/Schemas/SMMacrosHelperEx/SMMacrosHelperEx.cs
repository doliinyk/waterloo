namespace Terrasoft.Configuration.Utils
{
	using System;
	using System.Linq;
	using System.Collections.Generic;
	using System.Text.RegularExpressions;
	using System.Data.SqlClient;
	using Terrasoft.Core;
	using Terrasoft.Core.Entities;
	using Terrasoft.Common;
	using Terrasoft.Core.DB;
	using Terrasoft.Web.Http.Abstractions;
	using global::Common.Logging;


	/// <summary>
	/// Class implement processing macros.
	/// </summary>
	public class SMMacrosHelperEx : MacrosHelperV2
	{
		private readonly Guid _recipientMacrosId = new Guid("EBB220F0-F36B-1410-3088-1C6F65E24DB2");

        private readonly Guid _currentUserMacrosId = new Guid("3C5A2014-F46B-1410-2288-1C6F65E24DB2");

        private readonly Guid _ownerMacrosId = new Guid("1D151866-0C45-413E-B288-9654F44113AE");

        protected virtual bool IsRecipientMacrosExists(List<MacrosInfo> macrosInfos) {
			return macrosInfos.Any(macrosInfo => macrosInfo.Id != Guid.Empty
				&& macrosInfo.ParentId == _recipientMacrosId);
		}
		
		private SMMacrosHelperEx() : base() { } 
		
		public SMMacrosHelperEx(UserConnection userConnection) : base() {
			this.UserConnection = userConnection;
		}

        protected virtual void UpdateColumnPathForOwnerMacroses(IEnumerable<MacrosInfo> macrosCollection, string entitySchemaName)
        {
            if (string.IsNullOrEmpty(entitySchemaName))
            {
                entitySchemaName = "Contact";
            }
            EntitySchema entitySchema = UserConnection.EntitySchemaManager.FindInstanceByName(entitySchemaName);
            if (entitySchema != null && entitySchema.OwnerColumn != null)
            {
                foreach (MacrosInfo macros in macrosCollection.Where(x => x.ParentId == _ownerMacrosId))
                {
                    macros.ColumnPath = string.Format("{0}.{1}", entitySchema.OwnerColumn.Name, macros.ColumnPath);
                }
            }
        }
        /// <summary>
        /// Returns arguments for macros values
        /// </summary>
        /// <param name="entityName">Entity name which used to get arguments.</param>
        /// <param name="recordId">Record which used to get arguments.</param>
        /// <param name="macrosInfos">Macros storage.</param>
        /// <returns>Arguments.</returns>
        public virtual Dictionary<Guid, object> GetMacrosArguments(string entityName, Guid recordId,
				List<MacrosInfo> macrosInfos, Guid recipientId) {
			Dictionary<Guid, object> arguments = macrosInfos.Select(x => x.ParentId)
				.Distinct()
				.Where(x => x != _currentUserMacrosId && x != _recipientMacrosId)
				.ToDictionary(x => x, x => (object)new KeyValuePair<string, Guid>(entityName, recordId));
			if (IsRecipientMacrosExists(macrosInfos) && recipientId != Guid.Empty) {
				arguments.Add(_recipientMacrosId, new KeyValuePair<string, Guid>("Contact", recipientId));
			}
			if (IsCurrentUserMacrosExists(macrosInfos)) {
				arguments.Add(_currentUserMacrosId, new KeyValuePair<string, Guid>("Contact", UserConnection.CurrentUser.ContactId));
			}
			return arguments;
		}
		
		public string GetPlainTextTemplate(string textTemplate, string requestedEntityName, Guid requestedEntityId, Guid recipientId) {
			var macrosInfo = GetMacrosCollection(textTemplate);
			UpdateColumnPathForOwnerMacroses(macrosInfo, requestedEntityName);
			var arguments = GetMacrosArguments(requestedEntityName, requestedEntityId, macrosInfo, recipientId);
			var macrosValues = GetMacrosValues(macrosInfo as IEnumerable<MacrosInfo>, arguments);
			var plainTextTemplate = macrosValues.Count > 0 ? ReplaceMacros(textTemplate, macrosValues) : textTemplate;
			return plainTextTemplate;
		}

	}
}