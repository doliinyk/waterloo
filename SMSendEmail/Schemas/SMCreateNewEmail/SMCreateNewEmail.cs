namespace Terrasoft.Core.Process.Configuration
{

	using Newtonsoft.Json;
	using Newtonsoft.Json.Linq;
	using System;
	using System.Collections;
	using System.Collections.Generic;
	using System.Collections.ObjectModel;
	using System.Globalization;
	using Terrasoft.Common;
	using Terrasoft.Common.Json;
	using Terrasoft.Configuration;
	using Terrasoft.Core;
	using Terrasoft.Core.Configuration;
	using Terrasoft.Core.DB;
	using Terrasoft.Core.Entities;
	using Terrasoft.Core.Factories;
	using Terrasoft.Core.Process;
	using Terrasoft.Mail.Sender;
	using Terrasoft.UI.WebControls.Controls;

	#region Class: SMCreateNewEmail

	/// <exclude/>
	public partial class SMCreateNewEmail
	{

		#region Methods: Protected

		protected override bool InternalExecute(ProcessExecutingContext context) {
			var emailTemplateExecutor = new EmailTemplateExecutor(UserConnection);
						
			Guid ContactId =Contact == Guid.Empty ? (new Select(UserConnection)
				.Column("ContactId")
				.From("ContactCommunication")
				.Where("Number").IsEqual(Column.Parameter(Recipient)) as Select).ExecuteScalar<Guid>()
			: Contact;
			
			string sender =  (new Select(UserConnection)
				.Column("UserName")
				.From("MailboxSyncSettings")
				.Where("Id").IsEqual(Column.Parameter(Sender)) as Select).ExecuteScalar<string>();
			
			var EmailTemplateEntity = GetFormattedEmailTemplateEntityEx(EmailTemplateId, RecordId, ContactId);
			
			string subject = EmailTemplateEntity.GetTypedColumnValue<string>("Subject");
			string body = EmailTemplateEntity.GetTypedColumnValue<string>("Body");
			
			
			var activity = new Terrasoft.Configuration.Activity(UserConnection);
			activity.SetDefColumnValues();
			activity.TypeId = Terrasoft.Configuration.ActivityConsts.EmailTypeUId;
			activity.Recepient = Recipient;
			
			activity.Title = subject;
			activity.Body = body;
			
			activity.PriorityId = new Guid("AB96FA02-7FE6-DF11-971B-001D60E938C6");
			activity.StartDate = UserConnection.CurrentUser.GetCurrentDateTime();
			activity.DueDate = UserConnection.CurrentUser.GetCurrentDateTime();
			activity.OwnerId = UserConnection.CurrentUser.ContactId;
			activity.AuthorId = UserConnection.CurrentUser.ContactId;
			activity.StatusId = Terrasoft.Configuration.ActivityConsts.NewStatusUId;
			activity.MessageTypeId = Terrasoft.Configuration.ActivityConsts.OutgoingEmailTypeId;
			activity.Sender = Terrasoft.UI.WebControls.Utilities.ControlUtilities.HtmlDecode(string.Format("{0} <{1}>", UserConnection.CurrentUser.ContactName, sender));
			activity.ActivityCategoryId =  Terrasoft.Configuration.ActivityConsts.EmailActivityCategoryId;
			activity.IsHtmlBody = true;
			
			if (ContactId != Guid.Empty) {
				activity.ContactId = ContactId;
			}
			activity.Save();
			
			ActivityId = activity.Id;
			
			CommonUtilities.CopyFileDetail(UserConnection, EmailTemplateId, ActivityId, "EmailTemplate", "Activity", true);
			
			if (Send) {
					var emailClientFactory = ClassFactory.Get<EmailClientFactory>(new ConstructorArgument("userConnection", UserConnection));
					var activityEmailSender = new ActivityEmailSender(emailClientFactory, UserConnection);
					activityEmailSender.Send(ActivityId);
			}
			
			return true;
		}

		#endregion

		#region Methods: Public

		public override bool CompleteExecuting(params object[] parameters) {
			return base.CompleteExecuting(parameters);
		}

		public override void CancelExecuting(params object[] parameters) {
			base.CancelExecuting(parameters);
		}

		public override string GetExecutionData() {
			return string.Empty;
		}

		public override ProcessElementNotification GetNotificationData() {
			return base.GetNotificationData();
		}

		public virtual Entity GetFormattedEmailTemplateEntityEx(Guid templateId, Guid recordId, Guid recipientId) {
			string languageCode = GetLanguageCode(recipientId);
			string schemaName = languageCode == "en" ? "EmailTemplate" : "EmailTemplateLang";
			string emailTemplatePrefix = languageCode == "en" ? string.Empty : "EmailTemplate.";
			var templateESQ = new EntitySchemaQuery(UserConnection.EntitySchemaManager, schemaName);
			
			templateESQ.PrimaryQueryColumn.IsAlwaysSelect = true;
			templateESQ.AddColumn("Subject");
			templateESQ.AddColumn("Body");
			var objectSchemaColumnName = templateESQ.AddColumn(emailTemplatePrefix + "Object.Name").Name;
			
			templateESQ.Filters.Add(
				templateESQ.CreateFilterWithParameters(
					FilterComparisonType.Equal,
					emailTemplatePrefix + "Id",
					templateId
				)
			);
			
			if (languageCode != "en")
			{
				templateESQ.Filters.Add(
					templateESQ.CreateFilterWithParameters(
						FilterComparisonType.Contain,
						"Language.Code",
						languageCode + '-'
					)
				);
			}
			
			var entityCollection = templateESQ.GetEntityCollection(UserConnection);
			var subject = entityCollection[0].GetTypedColumnValue<string>("Subject");
			var body = entityCollection[0].GetTypedColumnValue<string>("Body");
			
			var objectSchemaName = entityCollection[0].GetTypedColumnValue<string>(objectSchemaColumnName);
			
			subject = new Terrasoft.Configuration.Utils.SMMacrosHelperEx(UserConnection).GetPlainTextTemplate(subject , objectSchemaName, recordId, recipientId);
			entityCollection[0].SetColumnValue("Subject", subject);
			
			body = new Terrasoft.Configuration.Utils.SMMacrosHelperEx(UserConnection).GetPlainTextTemplate(body , objectSchemaName, recordId, recipientId);
			entityCollection[0].SetColumnValue("Body", body);
			
			return entityCollection[0];
		}

		public virtual string GetLanguageCode(Guid contactId) {
			string languageCode = "en";
			if (contactId != Guid.Empty) {
				var select = new Select(UserConnection)
					.Column("l", "Code")
					.From("Contact", "c")
					.InnerJoin("SysLanguage").As("l").On("c", "LanguageId").IsEqual("l", "Id")
					.Where("c", "Id").IsEqual(Column.Parameter(contactId)) as Select;
				string contactLanguageCode = select.ExecuteScalar<string>();
				int hyphenIndex = contactLanguageCode.IndexOf('-');
				if (hyphenIndex != -1) {
					languageCode = contactLanguageCode.Substring(0, hyphenIndex);
				}
			}
			return languageCode;
		}

		#endregion

	}

	#endregion

}

