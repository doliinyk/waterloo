--Original Mar 24, 2021 @ 18:41 by OS
--EXEC SP_GENERATE_MERGE 'WatTripCancelReason'

MERGE INTO [WatTripCancelReason] AS [Target]
USING (VALUES
  (N'ACA0246D-B366-4A18-B8BA-3DFAA7F3E0D8','2021-03-24T22:37:23.8961983',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2021-03-24T22:37:23.8961983',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Personal Reason',N'',0)
 ,(N'93781D89-AA6B-4AD0-9D45-64F2333915F7','2021-03-24T22:37:01.3325040',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2021-03-24T22:37:35.9124418',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Violation of Waterloo Travel Policy',N'',0)
 ,(N'BE2A5166-DBB3-489F-9439-AAB1A7F310D3','2021-03-24T22:38:28.7913827',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2021-03-24T22:38:28.7913827',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Natural Desaster',N'',0)
 ,(N'3176C656-B983-4C7E-BE62-EAC4F0F1D625','2021-03-24T22:39:27.7328038',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2021-03-24T22:39:27.7328038',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Airline Cancellation',N'',0)
 ,(N'C6B9E6E5-C117-48F9-B3EC-F3024761E048','2021-03-24T22:38:13.3374909',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2021-03-24T22:38:13.3374909',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Political Unrest in Destination',N'',0)
) AS [Source] ([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[CreatedOn], [Target].[CreatedOn]) IS NOT NULL OR NULLIF([Target].[CreatedOn], [Source].[CreatedOn]) IS NOT NULL OR 
	NULLIF([Source].[CreatedById], [Target].[CreatedById]) IS NOT NULL OR NULLIF([Target].[CreatedById], [Source].[CreatedById]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedOn], [Target].[ModifiedOn]) IS NOT NULL OR NULLIF([Target].[ModifiedOn], [Source].[ModifiedOn]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedById], [Target].[ModifiedById]) IS NOT NULL OR NULLIF([Target].[ModifiedById], [Source].[ModifiedById]) IS NOT NULL OR 
	NULLIF([Source].[Name], [Target].[Name]) IS NOT NULL OR NULLIF([Target].[Name], [Source].[Name]) IS NOT NULL OR 
	NULLIF([Source].[Description], [Target].[Description]) IS NOT NULL OR NULLIF([Target].[Description], [Source].[Description]) IS NOT NULL OR 
	NULLIF([Source].[ProcessListeners], [Target].[ProcessListeners]) IS NOT NULL OR NULLIF([Target].[ProcessListeners], [Source].[ProcessListeners]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[CreatedOn] = [Source].[CreatedOn], 
  [Target].[CreatedById] = [Source].[CreatedById], 
  [Target].[ModifiedOn] = [Source].[ModifiedOn], 
  [Target].[ModifiedById] = [Source].[ModifiedById], 
  [Target].[Name] = [Source].[Name], 
  [Target].[Description] = [Source].[Description], 
  [Target].[ProcessListeners] = [Source].[ProcessListeners]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
 VALUES([Source].[Id],[Source].[CreatedOn],[Source].[CreatedById],[Source].[ModifiedOn],[Source].[ModifiedById],[Source].[Name],[Source].[Description],[Source].[ProcessListeners]);
