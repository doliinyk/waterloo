--Version Sep 29, 2020 @ by Kavian
--EXEC SP_GENERATE_MERGE 'WatFinalizationTasksLookup'

MERGE INTO [WatFinalizationTasksLookup] AS [Target]
USING (VALUES
  (N'0D609AB4-0BEC-428A-AD51-47A910F0BF6C','2020-11-14T17:57:46.0101874',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-11-14T17:57:46.0101874',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'2',N'Task 2',0,N'')
 ,(N'92004D15-561A-4D38-9194-95758DC18242','2020-11-14T17:57:50.5258664',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-11-14T17:57:57.5885035',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'3',N'Task 3',0,N'')
 ,(N'0A00468E-D1EA-4C63-829E-99E8D4A80D6C','2020-11-14T17:57:42.1038663',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-11-14T17:57:42.1038663',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'1',N'Task 1',0,N'')
 ,(N'99888C90-CF28-47F2-B1F9-A01871BFA940','2020-11-14T17:57:53.5728206',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-11-14T17:58:01.6979478',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'4',N'Task 4',0,N'')
) AS [Source] ([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners],[WatTaskNumber])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[CreatedOn], [Target].[CreatedOn]) IS NOT NULL OR NULLIF([Target].[CreatedOn], [Source].[CreatedOn]) IS NOT NULL OR 
	NULLIF([Source].[CreatedById], [Target].[CreatedById]) IS NOT NULL OR NULLIF([Target].[CreatedById], [Source].[CreatedById]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedOn], [Target].[ModifiedOn]) IS NOT NULL OR NULLIF([Target].[ModifiedOn], [Source].[ModifiedOn]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedById], [Target].[ModifiedById]) IS NOT NULL OR NULLIF([Target].[ModifiedById], [Source].[ModifiedById]) IS NOT NULL OR 
	NULLIF([Source].[Name], [Target].[Name]) IS NOT NULL OR NULLIF([Target].[Name], [Source].[Name]) IS NOT NULL OR 
	NULLIF([Source].[Description], [Target].[Description]) IS NOT NULL OR NULLIF([Target].[Description], [Source].[Description]) IS NOT NULL OR 
	NULLIF([Source].[ProcessListeners], [Target].[ProcessListeners]) IS NOT NULL OR NULLIF([Target].[ProcessListeners], [Source].[ProcessListeners]) IS NOT NULL OR 
	NULLIF([Source].[WatTaskNumber], [Target].[WatTaskNumber]) IS NOT NULL OR NULLIF([Target].[WatTaskNumber], [Source].[WatTaskNumber]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[CreatedOn] = [Source].[CreatedOn], 
  [Target].[CreatedById] = [Source].[CreatedById], 
  [Target].[ModifiedOn] = [Source].[ModifiedOn], 
  [Target].[ModifiedById] = [Source].[ModifiedById], 
  [Target].[Name] = [Source].[Name], 
  [Target].[Description] = [Source].[Description], 
  [Target].[ProcessListeners] = [Source].[ProcessListeners], 
  [Target].[WatTaskNumber] = [Source].[WatTaskNumber]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners],[WatTaskNumber])
 VALUES([Source].[Id],[Source].[CreatedOn],[Source].[CreatedById],[Source].[ModifiedOn],[Source].[ModifiedById],[Source].[Name],[Source].[Description],[Source].[ProcessListeners],[Source].[WatTaskNumber]);
