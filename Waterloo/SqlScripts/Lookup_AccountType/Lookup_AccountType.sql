--Version Seo 2, 2020 @ 12:19 by Oliver (Origin: Kavian)
--EXEC SP_GENERATE_MERGE 'AccountType'
--Sep 2, 2020 Removed Consortium from List and added Our Company

MERGE INTO [AccountType] AS [Target]
USING (VALUES
  (N'03A75490-53E6-DF11-971B-001D60E938C6',NULL,N'22C5540C-D9B1-49EF-8EB7-72419B78E57C','2020-08-31T14:39:59.302',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Organization',N'',0)
 ,(N'F2C0CE97-53E6-DF11-971B-001D60E938C6',NULL,N'22C5540C-D9B1-49EF-8EB7-72419B78E57C','2020-08-31T14:40:07.838',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Institution',N'',0)
 ,(N'57412FAD-53E6-DF11-971B-001D60E938C6',NULL,N'22C5540C-D9B1-49EF-8EB7-72419B78E57C','2020-08-13T03:10:35.687',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Our company',N'',0)
) AS [Source] ([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[CreatedOn], [Target].[CreatedOn]) IS NOT NULL OR NULLIF([Target].[CreatedOn], [Source].[CreatedOn]) IS NOT NULL OR 
	NULLIF([Source].[CreatedById], [Target].[CreatedById]) IS NOT NULL OR NULLIF([Target].[CreatedById], [Source].[CreatedById]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedOn], [Target].[ModifiedOn]) IS NOT NULL OR NULLIF([Target].[ModifiedOn], [Source].[ModifiedOn]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedById], [Target].[ModifiedById]) IS NOT NULL OR NULLIF([Target].[ModifiedById], [Source].[ModifiedById]) IS NOT NULL OR 
	NULLIF([Source].[Name], [Target].[Name]) IS NOT NULL OR NULLIF([Target].[Name], [Source].[Name]) IS NOT NULL OR 
	NULLIF([Source].[Description], [Target].[Description]) IS NOT NULL OR NULLIF([Target].[Description], [Source].[Description]) IS NOT NULL OR 
	NULLIF([Source].[ProcessListeners], [Target].[ProcessListeners]) IS NOT NULL OR NULLIF([Target].[ProcessListeners], [Source].[ProcessListeners]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[CreatedOn] = [Source].[CreatedOn], 
  [Target].[CreatedById] = [Source].[CreatedById], 
  [Target].[ModifiedOn] = [Source].[ModifiedOn], 
  [Target].[ModifiedById] = [Source].[ModifiedById], 
  [Target].[Name] = [Source].[Name], 
  [Target].[Description] = [Source].[Description], 
  [Target].[ProcessListeners] = [Source].[ProcessListeners]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
 VALUES([Source].[Id],[Source].[CreatedOn],[Source].[CreatedById],[Source].[ModifiedOn],[Source].[ModifiedById],[Source].[Name],[Source].[Description],[Source].[ProcessListeners]);