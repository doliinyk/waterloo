--Version Aug 28, 2020 @ 16:53 by Oliver
--EXEC SP_GENERATE_MERGE 'WatDelegationStage'

MERGE INTO [WatDelegationStage] AS [Target]
USING (VALUES
  (N'95EB03CB-0412-4074-8C92-059875DDB7D8','2020-08-28T15:08:24.7889883',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-08-28T15:08:24.7889883',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Arrival',N'',0)
 ,(N'A5BC052E-10D3-41DD-8788-23C9113B9DC6','2020-08-28T15:08:39.1242442',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-08-28T15:08:39.1242442',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Completed',N'',0)
 ,(N'42D4723F-5BDB-4B3A-A5AB-2D73703E35CA','2020-08-28T15:08:15.3283016',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-08-28T15:08:15.3283016',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Planning Complete',N'',0)
 ,(N'796F33F5-ACE6-434A-A2B6-7FC5FC184F8F','2020-08-28T15:07:58.5847408',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-08-28T15:07:58.5847408',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Approval',N'',0)
 ,(N'F1107FF3-A206-44BB-BF02-8A4F82BF9F2C','2020-08-28T15:07:42.0092614',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-08-28T15:07:42.0092614',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'New',N'',0)
 ,(N'F3BF73FD-67FF-4A6B-88BB-ACCDA1C7A26B','2020-08-28T15:08:47.4954468',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-08-28T15:08:47.4954468',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Declined',N'',0)
 ,(N'24812C21-CE9F-4159-8433-B13D851EFC70','2020-08-28T15:08:05.1093069',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-08-28T15:08:05.1093069',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Planning',N'',0)
 ,(N'AAE53289-762F-484E-A996-C8422679111E','2020-08-28T15:07:50.5133844',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-08-28T15:07:50.5133844',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Assessment',N'',0)
 ,(N'06130F04-E80E-49EC-91BC-EB6B6F699161','2020-08-28T15:08:56.6275515',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-08-28T15:08:56.6275515',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Cancelled',N'',0)
) AS [Source] ([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[CreatedOn], [Target].[CreatedOn]) IS NOT NULL OR NULLIF([Target].[CreatedOn], [Source].[CreatedOn]) IS NOT NULL OR 
	NULLIF([Source].[CreatedById], [Target].[CreatedById]) IS NOT NULL OR NULLIF([Target].[CreatedById], [Source].[CreatedById]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedOn], [Target].[ModifiedOn]) IS NOT NULL OR NULLIF([Target].[ModifiedOn], [Source].[ModifiedOn]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedById], [Target].[ModifiedById]) IS NOT NULL OR NULLIF([Target].[ModifiedById], [Source].[ModifiedById]) IS NOT NULL OR 
	NULLIF([Source].[Name], [Target].[Name]) IS NOT NULL OR NULLIF([Target].[Name], [Source].[Name]) IS NOT NULL OR 
	NULLIF([Source].[Description], [Target].[Description]) IS NOT NULL OR NULLIF([Target].[Description], [Source].[Description]) IS NOT NULL OR 
	NULLIF([Source].[ProcessListeners], [Target].[ProcessListeners]) IS NOT NULL OR NULLIF([Target].[ProcessListeners], [Source].[ProcessListeners]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[CreatedOn] = [Source].[CreatedOn], 
  [Target].[CreatedById] = [Source].[CreatedById], 
  [Target].[ModifiedOn] = [Source].[ModifiedOn], 
  [Target].[ModifiedById] = [Source].[ModifiedById], 
  [Target].[Name] = [Source].[Name], 
  [Target].[Description] = [Source].[Description], 
  [Target].[ProcessListeners] = [Source].[ProcessListeners]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
 VALUES([Source].[Id],[Source].[CreatedOn],[Source].[CreatedById],[Source].[ModifiedOn],[Source].[ModifiedById],[Source].[Name],[Source].[Description],[Source].[ProcessListeners]);