--Version Sep 16, 2020 @ 10:45 by Kavian
--EXEC SP_GENERATE_MERGE 'WatRequestType'

MERGE INTO [WatRequestType] AS [Target]
USING (VALUES
  (N'27A01368-F1C0-4051-B26D-21B3B213FE65','2020-09-15T17:20:14.7181752',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-09-15T17:20:14.7181752',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Agent',N'',0)
 ,(N'7C6D730D-EECB-41C0-8E5D-3E9CC4167DA9','2020-09-15T17:19:37.1115102',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-11-12T23:09:38.0355880',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Agreement Net New',N'',0)
 ,(N'D43C349A-63D0-43D9-900E-6FC9F69F4D70','2020-09-15T17:20:19.0836085',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-09-15T17:20:19.0836085',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Other',N'',0)
 ,(N'73DDA689-7C0C-4612-8D07-763141100AE9','2020-09-15T17:20:09.7361106',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-09-15T17:20:09.7361106',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Information',N'',0)
 ,(N'445E5C6F-8B13-4A3D-B2FC-7E350D828005','2020-09-15T17:19:44.1638653',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-09-15T17:19:44.1648618',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Agreement Renewal',N'',0)
 ,(N'0E0E61E1-C12C-49FB-90BE-F4E20A3C4FC7','2020-09-15T17:19:49.7497230',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-09-15T17:19:49.7497230',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Agreement Amendment/Extension',N'',0)
) AS [Source] ([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[CreatedOn], [Target].[CreatedOn]) IS NOT NULL OR NULLIF([Target].[CreatedOn], [Source].[CreatedOn]) IS NOT NULL OR 
	NULLIF([Source].[CreatedById], [Target].[CreatedById]) IS NOT NULL OR NULLIF([Target].[CreatedById], [Source].[CreatedById]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedOn], [Target].[ModifiedOn]) IS NOT NULL OR NULLIF([Target].[ModifiedOn], [Source].[ModifiedOn]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedById], [Target].[ModifiedById]) IS NOT NULL OR NULLIF([Target].[ModifiedById], [Source].[ModifiedById]) IS NOT NULL OR 
	NULLIF([Source].[Name], [Target].[Name]) IS NOT NULL OR NULLIF([Target].[Name], [Source].[Name]) IS NOT NULL OR 
	NULLIF([Source].[Description], [Target].[Description]) IS NOT NULL OR NULLIF([Target].[Description], [Source].[Description]) IS NOT NULL OR 
	NULLIF([Source].[ProcessListeners], [Target].[ProcessListeners]) IS NOT NULL OR NULLIF([Target].[ProcessListeners], [Source].[ProcessListeners]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[CreatedOn] = [Source].[CreatedOn], 
  [Target].[CreatedById] = [Source].[CreatedById], 
  [Target].[ModifiedOn] = [Source].[ModifiedOn], 
  [Target].[ModifiedById] = [Source].[ModifiedById], 
  [Target].[Name] = [Source].[Name], 
  [Target].[Description] = [Source].[Description], 
  [Target].[ProcessListeners] = [Source].[ProcessListeners]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
 VALUES([Source].[Id],[Source].[CreatedOn],[Source].[CreatedById],[Source].[ModifiedOn],[Source].[ModifiedById],[Source].[Name],[Source].[Description],[Source].[ProcessListeners]);
