--Version Sep 16, 2020 @ 10:50 by Kavian
--EXEC SP_GENERATE_MERGE 'WatPartnershipRequestStatus'

MERGE INTO [WatPartnershipRequestStatus] AS [Target]
USING (VALUES
  (N'6035C7FF-B402-4512-A209-234EBCD9A8FE','2020-09-15T17:18:45.5309798',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-09-15T17:18:45.5309798',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Canceled',N'',0)
 ,(N'C4FAF7D9-DFFF-47B4-9157-42236A072E30','2020-09-15T17:18:21.5023400',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-09-15T17:18:21.5023400',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Assessment',N'',0)
 ,(N'BC77E01A-92B7-44EE-B10C-4A89FBC335E1','2020-11-12T01:09:20.2118662',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-11-12T01:09:20.2118662',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Approval',N'',0)
 ,(N'FB8197F3-A384-485D-BF80-5BC059BC085F','2020-09-15T17:18:13.8665481',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-09-15T17:18:13.8665481',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'New',N'',0)
 ,(N'0F8FFEEF-3F2F-40F3-9A50-79AC01AA590A','2020-09-15T17:18:25.4731690',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-09-15T17:18:25.4731690',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Approved',N'',0)
 ,(N'96F37CCD-D7FB-4C6C-99E1-89EA798CA9FF','2020-09-15T17:18:39.7230195',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-09-15T17:18:39.7230195',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Declined',N'',0)
 ,(N'4E823080-19A6-48A7-95E7-F244FCA76D31','2020-09-15T17:18:32.1720921',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-09-15T17:18:32.1720921',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Completed',N'',0)
) AS [Source] ([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[CreatedOn], [Target].[CreatedOn]) IS NOT NULL OR NULLIF([Target].[CreatedOn], [Source].[CreatedOn]) IS NOT NULL OR 
	NULLIF([Source].[CreatedById], [Target].[CreatedById]) IS NOT NULL OR NULLIF([Target].[CreatedById], [Source].[CreatedById]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedOn], [Target].[ModifiedOn]) IS NOT NULL OR NULLIF([Target].[ModifiedOn], [Source].[ModifiedOn]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedById], [Target].[ModifiedById]) IS NOT NULL OR NULLIF([Target].[ModifiedById], [Source].[ModifiedById]) IS NOT NULL OR 
	NULLIF([Source].[Name], [Target].[Name]) IS NOT NULL OR NULLIF([Target].[Name], [Source].[Name]) IS NOT NULL OR 
	NULLIF([Source].[Description], [Target].[Description]) IS NOT NULL OR NULLIF([Target].[Description], [Source].[Description]) IS NOT NULL OR 
	NULLIF([Source].[ProcessListeners], [Target].[ProcessListeners]) IS NOT NULL OR NULLIF([Target].[ProcessListeners], [Source].[ProcessListeners]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[CreatedOn] = [Source].[CreatedOn], 
  [Target].[CreatedById] = [Source].[CreatedById], 
  [Target].[ModifiedOn] = [Source].[ModifiedOn], 
  [Target].[ModifiedById] = [Source].[ModifiedById], 
  [Target].[Name] = [Source].[Name], 
  [Target].[Description] = [Source].[Description], 
  [Target].[ProcessListeners] = [Source].[ProcessListeners]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
 VALUES([Source].[Id],[Source].[CreatedOn],[Source].[CreatedById],[Source].[ModifiedOn],[Source].[ModifiedById],[Source].[Name],[Source].[Description],[Source].[ProcessListeners]);
