--Version Oct 25, 2020 @ 13:53 by Oliver
--Original Version Sep 16, 2020 @ 17:49 by Oliver

--exec sp_generate_merge 'SysModuleInWorkplace', @from = 'from SysModuleInWorkplace where SysWorkplaceId in (select Id from SysWorkplace where Name in (''Delegations''))'

DELETE FROM SysModuleInWorkplace WHERE SysWorkplaceId in (select Id from SysWorkplace where Name in ('Delegations'))

MERGE INTO [SysModuleInWorkplace] AS [Target]
USING (VALUES
  (N'EAAFFB68-0369-4AA1-8F83-2168409F51E9','2020-10-25T17:38:15.860',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-10-25T17:38:15.860',N'EEFDE995-2248-41CE-9575-B7529329E8DF',0,6,N'0BAE043F-E729-4809-84DD-03F75C1EBBA6',N'8DB29F31-DD27-49D9-A9DE-2AE03F80D12D')
 ,(N'E63AA1E7-76D6-478D-AD17-3AD4E5200683','2020-10-25T17:38:14.704',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-10-25T17:38:14.704',N'EEFDE995-2248-41CE-9575-B7529329E8DF',0,0,N'0BAE043F-E729-4809-84DD-03F75C1EBBA6',N'0C5063C9-8180-E011-AFBC-00155D04320C')
 ,(N'AEF20B33-107C-46E9-B792-4C0E34252EFB','2020-10-25T17:38:16.469',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-10-25T17:38:16.469',N'EEFDE995-2248-41CE-9575-B7529329E8DF',0,4,N'0BAE043F-E729-4809-84DD-03F75C1EBBA6',N'005063C9-8180-E011-AFBC-00155D04320C')
 ,(N'DA20034F-8C45-438A-AA04-6D3E1EA39B06','2020-10-25T17:38:15.308',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-10-25T17:38:15.308',N'EEFDE995-2248-41CE-9575-B7529329E8DF',0,2,N'0BAE043F-E729-4809-84DD-03F75C1EBBA6',N'CCC81628-A0C6-4E4C-8FA2-1FB6C7568548')
 ,(N'52DA4B2D-00E2-4632-9A46-C07AFB197304','2020-10-25T17:38:17.101',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-10-25T17:38:17.101',N'EEFDE995-2248-41CE-9575-B7529329E8DF',0,1,N'0BAE043F-E729-4809-84DD-03F75C1EBBA6',N'055063C9-8180-E011-AFBC-00155D04320C')
 ,(N'F0A5AC9D-BE8C-4B83-973C-C700B34E1F3B','2020-10-25T17:38:14.012',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-10-25T17:38:14.012',N'EEFDE995-2248-41CE-9575-B7529329E8DF',0,3,N'0BAE043F-E729-4809-84DD-03F75C1EBBA6',N'A8730B4D-8E33-48DB-9D7C-BE95B0328124')
 ,(N'28CCCDF5-9F94-4291-BBCB-D1016D9B935E','2020-10-25T17:38:13.244',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-10-25T17:38:13.244',N'EEFDE995-2248-41CE-9575-B7529329E8DF',0,5,N'0BAE043F-E729-4809-84DD-03F75C1EBBA6',N'065063C9-8180-E011-AFBC-00155D04320C')
) AS [Source] ([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[ProcessListeners],[Position],[SysWorkplaceId],[SysModuleId])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[CreatedOn], [Target].[CreatedOn]) IS NOT NULL OR NULLIF([Target].[CreatedOn], [Source].[CreatedOn]) IS NOT NULL OR 
	NULLIF([Source].[CreatedById], [Target].[CreatedById]) IS NOT NULL OR NULLIF([Target].[CreatedById], [Source].[CreatedById]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedOn], [Target].[ModifiedOn]) IS NOT NULL OR NULLIF([Target].[ModifiedOn], [Source].[ModifiedOn]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedById], [Target].[ModifiedById]) IS NOT NULL OR NULLIF([Target].[ModifiedById], [Source].[ModifiedById]) IS NOT NULL OR 
	NULLIF([Source].[ProcessListeners], [Target].[ProcessListeners]) IS NOT NULL OR NULLIF([Target].[ProcessListeners], [Source].[ProcessListeners]) IS NOT NULL OR 
	NULLIF([Source].[Position], [Target].[Position]) IS NOT NULL OR NULLIF([Target].[Position], [Source].[Position]) IS NOT NULL OR 
	NULLIF([Source].[SysWorkplaceId], [Target].[SysWorkplaceId]) IS NOT NULL OR NULLIF([Target].[SysWorkplaceId], [Source].[SysWorkplaceId]) IS NOT NULL OR 
	NULLIF([Source].[SysModuleId], [Target].[SysModuleId]) IS NOT NULL OR NULLIF([Target].[SysModuleId], [Source].[SysModuleId]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[CreatedOn] = [Source].[CreatedOn], 
  [Target].[CreatedById] = [Source].[CreatedById], 
  [Target].[ModifiedOn] = [Source].[ModifiedOn], 
  [Target].[ModifiedById] = [Source].[ModifiedById], 
  [Target].[ProcessListeners] = [Source].[ProcessListeners], 
  [Target].[Position] = [Source].[Position], 
  [Target].[SysWorkplaceId] = [Source].[SysWorkplaceId], 
  [Target].[SysModuleId] = [Source].[SysModuleId]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[ProcessListeners],[Position],[SysWorkplaceId],[SysModuleId])
 VALUES([Source].[Id],[Source].[CreatedOn],[Source].[CreatedById],[Source].[ModifiedOn],[Source].[ModifiedById],[Source].[ProcessListeners],[Source].[Position],[Source].[SysWorkplaceId],[Source].[SysModuleId]);
