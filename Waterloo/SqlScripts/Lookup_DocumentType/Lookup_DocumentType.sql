--Version Oct 13, 2020 by Kavian
--EXEC SP_GENERATE_MERGE 'DocumentType'

MERGE INTO [DocumentType] AS [Target]
USING (VALUES
  (N'2CB5CAC1-1523-E011-A94A-00155D043204',NULL,N'22C5540C-D9B1-49EF-8EB7-72419B78E57C','2020-08-13T03:35:56.508',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Incoming document',N'',0,N'Correspondence')
 ,(N'61F7A573-52E6-DF11-971B-001D60E938C6',NULL,N'22C5540C-D9B1-49EF-8EB7-72419B78E57C','2020-08-13T03:35:57.085',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Regulation',N'',0,N'Decree')
 ,(N'39B28624-98E6-DF11-971B-001D60E938C6',NULL,N'22C5540C-D9B1-49EF-8EB7-72419B78E57C','2020-08-13T03:35:58.937',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Minutes',N'',0,N'Protocol')
 ,(N'63B657C9-A3AC-4B67-B011-B7403032CA80','2020-10-13T18:18:04.565',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-10-13T18:18:04.565',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Agreement document',N'',0,N'')
) AS [Source] ([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners],[Code])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[CreatedOn], [Target].[CreatedOn]) IS NOT NULL OR NULLIF([Target].[CreatedOn], [Source].[CreatedOn]) IS NOT NULL OR 
	NULLIF([Source].[CreatedById], [Target].[CreatedById]) IS NOT NULL OR NULLIF([Target].[CreatedById], [Source].[CreatedById]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedOn], [Target].[ModifiedOn]) IS NOT NULL OR NULLIF([Target].[ModifiedOn], [Source].[ModifiedOn]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedById], [Target].[ModifiedById]) IS NOT NULL OR NULLIF([Target].[ModifiedById], [Source].[ModifiedById]) IS NOT NULL OR 
	NULLIF([Source].[Name], [Target].[Name]) IS NOT NULL OR NULLIF([Target].[Name], [Source].[Name]) IS NOT NULL OR 
	NULLIF([Source].[Description], [Target].[Description]) IS NOT NULL OR NULLIF([Target].[Description], [Source].[Description]) IS NOT NULL OR 
	NULLIF([Source].[ProcessListeners], [Target].[ProcessListeners]) IS NOT NULL OR NULLIF([Target].[ProcessListeners], [Source].[ProcessListeners]) IS NOT NULL OR 
	NULLIF([Source].[Code], [Target].[Code]) IS NOT NULL OR NULLIF([Target].[Code], [Source].[Code]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[CreatedOn] = [Source].[CreatedOn], 
  [Target].[CreatedById] = [Source].[CreatedById], 
  [Target].[ModifiedOn] = [Source].[ModifiedOn], 
  [Target].[ModifiedById] = [Source].[ModifiedById], 
  [Target].[Name] = [Source].[Name], 
  [Target].[Description] = [Source].[Description], 
  [Target].[ProcessListeners] = [Source].[ProcessListeners], 
  [Target].[Code] = [Source].[Code]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners],[Code])
 VALUES([Source].[Id],[Source].[CreatedOn],[Source].[CreatedById],[Source].[ModifiedOn],[Source].[ModifiedById],[Source].[Name],[Source].[Description],[Source].[ProcessListeners],[Source].[Code]);