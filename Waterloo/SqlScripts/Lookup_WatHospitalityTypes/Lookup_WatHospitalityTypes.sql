--Version Sep 8, 2020 @ 11:24 by Oliver
--EXEC SP_GENERATE_MERGE 'WatHospitalityTypes'

MERGE INTO [WatHospitalityTypes] AS [Target]
USING (VALUES
  (N'2CD235B9-6DCC-40DE-AA28-07E16D0792D4','2020-09-06T16:31:23.1556590',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-09-06T16:31:23.1556590',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Food and Beverages',N'',0)
 ,(N'2E5FFBB7-9CE2-47A3-BEAA-42E8B769211F','2020-09-06T15:49:39.2056959',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-09-06T15:49:39.2056959',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Event Space',N'',0)
 ,(N'4AC4077B-ABA1-4014-9D03-46A0B1A55FB3','2020-09-06T15:50:08.9982479',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-09-06T15:50:08.9982479',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Transportation',N'',0)
 ,(N'9AB36A0B-3897-4BCB-A4B9-987607588F4E','2020-09-06T15:49:57.6586303',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-09-06T15:49:57.6586303',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Gifts',N'',0)
 ,(N'08A6B499-777B-4115-8DC0-C23B8644F56E','2020-09-06T16:28:39.8100210',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-09-06T16:28:39.8100210',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Event Space Services',N'',0)
 ,(N'F27A2A6F-FE0F-449F-86F7-C53E00F2C0A2','2020-09-06T15:49:52.5371725',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-09-06T15:49:52.5371725',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Food & Beverages',N'',0)
 ,(N'3986640D-02EB-4B15-BC9A-D231ED5178FA','2020-09-06T15:50:18.3103449',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-09-06T15:50:18.3103449',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Accommodation',N'',0)
) AS [Source] ([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[CreatedOn], [Target].[CreatedOn]) IS NOT NULL OR NULLIF([Target].[CreatedOn], [Source].[CreatedOn]) IS NOT NULL OR 
	NULLIF([Source].[CreatedById], [Target].[CreatedById]) IS NOT NULL OR NULLIF([Target].[CreatedById], [Source].[CreatedById]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedOn], [Target].[ModifiedOn]) IS NOT NULL OR NULLIF([Target].[ModifiedOn], [Source].[ModifiedOn]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedById], [Target].[ModifiedById]) IS NOT NULL OR NULLIF([Target].[ModifiedById], [Source].[ModifiedById]) IS NOT NULL OR 
	NULLIF([Source].[Name], [Target].[Name]) IS NOT NULL OR NULLIF([Target].[Name], [Source].[Name]) IS NOT NULL OR 
	NULLIF([Source].[Description], [Target].[Description]) IS NOT NULL OR NULLIF([Target].[Description], [Source].[Description]) IS NOT NULL OR 
	NULLIF([Source].[ProcessListeners], [Target].[ProcessListeners]) IS NOT NULL OR NULLIF([Target].[ProcessListeners], [Source].[ProcessListeners]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[CreatedOn] = [Source].[CreatedOn], 
  [Target].[CreatedById] = [Source].[CreatedById], 
  [Target].[ModifiedOn] = [Source].[ModifiedOn], 
  [Target].[ModifiedById] = [Source].[ModifiedById], 
  [Target].[Name] = [Source].[Name], 
  [Target].[Description] = [Source].[Description], 
  [Target].[ProcessListeners] = [Source].[ProcessListeners]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
 VALUES([Source].[Id],[Source].[CreatedOn],[Source].[CreatedById],[Source].[ModifiedOn],[Source].[ModifiedById],[Source].[Name],[Source].[Description],[Source].[ProcessListeners]);
