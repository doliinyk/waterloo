--Version Sep 16, 2020 @ 20:40 by Oliver
--exec sp_generate_merge 'SysSettings', @from = 'from SysSettings where Code = ''WatEmailAccountWaterlooInternational'''

MERGE INTO [SysSettings] AS [Target]
USING (VALUES
  (N'0D0965F7-4303-4A87-B40E-B87C4E933529','2020-09-17T00:37:32.174','2020-09-17T00:37:32.174',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Waterloo International Email Account',N'Lookup',0,1,NULL,N'WatEmailAccountWaterlooInternational',N'Email account from which general emails are being sent',0,N'5E487721-02E2-48EE-B755-DFA5160F5315',0)
) AS [Source] ([Id],[ModifiedOn],[CreatedOn],[CreatedById],[ModifiedById],[Name],[ValueTypeName],[IsPersonal],[IsCacheable],[SysFolderId],[Code],[Description],[ProcessListeners],[ReferenceSchemaUId],[IsSSPAvailable])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[ModifiedOn], [Target].[ModifiedOn]) IS NOT NULL OR NULLIF([Target].[ModifiedOn], [Source].[ModifiedOn]) IS NOT NULL OR 
	NULLIF([Source].[CreatedOn], [Target].[CreatedOn]) IS NOT NULL OR NULLIF([Target].[CreatedOn], [Source].[CreatedOn]) IS NOT NULL OR 
	NULLIF([Source].[CreatedById], [Target].[CreatedById]) IS NOT NULL OR NULLIF([Target].[CreatedById], [Source].[CreatedById]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedById], [Target].[ModifiedById]) IS NOT NULL OR NULLIF([Target].[ModifiedById], [Source].[ModifiedById]) IS NOT NULL OR 
	NULLIF([Source].[Name], [Target].[Name]) IS NOT NULL OR NULLIF([Target].[Name], [Source].[Name]) IS NOT NULL OR 
	NULLIF([Source].[ValueTypeName], [Target].[ValueTypeName]) IS NOT NULL OR NULLIF([Target].[ValueTypeName], [Source].[ValueTypeName]) IS NOT NULL OR 
	NULLIF([Source].[IsPersonal], [Target].[IsPersonal]) IS NOT NULL OR NULLIF([Target].[IsPersonal], [Source].[IsPersonal]) IS NOT NULL OR 
	NULLIF([Source].[IsCacheable], [Target].[IsCacheable]) IS NOT NULL OR NULLIF([Target].[IsCacheable], [Source].[IsCacheable]) IS NOT NULL OR 
	NULLIF([Source].[SysFolderId], [Target].[SysFolderId]) IS NOT NULL OR NULLIF([Target].[SysFolderId], [Source].[SysFolderId]) IS NOT NULL OR 
	NULLIF([Source].[Code], [Target].[Code]) IS NOT NULL OR NULLIF([Target].[Code], [Source].[Code]) IS NOT NULL OR 
	NULLIF([Source].[Description], [Target].[Description]) IS NOT NULL OR NULLIF([Target].[Description], [Source].[Description]) IS NOT NULL OR 
	NULLIF([Source].[ProcessListeners], [Target].[ProcessListeners]) IS NOT NULL OR NULLIF([Target].[ProcessListeners], [Source].[ProcessListeners]) IS NOT NULL OR 
	NULLIF([Source].[ReferenceSchemaUId], [Target].[ReferenceSchemaUId]) IS NOT NULL OR NULLIF([Target].[ReferenceSchemaUId], [Source].[ReferenceSchemaUId]) IS NOT NULL OR 
	NULLIF([Source].[IsSSPAvailable], [Target].[IsSSPAvailable]) IS NOT NULL OR NULLIF([Target].[IsSSPAvailable], [Source].[IsSSPAvailable]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[ModifiedOn] = [Source].[ModifiedOn], 
  [Target].[CreatedOn] = [Source].[CreatedOn], 
  [Target].[CreatedById] = [Source].[CreatedById], 
  [Target].[ModifiedById] = [Source].[ModifiedById], 
  [Target].[Name] = [Source].[Name], 
  [Target].[ValueTypeName] = [Source].[ValueTypeName], 
  [Target].[IsPersonal] = [Source].[IsPersonal], 
  [Target].[IsCacheable] = [Source].[IsCacheable], 
  [Target].[SysFolderId] = [Source].[SysFolderId], 
  [Target].[Code] = [Source].[Code], 
  [Target].[Description] = [Source].[Description], 
  [Target].[ProcessListeners] = [Source].[ProcessListeners], 
  [Target].[ReferenceSchemaUId] = [Source].[ReferenceSchemaUId], 
  [Target].[IsSSPAvailable] = [Source].[IsSSPAvailable]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[ModifiedOn],[CreatedOn],[CreatedById],[ModifiedById],[Name],[ValueTypeName],[IsPersonal],[IsCacheable],[SysFolderId],[Code],[Description],[ProcessListeners],[ReferenceSchemaUId],[IsSSPAvailable])
 VALUES([Source].[Id],[Source].[ModifiedOn],[Source].[CreatedOn],[Source].[CreatedById],[Source].[ModifiedById],[Source].[Name],[Source].[ValueTypeName],[Source].[IsPersonal],[Source].[IsCacheable],[Source].[SysFolderId],[Source].[Code],[Source].[Description],[Source].[ProcessListeners],[Source].[ReferenceSchemaUId],[Source].[IsSSPAvailable]);
