--Version Oct 15, 2020 by Kavian
--EXEC SP_GENERATE_MERGE 'WatPurposeOfTravel'

MERGE INTO [WatPurposeOfTravel] AS [Target]
USING (VALUES
  (N'BC7CD3FF-CB72-41F6-8EEE-345E77D63286','2020-10-15T14:06:37.4020124',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-10-15T14:06:37.4020124',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Study Abroad',N'',0)
 ,(N'8F413652-A32F-4AAA-B20C-5E74F84FD84C','2020-10-15T14:07:06.7046887',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-10-15T14:07:06.7046887',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Other',N'',0)
 ,(N'152AD42E-91D7-4C0F-B96D-69136B75AE9C','2020-10-15T14:06:45.4785496',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-10-15T14:06:45.4785496',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Co-op Work Term',N'',0)
 ,(N'0C20A884-8C85-4A59-BDBC-80E9CEE5DB4A','2020-10-15T14:06:50.9691510',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-10-15T14:06:50.9691510',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Research',N'',0)
 ,(N'DB7B8F1B-80B9-4B8A-BD35-AD97FB4EB075','2020-10-15T14:07:02.2327602',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-10-15T14:07:02.2327602',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Field Course',N'',0)
 ,(N'F4D91420-C20B-464D-9BC1-CC7E28ADDB14','2020-10-15T14:06:31.4049539',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-10-15T14:06:31.4049539',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Exchange',N'',0)
 ,(N'2B1A6192-5B5E-40EE-9211-D5FD37C23FFF','2020-10-15T14:06:56.3721120',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-10-15T14:06:56.3721120',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Field Work',N'',0)
) AS [Source] ([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[CreatedOn], [Target].[CreatedOn]) IS NOT NULL OR NULLIF([Target].[CreatedOn], [Source].[CreatedOn]) IS NOT NULL OR 
	NULLIF([Source].[CreatedById], [Target].[CreatedById]) IS NOT NULL OR NULLIF([Target].[CreatedById], [Source].[CreatedById]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedOn], [Target].[ModifiedOn]) IS NOT NULL OR NULLIF([Target].[ModifiedOn], [Source].[ModifiedOn]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedById], [Target].[ModifiedById]) IS NOT NULL OR NULLIF([Target].[ModifiedById], [Source].[ModifiedById]) IS NOT NULL OR 
	NULLIF([Source].[Name], [Target].[Name]) IS NOT NULL OR NULLIF([Target].[Name], [Source].[Name]) IS NOT NULL OR 
	NULLIF([Source].[Description], [Target].[Description]) IS NOT NULL OR NULLIF([Target].[Description], [Source].[Description]) IS NOT NULL OR 
	NULLIF([Source].[ProcessListeners], [Target].[ProcessListeners]) IS NOT NULL OR NULLIF([Target].[ProcessListeners], [Source].[ProcessListeners]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[CreatedOn] = [Source].[CreatedOn], 
  [Target].[CreatedById] = [Source].[CreatedById], 
  [Target].[ModifiedOn] = [Source].[ModifiedOn], 
  [Target].[ModifiedById] = [Source].[ModifiedById], 
  [Target].[Name] = [Source].[Name], 
  [Target].[Description] = [Source].[Description], 
  [Target].[ProcessListeners] = [Source].[ProcessListeners]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
 VALUES([Source].[Id],[Source].[CreatedOn],[Source].[CreatedById],[Source].[ModifiedOn],[Source].[ModifiedById],[Source].[Name],[Source].[Description],[Source].[ProcessListeners]);