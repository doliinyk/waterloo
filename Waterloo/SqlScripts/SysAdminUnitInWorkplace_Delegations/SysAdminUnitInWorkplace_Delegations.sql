/*Updated in Oct 9, 2020 AM by Kavian
exec sp_generate_merge 'SysAdminUnitInWorkplace ', 
@from='from SysAdminUnitInWorkplace where SysWorkplaceId in (select Id from SysWorkplace where Name in (''Delegations''))'*/
DELETE FROM SysModuleInWorkplace WHERE SysWorkplaceId in (select Id from SysWorkplace where Name in ('Delegations'))
MERGE INTO [SysAdminUnitInWorkplace ] AS [Target]
USING (VALUES
  (N'432A7EEE-26DF-4345-A6DA-28019CD41D17','2020-10-09T14:30:15.238',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-10-09T14:30:15.238',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',0,N'0BAE043F-E729-4809-84DD-03F75C1EBBA6',N'A29A3BA5-4B0D-DE11-9A51-005056C00008')
 ,(N'9C2C6D40-9DDB-4776-A4C0-94766F97D1CC','2020-10-09T14:30:21.074',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-10-09T14:30:21.074',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',0,N'0BAE043F-E729-4809-84DD-03F75C1EBBA6',N'83A43EBC-F36B-1410-298D-001E8C82BCAD')
) AS [Source] ([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[ProcessListeners],[SysWorkplaceId],[SysAdminUnitId])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[CreatedOn], [Target].[CreatedOn]) IS NOT NULL OR NULLIF([Target].[CreatedOn], [Source].[CreatedOn]) IS NOT NULL OR 
	NULLIF([Source].[CreatedById], [Target].[CreatedById]) IS NOT NULL OR NULLIF([Target].[CreatedById], [Source].[CreatedById]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedOn], [Target].[ModifiedOn]) IS NOT NULL OR NULLIF([Target].[ModifiedOn], [Source].[ModifiedOn]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedById], [Target].[ModifiedById]) IS NOT NULL OR NULLIF([Target].[ModifiedById], [Source].[ModifiedById]) IS NOT NULL OR 
	NULLIF([Source].[ProcessListeners], [Target].[ProcessListeners]) IS NOT NULL OR NULLIF([Target].[ProcessListeners], [Source].[ProcessListeners]) IS NOT NULL OR 
	NULLIF([Source].[SysWorkplaceId], [Target].[SysWorkplaceId]) IS NOT NULL OR NULLIF([Target].[SysWorkplaceId], [Source].[SysWorkplaceId]) IS NOT NULL OR 
	NULLIF([Source].[SysAdminUnitId], [Target].[SysAdminUnitId]) IS NOT NULL OR NULLIF([Target].[SysAdminUnitId], [Source].[SysAdminUnitId]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[CreatedOn] = [Source].[CreatedOn], 
  [Target].[CreatedById] = [Source].[CreatedById], 
  [Target].[ModifiedOn] = [Source].[ModifiedOn], 
  [Target].[ModifiedById] = [Source].[ModifiedById], 
  [Target].[ProcessListeners] = [Source].[ProcessListeners], 
  [Target].[SysWorkplaceId] = [Source].[SysWorkplaceId], 
  [Target].[SysAdminUnitId] = [Source].[SysAdminUnitId]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[ProcessListeners],[SysWorkplaceId],[SysAdminUnitId])
 VALUES([Source].[Id],[Source].[CreatedOn],[Source].[CreatedById],[Source].[ModifiedOn],[Source].[ModifiedById],[Source].[ProcessListeners],[Source].[SysWorkplaceId],[Source].[SysAdminUnitId]);
