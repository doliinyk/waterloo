--Update Mar 23, 2021 @ 10:04 by OS (adding Cancelled)
--Version Oct 15, 2020 by Kavian
--EXEC SP_GENERATE_MERGE 'WatTripStage'

MERGE INTO [WatTripStage] AS [Target]
USING (VALUES
  (N'9837EFF7-9F6F-4E2A-9D51-0754648060EE','2020-10-15T14:07:42.2435960',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-11-15T01:45:08.4918422',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Processing',N'',0)
 ,(N'AECD2417-694A-46BC-8A17-906035518524','2020-10-15T14:08:11.7801433',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-11-15T01:45:10.5856808',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Returned',N'',0)
 ,(N'B5470B22-5C4D-4736-B1AA-ABFA8FAB7E69','2020-10-15T14:07:37.3761001',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-11-15T01:45:12.6794718',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'New',N'',0)
 ,(N'9F8784BC-B352-4BBC-97D9-B1BC7029D327','2021-03-23T13:55:26.2359435',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2021-03-23T13:55:26.2359435',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Cancelled',N'',0)
 ,(N'FAE0EFA7-EEE2-455F-A6B7-BF3296106490','2020-10-15T14:08:05.4211908',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-11-15T01:45:14.4138995',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Arrival Confirmed',N'',0)
 ,(N'B61C6895-DCA1-47B7-9578-C3FE3F0200F0','2020-10-15T14:07:57.7314353',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-11-15T01:45:16.1796003',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Departed',N'',0)
 ,(N'D209DE8F-D55E-4ED0-88C6-C86F5DCA70A3','2020-10-15T14:08:20.3569410',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-11-15T01:45:17.6639963',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Not Recommended',N'',0)
 ,(N'E0F0AE24-FF21-4270-8DAA-CF516C4FE9D1','2020-10-15T14:08:30.8632231',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-11-15T01:45:19.3984556',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Not Returned',N'',0)
 ,(N'4AC08D99-7236-4005-9EC6-FEB678CC5201','2020-10-15T14:07:52.6500338',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-11-15T01:45:22.0547577',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Pre-Departure Completed',N'',0)
) AS [Source] ([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[CreatedOn], [Target].[CreatedOn]) IS NOT NULL OR NULLIF([Target].[CreatedOn], [Source].[CreatedOn]) IS NOT NULL OR 
	NULLIF([Source].[CreatedById], [Target].[CreatedById]) IS NOT NULL OR NULLIF([Target].[CreatedById], [Source].[CreatedById]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedOn], [Target].[ModifiedOn]) IS NOT NULL OR NULLIF([Target].[ModifiedOn], [Source].[ModifiedOn]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedById], [Target].[ModifiedById]) IS NOT NULL OR NULLIF([Target].[ModifiedById], [Source].[ModifiedById]) IS NOT NULL OR 
	NULLIF([Source].[Name], [Target].[Name]) IS NOT NULL OR NULLIF([Target].[Name], [Source].[Name]) IS NOT NULL OR 
	NULLIF([Source].[Description], [Target].[Description]) IS NOT NULL OR NULLIF([Target].[Description], [Source].[Description]) IS NOT NULL OR 
	NULLIF([Source].[ProcessListeners], [Target].[ProcessListeners]) IS NOT NULL OR NULLIF([Target].[ProcessListeners], [Source].[ProcessListeners]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[CreatedOn] = [Source].[CreatedOn], 
  [Target].[CreatedById] = [Source].[CreatedById], 
  [Target].[ModifiedOn] = [Source].[ModifiedOn], 
  [Target].[ModifiedById] = [Source].[ModifiedById], 
  [Target].[Name] = [Source].[Name], 
  [Target].[Description] = [Source].[Description], 
  [Target].[ProcessListeners] = [Source].[ProcessListeners]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
 VALUES([Source].[Id],[Source].[CreatedOn],[Source].[CreatedById],[Source].[ModifiedOn],[Source].[ModifiedById],[Source].[Name],[Source].[Description],[Source].[ProcessListeners]);
