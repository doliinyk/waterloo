--Version Nov 6, 2020 by Kavian
--EXEC SP_GENERATE_MERGE 'WatTravelSituationSource'

MERGE INTO [WatTravelSituationSource] AS [Target]
USING (VALUES
  (N'177244D6-C273-4E30-AEE0-688F14B2EC56','2020-11-09T02:06:29.6607079',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-11-09T02:06:29.6607079',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'ANVIL',N'',0)
 ,(N'E0CEE257-E7F3-4AEE-B2E5-D42F1F2C4034','2020-11-09T02:06:35.1451252',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-11-09T02:06:35.1451252',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Traveller',N'',0)
 ,(N'D3E39763-6521-4019-8B6E-DA7DB8431C5A','2020-11-09T02:06:39.9445433',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-11-09T02:06:39.9445433',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Other',N'',0)
) AS [Source] ([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[CreatedOn], [Target].[CreatedOn]) IS NOT NULL OR NULLIF([Target].[CreatedOn], [Source].[CreatedOn]) IS NOT NULL OR 
	NULLIF([Source].[CreatedById], [Target].[CreatedById]) IS NOT NULL OR NULLIF([Target].[CreatedById], [Source].[CreatedById]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedOn], [Target].[ModifiedOn]) IS NOT NULL OR NULLIF([Target].[ModifiedOn], [Source].[ModifiedOn]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedById], [Target].[ModifiedById]) IS NOT NULL OR NULLIF([Target].[ModifiedById], [Source].[ModifiedById]) IS NOT NULL OR 
	NULLIF([Source].[Name], [Target].[Name]) IS NOT NULL OR NULLIF([Target].[Name], [Source].[Name]) IS NOT NULL OR 
	NULLIF([Source].[Description], [Target].[Description]) IS NOT NULL OR NULLIF([Target].[Description], [Source].[Description]) IS NOT NULL OR 
	NULLIF([Source].[ProcessListeners], [Target].[ProcessListeners]) IS NOT NULL OR NULLIF([Target].[ProcessListeners], [Source].[ProcessListeners]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[CreatedOn] = [Source].[CreatedOn], 
  [Target].[CreatedById] = [Source].[CreatedById], 
  [Target].[ModifiedOn] = [Source].[ModifiedOn], 
  [Target].[ModifiedById] = [Source].[ModifiedById], 
  [Target].[Name] = [Source].[Name], 
  [Target].[Description] = [Source].[Description], 
  [Target].[ProcessListeners] = [Source].[ProcessListeners]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
 VALUES([Source].[Id],[Source].[CreatedOn],[Source].[CreatedById],[Source].[ModifiedOn],[Source].[ModifiedById],[Source].[Name],[Source].[Description],[Source].[ProcessListeners]);