--Version Sep 29, 2020 @ by Kavian
--EXEC SP_GENERATE_MERGE 'WatStakeholderType'

MERGE INTO [WatStakeholderType] AS [Target]
USING (VALUES
  (N'C61532A6-DBDC-42A7-8976-0BDD9E983286','2020-09-29T02:21:23.9231146',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-09-29T02:21:23.9231146',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'TBP',N'',0)
 ,(N'02820736-6B71-49FC-9E4E-16408FFD2E94','2020-09-29T02:20:47.5677261',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-09-29T02:20:47.5677261',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'ASU - GSPA',N'',0)
 ,(N'00DC0018-546E-45FC-B3DE-2E1222F73F9E','2020-09-29T02:20:53.9620410',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-09-29T02:20:53.9620410',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'ASU - RO',N'',0)
 ,(N'EB375560-BDCD-4CF9-A50E-499E3756CCB0','2020-09-29T02:21:03.8868329',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-09-29T02:21:03.8868329',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'ASU - OR',N'',0)
 ,(N'F3F88265-DC05-4946-9160-4D398C0AD23C','2020-09-29T02:21:18.8310961',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-09-29T02:21:18.8310961',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'FAC - ARTS',N'',0)
 ,(N'3DF5A2F5-BDED-42FF-A839-79D53EA0DEFA','2020-09-29T02:20:27.8026255',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-09-29T02:20:27.8026255',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'ASU - SSO',N'',0)
 ,(N'A009C080-B19A-4F98-AFCC-8A726A2E6D34','2020-09-29T02:20:39.0108469',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-09-29T02:20:39.0108469',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'ASU - Co-op',N'',0)
 ,(N'4D790571-0D82-489D-B2E7-D11324C7DEED','2020-09-29T02:21:12.2007520',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-09-29T02:21:12.2007520',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'FAC - AHS',N'',0)
) AS [Source] ([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[CreatedOn], [Target].[CreatedOn]) IS NOT NULL OR NULLIF([Target].[CreatedOn], [Source].[CreatedOn]) IS NOT NULL OR 
	NULLIF([Source].[CreatedById], [Target].[CreatedById]) IS NOT NULL OR NULLIF([Target].[CreatedById], [Source].[CreatedById]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedOn], [Target].[ModifiedOn]) IS NOT NULL OR NULLIF([Target].[ModifiedOn], [Source].[ModifiedOn]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedById], [Target].[ModifiedById]) IS NOT NULL OR NULLIF([Target].[ModifiedById], [Source].[ModifiedById]) IS NOT NULL OR 
	NULLIF([Source].[Name], [Target].[Name]) IS NOT NULL OR NULLIF([Target].[Name], [Source].[Name]) IS NOT NULL OR 
	NULLIF([Source].[Description], [Target].[Description]) IS NOT NULL OR NULLIF([Target].[Description], [Source].[Description]) IS NOT NULL OR 
	NULLIF([Source].[ProcessListeners], [Target].[ProcessListeners]) IS NOT NULL OR NULLIF([Target].[ProcessListeners], [Source].[ProcessListeners]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[CreatedOn] = [Source].[CreatedOn], 
  [Target].[CreatedById] = [Source].[CreatedById], 
  [Target].[ModifiedOn] = [Source].[ModifiedOn], 
  [Target].[ModifiedById] = [Source].[ModifiedById], 
  [Target].[Name] = [Source].[Name], 
  [Target].[Description] = [Source].[Description], 
  [Target].[ProcessListeners] = [Source].[ProcessListeners]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
 VALUES([Source].[Id],[Source].[CreatedOn],[Source].[CreatedById],[Source].[ModifiedOn],[Source].[ModifiedById],[Source].[Name],[Source].[Description],[Source].[ProcessListeners]);
