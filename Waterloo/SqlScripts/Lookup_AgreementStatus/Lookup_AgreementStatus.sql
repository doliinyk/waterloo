--Version Sep 16, 2020 @ 14:35 by Kavian
--EXEC SP_GENERATE_MERGE 'WatAgreementStatus'

MERGE INTO [WatAgreementStatus] AS [Target]
USING (VALUES
  (N'4E4CDA8F-EFE9-4388-BBE5-3F6336A580EF','2020-11-14T21:01:40.8871548',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-11-18T23:04:26.6152940',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'In Renewal',N'',0)
 ,(N'F44D362A-D3A6-4B07-A45B-703D91132910','2020-09-16T15:56:02.4575905',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-09-16T15:56:02.4575905',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Concluded',N'',0)
 ,(N'1B438794-1770-4112-8B9E-8FF25EBFCA56','2020-09-16T15:55:48.8586222',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-09-16T15:55:48.8586222',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'In Progress',N'',0)
 ,(N'08CB98B4-586D-4C01-B5AF-A573F7837930','2020-11-13T23:56:10.9348966',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-11-13T23:56:10.9348966',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Did Not Proceed',N'',0)
 ,(N'EA895A76-096A-403F-A2CC-BE0C6F8D56ED','2020-09-16T15:56:12.3529753',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-09-16T15:56:12.3529753',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Lapsed',N'',0)
 ,(N'EFFD2081-F8D4-4A1D-9237-EDB542F5582A','2020-09-16T15:55:52.6403793',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-09-16T15:55:52.6403793',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Active',N'',0)
 ,(N'005FD962-6AE6-4C13-85A7-F40A4558D8C1','2020-09-16T15:56:07.8540762',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-09-16T15:56:07.8540762',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Renewed',N'',0)
) AS [Source] ([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[CreatedOn], [Target].[CreatedOn]) IS NOT NULL OR NULLIF([Target].[CreatedOn], [Source].[CreatedOn]) IS NOT NULL OR 
	NULLIF([Source].[CreatedById], [Target].[CreatedById]) IS NOT NULL OR NULLIF([Target].[CreatedById], [Source].[CreatedById]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedOn], [Target].[ModifiedOn]) IS NOT NULL OR NULLIF([Target].[ModifiedOn], [Source].[ModifiedOn]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedById], [Target].[ModifiedById]) IS NOT NULL OR NULLIF([Target].[ModifiedById], [Source].[ModifiedById]) IS NOT NULL OR 
	NULLIF([Source].[Name], [Target].[Name]) IS NOT NULL OR NULLIF([Target].[Name], [Source].[Name]) IS NOT NULL OR 
	NULLIF([Source].[Description], [Target].[Description]) IS NOT NULL OR NULLIF([Target].[Description], [Source].[Description]) IS NOT NULL OR 
	NULLIF([Source].[ProcessListeners], [Target].[ProcessListeners]) IS NOT NULL OR NULLIF([Target].[ProcessListeners], [Source].[ProcessListeners]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[CreatedOn] = [Source].[CreatedOn], 
  [Target].[CreatedById] = [Source].[CreatedById], 
  [Target].[ModifiedOn] = [Source].[ModifiedOn], 
  [Target].[ModifiedById] = [Source].[ModifiedById], 
  [Target].[Name] = [Source].[Name], 
  [Target].[Description] = [Source].[Description], 
  [Target].[ProcessListeners] = [Source].[ProcessListeners]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
 VALUES([Source].[Id],[Source].[CreatedOn],[Source].[CreatedById],[Source].[ModifiedOn],[Source].[ModifiedById],[Source].[Name],[Source].[Description],[Source].[ProcessListeners]);
