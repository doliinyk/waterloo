MERGE INTO [SysSettings] AS [Target]
USING (VALUES
  (N'790BCB4F-83B1-4744-A3B9-85315BB0EFFA','2021-04-26T19:56:23.470','2021-04-26T19:56:23.470',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'LearnBaseUrl',N'MediumText',0,1,NULL,N'LearnBaseUrl',N'example: https://uwaterloodev02.decisions.com/Primary/restapi/learn/',0,NULL,0)
) AS [Source] ([Id],[ModifiedOn],[CreatedOn],[CreatedById],[ModifiedById],[Name],[ValueTypeName],[IsPersonal],[IsCacheable],[SysFolderId],[Code],[Description],[ProcessListeners],[ReferenceSchemaUId],[IsSSPAvailable])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[ModifiedOn], [Target].[ModifiedOn]) IS NOT NULL OR NULLIF([Target].[ModifiedOn], [Source].[ModifiedOn]) IS NOT NULL OR 
	NULLIF([Source].[CreatedOn], [Target].[CreatedOn]) IS NOT NULL OR NULLIF([Target].[CreatedOn], [Source].[CreatedOn]) IS NOT NULL OR 
	NULLIF([Source].[CreatedById], [Target].[CreatedById]) IS NOT NULL OR NULLIF([Target].[CreatedById], [Source].[CreatedById]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedById], [Target].[ModifiedById]) IS NOT NULL OR NULLIF([Target].[ModifiedById], [Source].[ModifiedById]) IS NOT NULL OR 
	NULLIF([Source].[Name], [Target].[Name]) IS NOT NULL OR NULLIF([Target].[Name], [Source].[Name]) IS NOT NULL OR 
	NULLIF([Source].[ValueTypeName], [Target].[ValueTypeName]) IS NOT NULL OR NULLIF([Target].[ValueTypeName], [Source].[ValueTypeName]) IS NOT NULL OR 
	NULLIF([Source].[IsPersonal], [Target].[IsPersonal]) IS NOT NULL OR NULLIF([Target].[IsPersonal], [Source].[IsPersonal]) IS NOT NULL OR 
	NULLIF([Source].[IsCacheable], [Target].[IsCacheable]) IS NOT NULL OR NULLIF([Target].[IsCacheable], [Source].[IsCacheable]) IS NOT NULL OR 
	NULLIF([Source].[SysFolderId], [Target].[SysFolderId]) IS NOT NULL OR NULLIF([Target].[SysFolderId], [Source].[SysFolderId]) IS NOT NULL OR 
	NULLIF([Source].[Code], [Target].[Code]) IS NOT NULL OR NULLIF([Target].[Code], [Source].[Code]) IS NOT NULL OR 
	NULLIF([Source].[Description], [Target].[Description]) IS NOT NULL OR NULLIF([Target].[Description], [Source].[Description]) IS NOT NULL OR 
	NULLIF([Source].[ProcessListeners], [Target].[ProcessListeners]) IS NOT NULL OR NULLIF([Target].[ProcessListeners], [Source].[ProcessListeners]) IS NOT NULL OR 
	NULLIF([Source].[ReferenceSchemaUId], [Target].[ReferenceSchemaUId]) IS NOT NULL OR NULLIF([Target].[ReferenceSchemaUId], [Source].[ReferenceSchemaUId]) IS NOT NULL OR 
	NULLIF([Source].[IsSSPAvailable], [Target].[IsSSPAvailable]) IS NOT NULL OR NULLIF([Target].[IsSSPAvailable], [Source].[IsSSPAvailable]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[ModifiedOn] = [Source].[ModifiedOn], 
  [Target].[CreatedOn] = [Source].[CreatedOn], 
  [Target].[CreatedById] = [Source].[CreatedById], 
  [Target].[ModifiedById] = [Source].[ModifiedById], 
  [Target].[Name] = [Source].[Name], 
  [Target].[ValueTypeName] = [Source].[ValueTypeName], 
  [Target].[IsPersonal] = [Source].[IsPersonal], 
  [Target].[IsCacheable] = [Source].[IsCacheable], 
  [Target].[SysFolderId] = [Source].[SysFolderId], 
  [Target].[Code] = [Source].[Code], 
  [Target].[Description] = [Source].[Description], 
  [Target].[ProcessListeners] = [Source].[ProcessListeners], 
  [Target].[ReferenceSchemaUId] = [Source].[ReferenceSchemaUId], 
  [Target].[IsSSPAvailable] = [Source].[IsSSPAvailable]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[ModifiedOn],[CreatedOn],[CreatedById],[ModifiedById],[Name],[ValueTypeName],[IsPersonal],[IsCacheable],[SysFolderId],[Code],[Description],[ProcessListeners],[ReferenceSchemaUId],[IsSSPAvailable])
 VALUES([Source].[Id],[Source].[ModifiedOn],[Source].[CreatedOn],[Source].[CreatedById],[Source].[ModifiedById],[Source].[Name],[Source].[ValueTypeName],[Source].[IsPersonal],[Source].[IsCacheable],[Source].[SysFolderId],[Source].[Code],[Source].[Description],[Source].[ProcessListeners],[Source].[ReferenceSchemaUId],[Source].[IsSSPAvailable]);

MERGE INTO [SysSettingsValue] AS [Target]
USING (VALUES
  (N'09725FD9-8BFE-4CC8-8714-C66ED3227BC4','2021-04-26T19:56:23.650','2021-04-26T19:56:23.598',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'790BCB4F-83B1-4744-A3B9-85315BB0EFFA',N'A29A3BA5-4B0D-DE11-9A51-005056C00008',0,N'https://uwaterloodev02.decisions.com/Primary/restapi/learn/',0,0.00,0,NULL,NULL,NULL,2147483647,0)
) AS [Source] ([Id],[ModifiedOn],[CreatedOn],[CreatedById],[ModifiedById],[SysSettingsId],[SysAdminUnitId],[IsDef],[TextValue],[IntegerValue],[FloatValue],[BooleanValue],[DateTimeValue],[GuidValue],[BinaryValue],[Position],[ProcessListeners])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[ModifiedOn], [Target].[ModifiedOn]) IS NOT NULL OR NULLIF([Target].[ModifiedOn], [Source].[ModifiedOn]) IS NOT NULL OR 
	NULLIF([Source].[CreatedOn], [Target].[CreatedOn]) IS NOT NULL OR NULLIF([Target].[CreatedOn], [Source].[CreatedOn]) IS NOT NULL OR 
	NULLIF([Source].[CreatedById], [Target].[CreatedById]) IS NOT NULL OR NULLIF([Target].[CreatedById], [Source].[CreatedById]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedById], [Target].[ModifiedById]) IS NOT NULL OR NULLIF([Target].[ModifiedById], [Source].[ModifiedById]) IS NOT NULL OR 
	NULLIF([Source].[SysSettingsId], [Target].[SysSettingsId]) IS NOT NULL OR NULLIF([Target].[SysSettingsId], [Source].[SysSettingsId]) IS NOT NULL OR 
	NULLIF([Source].[SysAdminUnitId], [Target].[SysAdminUnitId]) IS NOT NULL OR NULLIF([Target].[SysAdminUnitId], [Source].[SysAdminUnitId]) IS NOT NULL OR 
	NULLIF([Source].[IsDef], [Target].[IsDef]) IS NOT NULL OR NULLIF([Target].[IsDef], [Source].[IsDef]) IS NOT NULL OR 
	NULLIF([Source].[TextValue], [Target].[TextValue]) IS NOT NULL OR NULLIF([Target].[TextValue], [Source].[TextValue]) IS NOT NULL OR 
	NULLIF([Source].[IntegerValue], [Target].[IntegerValue]) IS NOT NULL OR NULLIF([Target].[IntegerValue], [Source].[IntegerValue]) IS NOT NULL OR 
	NULLIF([Source].[FloatValue], [Target].[FloatValue]) IS NOT NULL OR NULLIF([Target].[FloatValue], [Source].[FloatValue]) IS NOT NULL OR 
	NULLIF([Source].[BooleanValue], [Target].[BooleanValue]) IS NOT NULL OR NULLIF([Target].[BooleanValue], [Source].[BooleanValue]) IS NOT NULL OR 
	NULLIF([Source].[DateTimeValue], [Target].[DateTimeValue]) IS NOT NULL OR NULLIF([Target].[DateTimeValue], [Source].[DateTimeValue]) IS NOT NULL OR 
	NULLIF([Source].[GuidValue], [Target].[GuidValue]) IS NOT NULL OR NULLIF([Target].[GuidValue], [Source].[GuidValue]) IS NOT NULL OR 
	NULLIF([Source].[BinaryValue], [Target].[BinaryValue]) IS NOT NULL OR NULLIF([Target].[BinaryValue], [Source].[BinaryValue]) IS NOT NULL OR 
	NULLIF([Source].[Position], [Target].[Position]) IS NOT NULL OR NULLIF([Target].[Position], [Source].[Position]) IS NOT NULL OR 
	NULLIF([Source].[ProcessListeners], [Target].[ProcessListeners]) IS NOT NULL OR NULLIF([Target].[ProcessListeners], [Source].[ProcessListeners]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[ModifiedOn] = [Source].[ModifiedOn], 
  [Target].[CreatedOn] = [Source].[CreatedOn], 
  [Target].[CreatedById] = [Source].[CreatedById], 
  [Target].[ModifiedById] = [Source].[ModifiedById], 
  [Target].[SysSettingsId] = [Source].[SysSettingsId], 
  [Target].[SysAdminUnitId] = [Source].[SysAdminUnitId], 
  [Target].[IsDef] = [Source].[IsDef], 
  [Target].[TextValue] = [Source].[TextValue], 
  [Target].[IntegerValue] = [Source].[IntegerValue], 
  [Target].[FloatValue] = [Source].[FloatValue], 
  [Target].[BooleanValue] = [Source].[BooleanValue], 
  [Target].[DateTimeValue] = [Source].[DateTimeValue], 
  [Target].[GuidValue] = [Source].[GuidValue], 
  [Target].[BinaryValue] = [Source].[BinaryValue], 
  [Target].[Position] = [Source].[Position], 
  [Target].[ProcessListeners] = [Source].[ProcessListeners]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[ModifiedOn],[CreatedOn],[CreatedById],[ModifiedById],[SysSettingsId],[SysAdminUnitId],[IsDef],[TextValue],[IntegerValue],[FloatValue],[BooleanValue],[DateTimeValue],[GuidValue],[BinaryValue],[Position],[ProcessListeners])
 VALUES([Source].[Id],[Source].[ModifiedOn],[Source].[CreatedOn],[Source].[CreatedById],[Source].[ModifiedById],[Source].[SysSettingsId],[Source].[SysAdminUnitId],[Source].[IsDef],[Source].[TextValue],[Source].[IntegerValue],[Source].[FloatValue],[Source].[BooleanValue],[Source].[DateTimeValue],[Source].[GuidValue],[Source].[BinaryValue],[Source].[Position],[Source].[ProcessListeners]);
