-- Updated Dec 23, 2020 by Kavian
-- Version Nov 5, 2020 by Kavian
-- exec sp_generate_merge 'EmailTemplate', @from = 'from EmailTemplate where Name in (''Wat1 - Delegation Hospitality Request Update (US)'')', @cols_to_exclude = '''CopyRecipient'',''BlindCopyRecipient'',''Macros'',''Recipient'''

MERGE INTO [EmailTemplate] AS [Target]
USING (SELECT [Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[ProcessListeners],[Name],[ObjectId],[Subject],[PriorityId],[Body],[IsHtmlBody],[SendIndividualEmailId],[SaveAsActivity],[ObjectFieldInActivity],[ShowBeforeSending],[TemplateConfig],[ConfigType],[PreviewImageId] FROM [EmailTemplate] WHERE 1 = 0 -- Empty dataset (source table contained no rows at time of MERGE generation) 
) AS [Source] ([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[ProcessListeners],[Name],[ObjectId],[Subject],[PriorityId],[Body],[IsHtmlBody],[SendIndividualEmailId],[SaveAsActivity],[ObjectFieldInActivity],[ShowBeforeSending],[TemplateConfig],[ConfigType],[PreviewImageId])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[CreatedOn], [Target].[CreatedOn]) IS NOT NULL OR NULLIF([Target].[CreatedOn], [Source].[CreatedOn]) IS NOT NULL OR 
	NULLIF([Source].[CreatedById], [Target].[CreatedById]) IS NOT NULL OR NULLIF([Target].[CreatedById], [Source].[CreatedById]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedOn], [Target].[ModifiedOn]) IS NOT NULL OR NULLIF([Target].[ModifiedOn], [Source].[ModifiedOn]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedById], [Target].[ModifiedById]) IS NOT NULL OR NULLIF([Target].[ModifiedById], [Source].[ModifiedById]) IS NOT NULL OR 
	NULLIF([Source].[ProcessListeners], [Target].[ProcessListeners]) IS NOT NULL OR NULLIF([Target].[ProcessListeners], [Source].[ProcessListeners]) IS NOT NULL OR 
	NULLIF([Source].[Name], [Target].[Name]) IS NOT NULL OR NULLIF([Target].[Name], [Source].[Name]) IS NOT NULL OR 
	NULLIF([Source].[ObjectId], [Target].[ObjectId]) IS NOT NULL OR NULLIF([Target].[ObjectId], [Source].[ObjectId]) IS NOT NULL OR 
	NULLIF([Source].[Subject], [Target].[Subject]) IS NOT NULL OR NULLIF([Target].[Subject], [Source].[Subject]) IS NOT NULL OR 
	NULLIF([Source].[PriorityId], [Target].[PriorityId]) IS NOT NULL OR NULLIF([Target].[PriorityId], [Source].[PriorityId]) IS NOT NULL OR 
	NULLIF([Source].[Body], [Target].[Body]) IS NOT NULL OR NULLIF([Target].[Body], [Source].[Body]) IS NOT NULL OR 
	NULLIF([Source].[IsHtmlBody], [Target].[IsHtmlBody]) IS NOT NULL OR NULLIF([Target].[IsHtmlBody], [Source].[IsHtmlBody]) IS NOT NULL OR 
	NULLIF([Source].[SendIndividualEmailId], [Target].[SendIndividualEmailId]) IS NOT NULL OR NULLIF([Target].[SendIndividualEmailId], [Source].[SendIndividualEmailId]) IS NOT NULL OR 
	NULLIF([Source].[SaveAsActivity], [Target].[SaveAsActivity]) IS NOT NULL OR NULLIF([Target].[SaveAsActivity], [Source].[SaveAsActivity]) IS NOT NULL OR 
	NULLIF([Source].[ObjectFieldInActivity], [Target].[ObjectFieldInActivity]) IS NOT NULL OR NULLIF([Target].[ObjectFieldInActivity], [Source].[ObjectFieldInActivity]) IS NOT NULL OR 
	NULLIF([Source].[ShowBeforeSending], [Target].[ShowBeforeSending]) IS NOT NULL OR NULLIF([Target].[ShowBeforeSending], [Source].[ShowBeforeSending]) IS NOT NULL OR 
	NULLIF([Source].[TemplateConfig], [Target].[TemplateConfig]) IS NOT NULL OR NULLIF([Target].[TemplateConfig], [Source].[TemplateConfig]) IS NOT NULL OR 
	NULLIF([Source].[ConfigType], [Target].[ConfigType]) IS NOT NULL OR NULLIF([Target].[ConfigType], [Source].[ConfigType]) IS NOT NULL OR 
	NULLIF([Source].[PreviewImageId], [Target].[PreviewImageId]) IS NOT NULL OR NULLIF([Target].[PreviewImageId], [Source].[PreviewImageId]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[CreatedOn] = [Source].[CreatedOn], 
  [Target].[CreatedById] = [Source].[CreatedById], 
  [Target].[ModifiedOn] = [Source].[ModifiedOn], 
  [Target].[ModifiedById] = [Source].[ModifiedById], 
  [Target].[ProcessListeners] = [Source].[ProcessListeners], 
  [Target].[Name] = [Source].[Name], 
  [Target].[ObjectId] = [Source].[ObjectId], 
  [Target].[Subject] = [Source].[Subject], 
  [Target].[PriorityId] = [Source].[PriorityId], 
  [Target].[Body] = [Source].[Body], 
  [Target].[IsHtmlBody] = [Source].[IsHtmlBody], 
  [Target].[SendIndividualEmailId] = [Source].[SendIndividualEmailId], 
  [Target].[SaveAsActivity] = [Source].[SaveAsActivity], 
  [Target].[ObjectFieldInActivity] = [Source].[ObjectFieldInActivity], 
  [Target].[ShowBeforeSending] = [Source].[ShowBeforeSending], 
  [Target].[TemplateConfig] = [Source].[TemplateConfig], 
  [Target].[ConfigType] = [Source].[ConfigType], 
  [Target].[PreviewImageId] = [Source].[PreviewImageId]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[ProcessListeners],[Name],[ObjectId],[Subject],[PriorityId],[Body],[IsHtmlBody],[SendIndividualEmailId],[SaveAsActivity],[ObjectFieldInActivity],[ShowBeforeSending],[TemplateConfig],[ConfigType],[PreviewImageId])
 VALUES([Source].[Id],[Source].[CreatedOn],[Source].[CreatedById],[Source].[ModifiedOn],[Source].[ModifiedById],[Source].[ProcessListeners],[Source].[Name],[Source].[ObjectId],[Source].[Subject],[Source].[PriorityId],[Source].[Body],[Source].[IsHtmlBody],[Source].[SendIndividualEmailId],[Source].[SaveAsActivity],[Source].[ObjectFieldInActivity],[Source].[ShowBeforeSending],[Source].[TemplateConfig],[Source].[ConfigType],[Source].[PreviewImageId]);
