--Version Sep 16, 2020 @ 14:35 by Kavian
--EXEC SP_GENERATE_MERGE 'WatEFASELAS'

MERGE INTO [WatEFASELAS] AS [Target]
USING (VALUES
  (N'4A499D63-50A1-44FC-9D64-0DF1F640B6A3','2020-09-16T15:56:24.5134348',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-09-16T15:56:24.5134348',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'EFAS',N'',0)
 ,(N'2E7BF278-C221-48C6-8100-AEB8BC9A0315','2020-09-16T15:56:27.8384779',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-09-16T15:56:27.8384779',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'ELAS',N'',0)
 ,(N'BD77C9F8-02AB-450A-8385-B112F013D9A0','2020-09-16T15:56:33.2500609',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-09-16T15:56:33.2500609',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Neither',N'',0)
) AS [Source] ([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[CreatedOn], [Target].[CreatedOn]) IS NOT NULL OR NULLIF([Target].[CreatedOn], [Source].[CreatedOn]) IS NOT NULL OR 
	NULLIF([Source].[CreatedById], [Target].[CreatedById]) IS NOT NULL OR NULLIF([Target].[CreatedById], [Source].[CreatedById]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedOn], [Target].[ModifiedOn]) IS NOT NULL OR NULLIF([Target].[ModifiedOn], [Source].[ModifiedOn]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedById], [Target].[ModifiedById]) IS NOT NULL OR NULLIF([Target].[ModifiedById], [Source].[ModifiedById]) IS NOT NULL OR 
	NULLIF([Source].[Name], [Target].[Name]) IS NOT NULL OR NULLIF([Target].[Name], [Source].[Name]) IS NOT NULL OR 
	NULLIF([Source].[Description], [Target].[Description]) IS NOT NULL OR NULLIF([Target].[Description], [Source].[Description]) IS NOT NULL OR 
	NULLIF([Source].[ProcessListeners], [Target].[ProcessListeners]) IS NOT NULL OR NULLIF([Target].[ProcessListeners], [Source].[ProcessListeners]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[CreatedOn] = [Source].[CreatedOn], 
  [Target].[CreatedById] = [Source].[CreatedById], 
  [Target].[ModifiedOn] = [Source].[ModifiedOn], 
  [Target].[ModifiedById] = [Source].[ModifiedById], 
  [Target].[Name] = [Source].[Name], 
  [Target].[Description] = [Source].[Description], 
  [Target].[ProcessListeners] = [Source].[ProcessListeners]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
 VALUES([Source].[Id],[Source].[CreatedOn],[Source].[CreatedById],[Source].[ModifiedOn],[Source].[ModifiedById],[Source].[Name],[Source].[Description],[Source].[ProcessListeners]);
