--Version Apr 29, 2021 @ 11:22 by OS
--exec sp_generate_merge 'SysSettings', @from = 'from SysSettings where Code = ''WatLearnCourse3'''
--exec sp_generate_merge 'SysSettingsValue', @from = 'from SysSettingsValue where SysSettingsId = (select Id from SysSettings where Code = ''WatLearnCourse3'')'

MERGE INTO [SysSettings] AS [Target]
USING (VALUES
  (N'824ED7F8-6101-4243-A40E-A9FDD7B849B6','2021-04-29T13:12:33.466','2021-04-29T13:12:33.466',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'LEARN 3 Course',N'ShortText',0,1,NULL,N'WatLearnCourse3',N'Designates LEARN course Id for Faculty/Staff such as 383018',0,NULL,0)
) AS [Source] ([Id],[ModifiedOn],[CreatedOn],[CreatedById],[ModifiedById],[Name],[ValueTypeName],[IsPersonal],[IsCacheable],[SysFolderId],[Code],[Description],[ProcessListeners],[ReferenceSchemaUId],[IsSSPAvailable])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[ModifiedOn], [Target].[ModifiedOn]) IS NOT NULL OR NULLIF([Target].[ModifiedOn], [Source].[ModifiedOn]) IS NOT NULL OR 
	NULLIF([Source].[CreatedOn], [Target].[CreatedOn]) IS NOT NULL OR NULLIF([Target].[CreatedOn], [Source].[CreatedOn]) IS NOT NULL OR 
	NULLIF([Source].[CreatedById], [Target].[CreatedById]) IS NOT NULL OR NULLIF([Target].[CreatedById], [Source].[CreatedById]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedById], [Target].[ModifiedById]) IS NOT NULL OR NULLIF([Target].[ModifiedById], [Source].[ModifiedById]) IS NOT NULL OR 
	NULLIF([Source].[Name], [Target].[Name]) IS NOT NULL OR NULLIF([Target].[Name], [Source].[Name]) IS NOT NULL OR 
	NULLIF([Source].[ValueTypeName], [Target].[ValueTypeName]) IS NOT NULL OR NULLIF([Target].[ValueTypeName], [Source].[ValueTypeName]) IS NOT NULL OR 
	NULLIF([Source].[IsPersonal], [Target].[IsPersonal]) IS NOT NULL OR NULLIF([Target].[IsPersonal], [Source].[IsPersonal]) IS NOT NULL OR 
	NULLIF([Source].[IsCacheable], [Target].[IsCacheable]) IS NOT NULL OR NULLIF([Target].[IsCacheable], [Source].[IsCacheable]) IS NOT NULL OR 
	NULLIF([Source].[SysFolderId], [Target].[SysFolderId]) IS NOT NULL OR NULLIF([Target].[SysFolderId], [Source].[SysFolderId]) IS NOT NULL OR 
	NULLIF([Source].[Code], [Target].[Code]) IS NOT NULL OR NULLIF([Target].[Code], [Source].[Code]) IS NOT NULL OR 
	NULLIF([Source].[Description], [Target].[Description]) IS NOT NULL OR NULLIF([Target].[Description], [Source].[Description]) IS NOT NULL OR 
	NULLIF([Source].[ProcessListeners], [Target].[ProcessListeners]) IS NOT NULL OR NULLIF([Target].[ProcessListeners], [Source].[ProcessListeners]) IS NOT NULL OR 
	NULLIF([Source].[ReferenceSchemaUId], [Target].[ReferenceSchemaUId]) IS NOT NULL OR NULLIF([Target].[ReferenceSchemaUId], [Source].[ReferenceSchemaUId]) IS NOT NULL OR 
	NULLIF([Source].[IsSSPAvailable], [Target].[IsSSPAvailable]) IS NOT NULL OR NULLIF([Target].[IsSSPAvailable], [Source].[IsSSPAvailable]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[ModifiedOn] = [Source].[ModifiedOn], 
  [Target].[CreatedOn] = [Source].[CreatedOn], 
  [Target].[CreatedById] = [Source].[CreatedById], 
  [Target].[ModifiedById] = [Source].[ModifiedById], 
  [Target].[Name] = [Source].[Name], 
  [Target].[ValueTypeName] = [Source].[ValueTypeName], 
  [Target].[IsPersonal] = [Source].[IsPersonal], 
  [Target].[IsCacheable] = [Source].[IsCacheable], 
  [Target].[SysFolderId] = [Source].[SysFolderId], 
  [Target].[Code] = [Source].[Code], 
  [Target].[Description] = [Source].[Description], 
  [Target].[ProcessListeners] = [Source].[ProcessListeners], 
  [Target].[ReferenceSchemaUId] = [Source].[ReferenceSchemaUId], 
  [Target].[IsSSPAvailable] = [Source].[IsSSPAvailable]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[ModifiedOn],[CreatedOn],[CreatedById],[ModifiedById],[Name],[ValueTypeName],[IsPersonal],[IsCacheable],[SysFolderId],[Code],[Description],[ProcessListeners],[ReferenceSchemaUId],[IsSSPAvailable])
 VALUES([Source].[Id],[Source].[ModifiedOn],[Source].[CreatedOn],[Source].[CreatedById],[Source].[ModifiedById],[Source].[Name],[Source].[ValueTypeName],[Source].[IsPersonal],[Source].[IsCacheable],[Source].[SysFolderId],[Source].[Code],[Source].[Description],[Source].[ProcessListeners],[Source].[ReferenceSchemaUId],[Source].[IsSSPAvailable]);

MERGE INTO [SysSettingsValue] AS [Target]
USING (VALUES
  (N'DC021C4C-1CD9-421A-BD2A-202D0C971D84','2021-04-29T13:12:33.559','2021-04-29T13:12:33.528',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'824ED7F8-6101-4243-A40E-A9FDD7B849B6',N'A29A3BA5-4B0D-DE11-9A51-005056C00008',0,N'457756',0,0.00,0,NULL,NULL,NULL,2147483647,0)
) AS [Source] ([Id],[ModifiedOn],[CreatedOn],[CreatedById],[ModifiedById],[SysSettingsId],[SysAdminUnitId],[IsDef],[TextValue],[IntegerValue],[FloatValue],[BooleanValue],[DateTimeValue],[GuidValue],[BinaryValue],[Position],[ProcessListeners])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[ModifiedOn], [Target].[ModifiedOn]) IS NOT NULL OR NULLIF([Target].[ModifiedOn], [Source].[ModifiedOn]) IS NOT NULL OR 
	NULLIF([Source].[CreatedOn], [Target].[CreatedOn]) IS NOT NULL OR NULLIF([Target].[CreatedOn], [Source].[CreatedOn]) IS NOT NULL OR 
	NULLIF([Source].[CreatedById], [Target].[CreatedById]) IS NOT NULL OR NULLIF([Target].[CreatedById], [Source].[CreatedById]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedById], [Target].[ModifiedById]) IS NOT NULL OR NULLIF([Target].[ModifiedById], [Source].[ModifiedById]) IS NOT NULL OR 
	NULLIF([Source].[SysSettingsId], [Target].[SysSettingsId]) IS NOT NULL OR NULLIF([Target].[SysSettingsId], [Source].[SysSettingsId]) IS NOT NULL OR 
	NULLIF([Source].[SysAdminUnitId], [Target].[SysAdminUnitId]) IS NOT NULL OR NULLIF([Target].[SysAdminUnitId], [Source].[SysAdminUnitId]) IS NOT NULL OR 
	NULLIF([Source].[IsDef], [Target].[IsDef]) IS NOT NULL OR NULLIF([Target].[IsDef], [Source].[IsDef]) IS NOT NULL OR 
	NULLIF([Source].[TextValue], [Target].[TextValue]) IS NOT NULL OR NULLIF([Target].[TextValue], [Source].[TextValue]) IS NOT NULL OR 
	NULLIF([Source].[IntegerValue], [Target].[IntegerValue]) IS NOT NULL OR NULLIF([Target].[IntegerValue], [Source].[IntegerValue]) IS NOT NULL OR 
	NULLIF([Source].[FloatValue], [Target].[FloatValue]) IS NOT NULL OR NULLIF([Target].[FloatValue], [Source].[FloatValue]) IS NOT NULL OR 
	NULLIF([Source].[BooleanValue], [Target].[BooleanValue]) IS NOT NULL OR NULLIF([Target].[BooleanValue], [Source].[BooleanValue]) IS NOT NULL OR 
	NULLIF([Source].[DateTimeValue], [Target].[DateTimeValue]) IS NOT NULL OR NULLIF([Target].[DateTimeValue], [Source].[DateTimeValue]) IS NOT NULL OR 
	NULLIF([Source].[GuidValue], [Target].[GuidValue]) IS NOT NULL OR NULLIF([Target].[GuidValue], [Source].[GuidValue]) IS NOT NULL OR 
	NULLIF([Source].[BinaryValue], [Target].[BinaryValue]) IS NOT NULL OR NULLIF([Target].[BinaryValue], [Source].[BinaryValue]) IS NOT NULL OR 
	NULLIF([Source].[Position], [Target].[Position]) IS NOT NULL OR NULLIF([Target].[Position], [Source].[Position]) IS NOT NULL OR 
	NULLIF([Source].[ProcessListeners], [Target].[ProcessListeners]) IS NOT NULL OR NULLIF([Target].[ProcessListeners], [Source].[ProcessListeners]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[ModifiedOn] = [Source].[ModifiedOn], 
  [Target].[CreatedOn] = [Source].[CreatedOn], 
  [Target].[CreatedById] = [Source].[CreatedById], 
  [Target].[ModifiedById] = [Source].[ModifiedById], 
  [Target].[SysSettingsId] = [Source].[SysSettingsId], 
  [Target].[SysAdminUnitId] = [Source].[SysAdminUnitId], 
  [Target].[IsDef] = [Source].[IsDef], 
  [Target].[TextValue] = [Source].[TextValue], 
  [Target].[IntegerValue] = [Source].[IntegerValue], 
  [Target].[FloatValue] = [Source].[FloatValue], 
  [Target].[BooleanValue] = [Source].[BooleanValue], 
  [Target].[DateTimeValue] = [Source].[DateTimeValue], 
  [Target].[GuidValue] = [Source].[GuidValue], 
  [Target].[BinaryValue] = [Source].[BinaryValue], 
  [Target].[Position] = [Source].[Position], 
  [Target].[ProcessListeners] = [Source].[ProcessListeners]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[ModifiedOn],[CreatedOn],[CreatedById],[ModifiedById],[SysSettingsId],[SysAdminUnitId],[IsDef],[TextValue],[IntegerValue],[FloatValue],[BooleanValue],[DateTimeValue],[GuidValue],[BinaryValue],[Position],[ProcessListeners])
 VALUES([Source].[Id],[Source].[ModifiedOn],[Source].[CreatedOn],[Source].[CreatedById],[Source].[ModifiedById],[Source].[SysSettingsId],[Source].[SysAdminUnitId],[Source].[IsDef],[Source].[TextValue],[Source].[IntegerValue],[Source].[FloatValue],[Source].[BooleanValue],[Source].[DateTimeValue],[Source].[GuidValue],[Source].[BinaryValue],[Source].[Position],[Source].[ProcessListeners]);
