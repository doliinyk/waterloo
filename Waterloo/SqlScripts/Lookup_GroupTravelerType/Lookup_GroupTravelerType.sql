--Version Feb 10, 2021 @ 21:13 by OS (Split Faculty and Staff)
--Version Oct 15, 2020 by Kavian
--EXEC SP_GENERATE_MERGE 'WatGroupTravelerType'

MERGE INTO [WatGroupTravelerType] AS [Target]
USING (VALUES
  (N'CC276903-3A6A-4325-B4FB-AFF003F7EB38','2021-02-10T20:55:51.5830932',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2021-02-10T20:55:51.5830932',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Staff',N'',0)
 ,(N'4029F9DF-3793-43F1-ABC3-E59F4A6BF835','2020-10-15T17:01:23.0880329',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2021-02-10T20:55:48.0361299',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Faculty',N'',0)
 ,(N'D8258A6D-5B30-4A55-9A43-FDCC133DE485','2020-10-15T17:01:14.1221628',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-10-15T17:01:14.1221628',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Student',N'',0)
) AS [Source] ([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[CreatedOn], [Target].[CreatedOn]) IS NOT NULL OR NULLIF([Target].[CreatedOn], [Source].[CreatedOn]) IS NOT NULL OR 
	NULLIF([Source].[CreatedById], [Target].[CreatedById]) IS NOT NULL OR NULLIF([Target].[CreatedById], [Source].[CreatedById]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedOn], [Target].[ModifiedOn]) IS NOT NULL OR NULLIF([Target].[ModifiedOn], [Source].[ModifiedOn]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedById], [Target].[ModifiedById]) IS NOT NULL OR NULLIF([Target].[ModifiedById], [Source].[ModifiedById]) IS NOT NULL OR 
	NULLIF([Source].[Name], [Target].[Name]) IS NOT NULL OR NULLIF([Target].[Name], [Source].[Name]) IS NOT NULL OR 
	NULLIF([Source].[Description], [Target].[Description]) IS NOT NULL OR NULLIF([Target].[Description], [Source].[Description]) IS NOT NULL OR 
	NULLIF([Source].[ProcessListeners], [Target].[ProcessListeners]) IS NOT NULL OR NULLIF([Target].[ProcessListeners], [Source].[ProcessListeners]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[CreatedOn] = [Source].[CreatedOn], 
  [Target].[CreatedById] = [Source].[CreatedById], 
  [Target].[ModifiedOn] = [Source].[ModifiedOn], 
  [Target].[ModifiedById] = [Source].[ModifiedById], 
  [Target].[Name] = [Source].[Name], 
  [Target].[Description] = [Source].[Description], 
  [Target].[ProcessListeners] = [Source].[ProcessListeners]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
 VALUES([Source].[Id],[Source].[CreatedOn],[Source].[CreatedById],[Source].[ModifiedOn],[Source].[ModifiedById],[Source].[Name],[Source].[Description],[Source].[ProcessListeners]);
