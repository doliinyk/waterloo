/*Updated in Oct 9, 2020 AM by Kavian
exec sp_generate_merge 'SysAdminUnitInWorkplace ', 
@from='from SysAdminUnitInWorkplace where SysWorkplaceId in (select Id from SysWorkplace where Name in (''Agreements''))'*/
DELETE FROM SysModuleInWorkplace WHERE SysWorkplaceId in (select Id from SysWorkplace where Name in ('Agreements'))
MERGE INTO [SysAdminUnitInWorkplace ] AS [Target]
USING (VALUES
  (N'D22A69EF-0BA9-4B00-8EE9-27024B830A12','2020-10-09T14:32:17.320',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-10-09T14:32:17.320',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',0,N'6BCB6E88-E2AB-4B74-BBB5-782A98418BB7',N'A29A3BA5-4B0D-DE11-9A51-005056C00008')
 ,(N'54053C35-D274-4D39-97AE-DA88CE65EC3A','2020-10-09T14:32:17.332',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-10-09T14:32:17.332',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',0,N'6BCB6E88-E2AB-4B74-BBB5-782A98418BB7',N'83A43EBC-F36B-1410-298D-001E8C82BCAD')
) AS [Source] ([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[ProcessListeners],[SysWorkplaceId],[SysAdminUnitId])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[CreatedOn], [Target].[CreatedOn]) IS NOT NULL OR NULLIF([Target].[CreatedOn], [Source].[CreatedOn]) IS NOT NULL OR 
	NULLIF([Source].[CreatedById], [Target].[CreatedById]) IS NOT NULL OR NULLIF([Target].[CreatedById], [Source].[CreatedById]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedOn], [Target].[ModifiedOn]) IS NOT NULL OR NULLIF([Target].[ModifiedOn], [Source].[ModifiedOn]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedById], [Target].[ModifiedById]) IS NOT NULL OR NULLIF([Target].[ModifiedById], [Source].[ModifiedById]) IS NOT NULL OR 
	NULLIF([Source].[ProcessListeners], [Target].[ProcessListeners]) IS NOT NULL OR NULLIF([Target].[ProcessListeners], [Source].[ProcessListeners]) IS NOT NULL OR 
	NULLIF([Source].[SysWorkplaceId], [Target].[SysWorkplaceId]) IS NOT NULL OR NULLIF([Target].[SysWorkplaceId], [Source].[SysWorkplaceId]) IS NOT NULL OR 
	NULLIF([Source].[SysAdminUnitId], [Target].[SysAdminUnitId]) IS NOT NULL OR NULLIF([Target].[SysAdminUnitId], [Source].[SysAdminUnitId]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[CreatedOn] = [Source].[CreatedOn], 
  [Target].[CreatedById] = [Source].[CreatedById], 
  [Target].[ModifiedOn] = [Source].[ModifiedOn], 
  [Target].[ModifiedById] = [Source].[ModifiedById], 
  [Target].[ProcessListeners] = [Source].[ProcessListeners], 
  [Target].[SysWorkplaceId] = [Source].[SysWorkplaceId], 
  [Target].[SysAdminUnitId] = [Source].[SysAdminUnitId]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[ProcessListeners],[SysWorkplaceId],[SysAdminUnitId])
 VALUES([Source].[Id],[Source].[CreatedOn],[Source].[CreatedById],[Source].[ModifiedOn],[Source].[ModifiedById],[Source].[ProcessListeners],[Source].[SysWorkplaceId],[Source].[SysAdminUnitId]);
