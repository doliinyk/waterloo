--Updated Feb 11, 2021 @ 21:29 by OS (Change for Faculty Health)
--Updated Oct 6, 2020 @ 18:16 by Oliver 
--Original Version Sep 29, 2020 by Kavian
--EXEC SP_GENERATE_MERGE 'WatFaculty'
--added Centres and Institutes and Academic Support Units, Removed Other (Oct 6, 2020 - Oliver)

MERGE INTO [WatFaculty] AS [Target]
USING (VALUES
  (N'98F9014E-84E0-4912-A8DD-0ADB3505B2FC','2020-09-29T02:31:23.3743065',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-10-06T22:03:34.7881812',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Centres and Institutes',N'',0,NULL)
 ,(N'5AC26095-D38B-430F-A0C3-335423274ADA','2020-10-06T22:03:40.9937340',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-10-06T22:03:40.9957277',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Academic Support Units',N'',0,NULL)
 ,(N'DEE8D30A-321D-40D8-85EB-3D1866A9CB1B','2020-09-29T02:30:50.9784701',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-09-29T02:30:50.9784701',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Arts',N'',0,NULL)
 ,(N'416D27EE-E9FA-4D18-A3C2-40E4F6BD607C','2020-09-29T02:31:13.2570603',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-09-29T02:31:13.2570603',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Mathematics',N'',0,NULL)
 ,(N'FC039C89-197F-4608-AF73-62E9DA6E2CD4','2020-09-29T02:31:18.9919504',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-09-29T02:31:18.9919504',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Science',N'',0,NULL)
 ,(N'FC09CDDC-072E-4E0E-9C3F-6C9D7EE3D059','2020-09-29T02:31:03.2296124',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-09-29T02:31:03.2296124',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Environment',N'',0,NULL)
 ,(N'224135D3-5A21-4546-BE2E-7ABCB1078D7A','2020-09-29T02:30:56.6546988',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-09-29T02:30:56.6546988',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Engineering',N'',0,NULL)
 ,(N'542F16D4-6F66-4AC8-8B35-CDAFF3951003','2020-09-29T02:30:42.8700084',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-09-29T02:30:42.8700084',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Health',N'',0,NULL)
) AS [Source] ([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners],[WatFacultyDepartmentId])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[CreatedOn], [Target].[CreatedOn]) IS NOT NULL OR NULLIF([Target].[CreatedOn], [Source].[CreatedOn]) IS NOT NULL OR 
	NULLIF([Source].[CreatedById], [Target].[CreatedById]) IS NOT NULL OR NULLIF([Target].[CreatedById], [Source].[CreatedById]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedOn], [Target].[ModifiedOn]) IS NOT NULL OR NULLIF([Target].[ModifiedOn], [Source].[ModifiedOn]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedById], [Target].[ModifiedById]) IS NOT NULL OR NULLIF([Target].[ModifiedById], [Source].[ModifiedById]) IS NOT NULL OR 
	NULLIF([Source].[Name], [Target].[Name]) IS NOT NULL OR NULLIF([Target].[Name], [Source].[Name]) IS NOT NULL OR 
	NULLIF([Source].[Description], [Target].[Description]) IS NOT NULL OR NULLIF([Target].[Description], [Source].[Description]) IS NOT NULL OR 
	NULLIF([Source].[ProcessListeners], [Target].[ProcessListeners]) IS NOT NULL OR NULLIF([Target].[ProcessListeners], [Source].[ProcessListeners]) IS NOT NULL OR 
	NULLIF([Source].[WatFacultyDepartmentId], [Target].[WatFacultyDepartmentId]) IS NOT NULL OR NULLIF([Target].[WatFacultyDepartmentId], [Source].[WatFacultyDepartmentId]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[CreatedOn] = [Source].[CreatedOn], 
  [Target].[CreatedById] = [Source].[CreatedById], 
  [Target].[ModifiedOn] = [Source].[ModifiedOn], 
  [Target].[ModifiedById] = [Source].[ModifiedById], 
  [Target].[Name] = [Source].[Name], 
  [Target].[Description] = [Source].[Description], 
  [Target].[ProcessListeners] = [Source].[ProcessListeners], 
  [Target].[WatFacultyDepartmentId] = [Source].[WatFacultyDepartmentId]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners],[WatFacultyDepartmentId])
 VALUES([Source].[Id],[Source].[CreatedOn],[Source].[CreatedById],[Source].[ModifiedOn],[Source].[ModifiedById],[Source].[Name],[Source].[Description],[Source].[ProcessListeners],[Source].[WatFacultyDepartmentId]);
