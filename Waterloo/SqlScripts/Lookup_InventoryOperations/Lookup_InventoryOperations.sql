--Original Version October 25, 2020 @ 20:27 by Oliver
--EXEC SP_GENERATE_MERGE 'WatInventory'

MERGE INTO [WatInventory] AS [Target]
USING (VALUES
  (N'BFAE1E04-8A4B-4E74-BFF3-0615BBAFE734','2020-10-08T19:20:38.8412354',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-10-08T19:20:38.8412354',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Revise',N'',0)
 ,(N'14E42B7C-CFDB-4631-B58B-AA890025FA51','2020-10-08T19:20:23.5643576',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-10-08T19:20:23.5643576',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Subtract',N'',0)
 ,(N'451F56CC-EB84-4997-8E42-FFF6DCE2811D','2020-10-08T19:20:11.1670562',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-10-08T19:20:11.1670562',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Add',N'',0)
) AS [Source] ([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[CreatedOn], [Target].[CreatedOn]) IS NOT NULL OR NULLIF([Target].[CreatedOn], [Source].[CreatedOn]) IS NOT NULL OR 
	NULLIF([Source].[CreatedById], [Target].[CreatedById]) IS NOT NULL OR NULLIF([Target].[CreatedById], [Source].[CreatedById]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedOn], [Target].[ModifiedOn]) IS NOT NULL OR NULLIF([Target].[ModifiedOn], [Source].[ModifiedOn]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedById], [Target].[ModifiedById]) IS NOT NULL OR NULLIF([Target].[ModifiedById], [Source].[ModifiedById]) IS NOT NULL OR 
	NULLIF([Source].[Name], [Target].[Name]) IS NOT NULL OR NULLIF([Target].[Name], [Source].[Name]) IS NOT NULL OR 
	NULLIF([Source].[Description], [Target].[Description]) IS NOT NULL OR NULLIF([Target].[Description], [Source].[Description]) IS NOT NULL OR 
	NULLIF([Source].[ProcessListeners], [Target].[ProcessListeners]) IS NOT NULL OR NULLIF([Target].[ProcessListeners], [Source].[ProcessListeners]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[CreatedOn] = [Source].[CreatedOn], 
  [Target].[CreatedById] = [Source].[CreatedById], 
  [Target].[ModifiedOn] = [Source].[ModifiedOn], 
  [Target].[ModifiedById] = [Source].[ModifiedById], 
  [Target].[Name] = [Source].[Name], 
  [Target].[Description] = [Source].[Description], 
  [Target].[ProcessListeners] = [Source].[ProcessListeners]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
 VALUES([Source].[Id],[Source].[CreatedOn],[Source].[CreatedById],[Source].[ModifiedOn],[Source].[ModifiedById],[Source].[Name],[Source].[Description],[Source].[ProcessListeners]);
