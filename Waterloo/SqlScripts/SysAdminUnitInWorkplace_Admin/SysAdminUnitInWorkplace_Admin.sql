--Original Version Oct 25, 2020 @ 18:51 by Oliver
--exec sp_generate_merge 'SysAdminUnitInWorkplace ', @from='from SysAdminUnitInWorkplace where SysWorkplaceId in (select Id from SysWorkplace where Name in (''Admin''))'

DELETE FROM SysModuleInWorkplace WHERE SysWorkplaceId in (select Id from SysWorkplace where Name in ('Admin'))

MERGE INTO [SysAdminUnitInWorkplace ] AS [Target]
USING (VALUES
  (N'C068AA66-0779-42B4-83F5-41C24E5AB14D','2020-10-25T17:43:22.532',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-10-25T17:43:22.532',N'EEFDE995-2248-41CE-9575-B7529329E8DF',0,N'F3EC7EC0-AE93-4488-B97A-4DB29361791E',N'83A43EBC-F36B-1410-298D-001E8C82BCAD')
 ,(N'C844EFE2-CF2C-404C-99C5-9B6CC2394649','2020-10-25T17:43:49.320',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-10-25T17:43:49.321',N'EEFDE995-2248-41CE-9575-B7529329E8DF',0,N'F3EC7EC0-AE93-4488-B97A-4DB29361791E',N'417F7390-C9D4-4035-A54A-E4216D040B07')
) AS [Source] ([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[ProcessListeners],[SysWorkplaceId],[SysAdminUnitId])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[CreatedOn], [Target].[CreatedOn]) IS NOT NULL OR NULLIF([Target].[CreatedOn], [Source].[CreatedOn]) IS NOT NULL OR 
	NULLIF([Source].[CreatedById], [Target].[CreatedById]) IS NOT NULL OR NULLIF([Target].[CreatedById], [Source].[CreatedById]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedOn], [Target].[ModifiedOn]) IS NOT NULL OR NULLIF([Target].[ModifiedOn], [Source].[ModifiedOn]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedById], [Target].[ModifiedById]) IS NOT NULL OR NULLIF([Target].[ModifiedById], [Source].[ModifiedById]) IS NOT NULL OR 
	NULLIF([Source].[ProcessListeners], [Target].[ProcessListeners]) IS NOT NULL OR NULLIF([Target].[ProcessListeners], [Source].[ProcessListeners]) IS NOT NULL OR 
	NULLIF([Source].[SysWorkplaceId], [Target].[SysWorkplaceId]) IS NOT NULL OR NULLIF([Target].[SysWorkplaceId], [Source].[SysWorkplaceId]) IS NOT NULL OR 
	NULLIF([Source].[SysAdminUnitId], [Target].[SysAdminUnitId]) IS NOT NULL OR NULLIF([Target].[SysAdminUnitId], [Source].[SysAdminUnitId]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[CreatedOn] = [Source].[CreatedOn], 
  [Target].[CreatedById] = [Source].[CreatedById], 
  [Target].[ModifiedOn] = [Source].[ModifiedOn], 
  [Target].[ModifiedById] = [Source].[ModifiedById], 
  [Target].[ProcessListeners] = [Source].[ProcessListeners], 
  [Target].[SysWorkplaceId] = [Source].[SysWorkplaceId], 
  [Target].[SysAdminUnitId] = [Source].[SysAdminUnitId]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[ProcessListeners],[SysWorkplaceId],[SysAdminUnitId])
 VALUES([Source].[Id],[Source].[CreatedOn],[Source].[CreatedById],[Source].[ModifiedOn],[Source].[ModifiedById],[Source].[ProcessListeners],[Source].[SysWorkplaceId],[Source].[SysAdminUnitId]);
