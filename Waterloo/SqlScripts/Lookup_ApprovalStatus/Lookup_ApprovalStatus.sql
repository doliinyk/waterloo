--Version Dec 15, 2020 @ 13:26 by Oliver
--EXEC SP_GENERATE_MERGE 'VisaStatus'

MERGE INTO [VisaStatus] AS [Target]
USING (VALUES
  (N'E79FACB3-3C32-43E7-A59E-12BA125E6132','2013-12-12T09:13:18',N'22C5540C-D9B1-49EF-8EB7-72419B78E57C','2020-12-15T18:24:48.366',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Approved',N'',0,1)
 ,(N'9120FDEF-69BE-4168-B5F3-517714B7A4E9','2018-02-17T18:25:27.556',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-08-13T03:21:08.360',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Canceled',N'',0,1)
 ,(N'3462594D-77A7-4B0A-874A-6D8B54B293BC','2013-12-12T09:12:30',N'22C5540C-D9B1-49EF-8EB7-72419B78E57C','2020-11-16T00:02:48.289',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Requested',N'',0,0)
 ,(N'A93AB0B9-CA36-4B95-9B23-E01AA169C338','2013-12-12T09:13:27',N'22C5540C-D9B1-49EF-8EB7-72419B78E57C','2020-12-15T18:24:57.835',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Declined',N'',0,1)
) AS [Source] ([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners],[IsFinal])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[CreatedOn], [Target].[CreatedOn]) IS NOT NULL OR NULLIF([Target].[CreatedOn], [Source].[CreatedOn]) IS NOT NULL OR 
	NULLIF([Source].[CreatedById], [Target].[CreatedById]) IS NOT NULL OR NULLIF([Target].[CreatedById], [Source].[CreatedById]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedOn], [Target].[ModifiedOn]) IS NOT NULL OR NULLIF([Target].[ModifiedOn], [Source].[ModifiedOn]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedById], [Target].[ModifiedById]) IS NOT NULL OR NULLIF([Target].[ModifiedById], [Source].[ModifiedById]) IS NOT NULL OR 
	NULLIF([Source].[Name], [Target].[Name]) IS NOT NULL OR NULLIF([Target].[Name], [Source].[Name]) IS NOT NULL OR 
	NULLIF([Source].[Description], [Target].[Description]) IS NOT NULL OR NULLIF([Target].[Description], [Source].[Description]) IS NOT NULL OR 
	NULLIF([Source].[ProcessListeners], [Target].[ProcessListeners]) IS NOT NULL OR NULLIF([Target].[ProcessListeners], [Source].[ProcessListeners]) IS NOT NULL OR 
	NULLIF([Source].[IsFinal], [Target].[IsFinal]) IS NOT NULL OR NULLIF([Target].[IsFinal], [Source].[IsFinal]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[CreatedOn] = [Source].[CreatedOn], 
  [Target].[CreatedById] = [Source].[CreatedById], 
  [Target].[ModifiedOn] = [Source].[ModifiedOn], 
  [Target].[ModifiedById] = [Source].[ModifiedById], 
  [Target].[Name] = [Source].[Name], 
  [Target].[Description] = [Source].[Description], 
  [Target].[ProcessListeners] = [Source].[ProcessListeners], 
  [Target].[IsFinal] = [Source].[IsFinal]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners],[IsFinal])
 VALUES([Source].[Id],[Source].[CreatedOn],[Source].[CreatedById],[Source].[ModifiedOn],[Source].[ModifiedById],[Source].[Name],[Source].[Description],[Source].[ProcessListeners],[Source].[IsFinal]);
