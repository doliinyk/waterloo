--Original Mar 23, 2021 @ 20:57 by OS
--EXEC SP_GENERATE_MERGE 'WatPreDepartureWarning'

MERGE INTO [WatPreDepartureWarning] AS [Target]
USING (VALUES
  (N'36FD2539-F7C4-477A-9F49-64D3CE909897','2021-03-24T00:54:56.5330837',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2021-03-24T00:55:37.0508219',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'LEARN 7 Day Warning',N'',0)
 ,(N'642BB43F-15BB-4798-9434-C512BED83430','2021-03-24T00:55:57.2875073',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2021-03-24T00:55:57.2875073',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'LEARN - Departure Warning',N'',0)
 ,(N'190A1E06-AFEC-4A71-823D-DBA6C19D8BB4','2021-03-24T00:55:25.8471222',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2021-03-24T00:55:25.8471222',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'LEARN 2 Day Warning',N'',0)
) AS [Source] ([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[CreatedOn], [Target].[CreatedOn]) IS NOT NULL OR NULLIF([Target].[CreatedOn], [Source].[CreatedOn]) IS NOT NULL OR 
	NULLIF([Source].[CreatedById], [Target].[CreatedById]) IS NOT NULL OR NULLIF([Target].[CreatedById], [Source].[CreatedById]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedOn], [Target].[ModifiedOn]) IS NOT NULL OR NULLIF([Target].[ModifiedOn], [Source].[ModifiedOn]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedById], [Target].[ModifiedById]) IS NOT NULL OR NULLIF([Target].[ModifiedById], [Source].[ModifiedById]) IS NOT NULL OR 
	NULLIF([Source].[Name], [Target].[Name]) IS NOT NULL OR NULLIF([Target].[Name], [Source].[Name]) IS NOT NULL OR 
	NULLIF([Source].[Description], [Target].[Description]) IS NOT NULL OR NULLIF([Target].[Description], [Source].[Description]) IS NOT NULL OR 
	NULLIF([Source].[ProcessListeners], [Target].[ProcessListeners]) IS NOT NULL OR NULLIF([Target].[ProcessListeners], [Source].[ProcessListeners]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[CreatedOn] = [Source].[CreatedOn], 
  [Target].[CreatedById] = [Source].[CreatedById], 
  [Target].[ModifiedOn] = [Source].[ModifiedOn], 
  [Target].[ModifiedById] = [Source].[ModifiedById], 
  [Target].[Name] = [Source].[Name], 
  [Target].[Description] = [Source].[Description], 
  [Target].[ProcessListeners] = [Source].[ProcessListeners]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
 VALUES([Source].[Id],[Source].[CreatedOn],[Source].[CreatedById],[Source].[ModifiedOn],[Source].[ModifiedById],[Source].[Name],[Source].[Description],[Source].[ProcessListeners]);
