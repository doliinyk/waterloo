--Version Oct 15, 2020 by Kavian
--EXEC SP_GENERATE_MERGE 'WatGACAdvisoryLevel'

MERGE INTO [WatGACAdvisoryLevel] AS [Target]
USING (VALUES
  (N'C298096F-722A-4854-9B49-1052652332A8','2020-10-15T14:10:05.3341517',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-10-15T14:10:05.3341517',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Exercise high degree of caution',N'',0)
 ,(N'87C0C0E6-1794-46BB-82D1-4EFEBA52B021','2020-10-15T14:09:58.4728648',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-10-15T14:09:58.4728648',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Exercise normal security precautions',N'',0)
 ,(N'1769AAEC-08ED-425F-A0E5-5DD53CD35B1B','2020-10-15T14:10:19.5877029',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-10-15T14:10:19.5877029',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Avoid all travel',N'',0)
 ,(N'1F624336-F0B3-45B6-9EAE-D98D5DF9D55E','2020-10-15T14:10:11.0396926',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-10-15T14:10:11.0396926',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Avoid non-essential travel',N'',0)
) AS [Source] ([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[CreatedOn], [Target].[CreatedOn]) IS NOT NULL OR NULLIF([Target].[CreatedOn], [Source].[CreatedOn]) IS NOT NULL OR 
	NULLIF([Source].[CreatedById], [Target].[CreatedById]) IS NOT NULL OR NULLIF([Target].[CreatedById], [Source].[CreatedById]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedOn], [Target].[ModifiedOn]) IS NOT NULL OR NULLIF([Target].[ModifiedOn], [Source].[ModifiedOn]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedById], [Target].[ModifiedById]) IS NOT NULL OR NULLIF([Target].[ModifiedById], [Source].[ModifiedById]) IS NOT NULL OR 
	NULLIF([Source].[Name], [Target].[Name]) IS NOT NULL OR NULLIF([Target].[Name], [Source].[Name]) IS NOT NULL OR 
	NULLIF([Source].[Description], [Target].[Description]) IS NOT NULL OR NULLIF([Target].[Description], [Source].[Description]) IS NOT NULL OR 
	NULLIF([Source].[ProcessListeners], [Target].[ProcessListeners]) IS NOT NULL OR NULLIF([Target].[ProcessListeners], [Source].[ProcessListeners]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[CreatedOn] = [Source].[CreatedOn], 
  [Target].[CreatedById] = [Source].[CreatedById], 
  [Target].[ModifiedOn] = [Source].[ModifiedOn], 
  [Target].[ModifiedById] = [Source].[ModifiedById], 
  [Target].[Name] = [Source].[Name], 
  [Target].[Description] = [Source].[Description], 
  [Target].[ProcessListeners] = [Source].[ProcessListeners]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
 VALUES([Source].[Id],[Source].[CreatedOn],[Source].[CreatedById],[Source].[ModifiedOn],[Source].[ModifiedById],[Source].[Name],[Source].[Description],[Source].[ProcessListeners]);