--Version Sep 8, 2020 @ 11:24 by Oliver
--EXEC SP_GENERATE_MERGE 'WatGiftCategories'

MERGE INTO [WatGiftCategories] AS [Target]
USING (VALUES
  (N'A0E64EBE-7D29-4EAC-BD28-06CFF92FF47E','2020-09-02T18:25:32.2069812',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-09-02T18:25:32.2069812',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Leaf Ornaments',N'',0)
 ,(N'83D21332-BE47-4A7E-A74D-1F8F0570D79B','2020-09-02T18:25:43.3434751',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-09-02T18:25:43.3434751',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Chocolate',N'',0)
 ,(N'93EE8DEF-67FE-479F-A59E-1F937BA9A0AA','2020-09-02T18:25:54.2586287',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-09-02T18:25:54.2586287',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Miscellaneous',N'',0)
 ,(N'323B3DEE-3EF1-49AA-89D0-2551E0120029','2020-09-02T18:25:18.3777250',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-09-02T18:25:18.3777250',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Frames',N'',0)
 ,(N'EA3F4987-18DA-459A-8D1B-62A94B10C0DF','2020-09-02T18:25:26.0217376',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-09-02T18:25:26.0217376',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Bags',N'',0)
 ,(N'D0662BE6-3428-4152-B073-86B5A5EB01ED','2020-09-02T18:25:13.2089028',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-09-02T18:25:13.2089028',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Maple Syrup',N'',0)
 ,(N'5DC139BF-FE1E-435E-8018-98CA1ACD1C7A','2020-09-02T18:25:48.6589018',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-09-02T18:25:48.6589018',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Tea',N'',0)
 ,(N'67106701-D716-476A-A87A-A10F46E874BF','2020-09-02T18:25:39.0238729',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-09-02T18:25:39.0238729',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Pens',N'',0)
) AS [Source] ([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[CreatedOn], [Target].[CreatedOn]) IS NOT NULL OR NULLIF([Target].[CreatedOn], [Source].[CreatedOn]) IS NOT NULL OR 
	NULLIF([Source].[CreatedById], [Target].[CreatedById]) IS NOT NULL OR NULLIF([Target].[CreatedById], [Source].[CreatedById]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedOn], [Target].[ModifiedOn]) IS NOT NULL OR NULLIF([Target].[ModifiedOn], [Source].[ModifiedOn]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedById], [Target].[ModifiedById]) IS NOT NULL OR NULLIF([Target].[ModifiedById], [Source].[ModifiedById]) IS NOT NULL OR 
	NULLIF([Source].[Name], [Target].[Name]) IS NOT NULL OR NULLIF([Target].[Name], [Source].[Name]) IS NOT NULL OR 
	NULLIF([Source].[Description], [Target].[Description]) IS NOT NULL OR NULLIF([Target].[Description], [Source].[Description]) IS NOT NULL OR 
	NULLIF([Source].[ProcessListeners], [Target].[ProcessListeners]) IS NOT NULL OR NULLIF([Target].[ProcessListeners], [Source].[ProcessListeners]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[CreatedOn] = [Source].[CreatedOn], 
  [Target].[CreatedById] = [Source].[CreatedById], 
  [Target].[ModifiedOn] = [Source].[ModifiedOn], 
  [Target].[ModifiedById] = [Source].[ModifiedById], 
  [Target].[Name] = [Source].[Name], 
  [Target].[Description] = [Source].[Description], 
  [Target].[ProcessListeners] = [Source].[ProcessListeners]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
 VALUES([Source].[Id],[Source].[CreatedOn],[Source].[CreatedById],[Source].[ModifiedOn],[Source].[ModifiedById],[Source].[Name],[Source].[Description],[Source].[ProcessListeners]);
