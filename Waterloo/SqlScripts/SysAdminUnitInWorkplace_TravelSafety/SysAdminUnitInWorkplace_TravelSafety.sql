--Original Version Oct 25, 2020 @ 20:09 by Oliver
--exec sp_generate_merge 'SysAdminUnitInWorkplace ', @from='from SysAdminUnitInWorkplace where SysWorkplaceId in (select Id from SysWorkplace where Name in (''Travel Safety''))'

DELETE FROM SysModuleInWorkplace WHERE SysWorkplaceId in (select Id from SysWorkplace where Name in ('Travel Safety'))

MERGE INTO [SysAdminUnitInWorkplace ] AS [Target]
USING (VALUES
  (N'D375E1AC-557B-4867-ACE0-2354FC80158C','2020-10-26T00:05:34.419',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-10-26T00:05:34.419',N'EEFDE995-2248-41CE-9575-B7529329E8DF',0,N'E3CC9683-1C7A-48C3-8E02-81529343B761',N'C8EDA78B-9E95-428B-AFD8-4904446CD745')
 ,(N'7C3E6ADD-E6B9-4C21-935C-4543EF120A2B','2020-10-26T00:05:34.402',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-10-26T00:05:34.402',N'EEFDE995-2248-41CE-9575-B7529329E8DF',0,N'E3CC9683-1C7A-48C3-8E02-81529343B761',N'337BE652-89D8-4097-A1F6-FEAFE4D66AF0')
 ,(N'6B38A48F-AEBF-42EF-B87A-C4915BD153CE','2020-10-26T00:05:15.094',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-10-26T00:05:15.094',N'EEFDE995-2248-41CE-9575-B7529329E8DF',0,N'E3CC9683-1C7A-48C3-8E02-81529343B761',N'83A43EBC-F36B-1410-298D-001E8C82BCAD')
 ,(N'3A5704FD-0921-4A93-8B46-D28BA622E5BE','2020-10-26T00:05:34.432',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-10-26T00:05:34.432',N'EEFDE995-2248-41CE-9575-B7529329E8DF',0,N'E3CC9683-1C7A-48C3-8E02-81529343B761',N'38E4EC6B-D9C7-4882-8396-8EFB83F4A3DB')
 ,(N'8FEB26BA-8B8F-40C0-811B-E3399ABF8589','2020-10-26T00:05:34.447',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-10-26T00:05:34.447',N'EEFDE995-2248-41CE-9575-B7529329E8DF',0,N'E3CC9683-1C7A-48C3-8E02-81529343B761',N'9E5EB95C-C98B-4D8C-B35D-1FE02F806DFF')
) AS [Source] ([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[ProcessListeners],[SysWorkplaceId],[SysAdminUnitId])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[CreatedOn], [Target].[CreatedOn]) IS NOT NULL OR NULLIF([Target].[CreatedOn], [Source].[CreatedOn]) IS NOT NULL OR 
	NULLIF([Source].[CreatedById], [Target].[CreatedById]) IS NOT NULL OR NULLIF([Target].[CreatedById], [Source].[CreatedById]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedOn], [Target].[ModifiedOn]) IS NOT NULL OR NULLIF([Target].[ModifiedOn], [Source].[ModifiedOn]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedById], [Target].[ModifiedById]) IS NOT NULL OR NULLIF([Target].[ModifiedById], [Source].[ModifiedById]) IS NOT NULL OR 
	NULLIF([Source].[ProcessListeners], [Target].[ProcessListeners]) IS NOT NULL OR NULLIF([Target].[ProcessListeners], [Source].[ProcessListeners]) IS NOT NULL OR 
	NULLIF([Source].[SysWorkplaceId], [Target].[SysWorkplaceId]) IS NOT NULL OR NULLIF([Target].[SysWorkplaceId], [Source].[SysWorkplaceId]) IS NOT NULL OR 
	NULLIF([Source].[SysAdminUnitId], [Target].[SysAdminUnitId]) IS NOT NULL OR NULLIF([Target].[SysAdminUnitId], [Source].[SysAdminUnitId]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[CreatedOn] = [Source].[CreatedOn], 
  [Target].[CreatedById] = [Source].[CreatedById], 
  [Target].[ModifiedOn] = [Source].[ModifiedOn], 
  [Target].[ModifiedById] = [Source].[ModifiedById], 
  [Target].[ProcessListeners] = [Source].[ProcessListeners], 
  [Target].[SysWorkplaceId] = [Source].[SysWorkplaceId], 
  [Target].[SysAdminUnitId] = [Source].[SysAdminUnitId]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[ProcessListeners],[SysWorkplaceId],[SysAdminUnitId])
 VALUES([Source].[Id],[Source].[CreatedOn],[Source].[CreatedById],[Source].[ModifiedOn],[Source].[ModifiedById],[Source].[ProcessListeners],[Source].[SysWorkplaceId],[Source].[SysAdminUnitId]);
