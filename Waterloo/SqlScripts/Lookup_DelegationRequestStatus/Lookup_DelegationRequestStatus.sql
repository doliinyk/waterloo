--Version Aug 28, 2020 @ 16:55 by Oliver
--EXEC SP_GENERATE_MERGE 'WatDelegationRequestStatus'

MERGE INTO [WatDelegationRequestStatus] AS [Target]
USING (VALUES
  (N'A5141BBC-5263-49CB-97F3-4339694F6409','2020-08-28T15:11:35.3438622',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-08-28T15:11:35.3438622',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Cancelled',N'',0)
 ,(N'0A3B468F-94B9-4A9F-9A6B-6C7221969252','2020-08-28T15:11:11.1125116',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-08-28T15:11:11.1125116',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Accepted',N'',0)
 ,(N'7145AE31-56BE-4FCD-925E-7AC71313330D','2020-08-28T15:11:01.0171169',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-08-28T15:11:01.0171169',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Not Decided',N'',0)
 ,(N'0D2D58BD-A1A2-4885-BDFC-9213C187A342','2020-08-28T15:11:27.1948735',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-08-28T15:11:27.1948735',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'On Hold',N'',0)
 ,(N'0B9F407C-0A50-44A9-B11C-C271F7984579','2020-08-28T15:11:15.8166758',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-08-28T15:11:15.8166758',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Declined',N'',0)
) AS [Source] ([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[CreatedOn], [Target].[CreatedOn]) IS NOT NULL OR NULLIF([Target].[CreatedOn], [Source].[CreatedOn]) IS NOT NULL OR 
	NULLIF([Source].[CreatedById], [Target].[CreatedById]) IS NOT NULL OR NULLIF([Target].[CreatedById], [Source].[CreatedById]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedOn], [Target].[ModifiedOn]) IS NOT NULL OR NULLIF([Target].[ModifiedOn], [Source].[ModifiedOn]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedById], [Target].[ModifiedById]) IS NOT NULL OR NULLIF([Target].[ModifiedById], [Source].[ModifiedById]) IS NOT NULL OR 
	NULLIF([Source].[Name], [Target].[Name]) IS NOT NULL OR NULLIF([Target].[Name], [Source].[Name]) IS NOT NULL OR 
	NULLIF([Source].[Description], [Target].[Description]) IS NOT NULL OR NULLIF([Target].[Description], [Source].[Description]) IS NOT NULL OR 
	NULLIF([Source].[ProcessListeners], [Target].[ProcessListeners]) IS NOT NULL OR NULLIF([Target].[ProcessListeners], [Source].[ProcessListeners]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[CreatedOn] = [Source].[CreatedOn], 
  [Target].[CreatedById] = [Source].[CreatedById], 
  [Target].[ModifiedOn] = [Source].[ModifiedOn], 
  [Target].[ModifiedById] = [Source].[ModifiedById], 
  [Target].[Name] = [Source].[Name], 
  [Target].[Description] = [Source].[Description], 
  [Target].[ProcessListeners] = [Source].[ProcessListeners]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
 VALUES([Source].[Id],[Source].[CreatedOn],[Source].[CreatedById],[Source].[ModifiedOn],[Source].[ModifiedById],[Source].[Name],[Source].[Description],[Source].[ProcessListeners]);