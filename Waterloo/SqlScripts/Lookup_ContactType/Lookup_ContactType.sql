--Original Version Feb 10, 2021 @ 21:13 by OS
--EXEC SP_GENERATE_MERGE 'ContactType'

MERGE INTO [ContactType] AS [Target]
USING (VALUES
  (N'806732EE-F36B-1410-A883-16D83CAB0980','2010-12-24T10:38:33.123',N'22C5540C-D9B1-49EF-8EB7-72419B78E57C','2021-02-10T18:11:11.798',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'External Contact',N'',0)
 ,(N'00783EF6-F36B-1410-A883-16D83CAB0980','2010-12-24T10:38:41.041',N'22C5540C-D9B1-49EF-8EB7-72419B78E57C','2021-02-10T18:11:19.861',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Internal Contact',N'',0)
 ,(N'60733EFC-F36B-1410-A883-16D83CAB0980','2010-12-24T10:38:50.022',N'22C5540C-D9B1-49EF-8EB7-72419B78E57C','2020-08-13T03:12:36.564',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Employee',N'',0)
) AS [Source] ([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[CreatedOn], [Target].[CreatedOn]) IS NOT NULL OR NULLIF([Target].[CreatedOn], [Source].[CreatedOn]) IS NOT NULL OR 
	NULLIF([Source].[CreatedById], [Target].[CreatedById]) IS NOT NULL OR NULLIF([Target].[CreatedById], [Source].[CreatedById]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedOn], [Target].[ModifiedOn]) IS NOT NULL OR NULLIF([Target].[ModifiedOn], [Source].[ModifiedOn]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedById], [Target].[ModifiedById]) IS NOT NULL OR NULLIF([Target].[ModifiedById], [Source].[ModifiedById]) IS NOT NULL OR 
	NULLIF([Source].[Name], [Target].[Name]) IS NOT NULL OR NULLIF([Target].[Name], [Source].[Name]) IS NOT NULL OR 
	NULLIF([Source].[Description], [Target].[Description]) IS NOT NULL OR NULLIF([Target].[Description], [Source].[Description]) IS NOT NULL OR 
	NULLIF([Source].[ProcessListeners], [Target].[ProcessListeners]) IS NOT NULL OR NULLIF([Target].[ProcessListeners], [Source].[ProcessListeners]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[CreatedOn] = [Source].[CreatedOn], 
  [Target].[CreatedById] = [Source].[CreatedById], 
  [Target].[ModifiedOn] = [Source].[ModifiedOn], 
  [Target].[ModifiedById] = [Source].[ModifiedById], 
  [Target].[Name] = [Source].[Name], 
  [Target].[Description] = [Source].[Description], 
  [Target].[ProcessListeners] = [Source].[ProcessListeners]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
 VALUES([Source].[Id],[Source].[CreatedOn],[Source].[CreatedById],[Source].[ModifiedOn],[Source].[ModifiedById],[Source].[Name],[Source].[Description],[Source].[ProcessListeners]);
