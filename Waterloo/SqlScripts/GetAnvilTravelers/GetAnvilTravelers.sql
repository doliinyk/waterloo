--USE [universityofwaterloo]
--GO
/****** Object:  UserDefinedFunction [dbo].[GetAnvilTravelers]    Script Date: 5/10/2021 8:38:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER function [dbo].[GetAnvilTravelers] (@TravelerId uniqueidentifier, @TripId uniqueidentifier)
returns table
as
return
	select c.GivenName as [First Name]
		,c.Surname as [Last Name]
		,'Traveller Details' as [Type of Data]
		,traveler.WatPNR as [Record Locator]
		,c.Email as [Email Address]
		,LEFT(SUBSTRING(c.MobilePhone, PATINDEX('%[0-9.-]%', c.MobilePhone), 8000),
           PATINDEX('%[^0-9.-]%', SUBSTRING(c.MobilePhone, PATINDEX('%[0-9.-]%', c.MobilePhone), 8000) + 'X') -1) as [SMS (Int. Format)]
		,c.WatWatIAMId as [WatIAM ID]
		,'' as Affiliation
		,isnull(tt.Name, '') as [Traveler Type]
		,'' as [IATA Airline Code]
		,'' as [Flight Number]
		,'' as [Departure IATA Airport Code]
		,'' as [Flight Departure Date/Time]
		,'' as [Arrival IATA Airport Code]
		,'' as [Flight Arrival Date/Time]
		,'' as [Railway]
		,'' as [Train Number]
		,'' as [Rail Departure Station]
		,'' as [Rail Departure Country]
		,'' as [Rail Departure Date/Time]
		,'' as [Rail Arrival Station]
		,'' as [Rail Arrival Country]
		,'' as [Rail Arrival Date/Time]
		,'' as [Hotel Name]
		,'' as [Hotel Address]
		,'' as [Hotel City]
		,'' as [Hotel Country]
		,'' as [Hotel Telephone]
		,'' as [Hotel Check In Date]
		,'' as [Hotel Check Out Date]
	from WatTrip trip
		join WatTraveler traveler on traveler.WatTripId = trip.Id
		join Contact c on c.Id = traveler.WatContactId
		left join WatTravelerType tt on tt.Id = trip.WatTravelerTypeId
	where traveler.Id = @TravelerId
		and trip.Id = @TripId
		and traveler.WatAnvilReady = 1

	union all 

	select c.GivenName as [First Name]
		,c.Surname as [Last Name]
		,'Hotel Leg' as [Type of Data]
		,traveler.WatPNR as [Record Locator]
		,c.Email as [Email Address]
		,LEFT(SUBSTRING(c.MobilePhone, PATINDEX('%[0-9.-]%', c.MobilePhone), 8000),
           PATINDEX('%[^0-9.-]%', SUBSTRING(c.MobilePhone, PATINDEX('%[0-9.-]%', c.MobilePhone), 8000) + 'X') -1) as [SMS (Int. Format)]
		,c.WatWatIAMId as [WatIAM ID]
		,'' as Affiliation
		,isnull(tt.Name, '') as [Traveler Type]
		,'' as [IATA Airline Code]
		,'' as [Flight Number]
		,'' as [Departure IATA Airport Code]
		,'' as [Flight Departure Date/Time]
		,'' as [Arrival IATA Airport Code]
		,'' as [Flight Arrival Date/Time]
		,'' as [Railway]
		,'' as [Train Number]
		,'' as [Rail Departure Station]
		,'' as [Rail Departure Country]
		,'' as [Rail Departure Date/Time]
		,'' as [Rail Arrival Station]
		,'' as [Rail Arrival Country]
		,'' as [Rail Arrival Date/Time]	
		,trip.WatCityState + 
			case 
				when trip.WatCityState <> '' and trip.WatDestinationProvinceState <> '' then ', '
				else ''
			end
		+ trip.WatDestinationProvinceState as [Hotel Name]
		,trip.WatCityState + 
			case 
				when trip.WatCityState <> '' and trip.WatDestinationProvinceState <> '' then ', '
				else ''
			end
		+ trip.WatDestinationProvinceState as [Hotel Address]
		,trip.WatCityState +
			case 
				when trip.WatCityState <> '' and trip.WatDestinationProvinceState <> '' then ', '
				else ''
			end
		+ trip.WatDestinationProvinceState as [Hotel City]
		--, case
				--when leg.WatUseName = 1 then leg.Name
				--else leg.WatAlternativeCountryName
			--end as [Hotel Country]--	
		,leg.WatAnvilName as [Hotel Country]
		,'' as [Hotel Telephone]
		,iif(trip.WatDepartureDate is null, '', format(trip.WatDepartureDate, 'dd/MM/yyyy')) as [Hotel Check In Date]		
		,iif(leg2.Id is null, 
			iif(trip.WatReturnDate is null, '', format(trip.WatReturnDate, 'dd/MM/yyyy')), 
			iif(trip.WatDepartureDateLeg2 is null, '', format(trip.WatDepartureDateLeg2, 'dd/MM/yyyy'))
			) 		
	from WatTrip trip
		join WatTraveler traveler on traveler.WatTripId = trip.Id
		join Contact c on c.Id = traveler.WatContactId
		left join WatTravelerType tt on tt.Id = trip.WatTravelerTypeId
		join Country leg on leg.Id = trip.WatDestinationId
		left join Country leg2 on leg2.Id = trip.WatDestinationCountryLeg2Id
	where traveler.Id = @TravelerId
		and trip.Id = @TripId
		and traveler.WatAnvilReady = 1

	union all 

	select c.GivenName as [First Name]
		,c.Surname as [Last Name]
		,'Flight Leg' as [Type of Data]
		,traveler.WatPNR as [Record Locator]
		,c.Email as [Email Address]
		,LEFT(SUBSTRING(c.MobilePhone, PATINDEX('%[0-9.-]%', c.MobilePhone), 8000),
           PATINDEX('%[^0-9.-]%', SUBSTRING(c.MobilePhone, PATINDEX('%[0-9.-]%', c.MobilePhone), 8000) + 'X') -1) as [SMS (Int. Format)]
		,c.WatWatIAMId as [WatIAM ID]
		,'' as Affiliation
		,isnull(tt.Name, '') as [Traveler Type]
		,'AC' as [IATA Airline Code]
		,'1230' as [Flight Number]
		,'YYZ' as [Departure IATA Airport Code]
		,iif(trip.WatDepartureDate is null, '', format(trip.WatDepartureDate, 'dd/MM/yyyy hh:mm')) as [Flight Departure Date/Time]
		,leg.WatIATACode as [Arrival IATA Airport Code]
		,iif(trip.WatDepartureDate is null, '', format(trip.WatDepartureDate, 'dd/MM/yyyy hh:mm')) as [Flight Arrival Date/Time]		
		,'' as [Railway]
		,'' as [Train Number]
		,'' as [Rail Departure Station]
		,'' as [Rail Departure Country]
		,'' as [Rail Departure Date/Time]
		,'' as [Rail Arrival Station]
		,'' as [Rail Arrival Country]
		,'' as [Rail Arrival Date/Time]	
		,'' as [Hotel Name]
		,'' as [Hotel Address]
		,'' as [Hotel City]
		,'' as [Hotel Country]
		,'' as [Hotel Telephone]
		,'' as [Hotel Check In Date]
		,'' as [Hotel Check Out Date]
	from WatTrip trip
		join WatTraveler traveler on traveler.WatTripId = trip.Id
		join Contact c on c.Id = traveler.WatContactId
		join Country leg on leg.Id = trip.WatDestinationId
		left join WatTravelerType tt on tt.Id = trip.WatTravelerTypeId
	where traveler.Id = @TravelerId
		and trip.Id = @TripId
		and traveler.WatAnvilReady = 1

	-- leg 2

	union all 

	select c.GivenName as [First Name]
		,c.Surname as [Last Name]
		,'Hotel Leg' as [Type of Data]
		,traveler.WatPNR as [Record Locator]
		,c.Email as [Email Address]
		,LEFT(SUBSTRING(c.MobilePhone, PATINDEX('%[0-9.-]%', c.MobilePhone), 8000),
           PATINDEX('%[^0-9.-]%', SUBSTRING(c.MobilePhone, PATINDEX('%[0-9.-]%', c.MobilePhone), 8000) + 'X') -1) as [SMS (Int. Format)]
		,c.WatWatIAMId as [WatIAM ID]
		,'' as Affiliation
		,isnull(tt.Name, '') as [Traveler Type]
		,'' as [IATA Airline Code]
		,'' as [Flight Number]
		,'' as [Departure IATA Airport Code]
		,'' as [Flight Departure Date/Time]
		,'' as [Arrival IATA Airport Code]
		,'' as [Flight Arrival Date/Time]
		,'' as [Railway]
		,'' as [Train Number]
		,'' as [Rail Departure Station]
		,'' as [Rail Departure Country]
		,'' as [Rail Departure Date/Time]
		,'' as [Rail Arrival Station]
		,'' as [Rail Arrival Country]
		,'' as [Rail Arrival Date/Time]	
		,trip.WatDestinationCityLeg2 + 
			case 
				when trip.WatDestinationCityLeg2 <> '' and trip.WatDestinationProvinceStateLeg2 <> '' then ', '
				else ''
			end
		+ trip.WatDestinationProvinceStateLeg2 as [Hotel Name]
		,trip.WatDestinationCityLeg2 + 
			case 
				when trip.WatDestinationCityLeg2 <> '' and trip.WatDestinationProvinceStateLeg2 <> '' then ', '
				else ''
			end
		+ trip.WatDestinationProvinceStateLeg2 as [Hotel Address]
		,trip.WatDestinationCityLeg2 + 
			case 
				when trip.WatDestinationCityLeg2 <> '' and trip.WatDestinationProvinceStateLeg2 <> '' then ', '
				else ''
			end
		+ trip.WatDestinationProvinceStateLeg2 as [Hotel City]
		,leg2.WatAnvilName as [Hotel Country]
		,'' as [Hotel Telephone]
		,iif(trip.WatDepartureDateLeg2 is null, '', format(trip.WatDepartureDateLeg2, 'dd/MM/yyyy')) as [Hotel Check In Date]		
		,iif(leg3.Id is null, 
			iif(trip.WatReturnDate is null, '', format(trip.WatReturnDate, 'dd/MM/yyyy')), 
			iif(trip.WatDepartureDateLeg3 is null, '', format(trip.WatDepartureDateLeg3, 'dd/MM/yyyy'))
			) 		
	from WatTrip trip
		join WatTraveler traveler on traveler.WatTripId = trip.Id
		join Contact c on c.Id = traveler.WatContactId
		left join WatTravelerType tt on tt.Id = trip.WatTravelerTypeId
		join Country leg2 on leg2.Id = trip.WatDestinationCountryLeg2Id
		left join Country leg3 on leg3.Id = trip.WatDestinationCountryLeg3Id
	where traveler.Id = @TravelerId
		and trip.Id = @TripId
		and traveler.WatAnvilReady = 1

	union all 

	select c.GivenName as [First Name]
		,c.Surname as [Last Name]
		,'Flight Leg' as [Type of Data]
		,traveler.WatPNR as [Record Locator]
		,c.Email as [Email Address]
		,LEFT(SUBSTRING(c.MobilePhone, PATINDEX('%[0-9.-]%', c.MobilePhone), 8000),
           PATINDEX('%[^0-9.-]%', SUBSTRING(c.MobilePhone, PATINDEX('%[0-9.-]%', c.MobilePhone), 8000) + 'X') -1) as [SMS (Int. Format)]
		,c.WatWatIAMId as [WatIAM ID]
		,'' as Affiliation
		,isnull(tt.Name, '') as [Traveler Type]
		,'AC' as [IATA Airline Code]
		,'1230' as [Flight Number]
		,'YYZ' as [Departure IATA Airport Code]
		,iif(trip.WatDepartureDateLeg2 is null, '', format(trip.WatDepartureDateLeg2, 'dd/MM/yyyy hh:mm')) as [Flight Departure Date/Time]
		,leg2.WatIATACode as [Arrival IATA Airport Code]
		,iif(trip.WatDepartureDateLeg2 is null, '', format(trip.WatDepartureDateLeg2, 'dd/MM/yyyy hh:mm')) as [Flight Arrival Date/Time]		
		,'' as [Railway]
		,'' as [Train Number]
		,'' as [Rail Departure Station]
		,'' as [Rail Departure Country]
		,'' as [Rail Departure Date/Time]
		,'' as [Rail Arrival Station]
		,'' as [Rail Arrival Country]
		,'' as [Rail Arrival Date/Time]	
		,'' as [Hotel Name]
		,'' as [Hotel Address]
		,'' as [Hotel City]
		,'' as [Hotel Country]
		,'' as [Hotel Telephone]
		,'' as [Hotel Check In Date]
		,'' as [Hotel Check Out Date]
	from WatTrip trip
		join WatTraveler traveler on traveler.WatTripId = trip.Id
		join Contact c on c.Id = traveler.WatContactId
		join Country leg2 on leg2.Id = trip.WatDestinationCountryLeg2Id
		left join WatTravelerType tt on tt.Id = trip.WatTravelerTypeId
	where traveler.Id = @TravelerId
		and trip.Id = @TripId
		and traveler.WatAnvilReady = 1

	-- leg 3

	union all 

	select c.GivenName as [First Name]
		,c.Surname as [Last Name]
		,'Hotel Leg' as [Type of Data]
		,traveler.WatPNR as [Record Locator]
		,c.Email as [Email Address]
		,LEFT(SUBSTRING(c.MobilePhone, PATINDEX('%[0-9.-]%', c.MobilePhone), 8000),
           PATINDEX('%[^0-9.-]%', SUBSTRING(c.MobilePhone, PATINDEX('%[0-9.-]%', c.MobilePhone), 8000) + 'X') -1) as [SMS (Int. Format)]
		,c.WatWatIAMId as [WatIAM ID]
		,'' as Affiliation
		,isnull(tt.Name, '') as [Traveler Type]
		,'' as [IATA Airline Code]
		,'' as [Flight Number]
		,'' as [Departure IATA Airport Code]
		,'' as [Flight Departure Date/Time]
		,'' as [Arrival IATA Airport Code]
		,'' as [Flight Arrival Date/Time]
		,'' as [Railway]
		,'' as [Train Number]
		,'' as [Rail Departure Station]
		,'' as [Rail Departure Country]
		,'' as [Rail Departure Date/Time]
		,'' as [Rail Arrival Station]
		,'' as [Rail Arrival Country]
		,'' as [Rail Arrival Date/Time]	
		,trip.WatDestinationCityLeg3 + 
			case 
				when trip.WatDestinationCityLeg3 <> '' and trip.WatDestinationProvinceStateLeg3 <> '' then ', '
				else ''
			end
		+ trip.WatDestinationProvinceStateLeg3 as [Hotel Name]
		,trip.WatDestinationCityLeg3 + 
			case 
				when trip.WatDestinationCityLeg3 <> '' and trip.WatDestinationProvinceStateLeg3 <> '' then ', '
				else ''
			end
		+ trip.WatDestinationProvinceStateLeg3 as [Hotel Address]
		,trip.WatDestinationCityLeg3 + 
			case 
				when trip.WatDestinationCityLeg3 <> '' and trip.WatDestinationProvinceStateLeg3 <> '' then ', '
				else ''
			end
		+ trip.WatDestinationProvinceStateLeg3 as [Hotel City]
		,leg3.WatAnvilName as [Hotel Country]
		,'' as [Hotel Telephone]
		,iif(trip.WatDepartureDateLeg3 is null, '', format(trip.WatDepartureDateLeg3, 'dd/MM/yyyy')) as [Hotel Check In Date]		
		,iif(leg4.Id is null, 
			iif(trip.WatReturnDate is null, '', format(trip.WatReturnDate, 'dd/MM/yyyy')), 
			iif(trip.WatDepartureDateLeg4 is null, '', format(trip.WatDepartureDateLeg4, 'dd/MM/yyyy'))
			) 		
	from WatTrip trip
		join WatTraveler traveler on traveler.WatTripId = trip.Id
		join Contact c on c.Id = traveler.WatContactId
		left join WatTravelerType tt on tt.Id = trip.WatTravelerTypeId
		join Country leg3 on leg3.Id = trip.WatDestinationCountryLeg3Id
		left join Country leg4 on leg4.Id = trip.WatDestinationCountryLeg4Id
	where traveler.Id = @TravelerId
		and trip.Id = @TripId
		and traveler.WatAnvilReady = 1

	union all 

	select c.GivenName as [First Name]
		,c.Surname as [Last Name]
		,'Flight Leg' as [Type of Data]
		,traveler.WatPNR as [Record Locator]
		,c.Email as [Email Address]
		,LEFT(SUBSTRING(c.MobilePhone, PATINDEX('%[0-9.-]%', c.MobilePhone), 8000),
           PATINDEX('%[^0-9.-]%', SUBSTRING(c.MobilePhone, PATINDEX('%[0-9.-]%', c.MobilePhone), 8000) + 'X') -1) as [SMS (Int. Format)]
		,c.WatWatIAMId as [WatIAM ID]
		,'' as Affiliation
		,isnull(tt.Name, '') as [Traveler Type]
		,'AC' as [IATA Airline Code]
		,'1230' as [Flight Number]
		,'YYZ' as [Departure IATA Airport Code]
		,iif(trip.WatDepartureDateLeg3 is null, '', format(trip.WatDepartureDateLeg3, 'dd/MM/yyyy hh:mm')) as [Flight Departure Date/Time]
		,leg3.WatIATACode as [Arrival IATA Airport Code]
		,iif(trip.WatDepartureDateLeg3 is null, '', format(trip.WatDepartureDateLeg3, 'dd/MM/yyyy hh:mm')) as [Flight Arrival Date/Time]		
		,'' as [Railway]
		,'' as [Train Number]
		,'' as [Rail Departure Station]
		,'' as [Rail Departure Country]
		,'' as [Rail Departure Date/Time]
		,'' as [Rail Arrival Station]
		,'' as [Rail Arrival Country]
		,'' as [Rail Arrival Date/Time]	
		,'' as [Hotel Name]
		,'' as [Hotel Address]
		,'' as [Hotel City]
		,'' as [Hotel Country]
		,'' as [Hotel Telephone]
		,'' as [Hotel Check In Date]
		,'' as [Hotel Check Out Date]
	from WatTrip trip
		join WatTraveler traveler on traveler.WatTripId = trip.Id
		join Contact c on c.Id = traveler.WatContactId
		join Country leg3 on leg3.Id = trip.WatDestinationCountryLeg3Id
		left join WatTravelerType tt on tt.Id = trip.WatTravelerTypeId
	where traveler.Id = @TravelerId
		and trip.Id = @TripId
		and traveler.WatAnvilReady = 1

	-- leg 4

	union all 

	select c.GivenName as [First Name]
		,c.Surname as [Last Name]
		,'Hotel Leg' as [Type of Data]
		,traveler.WatPNR as [Record Locator]
		,c.Email as [Email Address]
		,LEFT(SUBSTRING(c.MobilePhone, PATINDEX('%[0-9.-]%', c.MobilePhone), 8000),
           PATINDEX('%[^0-9.-]%', SUBSTRING(c.MobilePhone, PATINDEX('%[0-9.-]%', c.MobilePhone), 8000) + 'X') -1) as [SMS (Int. Format)]
		,c.WatWatIAMId as [WatIAM ID]
		,'' as Affiliation
		,isnull(tt.Name, '') as [Traveler Type]
		,'' as [IATA Airline Code]
		,'' as [Flight Number]
		,'' as [Departure IATA Airport Code]
		,'' as [Flight Departure Date/Time]
		,'' as [Arrival IATA Airport Code]
		,'' as [Flight Arrival Date/Time]
		,'' as [Railway]
		,'' as [Train Number]
		,'' as [Rail Departure Station]
		,'' as [Rail Departure Country]
		,'' as [Rail Departure Date/Time]
		,'' as [Rail Arrival Station]
		,'' as [Rail Arrival Country]
		,'' as [Rail Arrival Date/Time]	
		,trip.WatDestinationCityLeg4 + 
			case 
				when trip.WatDestinationCityLeg4 <> '' and trip.WatDestinationProvinceStateLeg4 <> '' then ', '
				else ''
			end
		+ trip.WatDestinationProvinceStateLeg4 as [Hotel Name]
		,trip.WatDestinationCityLeg4 + 
			case 
				when trip.WatDestinationCityLeg4 <> '' and trip.WatDestinationProvinceStateLeg4 <> '' then ', '
				else ''
			end
		+ trip.WatDestinationProvinceStateLeg4 as [Hotel Address]
		,trip.WatDestinationCityLeg4 + 
			case 
				when trip.WatDestinationCityLeg4 <> '' and trip.WatDestinationProvinceStateLeg4 <> '' then ', '
				else ''
			end
		+ trip.WatDestinationProvinceStateLeg4 as [Hotel City]
		,leg4.WatAnvilName as [Hotel Country]
		,'' as [Hotel Telephone]
		,iif(trip.WatDepartureDateLeg4 is null, '', format(trip.WatDepartureDateLeg4, 'dd/MM/yyyy')) as [Hotel Check In Date]		
		,iif(leg5.Id is null, 
			iif(trip.WatReturnDate is null, '', format(trip.WatReturnDate, 'dd/MM/yyyy')), 
			iif(trip.WatDepartureDateLeg5 is null, '', format(trip.WatDepartureDateLeg5, 'dd/MM/yyyy'))
			) 		
	from WatTrip trip
		join WatTraveler traveler on traveler.WatTripId = trip.Id
		join Contact c on c.Id = traveler.WatContactId
		left join WatTravelerType tt on tt.Id = trip.WatTravelerTypeId
		join Country leg4 on leg4.Id = trip.WatDestinationCountryLeg4Id
		left join Country leg5 on leg5.Id = trip.WatDestinationCountryLeg5Id
	where traveler.Id = @TravelerId
		and trip.Id = @TripId
		and traveler.WatAnvilReady = 1

	union all 

	select c.GivenName as [First Name]
		,c.Surname as [Last Name]
		,'Flight Leg' as [Type of Data]
		,traveler.WatPNR as [Record Locator]
		,c.Email as [Email Address]
		,LEFT(SUBSTRING(c.MobilePhone, PATINDEX('%[0-9.-]%', c.MobilePhone), 8000),
           PATINDEX('%[^0-9.-]%', SUBSTRING(c.MobilePhone, PATINDEX('%[0-9.-]%', c.MobilePhone), 8000) + 'X') -1) as [SMS (Int. Format)]
		,c.WatWatIAMId as [WatIAM ID]
		,'' as Affiliation
		,isnull(tt.Name, '') as [Traveler Type]
		,'AC' as [IATA Airline Code]
		,'1230' as [Flight Number]
		,'YYZ' as [Departure IATA Airport Code]
		,iif(trip.WatDepartureDateLeg4 is null, '', format(trip.WatDepartureDateLeg4, 'dd/MM/yyyy hh:mm')) as [Flight Departure Date/Time]
		,leg4.WatIATACode as [Arrival IATA Airport Code]
		,iif(trip.WatDepartureDateLeg4 is null, '', format(trip.WatDepartureDateLeg4, 'dd/MM/yyyy hh:mm')) as [Flight Arrival Date/Time]		
		,'' as [Railway]
		,'' as [Train Number]
		,'' as [Rail Departure Station]
		,'' as [Rail Departure Country]
		,'' as [Rail Departure Date/Time]
		,'' as [Rail Arrival Station]
		,'' as [Rail Arrival Country]
		,'' as [Rail Arrival Date/Time]	
		,'' as [Hotel Name]
		,'' as [Hotel Address]
		,'' as [Hotel City]
		,'' as [Hotel Country]
		,'' as [Hotel Telephone]
		,'' as [Hotel Check In Date]
		,'' as [Hotel Check Out Date]
	from WatTrip trip
		join WatTraveler traveler on traveler.WatTripId = trip.Id
		join Contact c on c.Id = traveler.WatContactId
		join Country leg4 on leg4.Id = trip.WatDestinationCountryLeg4Id
		left join WatTravelerType tt on tt.Id = trip.WatTravelerTypeId
	where traveler.Id = @TravelerId
		and trip.Id = @TripId
		and traveler.WatAnvilReady = 1

	-- leg 5

	union all 

	select c.GivenName as [First Name]
		,c.Surname as [Last Name]
		,'Hotel Leg' as [Type of Data]
		,traveler.WatPNR as [Record Locator]
		,c.Email as [Email Address]
		,LEFT(SUBSTRING(c.MobilePhone, PATINDEX('%[0-9.-]%', c.MobilePhone), 8000),
           PATINDEX('%[^0-9.-]%', SUBSTRING(c.MobilePhone, PATINDEX('%[0-9.-]%', c.MobilePhone), 8000) + 'X') -1) as [SMS (Int. Format)]
		,c.WatWatIAMId as [WatIAM ID]
		,'' as Affiliation
		,isnull(tt.Name, '') as [Traveler Type]
		,'' as [IATA Airline Code]
		,'' as [Flight Number]
		,'' as [Departure IATA Airport Code]
		,'' as [Flight Departure Date/Time]
		,'' as [Arrival IATA Airport Code]
		,'' as [Flight Arrival Date/Time]
		,'' as [Railway]
		,'' as [Train Number]
		,'' as [Rail Departure Station]
		,'' as [Rail Departure Country]
		,'' as [Rail Departure Date/Time]
		,'' as [Rail Arrival Station]
		,'' as [Rail Arrival Country]
		,'' as [Rail Arrival Date/Time]	
		,trip.WatDestinationCityLeg5 + 
			case 
				when trip.WatDestinationCityLeg5 <> '' and trip.WatDestinationProvinceStateLeg5 <> '' then ', '
				else ''
			end
		+ trip.WatDestinationProvinceStateLeg5 as [Hotel Name]
		,trip.WatDestinationCityLeg5 + 
			case 
				when trip.WatDestinationCityLeg5 <> '' and trip.WatDestinationProvinceStateLeg5 <> '' then ', '
				else ''
			end
		+ trip.WatDestinationProvinceStateLeg5 as [Hotel Address]
		,trip.WatDestinationCityLeg5 + 
			case 
				when trip.WatDestinationCityLeg5 <> '' and trip.WatDestinationProvinceStateLeg5 <> '' then ', '
				else ''
			end
		+ trip.WatDestinationProvinceStateLeg5 as [Hotel City]
		,leg5.WatAnvilName as [Hotel Country]
		,'' as [Hotel Telephone]
		,iif(trip.WatDepartureDateLeg5 is null, '', format(trip.WatDepartureDateLeg5, 'dd/MM/yyyy')) as [Hotel Check In Date]		
		,iif(trip.WatReturnDate is null, '', format(trip.WatReturnDate, 'dd/MM/yyyy'))
	from WatTrip trip
		join WatTraveler traveler on traveler.WatTripId = trip.Id
		join Contact c on c.Id = traveler.WatContactId
		left join WatTravelerType tt on tt.Id = trip.WatTravelerTypeId
		join Country leg5 on leg5.Id = trip.WatDestinationCountryLeg5Id		
	where traveler.Id = @TravelerId
		and trip.Id = @TripId
		and traveler.WatAnvilReady = 1

	union all 

	select c.GivenName as [First Name]
		,c.Surname as [Last Name]
		,'Flight Leg' as [Type of Data]
		,traveler.WatPNR as [Record Locator]
		,c.Email as [Email Address]
		,LEFT(SUBSTRING(c.MobilePhone, PATINDEX('%[0-9.-]%', c.MobilePhone), 8000),
           PATINDEX('%[^0-9.-]%', SUBSTRING(c.MobilePhone, PATINDEX('%[0-9.-]%', c.MobilePhone), 8000) + 'X') -1) as [SMS (Int. Format)]
		,c.WatWatIAMId as [WatIAM ID]
		,'' as Affiliation
		,isnull(tt.Name, '') as [Traveler Type]
		,'AC' as [IATA Airline Code]
		,'1230' as [Flight Number]
		,'YYZ' as [Departure IATA Airport Code]
		,iif(trip.WatDepartureDateLeg5 is null, '', format(trip.WatDepartureDateLeg5, 'dd/MM/yyyy hh:mm')) as [Flight Departure Date/Time]
		,leg5.WatIATACode as [Arrival IATA Airport Code]
		,iif(trip.WatDepartureDateLeg5 is null, '', format(trip.WatDepartureDateLeg5, 'dd/MM/yyyy hh:mm')) as [Flight Arrival Date/Time]		
		,'' as [Railway]
		,'' as [Train Number]
		,'' as [Rail Departure Station]
		,'' as [Rail Departure Country]
		,'' as [Rail Departure Date/Time]
		,'' as [Rail Arrival Station]
		,'' as [Rail Arrival Country]
		,'' as [Rail Arrival Date/Time]	
		,'' as [Hotel Name]
		,'' as [Hotel Address]
		,'' as [Hotel City]
		,'' as [Hotel Country]
		,'' as [Hotel Telephone]
		,'' as [Hotel Check In Date]
		,'' as [Hotel Check Out Date]
	from WatTrip trip
		join WatTraveler traveler on traveler.WatTripId = trip.Id
		join Contact c on c.Id = traveler.WatContactId
		join Country leg5 on leg5.Id = trip.WatDestinationCountryLeg5Id
		left join WatTravelerType tt on tt.Id = trip.WatTravelerTypeId
	where traveler.Id = @TravelerId
		and trip.Id = @TripId
		and traveler.WatAnvilReady = 1