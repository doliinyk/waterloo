--Original Version Oct 26, 2020 @ 21:34 by Oliver
--EXEC SP_GENERATE_MERGE 'ActivityResult'

MERGE INTO [ActivityResult] AS [Target]
USING (VALUES
  (N'DBBF0E10-F46B-1410-6598-00155D043204','2011-01-26T14:13:27',N'22C5540C-D9B1-49EF-8EB7-72419B78E57C','2020-08-13T03:39:11',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Canceled',N'',0,N'4DB33CBC-BA7C-4103-9887-6B39B8D36B77',0)
 ,(N'45C96C70-106A-45E1-8669-4DEC3023DD3B','2020-10-09T01:03:22.668',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-10-09T01:03:22.668',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Acknowledged',N'',0,N'1D2E48F2-C5FD-417D-9551-30E71F35DF3D',0)
 ,(N'632AFDD2-F616-4EA6-87D2-8ED38EED8AFF','2014-05-14T11:04:01',N'22C5540C-D9B1-49EF-8EB7-72419B78E57C','2020-08-13T03:39:14.472',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Completed',N'',0,N'1D2E48F2-C5FD-417D-9551-30E71F35DF3D',0)
 ,(N'63976585-E1FA-41B1-A030-AD6B0C59299F','2020-11-13T23:40:28.915',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-11-14T18:41:23.671',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'2 - DO NOT PROCEED',N'',0,N'4DB33CBC-BA7C-4103-9887-6B39B8D36B77',0)
 ,(N'C9BEC207-E004-4566-A96E-B8D172B77885','2020-11-13T23:39:49.981',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-11-14T18:41:28.796',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'1 - PROCEED',N'',0,N'1D2E48F2-C5FD-417D-9551-30E71F35DF3D',0)
) AS [Source] ([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners],[CategoryId],[BusinessProcessOnly])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[CreatedOn], [Target].[CreatedOn]) IS NOT NULL OR NULLIF([Target].[CreatedOn], [Source].[CreatedOn]) IS NOT NULL OR 
	NULLIF([Source].[CreatedById], [Target].[CreatedById]) IS NOT NULL OR NULLIF([Target].[CreatedById], [Source].[CreatedById]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedOn], [Target].[ModifiedOn]) IS NOT NULL OR NULLIF([Target].[ModifiedOn], [Source].[ModifiedOn]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedById], [Target].[ModifiedById]) IS NOT NULL OR NULLIF([Target].[ModifiedById], [Source].[ModifiedById]) IS NOT NULL OR 
	NULLIF([Source].[Name], [Target].[Name]) IS NOT NULL OR NULLIF([Target].[Name], [Source].[Name]) IS NOT NULL OR 
	NULLIF([Source].[Description], [Target].[Description]) IS NOT NULL OR NULLIF([Target].[Description], [Source].[Description]) IS NOT NULL OR 
	NULLIF([Source].[ProcessListeners], [Target].[ProcessListeners]) IS NOT NULL OR NULLIF([Target].[ProcessListeners], [Source].[ProcessListeners]) IS NOT NULL OR 
	NULLIF([Source].[CategoryId], [Target].[CategoryId]) IS NOT NULL OR NULLIF([Target].[CategoryId], [Source].[CategoryId]) IS NOT NULL OR 
	NULLIF([Source].[BusinessProcessOnly], [Target].[BusinessProcessOnly]) IS NOT NULL OR NULLIF([Target].[BusinessProcessOnly], [Source].[BusinessProcessOnly]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[CreatedOn] = [Source].[CreatedOn], 
  [Target].[CreatedById] = [Source].[CreatedById], 
  [Target].[ModifiedOn] = [Source].[ModifiedOn], 
  [Target].[ModifiedById] = [Source].[ModifiedById], 
  [Target].[Name] = [Source].[Name], 
  [Target].[Description] = [Source].[Description], 
  [Target].[ProcessListeners] = [Source].[ProcessListeners], 
  [Target].[CategoryId] = [Source].[CategoryId], 
  [Target].[BusinessProcessOnly] = [Source].[BusinessProcessOnly]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners],[CategoryId],[BusinessProcessOnly])
 VALUES([Source].[Id],[Source].[CreatedOn],[Source].[CreatedById],[Source].[ModifiedOn],[Source].[ModifiedById],[Source].[Name],[Source].[Description],[Source].[ProcessListeners],[Source].[CategoryId],[Source].[BusinessProcessOnly]);
