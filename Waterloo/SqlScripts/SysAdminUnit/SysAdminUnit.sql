--Version Sep 21, 2020 @ 12:15 PM by Kavian
--exec sp_generate_merge 'SysAdminUnit', @from='from SysAdminUnit where ContactId is null'

MERGE INTO [SysAdminUnit] AS [Target]
USING (VALUES
  (N'83A43EBC-F36B-1410-298D-001E8C82BCAD','2011-02-04T15:30:45',N'22C5540C-D9B1-49EF-8EB7-72419B78E57C','2020-09-21T15:38:58.743',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'System Administrators',N'',N'A29A3BA5-4B0D-DE11-9A51-005056C00008',NULL,N'',N'',1,NULL,1,0,0,N'',N'',N'',0,0,N'A5420246-0A8E-E111-84A3-00155D054C03',0,N'',N'','2020-08-13T15:00:10.733',NULL,0,NULL,0,NULL,NULL,0,NULL)
 ,(N'A29A3BA5-4B0D-DE11-9A51-005056C00008','2009-03-10T10:15:42.353',N'22C5540C-D9B1-49EF-8EB7-72419B78E57C','2017-05-31T18:21:32.267',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'All employees',N'',NULL,NULL,N'',N'',0,N'E308B781-3C5B-4ECB-89EF-5C1ED4DA488E',0,0,0,N'Analysts',N'72426b84d64647c13cfdec7910cf964a',N'CN=Analysts,OU=groups,OU=Terrasoft,DC=tscrm,DC=com',0,0,N'A5420246-0A8E-E111-84A3-00155D054C03',0,N'',N'','2020-08-13T15:00:10.733',NULL,0,NULL,0,NULL,NULL,0,NULL)
 ,(N'7393FCE1-8CB8-4A5A-8B17-113D121B767F','2020-09-21T15:40:10.846',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-09-21T15:40:10.846',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Associate Director Operations',N'',N'9E5EB95C-C98B-4D8C-B35D-1FE02F806DFF',NULL,N'',N'',1,NULL,1,0,0,N'',N'',N'',0,0,N'A5420246-0A8E-E111-84A3-00155D054C03',0,N'',N'',NULL,NULL,0,NULL,0,NULL,NULL,0,NULL)
 ,(N'4BE9C4BB-8E79-4539-9173-17C8A766B671','2015-04-08T07:10:11.917',NULL,'2020-09-21T15:37:45.524',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Agreements Team',N'',N'A29A3BA5-4B0D-DE11-9A51-005056C00008',NULL,N'',N'',0,N'E308B781-3C5B-4ECB-89EF-5C1ED4DA488E',1,0,0,N'',N'',N'',0,0,N'A5420246-0A8E-E111-84A3-00155D054C03',0,N'',N'','2020-08-13T15:00:10.733',NULL,0,NULL,0,NULL,NULL,0,NULL)
 ,(N'3017AE20-8E3C-40F6-A465-1920F7143C0F','2019-10-29T16:56:08.783',NULL,'2019-10-29T17:20:12.902',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Partner portal users',N'',N'720B771C-E7A7-4F31-9CFB-52CD21C3739F',NULL,N'',N'',6,NULL,1,0,0,N'',N'',N'',0,0,N'A5420246-0A8E-E111-84A3-00155D054C03',0,N'',N'','2020-08-13T15:00:10.733',NULL,1,NULL,0,NULL,NULL,0,NULL)
 ,(N'9E5EB95C-C98B-4D8C-B35D-1FE02F806DFF','2020-09-21T15:40:00.033',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-09-21T15:40:00.033',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Senior Management',N'',N'A29A3BA5-4B0D-DE11-9A51-005056C00008',NULL,N'',N'',0,NULL,1,0,0,N'',N'',N'',0,0,N'A5420246-0A8E-E111-84A3-00155D054C03',0,N'',N'',NULL,NULL,0,NULL,0,NULL,NULL,0,NULL)
 ,(N'538593A3-7AE2-460E-838D-23D2A1C419BF','2020-09-21T15:35:20.658',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-09-21T15:35:20.658',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'IRM Relationship Lead',N'',N'9DC7626C-E03A-4978-B5C2-CDCF99DE7801',NULL,N'',N'',1,NULL,1,0,0,N'',N'',N'',0,0,N'A5420246-0A8E-E111-84A3-00155D054C03',0,N'',N'',NULL,NULL,0,NULL,0,NULL,NULL,0,NULL)
 ,(N'6F9AF602-3A22-455C-B056-2CF14241F943','2019-03-26T18:53:19.417',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2019-06-12T07:39:16.468',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Administrator for organization on the portal',N'',N'720B771C-E7A7-4F31-9CFB-52CD21C3739F',NULL,N'',N'',6,NULL,1,0,0,N'',N'',N'',0,0,N'A5420246-0A8E-E111-84A3-00155D054C03',0,N'',N'','2020-08-13T15:00:10.733',NULL,1,NULL,0,NULL,NULL,0,NULL)
 ,(N'C8EDA78B-9E95-428B-AFD8-4904446CD745','2020-09-21T15:41:02.876',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-09-21T15:41:02.876',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Safety Abroad Admin',N'',N'337BE652-89D8-4097-A1F6-FEAFE4D66AF0',NULL,N'',N'',1,NULL,1,0,0,N'',N'',N'',0,0,N'A5420246-0A8E-E111-84A3-00155D054C03',0,N'',N'',NULL,NULL,0,NULL,0,NULL,NULL,0,NULL)
 ,(N'720B771C-E7A7-4F31-9CFB-52CD21C3739F','2015-04-08T07:08:30.003',NULL,'2018-12-04T18:33:57.979',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'All portal users',N'',NULL,NULL,N'',N'',0,N'E308B781-3C5B-4ECB-89EF-5C1ED4DA488E',1,0,0,N'',N'',N'',0,0,N'A5420246-0A8E-E111-84A3-00155D054C03',0,N'',N'','2020-08-13T15:00:10.733',NULL,1,NULL,0,NULL,NULL,0,NULL)
 ,(N'E9541775-7749-4DA3-AD98-7D002EA7E08B','2020-09-21T15:38:19.915',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-09-21T15:38:19.915',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Agreements Manager',N'',N'4BE9C4BB-8E79-4539-9173-17C8A766B671',NULL,N'',N'',1,NULL,1,0,0,N'',N'',N'',0,0,N'A5420246-0A8E-E111-84A3-00155D054C03',0,N'',N'',NULL,NULL,0,NULL,0,NULL,NULL,0,NULL)
 ,(N'06209940-2B32-4163-9D40-856223041B9C','2020-09-21T15:40:27.918',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-09-21T15:40:27.918',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Associate Vice President (AVPI)',N'',N'9E5EB95C-C98B-4D8C-B35D-1FE02F806DFF',NULL,N'',N'',1,NULL,1,0,0,N'',N'',N'',0,0,N'A5420246-0A8E-E111-84A3-00155D054C03',0,N'',N'',NULL,NULL,0,NULL,0,NULL,NULL,0,NULL)
 ,(N'9827231B-D14A-4A41-B6BD-876AE68B7381','2019-11-19T20:03:36.450',NULL,'2019-11-19T20:03:36.450',NULL,N'Partners',N'',N'720B771C-E7A7-4F31-9CFB-52CD21C3739F',NULL,N'',N'',0,NULL,1,0,0,N'',N'',N'',0,0,N'A5420246-0A8E-E111-84A3-00155D054C03',0,N'',N'','2020-08-13T15:00:10.733',NULL,1,NULL,0,NULL,NULL,0,NULL)
 ,(N'93AD89C1-7C23-404A-A921-886CFB1CE89D','2020-09-21T15:38:42.966',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-09-21T15:38:42.966',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Agreements Admin',N'',N'4BE9C4BB-8E79-4539-9173-17C8A766B671',NULL,N'',N'',1,NULL,1,0,0,N'',N'',N'',0,0,N'A5420246-0A8E-E111-84A3-00155D054C03',0,N'',N'',NULL,NULL,0,NULL,0,NULL,NULL,0,NULL)
 ,(N'38E4EC6B-D9C7-4882-8396-8EFB83F4A3DB','2020-09-21T15:40:56.150',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-09-21T15:40:56.150',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Safety Abroad Manager',N'',N'337BE652-89D8-4097-A1F6-FEAFE4D66AF0',NULL,N'',N'',1,NULL,1,0,0,N'',N'',N'',0,0,N'A5420246-0A8E-E111-84A3-00155D054C03',0,N'',N'',NULL,NULL,0,NULL,0,NULL,NULL,0,NULL)
 ,(N'E777CAD0-9677-4C70-9BFB-9DD012CBC25B','2020-09-21T15:36:03.979',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-09-21T15:36:03.979',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Co-Op Student',N'',N'8DDE850B-379D-4E50-99EC-B6CC41AFF2D3',NULL,N'',N'',1,NULL,1,0,0,N'',N'',N'',0,0,N'A5420246-0A8E-E111-84A3-00155D054C03',0,N'',N'',NULL,NULL,0,NULL,0,NULL,NULL,0,NULL)
 ,(N'EB254F5F-A8B6-4EE8-9F59-B55FF1EF801A','2020-09-21T15:35:48.559',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-09-21T15:35:48.559',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Operations Manager',N'',N'8DDE850B-379D-4E50-99EC-B6CC41AFF2D3',NULL,N'',N'',1,NULL,1,0,0,N'',N'',N'',0,0,N'A5420246-0A8E-E111-84A3-00155D054C03',0,N'',N'',NULL,NULL,0,NULL,0,NULL,NULL,0,NULL)
 ,(N'D4D53476-93E3-4786-9A0B-B6B75926765F','2020-09-21T15:41:12.197',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-09-21T15:41:12.197',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'General WI Team',N'',N'A29A3BA5-4B0D-DE11-9A51-005056C00008',NULL,N'',N'',0,NULL,1,0,0,N'',N'',N'',0,0,N'A5420246-0A8E-E111-84A3-00155D054C03',0,N'',N'',NULL,NULL,0,NULL,0,NULL,NULL,0,NULL)
 ,(N'8DDE850B-379D-4E50-99EC-B6CC41AFF2D3','2018-10-30T18:37:08.935',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-09-21T15:34:53.903',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Operations Team',N'',N'A29A3BA5-4B0D-DE11-9A51-005056C00008',NULL,N'',N'',1,NULL,1,0,0,N'',N'',N'',0,0,N'A5420246-0A8E-E111-84A3-00155D054C03',0,N'',N'','2020-08-13T15:00:10.733',NULL,0,NULL,0,NULL,NULL,0,NULL)
 ,(N'998A26BC-43C2-449B-A9E0-C63D01E74143','2020-09-21T15:40:20.080',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-09-21T15:40:20.080',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Associate Director International Relations',N'',N'9E5EB95C-C98B-4D8C-B35D-1FE02F806DFF',NULL,N'',N'',1,NULL,1,0,0,N'',N'',N'',0,0,N'A5420246-0A8E-E111-84A3-00155D054C03',0,N'',N'',NULL,NULL,0,NULL,0,NULL,NULL,0,NULL)
 ,(N'9DC7626C-E03A-4978-B5C2-CDCF99DE7801','2015-04-08T07:10:11.910',NULL,'2020-09-21T15:34:01',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'IRM Team',N'',N'A29A3BA5-4B0D-DE11-9A51-005056C00008',NULL,N'',N'',0,N'E308B781-3C5B-4ECB-89EF-5C1ED4DA488E',1,0,0,N'',N'',N'',0,0,N'A5420246-0A8E-E111-84A3-00155D054C03',0,N'',N'','2020-08-13T15:00:10.733',NULL,0,NULL,0,NULL,NULL,0,NULL)
 ,(N'417F7390-C9D4-4035-A54A-E4216D040B07','2020-09-21T15:35:56.273',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-09-21T15:35:56.273',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Operations Admin',N'',N'8DDE850B-379D-4E50-99EC-B6CC41AFF2D3',NULL,N'',N'',1,NULL,1,0,0,N'',N'',N'',0,0,N'A5420246-0A8E-E111-84A3-00155D054C03',0,N'',N'',NULL,NULL,0,NULL,0,NULL,NULL,0,NULL)
 ,(N'337BE652-89D8-4097-A1F6-FEAFE4D66AF0','2020-09-21T15:40:49.132',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-09-21T15:40:49.132',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Safety Abroad',N'',N'A29A3BA5-4B0D-DE11-9A51-005056C00008',NULL,N'',N'',0,NULL,1,0,0,N'',N'',N'',0,0,N'A5420246-0A8E-E111-84A3-00155D054C03',0,N'',N'',NULL,NULL,0,NULL,0,NULL,NULL,0,NULL)
) AS [Source] ([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ParentRoleId],[ContactId],[TimeZoneId],[UserPassword],[SysAdminUnitTypeValue],[AccountId],[Active],[LoggedIn],[SynchronizeWithLDAP],[LDAPEntry],[LDAPEntryId],[LDAPEntryDN],[IsDirectoryEntry],[ProcessListeners],[SysCultureId],[LoginAttemptCount],[SourceControlLogin],[SourceControlPassword],[PasswordExpireDate],[HomePageId],[ConnectionType],[UnblockTime],[ForceChangePassword],[LDAPElementId],[DateTimeFormatId],[SessionTimeout],[PortalAccountId])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[CreatedOn], [Target].[CreatedOn]) IS NOT NULL OR NULLIF([Target].[CreatedOn], [Source].[CreatedOn]) IS NOT NULL OR 
	NULLIF([Source].[CreatedById], [Target].[CreatedById]) IS NOT NULL OR NULLIF([Target].[CreatedById], [Source].[CreatedById]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedOn], [Target].[ModifiedOn]) IS NOT NULL OR NULLIF([Target].[ModifiedOn], [Source].[ModifiedOn]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedById], [Target].[ModifiedById]) IS NOT NULL OR NULLIF([Target].[ModifiedById], [Source].[ModifiedById]) IS NOT NULL OR 
	NULLIF([Source].[Name], [Target].[Name]) IS NOT NULL OR NULLIF([Target].[Name], [Source].[Name]) IS NOT NULL OR 
	NULLIF([Source].[Description], [Target].[Description]) IS NOT NULL OR NULLIF([Target].[Description], [Source].[Description]) IS NOT NULL OR 
	NULLIF([Source].[ParentRoleId], [Target].[ParentRoleId]) IS NOT NULL OR NULLIF([Target].[ParentRoleId], [Source].[ParentRoleId]) IS NOT NULL OR 
	NULLIF([Source].[ContactId], [Target].[ContactId]) IS NOT NULL OR NULLIF([Target].[ContactId], [Source].[ContactId]) IS NOT NULL OR 
	NULLIF([Source].[TimeZoneId], [Target].[TimeZoneId]) IS NOT NULL OR NULLIF([Target].[TimeZoneId], [Source].[TimeZoneId]) IS NOT NULL OR 
	NULLIF([Source].[UserPassword], [Target].[UserPassword]) IS NOT NULL OR NULLIF([Target].[UserPassword], [Source].[UserPassword]) IS NOT NULL OR 
	NULLIF([Source].[SysAdminUnitTypeValue], [Target].[SysAdminUnitTypeValue]) IS NOT NULL OR NULLIF([Target].[SysAdminUnitTypeValue], [Source].[SysAdminUnitTypeValue]) IS NOT NULL OR 
	NULLIF([Source].[AccountId], [Target].[AccountId]) IS NOT NULL OR NULLIF([Target].[AccountId], [Source].[AccountId]) IS NOT NULL OR 
	NULLIF([Source].[Active], [Target].[Active]) IS NOT NULL OR NULLIF([Target].[Active], [Source].[Active]) IS NOT NULL OR 
	NULLIF([Source].[LoggedIn], [Target].[LoggedIn]) IS NOT NULL OR NULLIF([Target].[LoggedIn], [Source].[LoggedIn]) IS NOT NULL OR 
	NULLIF([Source].[SynchronizeWithLDAP], [Target].[SynchronizeWithLDAP]) IS NOT NULL OR NULLIF([Target].[SynchronizeWithLDAP], [Source].[SynchronizeWithLDAP]) IS NOT NULL OR 
	NULLIF([Source].[LDAPEntry], [Target].[LDAPEntry]) IS NOT NULL OR NULLIF([Target].[LDAPEntry], [Source].[LDAPEntry]) IS NOT NULL OR 
	NULLIF([Source].[LDAPEntryId], [Target].[LDAPEntryId]) IS NOT NULL OR NULLIF([Target].[LDAPEntryId], [Source].[LDAPEntryId]) IS NOT NULL OR 
	NULLIF([Source].[LDAPEntryDN], [Target].[LDAPEntryDN]) IS NOT NULL OR NULLIF([Target].[LDAPEntryDN], [Source].[LDAPEntryDN]) IS NOT NULL OR 
	NULLIF([Source].[IsDirectoryEntry], [Target].[IsDirectoryEntry]) IS NOT NULL OR NULLIF([Target].[IsDirectoryEntry], [Source].[IsDirectoryEntry]) IS NOT NULL OR 
	NULLIF([Source].[ProcessListeners], [Target].[ProcessListeners]) IS NOT NULL OR NULLIF([Target].[ProcessListeners], [Source].[ProcessListeners]) IS NOT NULL OR 
	NULLIF([Source].[SysCultureId], [Target].[SysCultureId]) IS NOT NULL OR NULLIF([Target].[SysCultureId], [Source].[SysCultureId]) IS NOT NULL OR 
	NULLIF([Source].[LoginAttemptCount], [Target].[LoginAttemptCount]) IS NOT NULL OR NULLIF([Target].[LoginAttemptCount], [Source].[LoginAttemptCount]) IS NOT NULL OR 
	NULLIF([Source].[SourceControlLogin], [Target].[SourceControlLogin]) IS NOT NULL OR NULLIF([Target].[SourceControlLogin], [Source].[SourceControlLogin]) IS NOT NULL OR 
	NULLIF([Source].[SourceControlPassword], [Target].[SourceControlPassword]) IS NOT NULL OR NULLIF([Target].[SourceControlPassword], [Source].[SourceControlPassword]) IS NOT NULL OR 
	NULLIF([Source].[PasswordExpireDate], [Target].[PasswordExpireDate]) IS NOT NULL OR NULLIF([Target].[PasswordExpireDate], [Source].[PasswordExpireDate]) IS NOT NULL OR 
	NULLIF([Source].[HomePageId], [Target].[HomePageId]) IS NOT NULL OR NULLIF([Target].[HomePageId], [Source].[HomePageId]) IS NOT NULL OR 
	NULLIF([Source].[ConnectionType], [Target].[ConnectionType]) IS NOT NULL OR NULLIF([Target].[ConnectionType], [Source].[ConnectionType]) IS NOT NULL OR 
	NULLIF([Source].[UnblockTime], [Target].[UnblockTime]) IS NOT NULL OR NULLIF([Target].[UnblockTime], [Source].[UnblockTime]) IS NOT NULL OR 
	NULLIF([Source].[ForceChangePassword], [Target].[ForceChangePassword]) IS NOT NULL OR NULLIF([Target].[ForceChangePassword], [Source].[ForceChangePassword]) IS NOT NULL OR 
	NULLIF([Source].[LDAPElementId], [Target].[LDAPElementId]) IS NOT NULL OR NULLIF([Target].[LDAPElementId], [Source].[LDAPElementId]) IS NOT NULL OR 
	NULLIF([Source].[DateTimeFormatId], [Target].[DateTimeFormatId]) IS NOT NULL OR NULLIF([Target].[DateTimeFormatId], [Source].[DateTimeFormatId]) IS NOT NULL OR 
	NULLIF([Source].[SessionTimeout], [Target].[SessionTimeout]) IS NOT NULL OR NULLIF([Target].[SessionTimeout], [Source].[SessionTimeout]) IS NOT NULL OR 
	NULLIF([Source].[PortalAccountId], [Target].[PortalAccountId]) IS NOT NULL OR NULLIF([Target].[PortalAccountId], [Source].[PortalAccountId]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[CreatedOn] = [Source].[CreatedOn], 
  [Target].[CreatedById] = [Source].[CreatedById], 
  [Target].[ModifiedOn] = [Source].[ModifiedOn], 
  [Target].[ModifiedById] = [Source].[ModifiedById], 
  [Target].[Name] = [Source].[Name], 
  [Target].[Description] = [Source].[Description], 
  [Target].[ParentRoleId] = [Source].[ParentRoleId], 
  [Target].[ContactId] = [Source].[ContactId], 
  [Target].[TimeZoneId] = [Source].[TimeZoneId], 
  [Target].[UserPassword] = [Source].[UserPassword], 
  [Target].[SysAdminUnitTypeValue] = [Source].[SysAdminUnitTypeValue], 
  [Target].[AccountId] = [Source].[AccountId], 
  [Target].[Active] = [Source].[Active], 
  [Target].[LoggedIn] = [Source].[LoggedIn], 
  [Target].[SynchronizeWithLDAP] = [Source].[SynchronizeWithLDAP], 
  [Target].[LDAPEntry] = [Source].[LDAPEntry], 
  [Target].[LDAPEntryId] = [Source].[LDAPEntryId], 
  [Target].[LDAPEntryDN] = [Source].[LDAPEntryDN], 
  [Target].[IsDirectoryEntry] = [Source].[IsDirectoryEntry], 
  [Target].[ProcessListeners] = [Source].[ProcessListeners], 
  [Target].[SysCultureId] = [Source].[SysCultureId], 
  [Target].[LoginAttemptCount] = [Source].[LoginAttemptCount], 
  [Target].[SourceControlLogin] = [Source].[SourceControlLogin], 
  [Target].[SourceControlPassword] = [Source].[SourceControlPassword], 
  [Target].[PasswordExpireDate] = [Source].[PasswordExpireDate], 
  [Target].[HomePageId] = [Source].[HomePageId], 
  [Target].[ConnectionType] = [Source].[ConnectionType], 
  [Target].[UnblockTime] = [Source].[UnblockTime], 
  [Target].[ForceChangePassword] = [Source].[ForceChangePassword], 
  [Target].[LDAPElementId] = [Source].[LDAPElementId], 
  [Target].[DateTimeFormatId] = [Source].[DateTimeFormatId], 
  [Target].[SessionTimeout] = [Source].[SessionTimeout], 
  [Target].[PortalAccountId] = [Source].[PortalAccountId]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ParentRoleId],[ContactId],[TimeZoneId],[UserPassword],[SysAdminUnitTypeValue],[AccountId],[Active],[LoggedIn],[SynchronizeWithLDAP],[LDAPEntry],[LDAPEntryId],[LDAPEntryDN],[IsDirectoryEntry],[ProcessListeners],[SysCultureId],[LoginAttemptCount],[SourceControlLogin],[SourceControlPassword],[PasswordExpireDate],[HomePageId],[ConnectionType],[UnblockTime],[ForceChangePassword],[LDAPElementId],[DateTimeFormatId],[SessionTimeout],[PortalAccountId])
 VALUES([Source].[Id],[Source].[CreatedOn],[Source].[CreatedById],[Source].[ModifiedOn],[Source].[ModifiedById],[Source].[Name],[Source].[Description],[Source].[ParentRoleId],[Source].[ContactId],[Source].[TimeZoneId],[Source].[UserPassword],[Source].[SysAdminUnitTypeValue],[Source].[AccountId],[Source].[Active],[Source].[LoggedIn],[Source].[SynchronizeWithLDAP],[Source].[LDAPEntry],[Source].[LDAPEntryId],[Source].[LDAPEntryDN],[Source].[IsDirectoryEntry],[Source].[ProcessListeners],[Source].[SysCultureId],[Source].[LoginAttemptCount],[Source].[SourceControlLogin],[Source].[SourceControlPassword],[Source].[PasswordExpireDate],[Source].[HomePageId],[Source].[ConnectionType],[Source].[UnblockTime],[Source].[ForceChangePassword],[Source].[LDAPElementId],[Source].[DateTimeFormatId],[Source].[SessionTimeout],[Source].[PortalAccountId]);