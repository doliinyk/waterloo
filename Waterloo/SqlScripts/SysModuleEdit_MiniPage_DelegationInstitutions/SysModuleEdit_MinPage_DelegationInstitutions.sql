--Original Version Oct 25, 2020 @ 14:41 by Oliver
--exec sp_generate_merge 'SysModuleEdit',@from = 'from SysModuleEdit where SysModuleEntityId in (select Id from SysModuleEntity where SysEntitySchemaUid in (select Uid from SysSchema where Name = ''WatDelegationInstitutions''))'

MERGE INTO [SysModuleEdit] AS [Target]
USING (VALUES
  (N'5E593B41-C568-4027-8539-1D20E3844C2F','2020-09-01T22:21:57.738',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-09-02T15:28:22.384',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'0DE14FD2-39EF-499F-8AAC-EEB60A4DB567',NULL,1,0,N'',0,NULL,N'A1D9F02B-D974-4EC0-A920-D8AFFD3C4E2B',N'',N'WatDelegationInstitutions1Page',N'Edit page: "Delegation Institutions"',N'65696465-9124-4B85-80D8-FA5DADDD60E7',NULL,N'add;;')
) AS [Source] ([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[SysModuleEntityId],[TypeColumnValue],[UseModuleDetails],[Position],[HelpContextId],[ProcessListeners],[SysPageSchemaUId],[CardSchemaUId],[ActionKindCaption],[ActionKindName],[PageCaption],[MiniPageSchemaUId],[SearchRowSchemaUId],[MiniPageModes])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[CreatedOn], [Target].[CreatedOn]) IS NOT NULL OR NULLIF([Target].[CreatedOn], [Source].[CreatedOn]) IS NOT NULL OR 
	NULLIF([Source].[CreatedById], [Target].[CreatedById]) IS NOT NULL OR NULLIF([Target].[CreatedById], [Source].[CreatedById]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedOn], [Target].[ModifiedOn]) IS NOT NULL OR NULLIF([Target].[ModifiedOn], [Source].[ModifiedOn]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedById], [Target].[ModifiedById]) IS NOT NULL OR NULLIF([Target].[ModifiedById], [Source].[ModifiedById]) IS NOT NULL OR 
	NULLIF([Source].[SysModuleEntityId], [Target].[SysModuleEntityId]) IS NOT NULL OR NULLIF([Target].[SysModuleEntityId], [Source].[SysModuleEntityId]) IS NOT NULL OR 
	NULLIF([Source].[TypeColumnValue], [Target].[TypeColumnValue]) IS NOT NULL OR NULLIF([Target].[TypeColumnValue], [Source].[TypeColumnValue]) IS NOT NULL OR 
	NULLIF([Source].[UseModuleDetails], [Target].[UseModuleDetails]) IS NOT NULL OR NULLIF([Target].[UseModuleDetails], [Source].[UseModuleDetails]) IS NOT NULL OR 
	NULLIF([Source].[Position], [Target].[Position]) IS NOT NULL OR NULLIF([Target].[Position], [Source].[Position]) IS NOT NULL OR 
	NULLIF([Source].[HelpContextId], [Target].[HelpContextId]) IS NOT NULL OR NULLIF([Target].[HelpContextId], [Source].[HelpContextId]) IS NOT NULL OR 
	NULLIF([Source].[ProcessListeners], [Target].[ProcessListeners]) IS NOT NULL OR NULLIF([Target].[ProcessListeners], [Source].[ProcessListeners]) IS NOT NULL OR 
	NULLIF([Source].[SysPageSchemaUId], [Target].[SysPageSchemaUId]) IS NOT NULL OR NULLIF([Target].[SysPageSchemaUId], [Source].[SysPageSchemaUId]) IS NOT NULL OR 
	NULLIF([Source].[CardSchemaUId], [Target].[CardSchemaUId]) IS NOT NULL OR NULLIF([Target].[CardSchemaUId], [Source].[CardSchemaUId]) IS NOT NULL OR 
	NULLIF([Source].[ActionKindCaption], [Target].[ActionKindCaption]) IS NOT NULL OR NULLIF([Target].[ActionKindCaption], [Source].[ActionKindCaption]) IS NOT NULL OR 
	NULLIF([Source].[ActionKindName], [Target].[ActionKindName]) IS NOT NULL OR NULLIF([Target].[ActionKindName], [Source].[ActionKindName]) IS NOT NULL OR 
	NULLIF([Source].[PageCaption], [Target].[PageCaption]) IS NOT NULL OR NULLIF([Target].[PageCaption], [Source].[PageCaption]) IS NOT NULL OR 
	NULLIF([Source].[MiniPageSchemaUId], [Target].[MiniPageSchemaUId]) IS NOT NULL OR NULLIF([Target].[MiniPageSchemaUId], [Source].[MiniPageSchemaUId]) IS NOT NULL OR 
	NULLIF([Source].[SearchRowSchemaUId], [Target].[SearchRowSchemaUId]) IS NOT NULL OR NULLIF([Target].[SearchRowSchemaUId], [Source].[SearchRowSchemaUId]) IS NOT NULL OR 
	NULLIF([Source].[MiniPageModes], [Target].[MiniPageModes]) IS NOT NULL OR NULLIF([Target].[MiniPageModes], [Source].[MiniPageModes]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[CreatedOn] = [Source].[CreatedOn], 
  [Target].[CreatedById] = [Source].[CreatedById], 
  [Target].[ModifiedOn] = [Source].[ModifiedOn], 
  [Target].[ModifiedById] = [Source].[ModifiedById], 
  [Target].[SysModuleEntityId] = [Source].[SysModuleEntityId], 
  [Target].[TypeColumnValue] = [Source].[TypeColumnValue], 
  [Target].[UseModuleDetails] = [Source].[UseModuleDetails], 
  [Target].[Position] = [Source].[Position], 
  [Target].[HelpContextId] = [Source].[HelpContextId], 
  [Target].[ProcessListeners] = [Source].[ProcessListeners], 
  [Target].[SysPageSchemaUId] = [Source].[SysPageSchemaUId], 
  [Target].[CardSchemaUId] = [Source].[CardSchemaUId], 
  [Target].[ActionKindCaption] = [Source].[ActionKindCaption], 
  [Target].[ActionKindName] = [Source].[ActionKindName], 
  [Target].[PageCaption] = [Source].[PageCaption], 
  [Target].[MiniPageSchemaUId] = [Source].[MiniPageSchemaUId], 
  [Target].[SearchRowSchemaUId] = [Source].[SearchRowSchemaUId], 
  [Target].[MiniPageModes] = [Source].[MiniPageModes]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[SysModuleEntityId],[TypeColumnValue],[UseModuleDetails],[Position],[HelpContextId],[ProcessListeners],[SysPageSchemaUId],[CardSchemaUId],[ActionKindCaption],[ActionKindName],[PageCaption],[MiniPageSchemaUId],[SearchRowSchemaUId],[MiniPageModes])
 VALUES([Source].[Id],[Source].[CreatedOn],[Source].[CreatedById],[Source].[ModifiedOn],[Source].[ModifiedById],[Source].[SysModuleEntityId],[Source].[TypeColumnValue],[Source].[UseModuleDetails],[Source].[Position],[Source].[HelpContextId],[Source].[ProcessListeners],[Source].[SysPageSchemaUId],[Source].[CardSchemaUId],[Source].[ActionKindCaption],[Source].[ActionKindName],[Source].[PageCaption],[Source].[MiniPageSchemaUId],[Source].[SearchRowSchemaUId],[Source].[MiniPageModes]);
