--Version Aug 31, 2020 @ 13:00 by Kavian
--EXEC SP_GENERATE_MERGE 'WatRankingType'

MERGE INTO [WatRankingType] AS [Target]
USING (VALUES
  (N'73F4FC44-0AB0-4CE5-A7B1-3E4C6FA8A441','2020-08-31T16:20:05.7231613',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-08-31T16:20:05.7231613',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'QS Top Universities',N'',0)
 ,(N'193B0F3E-2D3C-4BEF-BC2E-4247C44D3B47','2020-08-31T16:20:18.7459711',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-08-31T16:20:18.7459711',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'ARWU (Shanghai Rankings)',N'',0)
 ,(N'E82F904E-D292-4F5F-8452-89675DCEFC7C','2020-08-31T16:20:29.1238706',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-08-31T16:20:29.1238706',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Ranking Web of Universities',N'',0)
 ,(N'16F3C71C-4D2C-4BDD-A58E-CED5566C7E0E','2020-08-31T16:20:12.6283014',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-08-31T16:20:12.6283014',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Times Higher Education',N'',0)
) AS [Source] ([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[CreatedOn], [Target].[CreatedOn]) IS NOT NULL OR NULLIF([Target].[CreatedOn], [Source].[CreatedOn]) IS NOT NULL OR 
	NULLIF([Source].[CreatedById], [Target].[CreatedById]) IS NOT NULL OR NULLIF([Target].[CreatedById], [Source].[CreatedById]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedOn], [Target].[ModifiedOn]) IS NOT NULL OR NULLIF([Target].[ModifiedOn], [Source].[ModifiedOn]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedById], [Target].[ModifiedById]) IS NOT NULL OR NULLIF([Target].[ModifiedById], [Source].[ModifiedById]) IS NOT NULL OR 
	NULLIF([Source].[Name], [Target].[Name]) IS NOT NULL OR NULLIF([Target].[Name], [Source].[Name]) IS NOT NULL OR 
	NULLIF([Source].[Description], [Target].[Description]) IS NOT NULL OR NULLIF([Target].[Description], [Source].[Description]) IS NOT NULL OR 
	NULLIF([Source].[ProcessListeners], [Target].[ProcessListeners]) IS NOT NULL OR NULLIF([Target].[ProcessListeners], [Source].[ProcessListeners]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[CreatedOn] = [Source].[CreatedOn], 
  [Target].[CreatedById] = [Source].[CreatedById], 
  [Target].[ModifiedOn] = [Source].[ModifiedOn], 
  [Target].[ModifiedById] = [Source].[ModifiedById], 
  [Target].[Name] = [Source].[Name], 
  [Target].[Description] = [Source].[Description], 
  [Target].[ProcessListeners] = [Source].[ProcessListeners]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
 VALUES([Source].[Id],[Source].[CreatedOn],[Source].[CreatedById],[Source].[ModifiedOn],[Source].[ModifiedById],[Source].[Name],[Source].[Description],[Source].[ProcessListeners]);