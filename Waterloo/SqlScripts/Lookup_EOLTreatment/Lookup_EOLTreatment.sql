--Version Sep 16, 2020 @ 14:35 by Kavian
--EXEC SP_GENERATE_MERGE 'WatEOLTreatment'

MERGE INTO [WatEOLTreatment] AS [Target]
USING (VALUES
  (N'A6370A72-51BF-455C-A618-0F8B90BE8576','2020-09-16T15:55:25.5449794',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-11-20T19:48:23.9999659',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Obsolete',N'',0)
 ,(N'7E01B7F5-2714-4CF3-811E-14EF906EAE14','2020-09-16T15:55:17.2182936',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-11-20T19:48:14.2025396',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Will Conclude at Term End',N'',0)
 ,(N'1F96AD10-B6E4-4693-990B-55F8C21DF3E8','2020-09-16T15:55:33.7637212',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-11-20T19:47:58.2641440',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Assessment for Renewal',N'',0)
 ,(N'7978DDFE-09E4-42DD-BEEB-76CEF0F20B12','2020-09-16T15:55:11.0329813',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-11-20T19:47:19.2137476',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Auto-Renewal (Same Term)',N'',0)
 ,(N'BAB57A00-FA8E-4331-82A4-FB0D41B51BD6','2020-11-20T19:45:12.9541007',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-11-20T19:45:12.9541007',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Auto-Renewal (Different Term)',N'',0)
) AS [Source] ([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[CreatedOn], [Target].[CreatedOn]) IS NOT NULL OR NULLIF([Target].[CreatedOn], [Source].[CreatedOn]) IS NOT NULL OR 
	NULLIF([Source].[CreatedById], [Target].[CreatedById]) IS NOT NULL OR NULLIF([Target].[CreatedById], [Source].[CreatedById]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedOn], [Target].[ModifiedOn]) IS NOT NULL OR NULLIF([Target].[ModifiedOn], [Source].[ModifiedOn]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedById], [Target].[ModifiedById]) IS NOT NULL OR NULLIF([Target].[ModifiedById], [Source].[ModifiedById]) IS NOT NULL OR 
	NULLIF([Source].[Name], [Target].[Name]) IS NOT NULL OR NULLIF([Target].[Name], [Source].[Name]) IS NOT NULL OR 
	NULLIF([Source].[Description], [Target].[Description]) IS NOT NULL OR NULLIF([Target].[Description], [Source].[Description]) IS NOT NULL OR 
	NULLIF([Source].[ProcessListeners], [Target].[ProcessListeners]) IS NOT NULL OR NULLIF([Target].[ProcessListeners], [Source].[ProcessListeners]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[CreatedOn] = [Source].[CreatedOn], 
  [Target].[CreatedById] = [Source].[CreatedById], 
  [Target].[ModifiedOn] = [Source].[ModifiedOn], 
  [Target].[ModifiedById] = [Source].[ModifiedById], 
  [Target].[Name] = [Source].[Name], 
  [Target].[Description] = [Source].[Description], 
  [Target].[ProcessListeners] = [Source].[ProcessListeners]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
 VALUES([Source].[Id],[Source].[CreatedOn],[Source].[CreatedById],[Source].[ModifiedOn],[Source].[ModifiedById],[Source].[Name],[Source].[Description],[Source].[ProcessListeners]);
