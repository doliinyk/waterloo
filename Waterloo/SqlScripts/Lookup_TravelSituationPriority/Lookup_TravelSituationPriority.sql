--Version Nov 6, 2020 by Kavian
--EXEC SP_GENERATE_MERGE 'WatTravelSituationPriority'

MERGE INTO [WatTravelSituationPriority] AS [Target]
USING (VALUES
  (N'D9F57F78-2B64-4E27-81D9-1BB17E3EDCC2','2020-11-09T02:07:00.1059317',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-11-09T02:07:00.1059317',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Low',N'',0)
 ,(N'6A961812-7A2A-4539-88B2-6BF408194887','2020-11-09T02:06:54.5096510',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-11-09T02:06:54.5096510',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Medium',N'',0)
 ,(N'3C7C01D0-B449-499B-82DC-ED08393C61B9','2020-11-09T02:06:50.6502141',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-11-09T02:06:50.6502141',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'High',N'',0)
) AS [Source] ([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[CreatedOn], [Target].[CreatedOn]) IS NOT NULL OR NULLIF([Target].[CreatedOn], [Source].[CreatedOn]) IS NOT NULL OR 
	NULLIF([Source].[CreatedById], [Target].[CreatedById]) IS NOT NULL OR NULLIF([Target].[CreatedById], [Source].[CreatedById]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedOn], [Target].[ModifiedOn]) IS NOT NULL OR NULLIF([Target].[ModifiedOn], [Source].[ModifiedOn]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedById], [Target].[ModifiedById]) IS NOT NULL OR NULLIF([Target].[ModifiedById], [Source].[ModifiedById]) IS NOT NULL OR 
	NULLIF([Source].[Name], [Target].[Name]) IS NOT NULL OR NULLIF([Target].[Name], [Source].[Name]) IS NOT NULL OR 
	NULLIF([Source].[Description], [Target].[Description]) IS NOT NULL OR NULLIF([Target].[Description], [Source].[Description]) IS NOT NULL OR 
	NULLIF([Source].[ProcessListeners], [Target].[ProcessListeners]) IS NOT NULL OR NULLIF([Target].[ProcessListeners], [Source].[ProcessListeners]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[CreatedOn] = [Source].[CreatedOn], 
  [Target].[CreatedById] = [Source].[CreatedById], 
  [Target].[ModifiedOn] = [Source].[ModifiedOn], 
  [Target].[ModifiedById] = [Source].[ModifiedById], 
  [Target].[Name] = [Source].[Name], 
  [Target].[Description] = [Source].[Description], 
  [Target].[ProcessListeners] = [Source].[ProcessListeners]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
 VALUES([Source].[Id],[Source].[CreatedOn],[Source].[CreatedById],[Source].[ModifiedOn],[Source].[ModifiedById],[Source].[Name],[Source].[Description],[Source].[ProcessListeners]);