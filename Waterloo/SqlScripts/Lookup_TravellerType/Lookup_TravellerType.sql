--Version Apr 28, 2021 @ 17:15 by OS (adding Co-op)
--Version Feb 10, 2021 @ 21:13 by OS (Split Faculty and Staff)
--Version Oct 15, 2020 by Kavian
--EXEC SP_GENERATE_MERGE 'WatTravelerType'

MERGE INTO [WatTravelerType] AS [Target]
USING (VALUES
  (N'CB2B6DED-E650-4071-8FD9-4B6476BF0BD1','2021-04-28T21:14:31.3132317',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2021-04-28T21:14:31.3132317',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Co-op',N'',0)
 ,(N'C4F2BBD7-2441-4923-BF9A-5A067501168D','2020-10-15T14:05:57.3119845',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2021-02-10T20:56:28.4904087',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Faculty Individual Travel',N'',0)
 ,(N'D48C64EB-488F-46C5-BA45-C564645F5E82','2021-02-10T20:56:32.4280935',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2021-02-10T20:56:32.4280935',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Staff Individual Travel',N'',0)
 ,(N'3EC633E9-6646-4D08-A0BD-DBB211CD5711','2020-10-15T14:06:05.5538370',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-10-15T14:06:05.5538370',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Faculty/Staff Group Travel',N'',0)
 ,(N'FC07D4AD-98B0-4865-9292-FCFC564D2E91','2020-10-15T14:05:49.5671191',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-10-15T14:05:49.5671191',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Student',N'',0)
) AS [Source] ([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[CreatedOn], [Target].[CreatedOn]) IS NOT NULL OR NULLIF([Target].[CreatedOn], [Source].[CreatedOn]) IS NOT NULL OR 
	NULLIF([Source].[CreatedById], [Target].[CreatedById]) IS NOT NULL OR NULLIF([Target].[CreatedById], [Source].[CreatedById]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedOn], [Target].[ModifiedOn]) IS NOT NULL OR NULLIF([Target].[ModifiedOn], [Source].[ModifiedOn]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedById], [Target].[ModifiedById]) IS NOT NULL OR NULLIF([Target].[ModifiedById], [Source].[ModifiedById]) IS NOT NULL OR 
	NULLIF([Source].[Name], [Target].[Name]) IS NOT NULL OR NULLIF([Target].[Name], [Source].[Name]) IS NOT NULL OR 
	NULLIF([Source].[Description], [Target].[Description]) IS NOT NULL OR NULLIF([Target].[Description], [Source].[Description]) IS NOT NULL OR 
	NULLIF([Source].[ProcessListeners], [Target].[ProcessListeners]) IS NOT NULL OR NULLIF([Target].[ProcessListeners], [Source].[ProcessListeners]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[CreatedOn] = [Source].[CreatedOn], 
  [Target].[CreatedById] = [Source].[CreatedById], 
  [Target].[ModifiedOn] = [Source].[ModifiedOn], 
  [Target].[ModifiedById] = [Source].[ModifiedById], 
  [Target].[Name] = [Source].[Name], 
  [Target].[Description] = [Source].[Description], 
  [Target].[ProcessListeners] = [Source].[ProcessListeners]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
 VALUES([Source].[Id],[Source].[CreatedOn],[Source].[CreatedById],[Source].[ModifiedOn],[Source].[ModifiedById],[Source].[Name],[Source].[Description],[Source].[ProcessListeners]);
