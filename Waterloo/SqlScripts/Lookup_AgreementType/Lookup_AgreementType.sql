--Version Sep 17, 2020 @ 9:45 by Kavian (Origin: Kavian)
--EXEC SP_GENERATE_MERGE 'WatAgreementType'
-- Add: Nest WatAgreementClassification

MERGE INTO [WatAgreementType] AS [Target]
USING (VALUES
  (N'22F47E79-2F60-404A-A4DE-20EE3E9834FB','2020-09-16T15:51:48.6879910',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-11-25T19:17:25.9201595',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Joint Academic',N'',0)
 ,(N'2F6032A4-58AE-470E-967D-2AF7578165F5','2020-11-25T19:54:12.8177730',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-11-25T19:54:12.8177730',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Other',N'',0)
 ,(N'00B95A4B-A60B-419F-86A9-47EE62DC5374','2020-09-16T15:52:14.7199444',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-11-25T19:17:27.2483713',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Research Partnership',N'',0)
 ,(N'AB7596E6-5575-454D-BEF6-57BE8FA484BD','2020-09-16T15:52:25.2955270',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-11-25T19:17:28.4988490',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Strategic Partnership',N'',0)
 ,(N'14D10E70-49A9-43A7-B83F-658549383B36','2020-09-16T15:52:02.3210386',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-11-25T19:17:29.6236259',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Pathways',N'',0)
 ,(N'16257282-5967-4B4E-AB6C-9805CF300FDB','2020-09-16T15:51:56.1828555',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-11-25T19:17:35.7351598',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'MOU',N'',0)
 ,(N'9DA17D88-7F99-4CAB-A609-9928C0A69261','2020-09-16T15:52:37.4875025',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-11-25T19:17:38.6416283',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Student Mobility',N'',0)
) AS [Source] ([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[CreatedOn], [Target].[CreatedOn]) IS NOT NULL OR NULLIF([Target].[CreatedOn], [Source].[CreatedOn]) IS NOT NULL OR 
	NULLIF([Source].[CreatedById], [Target].[CreatedById]) IS NOT NULL OR NULLIF([Target].[CreatedById], [Source].[CreatedById]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedOn], [Target].[ModifiedOn]) IS NOT NULL OR NULLIF([Target].[ModifiedOn], [Source].[ModifiedOn]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedById], [Target].[ModifiedById]) IS NOT NULL OR NULLIF([Target].[ModifiedById], [Source].[ModifiedById]) IS NOT NULL OR 
	NULLIF([Source].[Name], [Target].[Name]) IS NOT NULL OR NULLIF([Target].[Name], [Source].[Name]) IS NOT NULL OR 
	NULLIF([Source].[Description], [Target].[Description]) IS NOT NULL OR NULLIF([Target].[Description], [Source].[Description]) IS NOT NULL OR 
	NULLIF([Source].[ProcessListeners], [Target].[ProcessListeners]) IS NOT NULL OR NULLIF([Target].[ProcessListeners], [Source].[ProcessListeners]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[CreatedOn] = [Source].[CreatedOn], 
  [Target].[CreatedById] = [Source].[CreatedById], 
  [Target].[ModifiedOn] = [Source].[ModifiedOn], 
  [Target].[ModifiedById] = [Source].[ModifiedById], 
  [Target].[Name] = [Source].[Name], 
  [Target].[Description] = [Source].[Description], 
  [Target].[ProcessListeners] = [Source].[ProcessListeners]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
 VALUES([Source].[Id],[Source].[CreatedOn],[Source].[CreatedById],[Source].[ModifiedOn],[Source].[ModifiedById],[Source].[Name],[Source].[Description],[Source].[ProcessListeners]);
