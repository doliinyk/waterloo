--Version Sep 8, 2020 @ 11:24 by Oliver
--EXEC SP_GENERATE_MERGE 'WatHospitalityRequestStatus'

MERGE INTO [WatHospitalityRequestStatus] AS [Target]
USING (VALUES
  (N'9B157A42-B3F9-4430-94F4-1337A96B5451','2020-09-06T16:13:09.8489895',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-09-06T16:13:43.9478690',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Impossible to provide',N'',0)
 ,(N'724F0C3C-90F7-4D05-BDCE-1A33761DA7FA','2020-09-06T16:12:36.5276325',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-09-06T16:12:36.5276325',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Confirmed',N'',0)
 ,(N'EE30B465-2B83-4D12-8F68-2383FF26669E','2020-09-06T16:11:35.1191103',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-09-06T16:11:35.1191103',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'New',N'',0)
 ,(N'D64E17A7-C754-41E9-A363-69746CF7ED01','2020-09-06T16:12:39.8954495',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-09-06T16:12:39.8954495',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Cancelled',N'',0)
 ,(N'B017E847-08AE-4142-B7AB-D60445F80092','2020-09-06T16:12:29.9147864',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-09-06T16:12:29.9147864',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Pending Confirmation',N'',0)
) AS [Source] ([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[CreatedOn], [Target].[CreatedOn]) IS NOT NULL OR NULLIF([Target].[CreatedOn], [Source].[CreatedOn]) IS NOT NULL OR 
	NULLIF([Source].[CreatedById], [Target].[CreatedById]) IS NOT NULL OR NULLIF([Target].[CreatedById], [Source].[CreatedById]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedOn], [Target].[ModifiedOn]) IS NOT NULL OR NULLIF([Target].[ModifiedOn], [Source].[ModifiedOn]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedById], [Target].[ModifiedById]) IS NOT NULL OR NULLIF([Target].[ModifiedById], [Source].[ModifiedById]) IS NOT NULL OR 
	NULLIF([Source].[Name], [Target].[Name]) IS NOT NULL OR NULLIF([Target].[Name], [Source].[Name]) IS NOT NULL OR 
	NULLIF([Source].[Description], [Target].[Description]) IS NOT NULL OR NULLIF([Target].[Description], [Source].[Description]) IS NOT NULL OR 
	NULLIF([Source].[ProcessListeners], [Target].[ProcessListeners]) IS NOT NULL OR NULLIF([Target].[ProcessListeners], [Source].[ProcessListeners]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[CreatedOn] = [Source].[CreatedOn], 
  [Target].[CreatedById] = [Source].[CreatedById], 
  [Target].[ModifiedOn] = [Source].[ModifiedOn], 
  [Target].[ModifiedById] = [Source].[ModifiedById], 
  [Target].[Name] = [Source].[Name], 
  [Target].[Description] = [Source].[Description], 
  [Target].[ProcessListeners] = [Source].[ProcessListeners]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
 VALUES([Source].[Id],[Source].[CreatedOn],[Source].[CreatedById],[Source].[ModifiedOn],[Source].[ModifiedById],[Source].[Name],[Source].[Description],[Source].[ProcessListeners]);
