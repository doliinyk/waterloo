--Version Aug 31, 2020 @ 12:00 by Kavian
--EXEC SP_GENERATE_MERGE 'WatDelegationPurposeLookup'

MERGE INTO [WatDelegationPurposeLookup] AS [Target]
USING (VALUES
  (N'83DC6A70-2B88-4E1D-9266-266C9DB90285','2020-08-31T15:44:11.9845667',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-08-31T15:44:11.9845667',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Teaching',N'',0)
 ,(N'A09060EE-55F1-4608-B98E-49E97E249896','2020-08-31T15:43:58.3308600',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-08-31T15:43:58.3308600',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Technology Transfer',N'',0)
 ,(N'72441975-4999-4E1C-A6F2-5A2F9282E59C','2020-08-31T15:44:04.9491397',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-08-31T15:44:04.9491397',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Scholarships',N'',0)
 ,(N'66589403-962D-47FB-A931-716316F7A081','2020-08-31T15:43:31.3665755',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-08-31T15:43:31.3665755',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Student Mobility',N'',0)
 ,(N'44CABFC2-DCA0-43F0-98A7-7F8CDA3EDBBA','2020-08-31T15:43:39.9937107',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-08-31T15:43:39.9937107',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Academic/Joint Programs',N'',0)
 ,(N'88662B71-8FAA-4561-8B6D-8943B9D5A0D0','2020-08-31T15:43:04.3012269',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-08-31T15:43:04.3012269',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Experiential Education (Co-op)',N'',0)
 ,(N'68AED819-FC3A-4740-B578-C9E8ADED2C3A','2020-08-31T15:43:23.9552611',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-08-31T15:43:23.9552611',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Research (Specify field)',N'',0)
 ,(N'BFF03822-92FC-4876-821F-DBBE75640C51','2020-08-31T15:43:10.5180365',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-08-31T15:43:10.5180365',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Entrepreneurship',N'',0)
 ,(N'645F500A-315A-454B-9024-E44485349918','2020-08-31T15:43:45.0359905',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-08-31T15:43:45.0359905',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Innovation',N'',0)
 ,(N'7F4BC78F-5376-4292-BDEC-E44C5CD34930','2020-08-31T15:43:51.1729552',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-08-31T15:43:51.1729552',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Intellectual Property',N'',0)
) AS [Source] ([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[CreatedOn], [Target].[CreatedOn]) IS NOT NULL OR NULLIF([Target].[CreatedOn], [Source].[CreatedOn]) IS NOT NULL OR 
	NULLIF([Source].[CreatedById], [Target].[CreatedById]) IS NOT NULL OR NULLIF([Target].[CreatedById], [Source].[CreatedById]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedOn], [Target].[ModifiedOn]) IS NOT NULL OR NULLIF([Target].[ModifiedOn], [Source].[ModifiedOn]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedById], [Target].[ModifiedById]) IS NOT NULL OR NULLIF([Target].[ModifiedById], [Source].[ModifiedById]) IS NOT NULL OR 
	NULLIF([Source].[Name], [Target].[Name]) IS NOT NULL OR NULLIF([Target].[Name], [Source].[Name]) IS NOT NULL OR 
	NULLIF([Source].[Description], [Target].[Description]) IS NOT NULL OR NULLIF([Target].[Description], [Source].[Description]) IS NOT NULL OR 
	NULLIF([Source].[ProcessListeners], [Target].[ProcessListeners]) IS NOT NULL OR NULLIF([Target].[ProcessListeners], [Source].[ProcessListeners]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[CreatedOn] = [Source].[CreatedOn], 
  [Target].[CreatedById] = [Source].[CreatedById], 
  [Target].[ModifiedOn] = [Source].[ModifiedOn], 
  [Target].[ModifiedById] = [Source].[ModifiedById], 
  [Target].[Name] = [Source].[Name], 
  [Target].[Description] = [Source].[Description], 
  [Target].[ProcessListeners] = [Source].[ProcessListeners]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
 VALUES([Source].[Id],[Source].[CreatedOn],[Source].[CreatedById],[Source].[ModifiedOn],[Source].[ModifiedById],[Source].[Name],[Source].[Description],[Source].[ProcessListeners]);