--Version Oct 15, 2020 by Kavian
--EXEC SP_GENERATE_MERGE 'WatTripStatus'

MERGE INTO [WatTripStatus] AS [Target]
USING (VALUES
  (N'AF64333D-9155-40F3-8CDB-0A1F3BC0B464','2020-10-15T14:09:39.0663462',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-10-15T14:09:39.0663462',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Exception',N'',0)
 ,(N'B7D06369-6B70-4828-AE94-B07FB32AA654','2020-10-15T14:08:43.8640961',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-10-15T14:08:43.8640961',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Normal',N'',0)
 ,(N'A17128F2-D117-4C33-97AF-DF550701D42F','2020-10-15T14:09:29.9527587',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-10-15T14:09:29.9527587',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Cancelled',N'',0)
) AS [Source] ([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[CreatedOn], [Target].[CreatedOn]) IS NOT NULL OR NULLIF([Target].[CreatedOn], [Source].[CreatedOn]) IS NOT NULL OR 
	NULLIF([Source].[CreatedById], [Target].[CreatedById]) IS NOT NULL OR NULLIF([Target].[CreatedById], [Source].[CreatedById]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedOn], [Target].[ModifiedOn]) IS NOT NULL OR NULLIF([Target].[ModifiedOn], [Source].[ModifiedOn]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedById], [Target].[ModifiedById]) IS NOT NULL OR NULLIF([Target].[ModifiedById], [Source].[ModifiedById]) IS NOT NULL OR 
	NULLIF([Source].[Name], [Target].[Name]) IS NOT NULL OR NULLIF([Target].[Name], [Source].[Name]) IS NOT NULL OR 
	NULLIF([Source].[Description], [Target].[Description]) IS NOT NULL OR NULLIF([Target].[Description], [Source].[Description]) IS NOT NULL OR 
	NULLIF([Source].[ProcessListeners], [Target].[ProcessListeners]) IS NOT NULL OR NULLIF([Target].[ProcessListeners], [Source].[ProcessListeners]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[CreatedOn] = [Source].[CreatedOn], 
  [Target].[CreatedById] = [Source].[CreatedById], 
  [Target].[ModifiedOn] = [Source].[ModifiedOn], 
  [Target].[ModifiedById] = [Source].[ModifiedById], 
  [Target].[Name] = [Source].[Name], 
  [Target].[Description] = [Source].[Description], 
  [Target].[ProcessListeners] = [Source].[ProcessListeners]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
 VALUES([Source].[Id],[Source].[CreatedOn],[Source].[CreatedById],[Source].[ModifiedOn],[Source].[ModifiedById],[Source].[Name],[Source].[Description],[Source].[ProcessListeners]);