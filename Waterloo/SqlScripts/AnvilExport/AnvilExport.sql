if object_id('AnvilExport') is not null
begin
	drop procedure AnvilExport
end
go

create procedure AnvilExport @AnvilExportId uniqueidentifier
as
begin

	declare @TravelerId uniqueidentifier = '00000000-0000-0000-0000-000000000000'
	declare @TripId uniqueidentifier

	declare @ReportGenerated bit = 0
	
	insert into WatAnvilExport (Id, WatName, WatStatus)
	select @AnvilExportId, 'Anvil Export', 'In Progress'

	while(1=1)
	begin
	
		select @TravelerId = min(Id) 
		from WatTraveler
		where Id > @TravelerId
			and WatAnvilReady = 1
			and WatAnvilExportId is null

		if @TravelerId is null
			break;

		select @TripId = WatTripId from WatTraveler where Id = @TravelerId

		if OBJECT_ID('tempdb..#temp') is null
		begin
			select * into #temp from dbo.GetAnvilTravelers(@TravelerId, @TripId)
		end
		else
		begin
			insert into #temp
			select * from dbo.GetAnvilTravelers(@TravelerId, @TripId)
		end

		update WatTraveler
			set WatAnvilExportId = @AnvilExportId
		where Id = @TravelerId

	end

	select * from #temp
	drop table #temp
end