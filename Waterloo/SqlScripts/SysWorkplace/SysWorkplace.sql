--Version Oct 25, 2020 @ 20:01 by Oliver (Added Travel Safety)
--Version Oct 25, 2020 @ 13:50 AM by Oliver (Added "Admin" workplace)
--Version Oct 9, 2020 @ 10:45 AM by Kavian (Added "Agreements" workplace)
--Original Version Sep 16, 2020 @ 17:28 by Oliver

--exec sp_generate_merge 'SysWorkplace', @from = 'from SysWorkplace where Name in (''Admin'',''Delegations'',''Agreements'')'

MERGE INTO [SysWorkplace] AS [Target]
USING (VALUES
  (N'0BAE043F-E729-4809-84DD-03F75C1EBBA6','2020-10-09T14:30:11.041',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-10-09T14:30:11.041',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',0,5,N'Delegations',0,N'3707A058-E6A8-4D2C-99CC-D01D9E6B70C6',N'195785B4-F55A-4E72-ACE3-6480B54C8FA5',N'000A9225-728B-4778-A951-C42439FFE024')
 ,(N'F3EC7EC0-AE93-4488-B97A-4DB29361791E','2020-10-25T17:41:10.270',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-10-25T17:41:10.270',N'EEFDE995-2248-41CE-9575-B7529329E8DF',0,7,N'Admin',0,N'3707A058-E6A8-4D2C-99CC-D01D9E6B70C6',N'195785B4-F55A-4E72-ACE3-6480B54C8FA5',N'000A9225-728B-4778-A951-C42439FFE024')
 ,(N'6BCB6E88-E2AB-4B74-BBB5-782A98418BB7','2020-10-09T14:32:13.412',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-10-09T14:32:13.412',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',0,6,N'Agreements',0,N'3707A058-E6A8-4D2C-99CC-D01D9E6B70C6',N'195785B4-F55A-4E72-ACE3-6480B54C8FA5',N'000A9225-728B-4778-A951-C42439FFE024')
 ,(N'E3CC9683-1C7A-48C3-8E02-81529343B761','2020-10-25T23:59:20.632',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-10-25T23:59:20.632',N'EEFDE995-2248-41CE-9575-B7529329E8DF',0,8,N'Travel Safety',0,N'3707A058-E6A8-4D2C-99CC-D01D9E6B70C6',N'195785B4-F55A-4E72-ACE3-6480B54C8FA5',N'000A9225-728B-4778-A951-C42439FFE024')
) AS [Source] ([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[ProcessListeners],[Position],[Name],[IsPersonal],[LoaderId],[SysApplicationClientTypeId],[TypeId])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[CreatedOn], [Target].[CreatedOn]) IS NOT NULL OR NULLIF([Target].[CreatedOn], [Source].[CreatedOn]) IS NOT NULL OR 
	NULLIF([Source].[CreatedById], [Target].[CreatedById]) IS NOT NULL OR NULLIF([Target].[CreatedById], [Source].[CreatedById]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedOn], [Target].[ModifiedOn]) IS NOT NULL OR NULLIF([Target].[ModifiedOn], [Source].[ModifiedOn]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedById], [Target].[ModifiedById]) IS NOT NULL OR NULLIF([Target].[ModifiedById], [Source].[ModifiedById]) IS NOT NULL OR 
	NULLIF([Source].[ProcessListeners], [Target].[ProcessListeners]) IS NOT NULL OR NULLIF([Target].[ProcessListeners], [Source].[ProcessListeners]) IS NOT NULL OR 
	NULLIF([Source].[Position], [Target].[Position]) IS NOT NULL OR NULLIF([Target].[Position], [Source].[Position]) IS NOT NULL OR 
	NULLIF([Source].[Name], [Target].[Name]) IS NOT NULL OR NULLIF([Target].[Name], [Source].[Name]) IS NOT NULL OR 
	NULLIF([Source].[IsPersonal], [Target].[IsPersonal]) IS NOT NULL OR NULLIF([Target].[IsPersonal], [Source].[IsPersonal]) IS NOT NULL OR 
	NULLIF([Source].[LoaderId], [Target].[LoaderId]) IS NOT NULL OR NULLIF([Target].[LoaderId], [Source].[LoaderId]) IS NOT NULL OR 
	NULLIF([Source].[SysApplicationClientTypeId], [Target].[SysApplicationClientTypeId]) IS NOT NULL OR NULLIF([Target].[SysApplicationClientTypeId], [Source].[SysApplicationClientTypeId]) IS NOT NULL OR 
	NULLIF([Source].[TypeId], [Target].[TypeId]) IS NOT NULL OR NULLIF([Target].[TypeId], [Source].[TypeId]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[CreatedOn] = [Source].[CreatedOn], 
  [Target].[CreatedById] = [Source].[CreatedById], 
  [Target].[ModifiedOn] = [Source].[ModifiedOn], 
  [Target].[ModifiedById] = [Source].[ModifiedById], 
  [Target].[ProcessListeners] = [Source].[ProcessListeners], 
  [Target].[Position] = [Source].[Position], 
  [Target].[Name] = [Source].[Name], 
  [Target].[IsPersonal] = [Source].[IsPersonal], 
  [Target].[LoaderId] = [Source].[LoaderId], 
  [Target].[SysApplicationClientTypeId] = [Source].[SysApplicationClientTypeId], 
  [Target].[TypeId] = [Source].[TypeId]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[ProcessListeners],[Position],[Name],[IsPersonal],[LoaderId],[SysApplicationClientTypeId],[TypeId])
 VALUES([Source].[Id],[Source].[CreatedOn],[Source].[CreatedById],[Source].[ModifiedOn],[Source].[ModifiedById],[Source].[ProcessListeners],[Source].[Position],[Source].[Name],[Source].[IsPersonal],[Source].[LoaderId],[Source].[SysApplicationClientTypeId],[Source].[TypeId]);
