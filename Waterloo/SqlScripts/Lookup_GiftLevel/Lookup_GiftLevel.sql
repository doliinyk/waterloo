--Version Aug 28, 2020 @ 16:55 by Oliver
--EXEC SP_GENERATE_MERGE 'WatGiftLevel'

MERGE INTO [WatGiftLevel] AS [Target]
USING (VALUES
  (N'71BE2691-2397-4D55-922B-1BB722DA439A','2020-08-28T17:42:13.8903131',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-08-28T17:42:13.8903131',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'D - Bronze',N'',0)
 ,(N'AC3BCB5A-1313-4491-99D1-7C9C7D820271','2020-08-28T17:42:04.7184949',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-08-28T17:42:04.7184949',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'C - Silver',N'',0)
 ,(N'B5E73CCE-E241-42FE-BD63-9751AE7FD626','2020-08-28T17:41:51.3645970',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-08-28T17:41:51.3645970',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'A - Platinum',N'',0)
 ,(N'833CEF00-C42C-444E-B412-A777A44F4624','2020-08-28T17:41:56.6410841',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-08-28T17:41:56.6410841',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'B - Gold',N'',0)
) AS [Source] ([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[CreatedOn], [Target].[CreatedOn]) IS NOT NULL OR NULLIF([Target].[CreatedOn], [Source].[CreatedOn]) IS NOT NULL OR 
	NULLIF([Source].[CreatedById], [Target].[CreatedById]) IS NOT NULL OR NULLIF([Target].[CreatedById], [Source].[CreatedById]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedOn], [Target].[ModifiedOn]) IS NOT NULL OR NULLIF([Target].[ModifiedOn], [Source].[ModifiedOn]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedById], [Target].[ModifiedById]) IS NOT NULL OR NULLIF([Target].[ModifiedById], [Source].[ModifiedById]) IS NOT NULL OR 
	NULLIF([Source].[Name], [Target].[Name]) IS NOT NULL OR NULLIF([Target].[Name], [Source].[Name]) IS NOT NULL OR 
	NULLIF([Source].[Description], [Target].[Description]) IS NOT NULL OR NULLIF([Target].[Description], [Source].[Description]) IS NOT NULL OR 
	NULLIF([Source].[ProcessListeners], [Target].[ProcessListeners]) IS NOT NULL OR NULLIF([Target].[ProcessListeners], [Source].[ProcessListeners]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[CreatedOn] = [Source].[CreatedOn], 
  [Target].[CreatedById] = [Source].[CreatedById], 
  [Target].[ModifiedOn] = [Source].[ModifiedOn], 
  [Target].[ModifiedById] = [Source].[ModifiedById], 
  [Target].[Name] = [Source].[Name], 
  [Target].[Description] = [Source].[Description], 
  [Target].[ProcessListeners] = [Source].[ProcessListeners]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
 VALUES([Source].[Id],[Source].[CreatedOn],[Source].[CreatedById],[Source].[ModifiedOn],[Source].[ModifiedById],[Source].[Name],[Source].[Description],[Source].[ProcessListeners]);