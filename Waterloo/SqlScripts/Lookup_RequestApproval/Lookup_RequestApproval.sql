--Version Nov 3, 2020 @ 21:16 by Oliver
--Original Version Sep 16, 2020 @ 10:45 by Kavian
--Nov 3, 2020 - Changed values to Forward to Approver, Approve, Forward to Approver - Reject, Reject
--EXEC SP_GENERATE_MERGE 'WatRequestApproval'

MERGE INTO [WatRequestApproval] AS [Target]
USING (VALUES
  (N'9EFDDCAD-419E-477B-BC63-1D70D539FC7A','2020-11-04T02:14:11.4889191',N'EEFDE995-2248-41CE-9575-B7529329E8DF','2020-11-04T02:14:11.4889191',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Forward to Approver - Reject',N'',0)
 ,(N'6D394910-65F9-4036-AC3B-A1964499729E','2020-09-15T17:19:17.2827838',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-11-04T02:14:16.0142201',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Reject',N'',0)
 ,(N'69FC213C-3F69-4D7D-8FBB-F3950440CA3C','2020-09-15T17:19:08.1400117',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-11-04T02:13:56.4954227',N'EEFDE995-2248-41CE-9575-B7529329E8DF',N'Forward to Approver - Approve',N'',0)
) AS [Source] ([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[CreatedOn], [Target].[CreatedOn]) IS NOT NULL OR NULLIF([Target].[CreatedOn], [Source].[CreatedOn]) IS NOT NULL OR 
	NULLIF([Source].[CreatedById], [Target].[CreatedById]) IS NOT NULL OR NULLIF([Target].[CreatedById], [Source].[CreatedById]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedOn], [Target].[ModifiedOn]) IS NOT NULL OR NULLIF([Target].[ModifiedOn], [Source].[ModifiedOn]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedById], [Target].[ModifiedById]) IS NOT NULL OR NULLIF([Target].[ModifiedById], [Source].[ModifiedById]) IS NOT NULL OR 
	NULLIF([Source].[Name], [Target].[Name]) IS NOT NULL OR NULLIF([Target].[Name], [Source].[Name]) IS NOT NULL OR 
	NULLIF([Source].[Description], [Target].[Description]) IS NOT NULL OR NULLIF([Target].[Description], [Source].[Description]) IS NOT NULL OR 
	NULLIF([Source].[ProcessListeners], [Target].[ProcessListeners]) IS NOT NULL OR NULLIF([Target].[ProcessListeners], [Source].[ProcessListeners]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[CreatedOn] = [Source].[CreatedOn], 
  [Target].[CreatedById] = [Source].[CreatedById], 
  [Target].[ModifiedOn] = [Source].[ModifiedOn], 
  [Target].[ModifiedById] = [Source].[ModifiedById], 
  [Target].[Name] = [Source].[Name], 
  [Target].[Description] = [Source].[Description], 
  [Target].[ProcessListeners] = [Source].[ProcessListeners]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
 VALUES([Source].[Id],[Source].[CreatedOn],[Source].[CreatedById],[Source].[ModifiedOn],[Source].[ModifiedById],[Source].[Name],[Source].[Description],[Source].[ProcessListeners]);
