--Updated Version Nov 6, 2020 by Kavian (added sections)
--Original Version Oct 25, 2020 @ 20:02 by Oliver
--exec sp_generate_merge 'SysModuleInWorkplace', @from = 'from SysModuleInWorkplace where SysWorkplaceId in (select Id from SysWorkplace where Name in (''Travel Safety''))'

DELETE FROM SysModuleInWorkplace WHERE SysWorkplaceId in (select Id from SysWorkplace where Name in ('Travel Safety'))

MERGE INTO [SysModuleInWorkplace] AS [Target]
USING (VALUES
  (N'096A2D24-AEF0-485B-929E-2C6B11810596','2020-11-09T00:47:17.612',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-11-09T00:47:17.612',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',0,62,N'E3CC9683-1C7A-48C3-8E02-81529343B761',N'40318063-D9DB-43F7-8E3F-A0BED1529726')
 ,(N'D3C0C3A3-8A45-4487-915E-447D84489C1B','2020-11-09T00:47:17.707',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-11-09T00:47:17.707',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',0,63,N'E3CC9683-1C7A-48C3-8E02-81529343B761',N'3233AEC8-8561-4FFB-A6FA-C4DA9288CB3D')
 ,(N'FD7E5B3E-EDDA-453C-944F-81B2033CE394','2020-11-09T00:47:17.345',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-11-09T00:47:17.345',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',0,59,N'E3CC9683-1C7A-48C3-8E02-81529343B761',N'055063C9-8180-E011-AFBC-00155D04320C')
 ,(N'A42DC25D-D384-4F4E-93C7-8A8D5D8FB936','2020-11-09T00:47:17.785',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-11-09T00:47:17.785',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',0,64,N'E3CC9683-1C7A-48C3-8E02-81529343B761',N'A531360C-D994-4FC4-AE2F-FD125E7144E3')
 ,(N'663D6B98-C088-4833-9609-A3D609FCD5FB','2020-11-09T00:49:10.165',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-11-09T00:49:10.165',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',0,65,N'E3CC9683-1C7A-48C3-8E02-81529343B761',N'B74296B5-898D-46AC-AF1D-B3915E2EA0D2')
 ,(N'C0A41631-EB11-4AE0-8D9D-A88AB203F06A','2020-11-09T00:47:17.235',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-11-09T00:47:17.235',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',0,58,N'E3CC9683-1C7A-48C3-8E02-81529343B761',N'0C5063C9-8180-E011-AFBC-00155D04320C')
 ,(N'26E27453-493A-4345-92B1-B5D5B09294B6','2020-11-09T00:47:17.532',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-11-09T00:47:17.532',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',0,61,N'E3CC9683-1C7A-48C3-8E02-81529343B761',N'065063C9-8180-E011-AFBC-00155D04320C')
 ,(N'BF7B0F38-C55F-47FE-9617-B65BB671F67E','2020-11-09T00:47:17.438',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-11-09T00:47:17.438',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',0,60,N'E3CC9683-1C7A-48C3-8E02-81529343B761',N'005063C9-8180-E011-AFBC-00155D04320C')
) AS [Source] ([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[ProcessListeners],[Position],[SysWorkplaceId],[SysModuleId])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[CreatedOn], [Target].[CreatedOn]) IS NOT NULL OR NULLIF([Target].[CreatedOn], [Source].[CreatedOn]) IS NOT NULL OR 
	NULLIF([Source].[CreatedById], [Target].[CreatedById]) IS NOT NULL OR NULLIF([Target].[CreatedById], [Source].[CreatedById]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedOn], [Target].[ModifiedOn]) IS NOT NULL OR NULLIF([Target].[ModifiedOn], [Source].[ModifiedOn]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedById], [Target].[ModifiedById]) IS NOT NULL OR NULLIF([Target].[ModifiedById], [Source].[ModifiedById]) IS NOT NULL OR 
	NULLIF([Source].[ProcessListeners], [Target].[ProcessListeners]) IS NOT NULL OR NULLIF([Target].[ProcessListeners], [Source].[ProcessListeners]) IS NOT NULL OR 
	NULLIF([Source].[Position], [Target].[Position]) IS NOT NULL OR NULLIF([Target].[Position], [Source].[Position]) IS NOT NULL OR 
	NULLIF([Source].[SysWorkplaceId], [Target].[SysWorkplaceId]) IS NOT NULL OR NULLIF([Target].[SysWorkplaceId], [Source].[SysWorkplaceId]) IS NOT NULL OR 
	NULLIF([Source].[SysModuleId], [Target].[SysModuleId]) IS NOT NULL OR NULLIF([Target].[SysModuleId], [Source].[SysModuleId]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[CreatedOn] = [Source].[CreatedOn], 
  [Target].[CreatedById] = [Source].[CreatedById], 
  [Target].[ModifiedOn] = [Source].[ModifiedOn], 
  [Target].[ModifiedById] = [Source].[ModifiedById], 
  [Target].[ProcessListeners] = [Source].[ProcessListeners], 
  [Target].[Position] = [Source].[Position], 
  [Target].[SysWorkplaceId] = [Source].[SysWorkplaceId], 
  [Target].[SysModuleId] = [Source].[SysModuleId]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[ProcessListeners],[Position],[SysWorkplaceId],[SysModuleId])
 VALUES([Source].[Id],[Source].[CreatedOn],[Source].[CreatedById],[Source].[ModifiedOn],[Source].[ModifiedById],[Source].[ProcessListeners],[Source].[Position],[Source].[SysWorkplaceId],[Source].[SysModuleId]);