--Version Oct 15, 2020 by Kavian
--EXEC SP_GENERATE_MERGE 'WatStudentType'

MERGE INTO [WatStudentType] AS [Target]
USING (VALUES
  (N'C95BFCCB-1024-4791-A2F1-921A3ED04BBE','2020-10-15T14:07:24.6993198',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-10-15T14:07:24.6993198',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Graduate',N'',0)
 ,(N'3525F1D3-9208-4E4E-9BCC-933474F9F747','2020-10-15T14:07:20.7928251',N'410006E1-CA4E-4502-A9EC-E54D922D2C00','2020-10-15T14:07:20.7928251',N'410006E1-CA4E-4502-A9EC-E54D922D2C00',N'Undergraduate',N'',0)
) AS [Source] ([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
ON ([Target].[Id] = [Source].[Id])
WHEN MATCHED AND (
	NULLIF([Source].[CreatedOn], [Target].[CreatedOn]) IS NOT NULL OR NULLIF([Target].[CreatedOn], [Source].[CreatedOn]) IS NOT NULL OR 
	NULLIF([Source].[CreatedById], [Target].[CreatedById]) IS NOT NULL OR NULLIF([Target].[CreatedById], [Source].[CreatedById]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedOn], [Target].[ModifiedOn]) IS NOT NULL OR NULLIF([Target].[ModifiedOn], [Source].[ModifiedOn]) IS NOT NULL OR 
	NULLIF([Source].[ModifiedById], [Target].[ModifiedById]) IS NOT NULL OR NULLIF([Target].[ModifiedById], [Source].[ModifiedById]) IS NOT NULL OR 
	NULLIF([Source].[Name], [Target].[Name]) IS NOT NULL OR NULLIF([Target].[Name], [Source].[Name]) IS NOT NULL OR 
	NULLIF([Source].[Description], [Target].[Description]) IS NOT NULL OR NULLIF([Target].[Description], [Source].[Description]) IS NOT NULL OR 
	NULLIF([Source].[ProcessListeners], [Target].[ProcessListeners]) IS NOT NULL OR NULLIF([Target].[ProcessListeners], [Source].[ProcessListeners]) IS NOT NULL) THEN
 UPDATE SET
  [Target].[CreatedOn] = [Source].[CreatedOn], 
  [Target].[CreatedById] = [Source].[CreatedById], 
  [Target].[ModifiedOn] = [Source].[ModifiedOn], 
  [Target].[ModifiedById] = [Source].[ModifiedById], 
  [Target].[Name] = [Source].[Name], 
  [Target].[Description] = [Source].[Description], 
  [Target].[ProcessListeners] = [Source].[ProcessListeners]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[CreatedOn],[CreatedById],[ModifiedOn],[ModifiedById],[Name],[Description],[ProcessListeners])
 VALUES([Source].[Id],[Source].[CreatedOn],[Source].[CreatedById],[Source].[ModifiedOn],[Source].[ModifiedById],[Source].[Name],[Source].[Description],[Source].[ProcessListeners]);