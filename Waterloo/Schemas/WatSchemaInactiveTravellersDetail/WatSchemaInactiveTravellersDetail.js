define("WatSchemaInactiveTravellersDetail", [], function() {
	return {
		entitySchemaName: "WatTraveler",
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		diff: /**SCHEMA_DIFF*/[]/**SCHEMA_DIFF*/,
		methods: {
			getDeleteRecordMenuItem: Terrasoft.emptyFn,
			getCopyRecordMenuItem: Terrasoft.emptyFn,
			getEditRecordMenuItem: Terrasoft.emptyFn,
			getAddRecordButtonVisible: function() {
            	return false;
            }
		}
	};
});
