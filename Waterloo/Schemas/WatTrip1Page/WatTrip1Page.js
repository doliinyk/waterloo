define("WatTrip1Page", [], function() {
	return {
		entitySchemaName: "WatTrip",
		attributes: {},
		modules: /**SCHEMA_MODULES*/{
			"Indicator8614728a-21aa-4c82-b515-1d79de62474b": {
				"moduleId": "Indicator8614728a-21aa-4c82-b515-1d79de62474b",
				"moduleName": "CardWidgetModule",
				"config": {
					"parameters": {
						"viewModelConfig": {
							"widgetKey": "Indicator8614728a-21aa-4c82-b515-1d79de62474b",
							"recordId": "51a682e0-fa1f-455b-a9da-fb89da365365",
							"primaryColumnValue": {
								"getValueMethod": "getPrimaryColumnValue"
							}
						}
					}
				}
			},
			"Chartae777278-24c5-4466-a91a-c78f55aff614": {
				"moduleId": "Chartae777278-24c5-4466-a91a-c78f55aff614",
				"moduleName": "CardWidgetModule",
				"config": {
					"parameters": {
						"viewModelConfig": {
							"widgetKey": "Chartae777278-24c5-4466-a91a-c78f55aff614",
							"recordId": "51a682e0-fa1f-455b-a9da-fb89da365365",
							"primaryColumnValue": {
								"getValueMethod": "getPrimaryColumnValue"
							}
						}
					}
				}
			},
			"Chart54edfd58-91eb-493f-b726-9c4264a2d453": {
				"moduleId": "Chart54edfd58-91eb-493f-b726-9c4264a2d453",
				"moduleName": "CardWidgetModule",
				"config": {
					"parameters": {
						"viewModelConfig": {
							"widgetKey": "Chart54edfd58-91eb-493f-b726-9c4264a2d453",
							"recordId": "51a682e0-fa1f-455b-a9da-fb89da365365",
							"primaryColumnValue": {
								"getValueMethod": "getPrimaryColumnValue"
							}
						}
					}
				}
			},
			"Chartf560b333-b1db-41e5-8032-332c5fb7a018": {
				"moduleId": "Chartf560b333-b1db-41e5-8032-332c5fb7a018",
				"moduleName": "CardWidgetModule",
				"config": {
					"parameters": {
						"viewModelConfig": {
							"widgetKey": "Chartf560b333-b1db-41e5-8032-332c5fb7a018",
							"recordId": "51a682e0-fa1f-455b-a9da-fb89da365365",
							"primaryColumnValue": {
								"getValueMethod": "getPrimaryColumnValue"
							}
						}
					}
				}
			},
			"Indicator4fcced61-7528-4f6d-9e07-fddd02c97128": {
				"moduleId": "Indicator4fcced61-7528-4f6d-9e07-fddd02c97128",
				"moduleName": "CardWidgetModule",
				"config": {
					"parameters": {
						"viewModelConfig": {
							"widgetKey": "Indicator4fcced61-7528-4f6d-9e07-fddd02c97128",
							"recordId": "51a682e0-fa1f-455b-a9da-fb89da365365",
							"primaryColumnValue": {
								"getValueMethod": "getPrimaryColumnValue"
							}
						}
					}
				}
			},
			"Chartcd9eaebf-100e-47a4-8fc3-c030521952e4": {
				"moduleId": "Chartcd9eaebf-100e-47a4-8fc3-c030521952e4",
				"moduleName": "CardWidgetModule",
				"config": {
					"parameters": {
						"viewModelConfig": {
							"widgetKey": "Chartcd9eaebf-100e-47a4-8fc3-c030521952e4",
							"recordId": "51a682e0-fa1f-455b-a9da-fb89da365365",
							"primaryColumnValue": {
								"getValueMethod": "getPrimaryColumnValue"
							}
						}
					}
				}
			}
		}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{
			"Files": {
				"schemaName": "FileDetailV2",
				"entitySchemaName": "WatTripFile",
				"filter": {
					"masterColumn": "Id",
					"detailColumn": "WatTrip"
				}
			},
			"WatSchemaTravelerDetail28c4522b": {
				"schemaName": "WatSchemaTravelerDetail",
				"entitySchemaName": "WatTraveler",
				"filter": {
					"detailColumn": "WatTrip",
					"masterColumn": "Id"
				},
				"filterMethod": "activeTravellerDetailFilter"
			},
			"WatSchemaArrivalNoticeDetail36dfc57e": {
				"schemaName": "WatSchemaArrivalNoticeDetail",
				"entitySchemaName": "WatArrivalNotice",
				"filter": {
					"detailColumn": "WatTrip",
					"masterColumn": "Id"
				}
			},
			"WatSchema7713dc14Detailfd025518": {
				"schemaName": "WatSchema7713dc14Detail",
				"entitySchemaName": "WatAssociatedTraveler",
				"filter": {
					"detailColumn": "WatTrip",
					"masterColumn": "Id"
				}
			},
			"WatSchema94c7e3c8Detaile7f71f28": {
				"schemaName": "WatSchema94c7e3c8Detail",
				"entitySchemaName": "WatDatedNotes",
				"filter": {
					"detailColumn": "WatTrip",
					"masterColumn": "Id"
				}
			},
			"DocumentDetailV2ed4560df": {
				"schemaName": "DocumentDetailV2",
				"entitySchemaName": "Document",
				"filter": {
					"detailColumn": "WatTrip",
					"masterColumn": "Id"
				}
			},
			"WatSchemaCustomEmailCallDetail1fec42d5": {
				"schemaName": "WatSchemaCustomEmailCallDetail",
				"entitySchemaName": "Activity",
				"filter": {
					"detailColumn": "WatWatTrip",
					"masterColumn": "Id"
				}
			},
			"WatSchemaCustomTaskDetailb4ba0d40": {
				"schemaName": "WatSchemaCustomTaskDetail",
				"entitySchemaName": "Activity",
				"filter": {
					"detailColumn": "WatWatTrip",
					"masterColumn": "Id"
				}
			},
			"WatSchemaInactiveTravellersDetailcd0ddff6": {
				"schemaName": "WatSchemaInactiveTravellersDetail",
				"entitySchemaName": "WatTraveler",
				"filter": {
					"detailColumn": "WatTrip",
					"masterColumn": "Id"
				},
				"filterMethod": "inactiveTravellerDetailFilter"
			}
		}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{
			"WatStudentNumber": {
				"f498ee03-d9fa-4c1d-82b6-e708122fd61f": {
					"uId": "f498ee03-d9fa-4c1d-82b6-e708122fd61f",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatTravelerType"
							},
							"rightExpression": {
								"type": 0,
								"value": "fc07d4ad-98b0-4865-9292-fcfc564d2e91",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"WatStudentType": {
				"79f5a42b-df21-4afc-9800-7bc6e22b6e43": {
					"uId": "79f5a42b-df21-4afc-9800-7bc6e22b6e43",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatTravelerType"
							},
							"rightExpression": {
								"type": 0,
								"value": "fc07d4ad-98b0-4865-9292-fcfc564d2e91",
								"dataValueType": 10
							}
						}
					]
				},
				"9aa8d7c5-f67a-40e7-97d3-43233b067342": {
					"uId": "9aa8d7c5-f67a-40e7-97d3-43233b067342",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "WatStudentType"
							}
						}
					]
				}
			},
			"WatUniquePNRForGRP": {
				"f7bf4cbb-d44f-48b1-8a19-011dab6d2ce2": {
					"uId": "f7bf4cbb-d44f-48b1-8a19-011dab6d2ce2",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatTravelerType"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ec633e9-6646-4d08-a0bd-dbb211cd5711",
								"dataValueType": 10
							}
						}
					]
				},
				"4a5ab103-700a-4758-bea5-682ba3e1e01c": {
					"uId": "4a5ab103-700a-4758-bea5-682ba3e1e01c",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatTravelerType"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ec633e9-6646-4d08-a0bd-dbb211cd5711",
								"dataValueType": 10
							}
						}
					]
				},
				"97f037d8-4daa-436f-a96c-f178228b2ef5": {
					"uId": "97f037d8-4daa-436f-a96c-f178228b2ef5",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatSysFieldGroupControlOpen"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"Tab322fa3bfTabLabelGroupf9632d62": {
				"f85032e8-97a5-4f38-8823-204fac1b8934": {
					"uId": "f85032e8-97a5-4f38-8823-204fac1b8934",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatTravelerType"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ec633e9-6646-4d08-a0bd-dbb211cd5711",
								"dataValueType": 10
							}
						}
					]
				},
				"bcfaf84f-055b-4941-9e6b-f46b6f0733e5": {
					"uId": "bcfaf84f-055b-4941-9e6b-f46b6f0733e5",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatTravelerType"
							},
							"rightExpression": {
								"type": 0,
								"value": "fc07d4ad-98b0-4865-9292-fcfc564d2e91",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatTravelerType"
							},
							"rightExpression": {
								"type": 0,
								"value": "c4f2bbd7-2441-4923-bf9a-5a067501168d",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatTravelerType"
							},
							"rightExpression": {
								"type": 0,
								"value": "d48c64eb-488f-46c5-ba45-c564645f5e82",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"WatFacultyDepartment": {
				"163cd311-aa51-4a23-aba2-3d52a3ab74ee": {
					"uId": "163cd311-aa51-4a23-aba2-3d52a3ab74ee",
					"enabled": true,
					"removed": false,
					"ruleType": 1,
					"baseAttributePatch": "WatFaculty",
					"comparisonType": 3,
					"autoClean": true,
					"autocomplete": true,
					"type": 1,
					"attribute": "WatFaculty"
				}
			},
			"Tab2454d764TabLabelGroup644b6604": {
				"0befd563-5e2e-49d7-aacc-e4205e11088d": {
					"uId": "0befd563-5e2e-49d7-aacc-e4205e11088d",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatMultiDestination"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"Tab322fa3bfTabLabelGroup777b57bf": {
				"06127809-fad6-4c5c-8646-741c76865b1e": {
					"uId": "06127809-fad6-4c5c-8646-741c76865b1e",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatTravelerType"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ec633e9-6646-4d08-a0bd-dbb211cd5711",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"Tab2454d764TabLabelGrouped55a963": {
				"f175b1f5-e86e-4be5-84c9-3bd230163bdb": {
					"uId": "f175b1f5-e86e-4be5-84c9-3bd230163bdb",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "WatDepartureDateLeg2"
							}
						},
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "WatDestinationCountryLeg2"
							}
						}
					]
				}
			},
			"Tab2454d764TabLabelGroupb0162ed9": {
				"6855bdb6-a46f-4e04-bcbb-f7de44c1f387": {
					"uId": "6855bdb6-a46f-4e04-bcbb-f7de44c1f387",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "WatDepartureDateLeg3"
							}
						},
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "WatDestinationCountryLeg3"
							}
						}
					]
				}
			},
			"Tab2454d764TabLabelGroup3d9d65ee": {
				"91956a93-b683-451b-9cec-dcab04f458d1": {
					"uId": "91956a93-b683-451b-9cec-dcab04f458d1",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "WatDepartureDateLeg4"
							}
						},
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "WatDestinationCountryLeg4"
							}
						}
					]
				}
			},
			"Tab2454d764TabLabel": {
				"0ef2baf4-4fd8-46cd-89a7-a17dd5a5fa27": {
					"uId": "0ef2baf4-4fd8-46cd-89a7-a17dd5a5fa27",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatMultiDestination"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"WatCancelTrip": {
				"1d0ebeac-2be6-465e-a2ed-48da03e21c20": {
					"uId": "1d0ebeac-2be6-465e-a2ed-48da03e21c20",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatTripStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "a17128f2-d117-4c33-97af-df550701d42f",
								"dataValueType": 10
							}
						}
					]
				},
				"19d75058-83f5-4069-8a5f-b4a881621275": {
					"uId": "19d75058-83f5-4069-8a5f-b4a881621275",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatTripStage"
							},
							"rightExpression": {
								"type": 0,
								"value": "b5470b22-5c4d-4736-b1aa-abfa8fab7e69",
								"dataValueType": 10
							}
						}
					]
				},
				"b0686747-cd45-4409-a83e-dd93ce4af511": {
					"uId": "b0686747-cd45-4409-a83e-dd93ce4af511",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "WatTripCancelReason"
							}
						}
					]
				},
				"01984e23-f96c-4806-b877-e02bf045d7a3": {
					"uId": "01984e23-f96c-4806-b877-e02bf045d7a3",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatTripStage"
							},
							"rightExpression": {
								"type": 0,
								"value": "9f8784bc-b352-4bbc-97d9-b1bc7029d327",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"WatPreDepartureWarning": {
				"e34dfd19-761f-4819-b90e-99d2f6492c58": {
					"uId": "e34dfd19-761f-4819-b90e-99d2f6492c58",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "WatPreDepartureWarning"
							}
						}
					]
				}
			},
			"Tab7f3aebceTabLabelGroupc9b2f47a": {
				"4f7f23cb-88a2-4f7d-b5eb-ab7901fb48e5": {
					"uId": "4f7f23cb-88a2-4f7d-b5eb-ab7901fb48e5",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatTravelerType"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ec633e9-6646-4d08-a0bd-dbb211cd5711",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"Tab7f3aebceTabLabelGroup39742aea": {
				"459ee5ad-1ade-4230-83ed-33b7107921b9": {
					"uId": "459ee5ad-1ade-4230-83ed-33b7107921b9",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatTravelerType"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ec633e9-6646-4d08-a0bd-dbb211cd5711",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"WatHighRiskCommentReviewer": {
				"770d37df-6986-498d-a442-dc6844b06bb7": {
					"uId": "770d37df-6986-498d-a442-dc6844b06bb7",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "WatHighRiskReviewer"
							}
						}
					]
				}
			},
			"Tab7f3aebceTabLabelGroupd56b6ed9": {
				"8f06c3d1-1810-42c8-ab84-27e2402ed7fc": {
					"uId": "8f06c3d1-1810-42c8-ab84-27e2402ed7fc",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatTravelerType"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ec633e9-6646-4d08-a0bd-dbb211cd5711",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatSysFieldGroupControlOpen"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"Tab322fa3bfTabLabelGroup4fe63dea": {
				"d6d2bdb4-b40e-4690-b2be-cc98c064ca13": {
					"uId": "d6d2bdb4-b40e-4690-b2be-cc98c064ca13",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatTravelerType"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ec633e9-6646-4d08-a0bd-dbb211cd5711",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"WatTripCancelReason": {
				"d563dfd8-f08f-4749-807e-dd05e4638bf9": {
					"uId": "d563dfd8-f08f-4749-807e-dd05e4638bf9",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatCancelTrip"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				},
				"996cfb1d-214c-49be-a699-622775d0ee11": {
					"uId": "996cfb1d-214c-49be-a699-622775d0ee11",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatCancelTrip"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				},
				"44125efd-d81f-48e9-bfd8-367a0cd32c4f": {
					"uId": "44125efd-d81f-48e9-bfd8-367a0cd32c4f",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatTripStage"
							},
							"rightExpression": {
								"type": 0,
								"value": "9f8784bc-b352-4bbc-97d9-b1bc7029d327",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"Tab322fa3bfTabLabelGroup17338d9e": {
				"840229b8-071b-4ad1-b589-f22eaa239fba": {
					"uId": "840229b8-071b-4ad1-b589-f22eaa239fba",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatTravelerType"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ec633e9-6646-4d08-a0bd-dbb211cd5711",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"WatEnrollInLEARN": {
				"a9df01d1-4b8f-4d67-8f08-0022411e7530": {
					"uId": "a9df01d1-4b8f-4d67-8f08-0022411e7530",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatTravelerType"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ec633e9-6646-4d08-a0bd-dbb211cd5711",
								"dataValueType": 10
							}
						}
					]
				},
				"3d4c1e22-4056-464f-8239-e850c459855e": {
					"uId": "3d4c1e22-4056-464f-8239-e850c459855e",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "WatLearnEnrollDate"
							}
						}
					]
				}
			},
			"WatMainTraveller": {
				"09dc8422-e79e-41eb-9577-e41cfcafa52a": {
					"uId": "09dc8422-e79e-41eb-9577-e41cfcafa52a",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatTravelerType"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ec633e9-6646-4d08-a0bd-dbb211cd5711",
								"dataValueType": 10
							}
						}
					]
				},
				"794dd891-d28e-4e69-9bc1-edb2dc13d1c9": {
					"uId": "794dd891-d28e-4e69-9bc1-edb2dc13d1c9",
					"enabled": true,
					"removed": false,
					"ruleType": 1,
					"baseAttributePatch": "WatTrip.WatTripPNR",
					"comparisonType": 3,
					"autoClean": false,
					"autocomplete": false,
					"type": 1,
					"attribute": "WatTripPNR"
				},
				"92a706cd-a026-4156-94dd-b5ed41d3c0a6": {
					"uId": "92a706cd-a026-4156-94dd-b5ed41d3c0a6",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatTravelerType"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ec633e9-6646-4d08-a0bd-dbb211cd5711",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "CreatedOn"
							}
						}
					]
				}
			},
			"WatTravelerType": {
				"1c2903af-7f42-429b-aaaa-20eeeb6f79f2": {
					"uId": "1c2903af-7f42-429b-aaaa-20eeeb6f79f2",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "WatTravelerType"
							}
						},
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "CreatedOn"
							}
						}
					]
				}
			},
			"Tab7f3aebceTabLabelGroup0c84abb1": {
				"efd21e39-b4c7-45b7-bb92-8b647841f826": {
					"uId": "efd21e39-b4c7-45b7-bb92-8b647841f826",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatTravelerType"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ec633e9-6646-4d08-a0bd-dbb211cd5711",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"WatAnvilReady": {
				"e3a258b9-938d-4dcd-9d43-547beb7ac9ec": {
					"uId": "e3a258b9-938d-4dcd-9d43-547beb7ac9ec",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAnvilReadyDate"
							}
						}
					]
				}
			},
			"WatTravellerNumber": {
				"33fe5485-d400-4cf3-9109-fc028d6c521b": {
					"uId": "33fe5485-d400-4cf3-9109-fc028d6c521b",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatTravelerType"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ec633e9-6646-4d08-a0bd-dbb211cd5711",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"NotesControlGroup": {
				"0c1f6ddb-392b-4419-a57b-17aff6469e4a": {
					"uId": "0c1f6ddb-392b-4419-a57b-17aff6469e4a",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 0,
								"value": 1,
								"dataValueType": 4
							},
							"rightExpression": {
								"type": 0,
								"value": 2,
								"dataValueType": 4
							}
						}
					]
				}
			},
			"WatGACAdvisoryLevel": {
				"33b1b15d-c715-4c8c-9b12-52d0cfd8efe3": {
					"uId": "33b1b15d-c715-4c8c-9b12-52d0cfd8efe3",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "WatHighRiskReviewer"
							}
						}
					]
				},
				"b65dffeb-4839-4a27-84d4-da5e999a9715": {
					"uId": "b65dffeb-4839-4a27-84d4-da5e999a9715",
					"enabled": true,
					"removed": false,
					"ruleType": 1,
					"baseAttributePatch": "WatHighRisk",
					"comparisonType": 3,
					"autoClean": true,
					"autocomplete": true,
					"type": 1,
					"attribute": "WatHighRiskReviewer"
				}
			},
			"WatArts": {
				"c2892201-e73e-4982-bab3-aff46dad7ee6": {
					"uId": "c2892201-e73e-4982-bab3-aff46dad7ee6",
					"enabled": true,
					"removed": false,
					"ruleType": 1,
					"baseAttributePatch": "WatFaculty",
					"comparisonType": 3,
					"autoClean": false,
					"autocomplete": true,
					"type": 1,
					"value": "dee8d30a-321d-40d8-85eb-3d1866a9cb1b",
					"dataValueType": 10,
					"attribute": "WatFaculty"
				}
			},
			"WatAcademicSupportUnit": {
				"4663e0e7-1780-4959-a516-d58ee0005838": {
					"uId": "4663e0e7-1780-4959-a516-d58ee0005838",
					"enabled": true,
					"removed": false,
					"ruleType": 1,
					"baseAttributePatch": "WatFaculty",
					"comparisonType": 3,
					"autoClean": false,
					"autocomplete": true,
					"type": 1,
					"attribute": "WatFaculty"
				}
			},
			"WatCentresAndInstitutes": {
				"dbd5364c-ac6b-4acc-becc-2e66d3ee39c2": {
					"uId": "dbd5364c-ac6b-4acc-becc-2e66d3ee39c2",
					"enabled": true,
					"removed": false,
					"ruleType": 1,
					"baseAttributePatch": "WatFaculty",
					"comparisonType": 3,
					"autoClean": false,
					"autocomplete": true,
					"type": 1,
					"attribute": "WatFaculty"
				}
			},
			"WatEngineering": {
				"d8c7c61b-8104-4fbe-9703-025cd74414e4": {
					"uId": "d8c7c61b-8104-4fbe-9703-025cd74414e4",
					"enabled": true,
					"removed": false,
					"ruleType": 1,
					"baseAttributePatch": "WatFaculty",
					"comparisonType": 3,
					"autoClean": false,
					"autocomplete": true,
					"type": 1,
					"attribute": "WatFaculty"
				}
			},
			"WatEnvironment": {
				"1e55b5c3-64e8-4d74-964e-89d276698416": {
					"uId": "1e55b5c3-64e8-4d74-964e-89d276698416",
					"enabled": true,
					"removed": false,
					"ruleType": 1,
					"baseAttributePatch": "WatFaculty",
					"comparisonType": 3,
					"autoClean": false,
					"autocomplete": true,
					"type": 1,
					"attribute": "WatFaculty"
				}
			},
			"WatHealth": {
				"2f569eb2-5290-4809-9b46-2c5c5a3d55c1": {
					"uId": "2f569eb2-5290-4809-9b46-2c5c5a3d55c1",
					"enabled": true,
					"removed": false,
					"ruleType": 1,
					"baseAttributePatch": "WatFaculty",
					"comparisonType": 3,
					"autoClean": false,
					"autocomplete": true,
					"type": 1,
					"attribute": "WatFaculty"
				}
			},
			"WatMath": {
				"7adc927d-33d8-4149-bcb2-ac82908ee7f9": {
					"uId": "7adc927d-33d8-4149-bcb2-ac82908ee7f9",
					"enabled": true,
					"removed": false,
					"ruleType": 1,
					"baseAttributePatch": "WatFaculty",
					"comparisonType": 3,
					"autoClean": false,
					"autocomplete": true,
					"type": 1,
					"attribute": "WatFaculty"
				}
			},
			"WatScience": {
				"0483369e-d10f-42cf-a8be-54d21f656c29": {
					"uId": "0483369e-d10f-42cf-a8be-54d21f656c29",
					"enabled": true,
					"removed": false,
					"ruleType": 1,
					"baseAttributePatch": "WatFaculty",
					"comparisonType": 3,
					"autoClean": false,
					"autocomplete": true,
					"type": 1,
					"attribute": "WatFaculty"
				}
			},
			"WatCoOpWorkTerm": {
				"7fd6ad0a-ced4-443f-ba4d-9c6166c9b0f7": {
					"uId": "7fd6ad0a-ced4-443f-ba4d-9c6166c9b0f7",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatTravelerType"
							},
							"rightExpression": {
								"type": 0,
								"value": "cb2b6ded-e650-4071-8fd9-4b6476bf0bd1",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "WatCoOpWorkTerm"
							}
						}
					]
				}
			},
			"WatExchangeTerm": {
				"c315e257-df4e-47af-aa6d-cbfa230c10ac": {
					"uId": "c315e257-df4e-47af-aa6d-cbfa230c10ac",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatPurposeOfTravel"
							},
							"rightExpression": {
								"type": 0,
								"value": "f4d91420-c20b-464d-9bc1-cc7e28addb14",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"WatSchemaInactiveTravellersDetailcd0ddff6": {
				"12e76ece-e4ab-44c6-88c7-033976a877a7": {
					"uId": "12e76ece-e4ab-44c6-88c7-033976a877a7",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatTravelerType"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ec633e9-6646-4d08-a0bd-dbb211cd5711",
								"dataValueType": 10
							}
						}
					]
				}
			}
		}/**SCHEMA_BUSINESS_RULES*/,
		methods:  {
			activeTravellerDetailFilter: function() {
				const filterGroup = Terrasoft.createFilterGroup();
				filterGroup.add("ActiveTravellerFilter",
					Terrasoft.createColumnFilterWithParameter(Terrasoft.ComparisonType.NOT_EQUAL, "WatActiveTraveller", "4f88722e-bb15-4c21-86b8-23a17b5f2e05")); //Active = Not No
				filterGroup.add("TripFilter", 
					Terrasoft.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL, "WatTrip", this.get("Id")));
				return filterGroup;
			},
			inactiveTravellerDetailFilter: function() {
				const filterGroup = Terrasoft.createFilterGroup();
				filterGroup.add("InactiveTravellerFilter", 
					Terrasoft.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL, "WatActiveTraveller", "4f88722e-bb15-4c21-86b8-23a17b5f2e05")); //Active = No
				filterGroup.add("TripFilter", 
					Terrasoft.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL, "WatTrip", this.get("Id")));
				return filterGroup;
			}
		},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"name": "WatTripPNR618fb359-a316-4e20-8903-33bafee3827b",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "WatTripPNR",
					"enabled": false
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatTripStageb7558dc8-3fbd-4f0a-ae03-ac70ff1e58e4",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "WatTripStage",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "WatTripStatusbebae261-117e-43f1-87fb-7788f99567a9",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "WatTripStatus",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "WatTripStatusDetailbc4e1d73-1576-4871-8e09-8a5b8f8f1d1c",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "WatTripStatusDetail",
					"enabled": true,
					"contentType": 0
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "LOOKUP5fa3d76a-69b9-4740-b8d9-e44c0e9b25ca",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "WatMainTraveller",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "WatRequesterContact7dfce978-fd7e-4e64-8b3b-69e7a86d73a8",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 5,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "WatRequesterContact",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatRequesterContact7dfce978fd7e4e648b3b69e7a86d73a8LabelCaption"
						}
					},
					"enabled": true,
					"contentType": 5
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "WatStudentType0b93fec3-566f-4662-b386-15287f09f7f2",
				"values": {
					"layout": {
						"colSpan": 4,
						"rowSpan": 1,
						"column": 14,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "WatStudentType",
					"labelConfig": {
						"visible": false
					},
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatLastMinuteFlag06daa5c7-51cd-4df3-9544-b19554bde481",
				"values": {
					"layout": {
						"colSpan": 6,
						"rowSpan": 1,
						"column": 18,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "WatLastMinuteFlag",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatLastMinuteFlag06daa5c751cd4df39544b19554bde481LabelCaption"
						}
					},
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "WatDestination0021ecdd-ab82-47df-8ba5-6d5903969b93",
				"values": {
					"layout": {
						"colSpan": 7,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "WatDestination",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatDestination0021ecddab8247df8ba56d5903969b93LabelCaption"
						}
					},
					"enabled": true,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "WatTravelerType15764812-d827-432d-92f2-1f4f037c6acb",
				"values": {
					"layout": {
						"colSpan": 7,
						"rowSpan": 1,
						"column": 7,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "WatTravelerType",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatTravelerType15764812d827432d92f21f4f037c6acbLabelCaption"
						}
					},
					"enabled": true,
					"contentType": 3
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "WatLEARNDueDate4bceede1-ae93-4d8c-b1e6-088cb3b18680",
				"values": {
					"layout": {
						"colSpan": 6,
						"rowSpan": 1,
						"column": 18,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "WatLEARNDueDate",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "WatCityState9048eebc-fc79-4fcd-821f-a0b46ec3d4b3",
				"values": {
					"layout": {
						"colSpan": 7,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "WatCityState",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatCityState9048eebcfc794fcd821fa0b46ec3d4b3LabelCaption"
						}
					},
					"enabled": true
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "WatPurposeOfTravel1e520e10-c224-4654-9792-591e4f1551bf",
				"values": {
					"layout": {
						"colSpan": 11,
						"rowSpan": 1,
						"column": 7,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "WatPurposeOfTravel",
					"enabled": true,
					"contentType": 3,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatPurposeOfTravel1e520e10c22446549792591e4f1551bfLabelCaption"
						}
					}
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "BOOLEAN65615948-9371-427d-959e-cf249491c793",
				"values": {
					"layout": {
						"colSpan": 6,
						"rowSpan": 1,
						"column": 18,
						"row": 2,
						"layoutName": "Header"
					},
					"bindTo": "WatLEARNCompleted",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "WatDestinationProvinceState566639b9-848c-4cd2-908e-fdc877341373",
				"values": {
					"layout": {
						"colSpan": 7,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Header"
					},
					"bindTo": "WatDestinationProvinceState",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatDestinationProvinceState566639b9848c4cd2908efdc877341373LabelCaption"
						}
					},
					"enabled": true
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 8
			},
			{
				"operation": "insert",
				"name": "WatFacultyad422727-bcaf-45ac-8eba-8ed0b060b2bb",
				"values": {
					"layout": {
						"colSpan": 11,
						"rowSpan": 1,
						"column": 7,
						"row": 2,
						"layoutName": "Header"
					},
					"bindTo": "WatFaculty",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatFacultyad422727bcaf45ac8eba8ed0b060b2bbLabelCaption"
						}
					},
					"enabled": true,
					"contentType": 3
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 9
			},
			{
				"operation": "insert",
				"name": "WatUniquePNRForGRPbcfbb5cd-dc46-4083-b0b9-81f1dbf10f43",
				"values": {
					"layout": {
						"colSpan": 6,
						"rowSpan": 1,
						"column": 18,
						"row": 3,
						"layoutName": "Header"
					},
					"bindTo": "WatUniquePNRForGRP"
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 10
			},
			{
				"operation": "insert",
				"name": "WatDepartureDate693b4e63-8286-4fe2-afd9-a6eafe6043cf",
				"values": {
					"layout": {
						"colSpan": 7,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Header"
					},
					"bindTo": "WatDepartureDate",
					"enabled": true,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatDepartureDate693b4e6382864fe2afd9a6eafe6043cfLabelCaption"
						}
					}
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 11
			},
			{
				"operation": "insert",
				"name": "WatFacultyDepartment35db8d0c-485e-4417-8845-860d35984fb7",
				"values": {
					"layout": {
						"colSpan": 11,
						"rowSpan": 1,
						"column": 7,
						"row": 3,
						"layoutName": "Header"
					},
					"bindTo": "WatFacultyDepartment",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 12
			},
			{
				"operation": "insert",
				"name": "WatReturnDate17c176d2-9d5a-4b3d-9bda-74f20a2d30a8",
				"values": {
					"layout": {
						"colSpan": 7,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "Header"
					},
					"bindTo": "WatReturnDate",
					"enabled": true,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatReturnDate17c176d29d5a4b3d9bda74f20a2d30a8LabelCaption"
						}
					}
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 13
			},
			{
				"operation": "insert",
				"name": "WatOtherTravelPurposeDetail10aa2c8e-4234-4f17-9929-434814c25040",
				"values": {
					"layout": {
						"colSpan": 17,
						"rowSpan": 1,
						"column": 7,
						"row": 4,
						"layoutName": "Header"
					},
					"bindTo": "WatOtherTravelPurposeDetail",
					"enabled": true,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatOtherTravelPurposeDetail10aa2c8e42344f179929434814c25040LabelCaption"
						}
					}
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 14
			},
			{
				"operation": "insert",
				"name": "WatMultiDestination7db17226-5447-432d-ac1d-733c1dff40bc",
				"values": {
					"layout": {
						"colSpan": 7,
						"rowSpan": 1,
						"column": 0,
						"row": 5,
						"layoutName": "Header"
					},
					"bindTo": "WatMultiDestination",
					"enabled": true,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatMultiDestination7db172265447432dac1d733c1dff40bcLabelCaption"
						}
					}
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 15
			},
			{
				"operation": "insert",
				"name": "LOOKUP3417cc58-3322-4372-a815-12d00a95879b",
				"values": {
					"layout": {
						"colSpan": 7,
						"rowSpan": 1,
						"column": 7,
						"row": 5,
						"layoutName": "Header"
					},
					"bindTo": "WatCoOpWorkTerm",
					"enabled": false,
					"contentType": 3
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 16
			},
			{
				"operation": "insert",
				"name": "WatHighRiskReviewere5c2d621-6b9f-4b2e-92ef-acc35998a327",
				"values": {
					"layout": {
						"colSpan": 7,
						"rowSpan": 1,
						"column": 17,
						"row": 5,
						"layoutName": "Header"
					},
					"bindTo": "WatHighRiskReviewer",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatHighRiskReviewere5c2d6216b9f4b2e92efacc35998a327LabelCaption"
						}
					},
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 17
			},
			{
				"operation": "insert",
				"name": "BOOLEANab1bbd90-fe88-4fa0-86b1-16ec75868276",
				"values": {
					"layout": {
						"colSpan": 7,
						"rowSpan": 1,
						"column": 0,
						"row": 6,
						"layoutName": "Header"
					},
					"bindTo": "WatCancelTrip",
					"enabled": true,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.BOOLEANab1bbd90fe884fa086b116ec75868276LabelCaption"
						}
					}
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 18
			},
			{
				"operation": "insert",
				"name": "LOOKUP2c318f56-ec2b-426a-b638-549cd3831cb1",
				"values": {
					"layout": {
						"colSpan": 7,
						"rowSpan": 1,
						"column": 7,
						"row": 6,
						"layoutName": "Header"
					},
					"bindTo": "WatExchangeTerm",
					"enabled": true,
					"contentType": 3
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 19
			},
			{
				"operation": "insert",
				"name": "INTEGER1c0f0279-8733-40c8-a374-8c1cacc91b3e",
				"values": {
					"layout": {
						"colSpan": 7,
						"rowSpan": 1,
						"column": 17,
						"row": 6,
						"layoutName": "Header"
					},
					"bindTo": "WatTravellerNumber",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 20
			},
			{
				"operation": "insert",
				"name": "LOOKUP03888cf6-4331-4757-b4be-fd2685af6555",
				"values": {
					"layout": {
						"colSpan": 7,
						"rowSpan": 1,
						"column": 0,
						"row": 7,
						"layoutName": "Header"
					},
					"bindTo": "WatTripCancelReason",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.LOOKUP03888cf643314757b4befd2685af6555LabelCaption"
						}
					},
					"enabled": true,
					"contentType": 3
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 21
			},
			{
				"operation": "insert",
				"name": "Tab322fa3bfTabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab322fa3bfTabLabelTabCaption"
					},
					"items": [],
					"order": 0
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tab322fa3bfTabLabelGroup4fe63dea",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab322fa3bfTabLabelGroup4fe63deaGroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tab322fa3bfTabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tab322fa3bfTabLabelGridLayout7d84ee35",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tab322fa3bfTabLabelGroup4fe63dea",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatFullNameMainContact83c79365-be86-493f-b3ac-23661265cf95",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tab322fa3bfTabLabelGridLayout7d84ee35"
					},
					"bindTo": "WatFullNameMainContact",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatFullNameMainContact83c79365be86493fb3ac23661265cf95LabelCaption"
						}
					},
					"enabled": true
				},
				"parentName": "Tab322fa3bfTabLabelGridLayout7d84ee35",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatEmailGroupMainContact92c4dfa2-68e2-464d-89e8-e204a1d806a1",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "Tab322fa3bfTabLabelGridLayout7d84ee35"
					},
					"bindTo": "WatEmailGroupMainContact",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatEmailGroupMainContact92c4dfa268e2464d89e8e204a1d806a1LabelCaption"
						}
					},
					"enabled": true
				},
				"parentName": "Tab322fa3bfTabLabelGridLayout7d84ee35",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Tab322fa3bfTabLabelGroup17338d9e",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab322fa3bfTabLabelGroup17338d9eGroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tab322fa3bfTabLabel",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Tab322fa3bfTabLabelGridLayout65711ba2",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tab322fa3bfTabLabelGroup17338d9e",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatWaterlooContact603daf6a-180c-4d16-8f65-d5cc00e70fdc",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tab322fa3bfTabLabelGridLayout65711ba2"
					},
					"bindTo": "WatWaterlooContact",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatWaterlooContact603daf6a180c4d168f65d5cc00e70fdcLabelCaption"
						}
					},
					"enabled": true
				},
				"parentName": "Tab322fa3bfTabLabelGridLayout65711ba2",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatWaterlooContactEmailAddresse8d0cba4-5abe-4ce8-97b7-d392e9144c2b",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "Tab322fa3bfTabLabelGridLayout65711ba2"
					},
					"bindTo": "WatWaterlooContactEmailAddress",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatWaterlooContactEmailAddresse8d0cba45abe4ce897b7d392e9144c2bLabelCaption"
						}
					},
					"enabled": true
				},
				"parentName": "Tab322fa3bfTabLabelGridLayout65711ba2",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Tab322fa3bfTabLabelGroup12d7b0f6",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab322fa3bfTabLabelGroup12d7b0f6GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tab322fa3bfTabLabel",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "Tab322fa3bfTabLabelGridLayout98cd36fd",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tab322fa3bfTabLabelGroup12d7b0f6",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatHighRiskDestinationd10cf2a6-8f0e-4fbe-8cd4-5e7ddc25e0ae",
				"values": {
					"layout": {
						"colSpan": 6,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tab322fa3bfTabLabelGridLayout98cd36fd"
					},
					"bindTo": "WatHighRiskDestination",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatHighRiskDestinationd10cf2a68f0e4fbe8cd45e7ddc25e0aeLabelCaption"
						}
					},
					"enabled": false
				},
				"parentName": "Tab322fa3bfTabLabelGridLayout98cd36fd",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatHighRiskComment4b79d5f5-b4f1-4a0f-a67d-67ccd6145f13",
				"values": {
					"layout": {
						"colSpan": 18,
						"rowSpan": 1,
						"column": 6,
						"row": 0,
						"layoutName": "Tab322fa3bfTabLabelGridLayout98cd36fd"
					},
					"bindTo": "WatHighRiskComment",
					"enabled": false,
					"contentType": 0,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatHighRiskComment4b79d5f5b4f14a0fa67d67ccd6145f13LabelCaption"
						}
					}
				},
				"parentName": "Tab322fa3bfTabLabelGridLayout98cd36fd",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "WatHighRiskReviewer0d105ce4-4447-4e67-af57-d2cead921f6c",
				"values": {
					"layout": {
						"colSpan": 6,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Tab322fa3bfTabLabelGridLayout98cd36fd"
					},
					"bindTo": "WatHighRiskReviewer",
					"enabled": true,
					"contentType": 3
				},
				"parentName": "Tab322fa3bfTabLabelGridLayout98cd36fd",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "WatGACAdvisoryLevel907ae848-64d4-490e-ba91-a566115ae972",
				"values": {
					"layout": {
						"colSpan": 11,
						"rowSpan": 1,
						"column": 6,
						"row": 2,
						"layoutName": "Tab322fa3bfTabLabelGridLayout98cd36fd"
					},
					"bindTo": "WatGACAdvisoryLevel",
					"enabled": true,
					"contentType": 3
				},
				"parentName": "Tab322fa3bfTabLabelGridLayout98cd36fd",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "WatHighRiskCommentReviewer7b8dd9b5-7470-40ed-9bda-f74956392044",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Tab322fa3bfTabLabelGridLayout98cd36fd"
					},
					"bindTo": "WatHighRiskCommentReviewer",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatHighRiskCommentReviewer7b8dd9b5747040ed9bdaf74956392044LabelCaption"
						}
					},
					"enabled": true,
					"contentType": 0
				},
				"parentName": "Tab322fa3bfTabLabelGridLayout98cd36fd",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "Tab322fa3bfTabLabelGroup02276d3b",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab322fa3bfTabLabelGroup02276d3bGroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tab322fa3bfTabLabel",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "Tab322fa3bfTabLabelGridLayoutd3df52da",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tab322fa3bfTabLabelGroup02276d3b",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatAssociateVPRecommended1e1c6a22-8d9f-463d-9c49-85508a31a72d",
				"values": {
					"layout": {
						"colSpan": 7,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tab322fa3bfTabLabelGridLayoutd3df52da"
					},
					"bindTo": "WatAssociateVPRecommended",
					"enabled": false,
					"contentType": 3
				},
				"parentName": "Tab322fa3bfTabLabelGridLayoutd3df52da",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "DATETIME760aefca-eaf1-4105-95f1-ec177f7730a9",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 7,
						"row": 0,
						"layoutName": "Tab322fa3bfTabLabelGridLayoutd3df52da"
					},
					"bindTo": "WatAVPDecisionDate",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.DATETIME760aefcaeaf1410595f1ec177f7730a9LabelCaption"
						}
					},
					"enabled": false
				},
				"parentName": "Tab322fa3bfTabLabelGridLayoutd3df52da",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "WatProvostApprovalb82adc7f-d9fb-4e64-9364-83d30459fad4",
				"values": {
					"layout": {
						"colSpan": 7,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Tab322fa3bfTabLabelGridLayoutd3df52da"
					},
					"bindTo": "WatProvostApproval",
					"enabled": false,
					"contentType": 3
				},
				"parentName": "Tab322fa3bfTabLabelGridLayoutd3df52da",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "DATETIMEc7adbad0-0958-45dc-82f6-5d98f261f76b",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 7,
						"row": 1,
						"layoutName": "Tab322fa3bfTabLabelGridLayoutd3df52da"
					},
					"bindTo": "WatProvostDecisionDate",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.DATETIMEc7adbad0095845dc82f65d98f261f76bLabelCaption"
						}
					},
					"enabled": false
				},
				"parentName": "Tab322fa3bfTabLabelGridLayoutd3df52da",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "WatTripRecommended9738b16a-5ea2-498f-bca8-1e2e3c77737b",
				"values": {
					"layout": {
						"colSpan": 7,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Tab322fa3bfTabLabelGridLayoutd3df52da"
					},
					"bindTo": "WatTripRecommended",
					"enabled": true,
					"contentType": 3
				},
				"parentName": "Tab322fa3bfTabLabelGridLayoutd3df52da",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "Tab7f3aebceTabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab7f3aebceTabLabelTabCaption"
					},
					"items": [],
					"order": 1
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Tab7f3aebceTabLabelGroupc9b2f47a",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab7f3aebceTabLabelGroupc9b2f47aGroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tab7f3aebceTabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tab7f3aebceTabLabelGridLayout81edfef6",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tab7f3aebceTabLabelGroupc9b2f47a",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatTravellerFirstName7d021794-7935-4793-a446-ae8519818954",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tab7f3aebceTabLabelGridLayout81edfef6"
					},
					"bindTo": "WatTravellerFirstName",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatTravellerFirstName7d02179479354793a446ae8519818954LabelCaption"
						}
					},
					"enabled": false
				},
				"parentName": "Tab7f3aebceTabLabelGridLayout81edfef6",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatTravellerWatIAMID8b18e483-af29-4735-be06-b7527d98b86d",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 8,
						"row": 0,
						"layoutName": "Tab7f3aebceTabLabelGridLayout81edfef6"
					},
					"bindTo": "WatTravellerWatIAMID",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatTravellerWatIAMID8b18e483af294735be06b7527d98b86dLabelCaption"
						}
					},
					"enabled": false
				},
				"parentName": "Tab7f3aebceTabLabelGridLayout81edfef6",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "WatStudentType68bb437a-a38b-4cfb-a7aa-faeb2f2a342f",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 16,
						"row": 0,
						"layoutName": "Tab7f3aebceTabLabelGridLayout81edfef6"
					},
					"bindTo": "WatStudentType",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatStudentType68bb437aa38b4cfba7aafaeb2f2a342fLabelCaption"
						}
					},
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Tab7f3aebceTabLabelGridLayout81edfef6",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "WatTravellerLastName7a6c6fba-92f3-4a25-a3b5-470ce02408ec",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Tab7f3aebceTabLabelGridLayout81edfef6"
					},
					"bindTo": "WatTravellerLastName",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatTravellerLastName7a6c6fba92f34a25a3b5470ce02408ecLabelCaption"
						}
					},
					"enabled": false
				},
				"parentName": "Tab7f3aebceTabLabelGridLayout81edfef6",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "WatTravellerUWEmailaa08c6a2-3c8e-402e-bf28-dd25260c4d91",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 8,
						"row": 1,
						"layoutName": "Tab7f3aebceTabLabelGridLayout81edfef6"
					},
					"bindTo": "WatTravellerUWEmail",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatTravellerUWEmailaa08c6a23c8e402ebf28dd25260c4d91LabelCaption"
						}
					},
					"enabled": false
				},
				"parentName": "Tab7f3aebceTabLabelGridLayout81edfef6",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "Tab7f3aebceTabLabelGroup39742aea",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab7f3aebceTabLabelGroup39742aeaGroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tab7f3aebceTabLabel",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Tab7f3aebceTabLabelGridLayoutdf44f075",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tab7f3aebceTabLabelGroup39742aea",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatTripLeaderFirstName5413d107-9359-4cbf-a54b-570e80b11733",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tab7f3aebceTabLabelGridLayoutdf44f075"
					},
					"bindTo": "WatTripLeaderFirstName",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatTripLeaderFirstName5413d10793594cbfa54b570e80b11733LabelCaption"
						}
					},
					"enabled": false
				},
				"parentName": "Tab7f3aebceTabLabelGridLayoutdf44f075",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatTripLeaderWatIAMID06dd26d8-3467-4778-846e-4e9f94ba0371",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 8,
						"row": 0,
						"layoutName": "Tab7f3aebceTabLabelGridLayoutdf44f075"
					},
					"bindTo": "WatTripLeaderWatIAMID",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatTripLeaderWatIAMID06dd26d834674778846e4e9f94ba0371LabelCaption"
						}
					},
					"enabled": false
				},
				"parentName": "Tab7f3aebceTabLabelGridLayoutdf44f075",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "WatTripLeaderLastName4307e28c-f1b2-4a48-ad36-7712722c7403",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Tab7f3aebceTabLabelGridLayoutdf44f075"
					},
					"bindTo": "WatTripLeaderLastName",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatTripLeaderLastName4307e28cf1b24a48ad367712722c7403LabelCaption"
						}
					},
					"enabled": false
				},
				"parentName": "Tab7f3aebceTabLabelGridLayoutdf44f075",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "WatTripLeaderEmailefb6d706-1ae0-474d-9741-9d2ef1b7f34e",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 8,
						"row": 1,
						"layoutName": "Tab7f3aebceTabLabelGridLayoutdf44f075"
					},
					"bindTo": "WatTripLeaderEmail",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatTripLeaderEmailefb6d7061ae0474d97419d2ef1b7f34eLabelCaption"
						}
					},
					"enabled": true
				},
				"parentName": "Tab7f3aebceTabLabelGridLayoutdf44f075",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "Tab7f3aebceTabLabelGroupd56b6ed9",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab7f3aebceTabLabelGroupd56b6ed9GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tab7f3aebceTabLabel",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "Tab7f3aebceTabLabelGridLayout35ca6bc5",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tab7f3aebceTabLabelGroupd56b6ed9",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "BOOLEAN37fc409a-e16f-46c0-bc78-237d59bd9f9b",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tab7f3aebceTabLabelGridLayout35ca6bc5"
					},
					"bindTo": "WatAnvilReady",
					"enabled": true
				},
				"parentName": "Tab7f3aebceTabLabelGridLayout35ca6bc5",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "DATETIMEb1641f3d-2ef4-4c7c-ad1b-d539fa99dc75",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 8,
						"row": 0,
						"layoutName": "Tab7f3aebceTabLabelGridLayout35ca6bc5"
					},
					"bindTo": "WatAnvilReadyDate",
					"enabled": false
				},
				"parentName": "Tab7f3aebceTabLabelGridLayout35ca6bc5",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "BOOLEAN2626aed0-da25-4e32-b4d5-eb2ca56b41ab",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 16,
						"row": 0,
						"layoutName": "Tab7f3aebceTabLabelGridLayout35ca6bc5"
					},
					"bindTo": "WatAnvilExportRepeat",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.BOOLEAN2626aed0da254e32b4d5eb2ca56b41abLabelCaption"
						}
					},
					"tip": {
						"content": {
							"bindTo": "Resources.Strings.BOOLEAN2626aed0da254e32b4d5eb2ca56b41abTip"
						}
					},
					"enabled": true
				},
				"parentName": "Tab7f3aebceTabLabelGridLayout35ca6bc5",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "BOOLEANe688ee23-e785-4932-aa2d-58e4afdb43ba",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Tab7f3aebceTabLabelGridLayout35ca6bc5"
					},
					"bindTo": "WatEnrollInLEARN",
					"enabled": true
				},
				"parentName": "Tab7f3aebceTabLabelGridLayout35ca6bc5",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "DATETIME84fcbb9d-96c2-4368-bf26-a3d4e9c41415",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 8,
						"row": 1,
						"layoutName": "Tab7f3aebceTabLabelGridLayout35ca6bc5"
					},
					"bindTo": "WatLearnEnrollDate",
					"enabled": false
				},
				"parentName": "Tab7f3aebceTabLabelGridLayout35ca6bc5",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "WatSchemaTravelerDetail28c4522b",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab7f3aebceTabLabel",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "WatSchemaInactiveTravellersDetailcd0ddff6",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab7f3aebceTabLabel",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "Tab7f3aebceTabLabelGroup0c84abb1",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab7f3aebceTabLabelGroup0c84abb1GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tab7f3aebceTabLabel",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "Tab7f3aebceTabLabelGridLayout656d8b26",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tab7f3aebceTabLabelGroup0c84abb1",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Indicator4fcced61-7528-4f6d-9e07-fddd02c97128",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 3,
						"column": 0,
						"row": 0,
						"layoutName": "Tab7f3aebceTabLabelGridLayout656d8b26",
						"useFixedColumnHeight": true
					},
					"itemType": 4,
					"classes": {
						"wrapClassName": [
							"card-widget-grid-layout-item"
						]
					}
				},
				"parentName": "Tab7f3aebceTabLabelGridLayout656d8b26",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Indicator8614728a-21aa-4c82-b515-1d79de62474b",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 3,
						"column": 12,
						"row": 0,
						"layoutName": "Tab7f3aebceTabLabelGridLayout656d8b26",
						"useFixedColumnHeight": true
					},
					"itemType": 4,
					"classes": {
						"wrapClassName": [
							"card-widget-grid-layout-item"
						]
					}
				},
				"parentName": "Tab7f3aebceTabLabelGridLayout656d8b26",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Chartf560b333-b1db-41e5-8032-332c5fb7a018",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 12,
						"column": 0,
						"row": 3,
						"layoutName": "Tab7f3aebceTabLabelGridLayout656d8b26",
						"useFixedColumnHeight": true
					},
					"itemType": 4,
					"classes": {
						"wrapClassName": [
							"card-widget-grid-layout-item"
						]
					}
				},
				"parentName": "Tab7f3aebceTabLabelGridLayout656d8b26",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "Chart54edfd58-91eb-493f-b726-9c4264a2d453",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 12,
						"column": 8,
						"row": 3,
						"layoutName": "Tab7f3aebceTabLabelGridLayout656d8b26",
						"useFixedColumnHeight": true
					},
					"itemType": 4,
					"classes": {
						"wrapClassName": [
							"card-widget-grid-layout-item"
						]
					}
				},
				"parentName": "Tab7f3aebceTabLabelGridLayout656d8b26",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "Chartcd9eaebf-100e-47a4-8fc3-c030521952e4",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 12,
						"column": 16,
						"row": 3,
						"layoutName": "Tab7f3aebceTabLabelGridLayout656d8b26",
						"useFixedColumnHeight": true
					},
					"itemType": 4,
					"classes": {
						"wrapClassName": [
							"card-widget-grid-layout-item"
						]
					}
				},
				"parentName": "Tab7f3aebceTabLabelGridLayout656d8b26",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "WatSchema7713dc14Detailfd025518",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab7f3aebceTabLabel",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "Tab2454d764TabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab2454d764TabLabelTabCaption"
					},
					"items": [],
					"order": 2
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "Tab2454d764TabLabelGroup9568ab39",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab2454d764TabLabelGroup9568ab39GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tab2454d764TabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tab2454d764TabLabelGridLayoutc3b6761f",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tab2454d764TabLabelGroup9568ab39",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatDepartureDate114a139a-095b-4f80-80f2-3228ee55fb14",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tab2454d764TabLabelGridLayoutc3b6761f"
					},
					"bindTo": "WatDepartureDate",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatDepartureDate114a139a095b4f8080f23228ee55fb14LabelCaption"
						}
					},
					"enabled": true
				},
				"parentName": "Tab2454d764TabLabelGridLayoutc3b6761f",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatDestination502e7295-ec17-43f2-856a-d186cb11a896",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 8,
						"row": 0,
						"layoutName": "Tab2454d764TabLabelGridLayoutc3b6761f"
					},
					"bindTo": "WatDestination",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatDestination502e7295ec1743f2856ad186cb11a896LabelCaption"
						}
					},
					"enabled": true,
					"contentType": 5
				},
				"parentName": "Tab2454d764TabLabelGridLayoutc3b6761f",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "WatDestinationProvinceState11018c7b-78b2-4446-8cdb-c288ee49b05a",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Tab2454d764TabLabelGridLayoutc3b6761f"
					},
					"bindTo": "WatDestinationProvinceState",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatDestinationProvinceState11018c7b78b244468cdbc288ee49b05aLabelCaption"
						}
					},
					"enabled": true
				},
				"parentName": "Tab2454d764TabLabelGridLayoutc3b6761f",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "WatCityStatecc0529da-0bca-432c-b405-aa488b57926b",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 8,
						"row": 1,
						"layoutName": "Tab2454d764TabLabelGridLayoutc3b6761f"
					},
					"bindTo": "WatCityState",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatCityStatecc0529da0bca432cb405aa488b57926bLabelCaption"
						}
					},
					"enabled": true
				},
				"parentName": "Tab2454d764TabLabelGridLayoutc3b6761f",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "Tab2454d764TabLabelGroup644b6604",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab2454d764TabLabelGroup644b6604GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tab2454d764TabLabel",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Tab2454d764TabLabelGridLayout2d1cfbc7",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tab2454d764TabLabelGroup644b6604",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "DATETIME8fc337b3-e721-41c5-866e-9f9dc994197e",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tab2454d764TabLabelGridLayout2d1cfbc7"
					},
					"bindTo": "WatDepartureDateLeg2",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.DATETIME8fc337b3e72141c5866e9f9dc994197eLabelCaption"
						}
					},
					"enabled": true
				},
				"parentName": "Tab2454d764TabLabelGridLayout2d1cfbc7",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "LOOKUP0dc4af4f-8ac9-4a59-b8fd-e1c84bcd008d",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 8,
						"row": 0,
						"layoutName": "Tab2454d764TabLabelGridLayout2d1cfbc7"
					},
					"bindTo": "WatDestinationCountryLeg2",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.LOOKUP0dc4af4f8ac94a59b8fde1c84bcd008dLabelCaption"
						}
					},
					"enabled": true,
					"contentType": 5
				},
				"parentName": "Tab2454d764TabLabelGridLayout2d1cfbc7",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "STRING347f26c3-379b-4c2f-8372-2b297e891e49",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Tab2454d764TabLabelGridLayout2d1cfbc7"
					},
					"bindTo": "WatDestinationProvinceStateLeg2",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.STRING347f26c3379b4c2f83722b297e891e49LabelCaption"
						}
					},
					"enabled": true
				},
				"parentName": "Tab2454d764TabLabelGridLayout2d1cfbc7",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "STRING7288d3ca-8a20-453d-a39f-268db10aee56",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 8,
						"row": 1,
						"layoutName": "Tab2454d764TabLabelGridLayout2d1cfbc7"
					},
					"bindTo": "WatDestinationCityLeg2",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.STRING7288d3ca8a20453da39f268db10aee56LabelCaption"
						}
					},
					"enabled": true
				},
				"parentName": "Tab2454d764TabLabelGridLayout2d1cfbc7",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "Tab2454d764TabLabelGrouped55a963",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab2454d764TabLabelGrouped55a963GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tab2454d764TabLabel",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "Tab2454d764TabLabelGridLayout18bd53ff",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tab2454d764TabLabelGrouped55a963",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatDepartureDateLeg3383c6c61-119f-4baa-93d8-7c357498f66f",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tab2454d764TabLabelGridLayout18bd53ff"
					},
					"bindTo": "WatDepartureDateLeg3",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatDepartureDateLeg3383c6c61119f4baa93d87c357498f66fLabelCaption"
						}
					},
					"enabled": true
				},
				"parentName": "Tab2454d764TabLabelGridLayout18bd53ff",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "LOOKUP9770c86e-d160-43d3-a18a-a836087bf12b",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 8,
						"row": 0,
						"layoutName": "Tab2454d764TabLabelGridLayout18bd53ff"
					},
					"bindTo": "WatDestinationCountryLeg3",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.LOOKUP9770c86ed16043d3a18aa836087bf12bLabelCaption"
						}
					},
					"enabled": true,
					"contentType": 5
				},
				"parentName": "Tab2454d764TabLabelGridLayout18bd53ff",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "STRING477a1404-88a0-481e-8a94-c32f860de454",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Tab2454d764TabLabelGridLayout18bd53ff"
					},
					"bindTo": "WatDestinationProvinceStateLeg3",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.STRING477a140488a0481e8a94c32f860de454LabelCaption"
						}
					},
					"enabled": true
				},
				"parentName": "Tab2454d764TabLabelGridLayout18bd53ff",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "STRINGcd3e7234-4964-4566-a0df-be8b69a74cf7",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 8,
						"row": 1,
						"layoutName": "Tab2454d764TabLabelGridLayout18bd53ff"
					},
					"bindTo": "WatDestinationCityLeg3",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.STRINGcd3e723449644566a0dfbe8b69a74cf7LabelCaption"
						}
					},
					"enabled": true
				},
				"parentName": "Tab2454d764TabLabelGridLayout18bd53ff",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "Tab2454d764TabLabelGroupb0162ed9",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab2454d764TabLabelGroupb0162ed9GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tab2454d764TabLabel",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "Tab2454d764TabLabelGridLayoutc6c41d55",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tab2454d764TabLabelGroupb0162ed9",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "DATETIME3ac0032a-564b-4263-8b68-429aed63fdbe",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tab2454d764TabLabelGridLayoutc6c41d55"
					},
					"bindTo": "WatDepartureDateLeg4",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.DATETIME3ac0032a564b42638b68429aed63fdbeLabelCaption"
						}
					},
					"enabled": true
				},
				"parentName": "Tab2454d764TabLabelGridLayoutc6c41d55",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "LOOKUP652b0a30-ee75-4f6c-8323-3ee443dc67b8",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 8,
						"row": 0,
						"layoutName": "Tab2454d764TabLabelGridLayoutc6c41d55"
					},
					"bindTo": "WatDestinationCountryLeg4",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.LOOKUP652b0a30ee754f6c83233ee443dc67b8LabelCaption"
						}
					},
					"enabled": true,
					"contentType": 5
				},
				"parentName": "Tab2454d764TabLabelGridLayoutc6c41d55",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "STRINGd4976c67-ed0f-4c39-b87b-6f8888e2663e",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Tab2454d764TabLabelGridLayoutc6c41d55"
					},
					"bindTo": "WatDestinationProvinceStateLeg4",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.STRINGd4976c67ed0f4c39b87b6f8888e2663eLabelCaption"
						}
					},
					"enabled": true
				},
				"parentName": "Tab2454d764TabLabelGridLayoutc6c41d55",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "STRING185312f5-4f97-4b8d-a996-1c587efac022",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 8,
						"row": 1,
						"layoutName": "Tab2454d764TabLabelGridLayoutc6c41d55"
					},
					"bindTo": "WatDestinationCityLeg4",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.STRING185312f54f974b8da9961c587efac022LabelCaption"
						}
					},
					"enabled": true
				},
				"parentName": "Tab2454d764TabLabelGridLayoutc6c41d55",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "Tab2454d764TabLabelGroup3d9d65ee",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab2454d764TabLabelGroup3d9d65eeGroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tab2454d764TabLabel",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "Tab2454d764TabLabelGridLayoutcc7dde04",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tab2454d764TabLabelGroup3d9d65ee",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "DATETIMEf21ecf66-cbd1-4b96-9abb-c7828681bcd7",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tab2454d764TabLabelGridLayoutcc7dde04"
					},
					"bindTo": "WatDepartureDateLeg5",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.DATETIMEf21ecf66cbd14b969abbc7828681bcd7LabelCaption"
						}
					},
					"enabled": true
				},
				"parentName": "Tab2454d764TabLabelGridLayoutcc7dde04",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "LOOKUPab5d597a-40e5-4890-9f48-0f8119fde8ee",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 8,
						"row": 0,
						"layoutName": "Tab2454d764TabLabelGridLayoutcc7dde04"
					},
					"bindTo": "WatDestinationCountryLeg5",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.LOOKUPab5d597a40e548909f480f8119fde8eeLabelCaption"
						}
					},
					"enabled": true,
					"contentType": 5
				},
				"parentName": "Tab2454d764TabLabelGridLayoutcc7dde04",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "STRINGe2f12cfb-f0b6-4f13-9a55-a0373f1d5442",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Tab2454d764TabLabelGridLayoutcc7dde04"
					},
					"bindTo": "WatDestinationProvinceStateLeg5",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.STRINGe2f12cfbf0b64f139a55a0373f1d5442LabelCaption"
						}
					},
					"enabled": true
				},
				"parentName": "Tab2454d764TabLabelGridLayoutcc7dde04",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "STRING4c93e366-cb25-4004-aac4-670f98a4ea25",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 8,
						"row": 1,
						"layoutName": "Tab2454d764TabLabelGridLayoutcc7dde04"
					},
					"bindTo": "WatDestinationCityLeg5",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.STRING4c93e366cb254004aac4670f98a4ea25LabelCaption"
						}
					},
					"enabled": true
				},
				"parentName": "Tab2454d764TabLabelGridLayoutcc7dde04",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "Tab2454d764TabLabelGroup459f6448",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab2454d764TabLabelGroup459f6448GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tab2454d764TabLabel",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "Tab2454d764TabLabelGridLayout80206229",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tab2454d764TabLabelGroup459f6448",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatReturnDate774cf1f5-e5b9-4f7e-a292-4fb7c26f4e45",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tab2454d764TabLabelGridLayout80206229"
					},
					"bindTo": "WatReturnDate",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatReturnDate774cf1f5e5b94f7ea2924fb7c26f4e45LabelCaption"
						}
					},
					"enabled": true
				},
				"parentName": "Tab2454d764TabLabelGridLayout80206229",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tabf11067fcTabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tabf11067fcTabLabelTabCaption"
					},
					"items": [],
					"order": 3
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "Tabf11067fcTabLabelGroup8d3df29f",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tabf11067fcTabLabelGroup8d3df29fGroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tabf11067fcTabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tabf11067fcTabLabelGridLayoutcd4dea81",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tabf11067fcTabLabelGroup8d3df29f",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatArrived1e99a0ec1-ac7b-45d0-a9b6-6a89e3ed8358",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tabf11067fcTabLabelGridLayoutcd4dea81"
					},
					"bindTo": "WatArrived1",
					"enabled": false,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatArrived1e99a0ec1ac7b45d0a9b66a89e3ed8358LabelCaption"
						}
					}
				},
				"parentName": "Tabf11067fcTabLabelGridLayoutcd4dea81",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatSchemaArrivalNoticeDetail36dfc57e",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tabf11067fcTabLabel",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Tab3b689c04TabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab3b689c04TabLabelTabCaption"
					},
					"items": [],
					"order": 4
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "WatSchemaCustomEmailCallDetail1fec42d5",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab3b689c04TabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatSchemaCustomTaskDetailb4ba0d40",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab3b689c04TabLabel",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "NotesAndFilesTab",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.NotesAndFilesTabCaption"
					},
					"items": [],
					"order": 5
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "DocumentDetailV2ed4560df",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Files",
				"values": {
					"itemType": 2
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "NotesControlGroup",
				"values": {
					"itemType": 15,
					"caption": {
						"bindTo": "Resources.Strings.NotesGroupCaption"
					},
					"items": []
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "Notes",
				"values": {
					"bindTo": "WatNotes",
					"dataValueType": 1,
					"contentType": 4,
					"layout": {
						"column": 0,
						"row": 0,
						"colSpan": 24
					},
					"labelConfig": {
						"visible": false
					},
					"controlConfig": {
						"imageLoaded": {
							"bindTo": "insertImagesToNotes"
						},
						"images": {
							"bindTo": "NotesImagesCollection"
						}
					}
				},
				"parentName": "NotesControlGroup",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "merge",
				"name": "ESNTab",
				"values": {
					"order": 6
				}
			},
			{
				"operation": "insert",
				"name": "WatSchema94c7e3c8Detaile7f71f28",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "ESNTab",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tab2bb20961TabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab2bb20961TabLabelTabCaption"
					},
					"items": [],
					"order": 7
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "Tab2bb20961TabLabelGroup849b1675",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab2bb20961TabLabelGroup849b1675GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tab2bb20961TabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tab2bb20961TabLabelGridLayout4a414e07",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tab2bb20961TabLabelGroup849b1675",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatFirstName24923483-8c95-49e5-8630-15ed70b0e375",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatFirstName",
					"enabled": true,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatFirstName249234838c9549e5863015ed70b0e375LabelCaption"
						}
					}
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "STRING6b9f3683-79ab-4e9a-823e-dfc8f5f26eb5",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatWaterlooContact",
					"enabled": true,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.STRING6b9f368379ab4e9a823edfc8f5f26eb5LabelCaption"
						}
					}
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "WatLastName593b86e5-ab59-4ff9-ae98-6e2945f1a689",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatLastName",
					"enabled": true,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatLastName593b86e5ab594ff9ae986e2945f1a689LabelCaption"
						}
					}
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "WatWaterlooContactEmailAddress4bf6c2ef-d86f-41b1-a3ec-29121518bbd3",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatWaterlooContactEmailAddress",
					"enabled": true,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatWaterlooContactEmailAddress4bf6c2efd86f41b1a3ec29121518bbd3LabelCaption"
						}
					}
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "WatWatIAMId62172d55-887d-4fe8-96b7-714cb9ed3bb4",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatWatIAMId",
					"enabled": true,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatWatIAMId62172d55887d4fe896b7714cb9ed3bb4LabelCaption"
						}
					}
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "WatTravelerTypeb081d78b-4d68-4dd5-8745-2c02590b0526",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 2,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatTravelerType",
					"enabled": true,
					"contentType": 3,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatTravelerTypeb081d78b4d684dd587452c02590b0526LabelCaption"
						}
					}
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "WatRequesterEmailbeb0f1aa-2a3e-485e-b6b6-ce83a522d0ac",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatRequesterEmail",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatRequesterEmailbeb0f1aa2a3e485eb6b6ce83a522d0acLabelCaption"
						}
					},
					"enabled": true
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "STRING2b798158-8ec5-49ec-bede-9a461d1d62fa",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 3,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatTravellerFirstName",
					"enabled": true,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.STRING2b7981588ec549ecbede9a461d1d62faLabelCaption"
						}
					}
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "STRINGb4b64f23-f05f-4158-8ca1-59c97cabd47e",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatPhoneRequester",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.STRINGb4b64f23f05f41588ca159c97cabd47eLabelCaption"
						}
					},
					"enabled": true
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 8
			},
			{
				"operation": "insert",
				"name": "STRING660c505c-37d7-44d5-8aff-ae73e60e229c",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 4,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatTravellerLastName",
					"enabled": true,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.STRING660c505c37d744d58affae73e60e229cLabelCaption"
						}
					}
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 9
			},
			{
				"operation": "insert",
				"name": "STRING39f246c9-827b-4381-ba13-dfba829c490e",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 5,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatTripLeaderFirstName",
					"enabled": true,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.STRING39f246c9827b4381ba13dfba829c490eLabelCaption"
						}
					}
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 10
			},
			{
				"operation": "insert",
				"name": "STRINGe1e4c692-0c1a-4ed3-875a-d9e1711b4adf",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 5,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatTravellerUWEmail",
					"enabled": true,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.STRINGe1e4c6920c1a4ed3875ad9e1711b4adfLabelCaption"
						}
					}
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 11
			},
			{
				"operation": "insert",
				"name": "STRINGcd927ed7-5036-4edf-ab36-978d31b4394d",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 6,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatTripLeaderLastName",
					"enabled": true,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.STRINGcd927ed750364edfab36978d31b4394dLabelCaption"
						}
					}
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 12
			},
			{
				"operation": "insert",
				"name": "STRING1666570e-c695-46ac-b7a6-d221437fe689",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 6,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatTravellerWatIAMID",
					"enabled": true,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.STRING1666570ec69546acb7a6d221437fe689LabelCaption"
						}
					}
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 13
			},
			{
				"operation": "insert",
				"name": "STRINGe006cd6e-746d-4d75-b368-5b390c345458",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 7,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatTripLeaderEmail",
					"enabled": true,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.STRINGe006cd6e746d4d75b3685b390c345458LabelCaption"
						}
					}
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 14
			},
			{
				"operation": "insert",
				"name": "WatStudentNumber9f1720ac-86dd-4043-a478-76d7bf906322",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 7,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatStudentNumber",
					"enabled": true,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatStudentNumber9f1720ac86dd4043a47876d7bf906322LabelCaption"
						}
					}
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 15
			},
			{
				"operation": "insert",
				"name": "STRING08595e71-2ebb-4289-a24a-ff94cf3de377",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 8,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatTripLeaderWatIAMID",
					"enabled": true,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.STRING08595e712ebb4289a24aff94cf3de377LabelCaption"
						}
					}
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 16
			},
			{
				"operation": "insert",
				"name": "WatStudentTypef1de0b80-5673-4435-baa6-284e19d4911b",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 8,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatStudentType",
					"enabled": true,
					"contentType": 3,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatStudentTypef1de0b8056734435baa6284e19d4911bLabelCaption"
						}
					}
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 17
			},
			{
				"operation": "insert",
				"name": "DATETIME43e85212-d4d5-427b-8deb-c9b2831790f5",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 9,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatSubmittedOn",
					"enabled": true,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.DATETIME43e85212d4d5427b8debc9b2831790f5LabelCaption"
						}
					}
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 18
			},
			{
				"operation": "insert",
				"name": "WatFaculty3e0d2331-90cd-4f63-8961-a5c9030e6bed",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 9,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatFaculty",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatFaculty3e0d233190cd4f638961a5c9030e6bedLabelCaption"
						}
					},
					"enabled": true,
					"contentType": 5
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 19
			},
			{
				"operation": "insert",
				"name": "STRING4e203ebd-b6bd-449d-8602-ec4cfcae9b82",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 10,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatFullNameMainContact",
					"enabled": true,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.STRING4e203ebdb6bd449d8602ec4cfcae9b82LabelCaption"
						}
					}
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 20
			},
			{
				"operation": "insert",
				"name": "LOOKUPa4a7a740-efef-4c99-b9e3-10b0140e7cd5",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 10,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatArts",
					"enabled": true,
					"contentType": 3,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.LOOKUPa4a7a740efef4c99b9e310b0140e7cd5LabelCaption"
						}
					}
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 21
			},
			{
				"operation": "insert",
				"name": "STRINGe2748df7-469c-4ecd-a119-e91cf08c43d0",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 11,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatEmailGroupMainContact",
					"enabled": true,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.STRINGe2748df7469c4ecda119e91cf08c43d0LabelCaption"
						}
					}
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 22
			},
			{
				"operation": "insert",
				"name": "LOOKUPfd60f378-784b-4611-bc30-c7928694b1af",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 11,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatEngineering",
					"enabled": true,
					"contentType": 3,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.LOOKUPfd60f378784b4611bc30c7928694b1afLabelCaption"
						}
					}
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 23
			},
			{
				"operation": "insert",
				"name": "WatPurposeOfTravel86af30de-e983-4214-8b90-be5757a1057f",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 12,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatPurposeOfTravel",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatPurposeOfTravel86af30dee98342148b90be5757a1057fLabelCaption"
						}
					},
					"enabled": true,
					"contentType": 5
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 24
			},
			{
				"operation": "insert",
				"name": "LOOKUP1698b3d4-4f54-493a-a32c-2592d96b2c43",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 12,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatEnvironment",
					"enabled": true,
					"contentType": 3,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.LOOKUP1698b3d44f54493aa32c2592d96b2c43LabelCaption"
						}
					}
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 25
			},
			{
				"operation": "insert",
				"name": "WatHighRiskDestination7022b80e-91d0-44e0-baf3-b84ecf1b6705",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 13,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatHighRiskDestination",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatHighRiskDestination7022b80e91d044e0baf3b84ecf1b6705LabelCaption"
						}
					},
					"enabled": true
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 26
			},
			{
				"operation": "insert",
				"name": "LOOKUPacd1cce3-8411-4443-9c3c-64ecb983ea0c",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 13,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatHealth",
					"enabled": true,
					"contentType": 5,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.LOOKUPacd1cce3841144439c3c64ecb983ea0cLabelCaption"
						}
					}
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 27
			},
			{
				"operation": "insert",
				"name": "INTEGER96dc37e9-ad67-412c-8b94-b9d0dbf9c445",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 14,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatSID",
					"enabled": true,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.INTEGER96dc37e9ad67412c8b94b9d0dbf9c445LabelCaption"
						}
					}
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 28
			},
			{
				"operation": "insert",
				"name": "LOOKUPa10bc89f-24b1-4b31-9b85-9c16729fa1e9",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 14,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatMath",
					"enabled": true,
					"contentType": 3,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.LOOKUPa10bc89f24b14b319b859c16729fa1e9LabelCaption"
						}
					}
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 29
			},
			{
				"operation": "insert",
				"name": "STRING8f0c3631-efb1-4488-83a6-48a2df1a1637",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 15,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatCountryString1",
					"enabled": true
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 30
			},
			{
				"operation": "insert",
				"name": "LOOKUPf0864d3f-2c29-433b-979e-8da3e2df5c79",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 15,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatScience",
					"enabled": true,
					"contentType": 3,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.LOOKUPf0864d3f2c29433b979e8da3e2df5c79LabelCaption"
						}
					}
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 31
			},
			{
				"operation": "insert",
				"name": "WatCityState0d4fd57b-84c8-4ba6-9556-c496742db653",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 16,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatCityState"
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 32
			},
			{
				"operation": "insert",
				"name": "LOOKUP01627185-193e-4186-9160-ea5429db00f5",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 16,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatCentresAndInstitutes",
					"enabled": true,
					"contentType": 3,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.LOOKUP01627185193e41869160ea5429db00f5LabelCaption"
						}
					}
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 33
			},
			{
				"operation": "insert",
				"name": "WatDestinationProvinceStateddc42583-0606-4cd7-8c38-2579393670a9",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 17,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatDestinationProvinceState"
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 34
			},
			{
				"operation": "insert",
				"name": "LOOKUP67a39ba6-acbe-4448-961b-d199656f808f",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 17,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatAcademicSupportUnit",
					"enabled": true,
					"contentType": 3,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.LOOKUP67a39ba6acbe4448961bd199656f808fLabelCaption"
						}
					}
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 35
			},
			{
				"operation": "insert",
				"name": "STRING3e4bb04c-892f-47ba-a6f1-3beb2396f1f4",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 18,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatCountryString2",
					"enabled": true
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 36
			},
			{
				"operation": "insert",
				"name": "BOOLEAN45b5c38c-8691-4116-80a2-dcbd5f2989d6",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 18,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatMultiDestination",
					"enabled": true,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.BOOLEAN45b5c38c8691411680a2dcbd5f2989d6LabelCaption"
						}
					}
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 37
			},
			{
				"operation": "insert",
				"name": "WatDestinationCityLeg286339398-1ffa-4155-8fe9-46630c366a78",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 19,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatDestinationCityLeg2"
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 38
			},
			{
				"operation": "insert",
				"name": "STRING880619ad-cb91-49bb-a85b-9d16c3ea1b18",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 19,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatMultiDestinationDetails",
					"enabled": true
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 39
			},
			{
				"operation": "insert",
				"name": "WatDestinationProvinceStateLeg2eb7784dd-f6b0-4c69-aae0-3449b1d8c7a0",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 20,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatDestinationProvinceStateLeg2"
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 40
			},
			{
				"operation": "insert",
				"name": "WatDepartureDate95646411-0bf5-42fe-8214-14ee89607fcf",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 20,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatDepartureDate"
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 41
			},
			{
				"operation": "insert",
				"name": "STRING00b72093-72fa-468f-8c2e-bf79f637e63e",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 21,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatCountryString3",
					"enabled": true
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 42
			},
			{
				"operation": "insert",
				"name": "WatDepartureDateLeg2f98313bc-0597-4a66-88de-e136c6a28aba",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 21,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatDepartureDateLeg2",
					"enabled": true
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 43
			},
			{
				"operation": "insert",
				"name": "WatDestinationCityLeg3dd04c63c-479d-4a0d-8a7a-949057bd4265",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 22,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatDestinationCityLeg3"
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 44
			},
			{
				"operation": "insert",
				"name": "WatDepartureDateLeg3323dcfd5-e318-470f-9f4f-806c58b3909c",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 22,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatDepartureDateLeg3",
					"enabled": true
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 45
			},
			{
				"operation": "insert",
				"name": "WatDestinationProvinceStateLeg3ec9d8c9c-f651-46a7-8e0a-0e4dac89e726",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 23,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatDestinationProvinceStateLeg3"
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 46
			},
			{
				"operation": "insert",
				"name": "WatDepartureDateLeg4794097a5-add5-4f6c-9e1f-24b88f2f7bc7",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 23,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatDepartureDateLeg4",
					"enabled": true
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 47
			},
			{
				"operation": "insert",
				"name": "STRINGfa3d2be8-c58f-4c23-b612-46d148c7ea4e",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 24,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatCountryString4",
					"enabled": true
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 48
			},
			{
				"operation": "insert",
				"name": "WatDepartureDateLeg5e8b9fa91-eede-441f-b352-5ea4fef8f468",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 24,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatDepartureDateLeg5",
					"enabled": true
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 49
			},
			{
				"operation": "insert",
				"name": "WatDestinationCityLeg411e4d386-7719-46a6-991b-b79f65180769",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 25,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatDestinationCityLeg4"
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 50
			},
			{
				"operation": "insert",
				"name": "WatReturnDatece192c50-3939-4634-a201-67ca818bb4af",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 25,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatReturnDate"
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 51
			},
			{
				"operation": "insert",
				"name": "WatDestinationProvinceStateLeg4d8872054-d54c-43a8-9eb4-840676eee45b",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 26,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatDestinationProvinceStateLeg4"
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 52
			},
			{
				"operation": "insert",
				"name": "WatStudentTypeStringe9fa0e10-7d56-4184-9489-4443a5b9e1b1",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 26,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatStudentTypeString"
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 53
			},
			{
				"operation": "insert",
				"name": "STRING8b5d3acc-73ce-420a-9156-db7b360a441a",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 27,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatCountryString5",
					"enabled": true
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 54
			},
			{
				"operation": "insert",
				"name": "WatDestinationCityLeg593aaf417-340d-45d2-8202-a3b03bd83aa6",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 28,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatDestinationCityLeg5"
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 55
			},
			{
				"operation": "insert",
				"name": "WatDestinationProvinceStateLeg5b691dbeb-7e05-4e13-a7ec-7f473c176102",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 29,
						"layoutName": "Tab2bb20961TabLabelGridLayout4a414e07"
					},
					"bindTo": "WatDestinationProvinceStateLeg5"
				},
				"parentName": "Tab2bb20961TabLabelGridLayout4a414e07",
				"propertyName": "items",
				"index": 56
			},
			{
				"operation": "insert",
				"name": "Tab4456fab4TabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab4456fab4TabLabelTabCaption"
					},
					"items": [],
					"order": 8
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 8
			},
			{
				"operation": "insert",
				"name": "Tab4456fab4TabLabelGroup7cd4dfa0",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab4456fab4TabLabelGroup7cd4dfa0GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tab4456fab4TabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tab4456fab4TabLabelGridLayout8bf2a782",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tab4456fab4TabLabelGroup7cd4dfa0",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatLearnEnrollDateb483cacd-4d8c-49d8-b8f1-4abf98bf2eb4",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tab4456fab4TabLabelGridLayout8bf2a782"
					},
					"bindTo": "WatLearnEnrollDate"
				},
				"parentName": "Tab4456fab4TabLabelGridLayout8bf2a782",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatTripStage77b29282-1ee8-44f2-8751-7f7c49b9e529",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "Tab4456fab4TabLabelGridLayout8bf2a782"
					},
					"bindTo": "WatTripStage",
					"enabled": true,
					"contentType": 3
				},
				"parentName": "Tab4456fab4TabLabelGridLayout8bf2a782",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "WatAnvilReadyDate5056a56c-9c26-45aa-81a5-3a15dc055a62",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Tab4456fab4TabLabelGridLayout8bf2a782"
					},
					"bindTo": "WatAnvilReadyDate"
				},
				"parentName": "Tab4456fab4TabLabelGridLayout8bf2a782",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "WatTripStatus5229dd6e-feb9-47c7-9220-e3ff06fe24c2",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "Tab4456fab4TabLabelGridLayout8bf2a782"
					},
					"bindTo": "WatTripStatus",
					"enabled": true,
					"contentType": 3
				},
				"parentName": "Tab4456fab4TabLabelGridLayout8bf2a782",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "WatSysFieldTriggerPNRae336ebf-ed91-4b26-8187-9110609b56fe",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Tab4456fab4TabLabelGridLayout8bf2a782"
					},
					"bindTo": "WatSysFieldTriggerPNR",
					"enabled": false
				},
				"parentName": "Tab4456fab4TabLabelGridLayout8bf2a782",
				"propertyName": "items",
				"index": 4
			}
		]/**SCHEMA_DIFF*/
	};
});
