define("WatSchemaCustomEmailCallDetail", [], function() {
	return {
		entitySchemaName: "Activity",
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		diff: /**SCHEMA_DIFF*/[]/**SCHEMA_DIFF*/,
		methods: {
			getFilters: function() {
				
				var filterGroup = this.callParent(arguments); //constant
				var additionalFilterGroup = this.Ext.create("Terrasoft.FilterGroup");//constant
				additionalFilterGroup.logicalOperation = this.Terrasoft.LogicalOperatorType.OR; 
				//only if or between extra conditions
				additionalFilterGroup.add("DocumentAFilter", this.Terrasoft.createColumnFilterWithParameter(
					this.Terrasoft.ComparisonType.EQUAL, "Type", "e2831dec-cfc0-df11-b00f-001d60e938c6"));
				additionalFilterGroup.add("DocumentBFilter", this.Terrasoft.createColumnFilterWithParameter(
					this.Terrasoft.ComparisonType.EQUAL, "ActivityCategory", "03df85bf-6b19-4dea-8463-d5d49b80bb28"));
				additionalFilterGroup.add("DocumentCFilter", this.Terrasoft.createColumnFilterWithParameter(
					this.Terrasoft.ComparisonType.EQUAL, "Type", "e1831dec-cfc0-df11-b00f-001d60e938c6"));
					
				//DocumentAFilter - is the name of the filter. should be unique for each filter
					
				filterGroup.add("ExtraFilter", additionalFilterGroup); //constant
				return  filterGroup;
				
			}
		}
	};
});
