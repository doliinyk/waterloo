define("WatTraveler1Page", [], function() {
	return {
		entitySchemaName: "WatTraveler",
		attributes: {},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{
			"Files": {
				"schemaName": "FileDetailV2",
				"entitySchemaName": "WatTravelerFile",
				"filter": {
					"masterColumn": "Id",
					"detailColumn": "WatTraveler"
				}
			},
			"WatSchemaArrivalNoticeDetail7dbedaa5": {
				"schemaName": "WatSchemaArrivalNoticeDetail",
				"entitySchemaName": "WatArrivalNotice",
				"filter": {
					"detailColumn": "WatTraveler",
					"masterColumn": "Id"
				}
			},
			"WatSchema7713dc14Detailf663eee8": {
				"schemaName": "WatSchema7713dc14Detail",
				"entitySchemaName": "WatAssociatedTraveler",
				"filter": {
					"detailColumn": "WatTraveler",
					"masterColumn": "Id"
				}
			},
			"WatSchemaf2370fffDetaile516e8bc": {
				"schemaName": "WatSchemaf2370fffDetail",
				"entitySchemaName": "WatLearnHistory",
				"filter": {
					"detailColumn": "WatTraveller",
					"masterColumn": "Id"
				}
			},
			"DocumentDetailV2604a8e8e": {
				"schemaName": "DocumentDetailV2",
				"entitySchemaName": "Document",
				"filter": {
					"detailColumn": "WatTraveller",
					"masterColumn": "Id"
				}
			},
			"WatSchema94c7e3c8Detail313443d3": {
				"schemaName": "WatSchema94c7e3c8Detail",
				"entitySchemaName": "WatDatedNotes",
				"filter": {
					"detailColumn": "WatTraveler",
					"masterColumn": "Id"
				}
			},
			"WatSchemaLearnRegistrationDetail9f7fcbca": {
				"schemaName": "WatSchemaLearnRegistrationDetail",
				"entitySchemaName": "WatLEARNRegistration",
				"filter": {
					"detailColumn": "WatContact",
					"masterColumn": "WatContact"
				}
			},
			"WatSchemaCustomEmailCallDetail56db1802": {
				"schemaName": "WatSchemaCustomEmailCallDetail",
				"entitySchemaName": "Activity",
				"filter": {
					"detailColumn": "WatTraveller",
					"masterColumn": "Id"
				}
			},
			"WatSchemaCustomTaskDetail8ba819a4": {
				"schemaName": "WatSchemaCustomTaskDetail",
				"entitySchemaName": "Activity",
				"filter": {
					"detailColumn": "WatTraveller",
					"masterColumn": "Id"
				}
			}
		}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{
			"WatTravelerPhone": {
				"cb41b6a4-5a50-4bac-a6ec-f2f16b876a66": {
					"uId": "cb41b6a4-5a50-4bac-a6ec-f2f16b876a66",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatIsLeader"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"WatGroupTravelerType": {
				"1f9529f1-274d-4638-b7ec-12bff5fbff1e": {
					"uId": "1f9529f1-274d-4638-b7ec-12bff5fbff1e",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatTrip",
								"attributePath": "WatTravelerType"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ec633e9-6646-4d08-a0bd-dbb211cd5711",
								"dataValueType": 10
							}
						}
					]
				},
				"8f896d51-1960-43fa-b627-b2759534f51c": {
					"uId": "8f896d51-1960-43fa-b627-b2759534f51c",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatTrip",
								"attributePath": "WatTravelerType"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ec633e9-6646-4d08-a0bd-dbb211cd5711",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"WatStudentStatus": {
				"e3c9a36b-c197-462b-9d6d-3e4268722397": {
					"uId": "e3c9a36b-c197-462b-9d6d-3e4268722397",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatTrip",
								"attributePath": "WatTravelerType"
							},
							"rightExpression": {
								"type": 0,
								"value": "fc07d4ad-98b0-4865-9292-fcfc564d2e91",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatGroupTravelerType"
							},
							"rightExpression": {
								"type": 0,
								"value": "d8258a6d-5b30-4a55-9a43-fdcc133de485",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"WatPreDepartureWarning": {
				"3fae0214-b46a-4845-a112-ebc2de5a9bec": {
					"uId": "3fae0214-b46a-4845-a112-ebc2de5a9bec",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "WatPreDepartureWarning"
							}
						}
					]
				}
			},
			"WatActiveTraveller": {
				"dc036231-12b3-406b-abe7-404a84ec3de0": {
					"uId": "dc036231-12b3-406b-abe7-404a84ec3de0",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatTrip",
								"attributePath": "WatTravelerType"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ec633e9-6646-4d08-a0bd-dbb211cd5711",
								"dataValueType": 10
							}
						}
					]
				},
				"09424c32-d82a-4377-9fd5-6ee27ecacd3d": {
					"uId": "09424c32-d82a-4377-9fd5-6ee27ecacd3d",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatTrip",
								"attributePath": "WatTravelerType"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ec633e9-6646-4d08-a0bd-dbb211cd5711",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"WatContact": {
				"b6239e54-9c4f-4477-a931-8f671311c3f9": {
					"uId": "b6239e54-9c4f-4477-a931-8f671311c3f9",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "CreatedOn"
							}
						},
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "WatWatIAMId"
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatTrip",
								"attributePath": "WatTravelerType"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ec633e9-6646-4d08-a0bd-dbb211cd5711",
								"dataValueType": 10
							}
						}
					]
				},
				"3f6b2454-2e9d-4103-8ae3-c78cd2fa71b8": {
					"uId": "3f6b2454-2e9d-4103-8ae3-c78cd2fa71b8",
					"enabled": true,
					"removed": false,
					"ruleType": 1,
					"baseAttributePatch": "Type",
					"comparisonType": 3,
					"autoClean": false,
					"autocomplete": false,
					"type": 0,
					"value": "00783ef6-f36b-1410-a883-16d83cab0980",
					"dataValueType": 10
				}
			},
			"WatWatIAMId": {
				"6f63080d-2ed4-4710-b010-b915d0fd7737": {
					"uId": "6f63080d-2ed4-4710-b010-b915d0fd7737",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "CreatedOn"
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatTrip",
								"attributePath": "WatTravelerType"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ec633e9-6646-4d08-a0bd-dbb211cd5711",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "WatContact"
							}
						}
					]
				},
				"74180075-81de-4efa-a1c3-698d6129cf3e": {
					"uId": "74180075-81de-4efa-a1c3-698d6129cf3e",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatTrip",
								"attributePath": "WatTravelerType"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ec633e9-6646-4d08-a0bd-dbb211cd5711",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "WatContact"
							}
						},
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "CreatedOn"
							}
						}
					]
				}
			},
			"WatEnrollLearn": {
				"1f20bc79-c905-4f64-9145-c472e784ab6d": {
					"uId": "1f20bc79-c905-4f64-9145-c472e784ab6d",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "WatLEARNEnrollDate"
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatLEARNError"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"WatTravelerFirstName": {
				"ed618a4d-8b7f-4aea-b507-3c03c585d063": {
					"uId": "ed618a4d-8b7f-4aea-b507-3c03c585d063",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "WatContact"
							}
						},
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "CreatedOn"
							}
						}
					]
				},
				"b8ba02f3-68a1-4dbc-bbca-6b22aa1df8db": {
					"uId": "b8ba02f3-68a1-4dbc-bbca-6b22aa1df8db",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatTrip",
								"attributePath": "WatTravelerType"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ec633e9-6646-4d08-a0bd-dbb211cd5711",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "WatContact"
							}
						},
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "CreatedOn"
							}
						}
					]
				}
			},
			"WatTravelerLastName": {
				"4d00e2a6-98ef-4090-a804-3fdd21d2affa": {
					"uId": "4d00e2a6-98ef-4090-a804-3fdd21d2affa",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "WatContact"
							}
						},
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "CreatedOn"
							}
						}
					]
				},
				"ef345384-e2b0-4a77-a7ba-cbc1e6a4ab23": {
					"uId": "ef345384-e2b0-4a77-a7ba-cbc1e6a4ab23",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatTrip",
								"attributePath": "WatTravelerType"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ec633e9-6646-4d08-a0bd-dbb211cd5711",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "WatContact"
							}
						},
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "CreatedOn"
							}
						}
					]
				}
			},
			"WatTravelerEmail": {
				"935019b5-71fa-495d-9402-d1ed98541a58": {
					"uId": "935019b5-71fa-495d-9402-d1ed98541a58",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "WatContact"
							}
						}
					]
				}
			},
			"NotesControlGroup": {
				"4cbcdd64-82cd-4b6d-9577-962168eb5b47": {
					"uId": "4cbcdd64-82cd-4b6d-9577-962168eb5b47",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 0,
								"value": 1,
								"dataValueType": 4
							},
							"rightExpression": {
								"type": 0,
								"value": 2,
								"dataValueType": 4
							}
						}
					]
				}
			},
			"WatLEARNError": {
				"7c38573d-6425-488b-abaa-d9c43b3148ee": {
					"uId": "7c38573d-6425-488b-abaa-d9c43b3148ee",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatLEARNError"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"WatTravelerArrivedOn": {
				"b06a2a1f-33d4-445a-97d7-50eeef1076a6": {
					"uId": "b06a2a1f-33d4-445a-97d7-50eeef1076a6",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "WatTravelerArrivedOn"
							}
						}
					]
				}
			}
		}/**SCHEMA_BUSINESS_RULES*/,
		methods: {},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"name": "WatContact542a512c-98ab-4f59-9a74-fcec3ec996ed",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "WatContact",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatPNR80fcba9c-410e-4c49-979a-3524fbe56c05",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "WatPNR",
					"enabled": false
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "WatIsLeaderc19c526f-d3d6-42e3-b96b-5aedac987c5e",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "WatIsLeader"
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "BOOLEANc235abb6-d449-4a5f-9f6e-2b5832093070",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "WatPolicyViolation",
					"enabled": false
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "LOOKUP4e16c05b-593d-4f25-9bb7-515732b2bca9",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "WatActiveTraveller",
					"enabled": true,
					"contentType": 3
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "WatTrip716dd0e0-e85a-4454-b79a-e023e124940b",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 5,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "WatTrip",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "WatTravelerArrived381c71ec-f87f-40ea-87af-cff401c11155",
				"values": {
					"layout": {
						"colSpan": 6,
						"rowSpan": 1,
						"column": 18,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "WatTravelerArrived",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatTravelerPhone6988d96f-7abb-45d8-a743-ef8831dfa3ec",
				"values": {
					"layout": {
						"colSpan": 9,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "WatTravelerPhone",
					"enabled": true
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "WatFaculty1798ae78-95a2-4dbb-995a-dc8a33fd74eb",
				"values": {
					"layout": {
						"colSpan": 9,
						"rowSpan": 1,
						"column": 9,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "WatFaculty",
					"enabled": true,
					"contentType": 3
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "WatWatIAMId0730c8db-13ac-4749-8d8f-169b7c4e5dd3",
				"values": {
					"layout": {
						"colSpan": 9,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "WatWatIAMId",
					"enabled": false,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatWatIAMId0730c8db13ac47498d8f169b7c4e5dd3LabelCaption"
						}
					}
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "WatGroupTravelerType8cfec847-0396-4785-a375-468fe9f3ffea",
				"values": {
					"layout": {
						"colSpan": 9,
						"rowSpan": 1,
						"column": 9,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "WatGroupTravelerType",
					"enabled": true,
					"contentType": 3
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "WatTravelerFirstName4b66d15f-b65a-4d43-ac37-4beeaf7f3fc4",
				"values": {
					"layout": {
						"colSpan": 9,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Header"
					},
					"bindTo": "WatTravelerFirstName"
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "WatStudentStatus5d5da802-037c-489a-8033-35626972d732",
				"values": {
					"layout": {
						"colSpan": 9,
						"rowSpan": 1,
						"column": 9,
						"row": 2,
						"layoutName": "Header"
					},
					"bindTo": "WatStudentStatus",
					"enabled": true,
					"contentType": 3
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "WatTravelerLastName68d76590-1fb4-4a63-a059-69b5c904d397",
				"values": {
					"layout": {
						"colSpan": 9,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Header"
					},
					"bindTo": "WatTravelerLastName"
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "Tabd0953ec1TabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tabd0953ec1TabLabelTabCaption"
					},
					"items": [],
					"order": 0
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tabd0953ec1TabLabelGroup5f943d9e",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tabd0953ec1TabLabelGroup5f943d9eGroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tabd0953ec1TabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tabd0953ec1TabLabelGridLayout78da9ba8",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tabd0953ec1TabLabelGroup5f943d9e",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatCompleteRequiredFormsa4449ee2-acd2-447d-bf21-74fda19dfa4b",
				"values": {
					"layout": {
						"colSpan": 6,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tabd0953ec1TabLabelGridLayout78da9ba8"
					},
					"bindTo": "WatCompleteRequiredForms",
					"enabled": true
				},
				"parentName": "Tabd0953ec1TabLabelGridLayout78da9ba8",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatQUESTContactInfo39cfbb2f-0873-437b-bf83-93c23dd20572",
				"values": {
					"layout": {
						"colSpan": 6,
						"rowSpan": 1,
						"column": 6,
						"row": 0,
						"layoutName": "Tabd0953ec1TabLabelGridLayout78da9ba8"
					},
					"bindTo": "WatQUESTContactInfo"
				},
				"parentName": "Tabd0953ec1TabLabelGridLayout78da9ba8",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "WatFieldworkRiskManagementForm74c886dd-8726-4e46-9a26-a4bcbe94ad3e",
				"values": {
					"layout": {
						"colSpan": 6,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "Tabd0953ec1TabLabelGridLayout78da9ba8"
					},
					"bindTo": "WatFieldworkRiskManagementForm"
				},
				"parentName": "Tabd0953ec1TabLabelGridLayout78da9ba8",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "WatDownloadSafetyAppeb6c4448-ce3c-4377-8cf2-3e0bf12d2dbb",
				"values": {
					"layout": {
						"colSpan": 6,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Tabd0953ec1TabLabelGridLayout78da9ba8"
					},
					"bindTo": "WatDownloadSafetyApp"
				},
				"parentName": "Tabd0953ec1TabLabelGridLayout78da9ba8",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "WatInsuranceObtainedac7dd32c-87b1-448a-b886-686652070bb3",
				"values": {
					"layout": {
						"colSpan": 6,
						"rowSpan": 1,
						"column": 6,
						"row": 1,
						"layoutName": "Tabd0953ec1TabLabelGridLayout78da9ba8"
					},
					"bindTo": "WatInsuranceObtained"
				},
				"parentName": "Tabd0953ec1TabLabelGridLayout78da9ba8",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "WatPROVOSTApproval7cb05fc9-061e-4bf1-bbec-32c1aaaf5610",
				"values": {
					"layout": {
						"colSpan": 6,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "Tabd0953ec1TabLabelGridLayout78da9ba8"
					},
					"bindTo": "WatPROVOSTApproval"
				},
				"parentName": "Tabd0953ec1TabLabelGridLayout78da9ba8",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "WatGovernmentRegistrationAndVISA6824c31b-5844-4947-8cc2-d8a93d29ca63",
				"values": {
					"layout": {
						"colSpan": 6,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Tabd0953ec1TabLabelGridLayout78da9ba8"
					},
					"bindTo": "WatGovernmentRegistrationAndVISA"
				},
				"parentName": "Tabd0953ec1TabLabelGridLayout78da9ba8",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "WatHealthAndVaccinations57251dba-d242-4e34-aeee-a4642e202c9d",
				"values": {
					"layout": {
						"colSpan": 6,
						"rowSpan": 1,
						"column": 6,
						"row": 2,
						"layoutName": "Tabd0953ec1TabLabelGridLayout78da9ba8"
					},
					"bindTo": "WatHealthAndVaccinations"
				},
				"parentName": "Tabd0953ec1TabLabelGridLayout78da9ba8",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "Tabd0953ec1TabLabelGroup3d9dccd3",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tabd0953ec1TabLabelGroup3d9dccd3GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tabd0953ec1TabLabel",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Tabd0953ec1TabLabelGridLayoutab8fc58b",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tabd0953ec1TabLabelGroup3d9dccd3",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatAnvilReadyfbf89573-8f7e-4ef7-a56c-e071a2478dcb",
				"values": {
					"layout": {
						"colSpan": 6,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tabd0953ec1TabLabelGridLayoutab8fc58b"
					},
					"bindTo": "WatAnvilReady"
				},
				"parentName": "Tabd0953ec1TabLabelGridLayoutab8fc58b",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatAnvilExport6fc246a2-2ec6-4822-b9ce-d903e7da5204",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 6,
						"row": 0,
						"layoutName": "Tabd0953ec1TabLabelGridLayoutab8fc58b"
					},
					"bindTo": "WatAnvilExport",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatAnvilExport6fc246a22ec64822b9ced903e7da5204LabelCaption"
						}
					},
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Tabd0953ec1TabLabelGridLayoutab8fc58b",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "BOOLEAN7c2c8d90-4003-4298-94f4-4febfb9b14ac",
				"values": {
					"layout": {
						"colSpan": 6,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Tabd0953ec1TabLabelGridLayoutab8fc58b"
					},
					"bindTo": "WatRepeatExport",
					"enabled": true
				},
				"parentName": "Tabd0953ec1TabLabelGridLayoutab8fc58b",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "Tabd0953ec1TabLabelGroupea693f91",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tabd0953ec1TabLabelGroupea693f91GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tabd0953ec1TabLabel",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "Tabd0953ec1TabLabelGridLayoute9116cfc",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tabd0953ec1TabLabelGroupea693f91",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatEnrollLearnf0e7a26e-f7bb-4815-95ae-af2f68fd268c",
				"values": {
					"layout": {
						"colSpan": 6,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tabd0953ec1TabLabelGridLayoute9116cfc"
					},
					"bindTo": "WatEnrollLearn",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatEnrollLearnf0e7a26ef7bb481595aeaf2f68fd268cLabelCaption"
						}
					},
					"enabled": true
				},
				"parentName": "Tabd0953ec1TabLabelGridLayoute9116cfc",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "BOOLEAN4f605a29-9699-4cb0-a776-4d9216480a80",
				"values": {
					"layout": {
						"colSpan": 6,
						"rowSpan": 1,
						"column": 6,
						"row": 0,
						"layoutName": "Tabd0953ec1TabLabelGridLayoute9116cfc"
					},
					"bindTo": "WatLEARNError",
					"enabled": false,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.BOOLEAN4f605a2996994cb0a7764d9216480a80LabelCaption"
						}
					}
				},
				"parentName": "Tabd0953ec1TabLabelGridLayoute9116cfc",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "STRING91d31003-b9c3-4bb6-b241-34caa4ed0909",
				"values": {
					"layout": {
						"colSpan": 6,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Tabd0953ec1TabLabelGridLayoute9116cfc"
					},
					"bindTo": "WatLearnCourseID",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.STRING91d31003b9c34bb6b24134caa4ed0909LabelCaption"
						}
					},
					"enabled": false
				},
				"parentName": "Tabd0953ec1TabLabelGridLayoute9116cfc",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "LOOKUPb6c792d4-822d-4c36-8563-1de3c1504ca6",
				"values": {
					"layout": {
						"colSpan": 6,
						"rowSpan": 1,
						"column": 6,
						"row": 1,
						"layoutName": "Tabd0953ec1TabLabelGridLayoute9116cfc"
					},
					"bindTo": "WatLEARNRegistration",
					"enabled": false,
					"contentType": 5,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.LOOKUPb6c792d4822d4c3685631de3c1504ca6LabelCaption"
						}
					}
				},
				"parentName": "Tabd0953ec1TabLabelGridLayoute9116cfc",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "DATETIMEc15d6c30-0b00-4975-8640-55008c72892d",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Tabd0953ec1TabLabelGridLayoute9116cfc"
					},
					"bindTo": "WatLEARNEnrollDate",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.DATETIMEc15d6c300b004975864055008c72892dLabelCaption"
						}
					},
					"enabled": false
				},
				"parentName": "Tabd0953ec1TabLabelGridLayoute9116cfc",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "WatLEARNCompleted794827a2-6680-4d7c-a99b-ac42040cf73a",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Tabd0953ec1TabLabelGridLayoute9116cfc"
					},
					"bindTo": "WatLEARNCompleted",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatLEARNCompleted794827a266804d7ca99bac42040cf73aLabelCaption"
						}
					},
					"enabled": false
				},
				"parentName": "Tabd0953ec1TabLabelGridLayoute9116cfc",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "LOOKUP6d2903d0-38a1-4cec-a874-af7f2a00a5ae",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "Tabd0953ec1TabLabelGridLayoute9116cfc"
					},
					"bindTo": "WatLEARNCompletionEmail",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Tabd0953ec1TabLabelGridLayoute9116cfc",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "DATETIMEcb938627-60c6-40b9-bb9d-f4162c39d3fb",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 4,
						"layoutName": "Tabd0953ec1TabLabelGridLayoute9116cfc"
					},
					"bindTo": "WatLEARNCompletionEmailDate",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.DATETIMEcb93862760c640b9bb9df4162c39d3fbLabelCaption"
						}
					},
					"enabled": false
				},
				"parentName": "Tabd0953ec1TabLabelGridLayoute9116cfc",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "WatSchemaLearnRegistrationDetail9f7fcbca",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tabd0953ec1TabLabel",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "Tabd0953ec1TabLabelGroup77bc2a57",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tabd0953ec1TabLabelGroup77bc2a57GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tabd0953ec1TabLabel",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "Tabd0953ec1TabLabelGridLayout13768d67",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tabd0953ec1TabLabelGroup77bc2a57",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatTravelerArrived8ec29b15-25e8-4f09-a565-3eae3afdc198",
				"values": {
					"layout": {
						"colSpan": 6,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tabd0953ec1TabLabelGridLayout13768d67"
					},
					"bindTo": "WatTravelerArrived"
				},
				"parentName": "Tabd0953ec1TabLabelGridLayout13768d67",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatTravelerArrivedOn4afb021b-bc43-4a93-aa1b-fa96bdc56f4f",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Tabd0953ec1TabLabelGridLayout13768d67"
					},
					"bindTo": "WatTravelerArrivedOn",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatTravelerArrivedOn4afb021bbc434a93aa1bfa96bdc56f4fLabelCaption"
						}
					},
					"enabled": true
				},
				"parentName": "Tabd0953ec1TabLabelGridLayout13768d67",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "WatSchemaArrivalNoticeDetail7dbedaa5",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tabd0953ec1TabLabel",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "WatSchemaf2370fffDetaile516e8bc",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tabd0953ec1TabLabel",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "Tab734ad64bTabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab734ad64bTabLabelTabCaption"
					},
					"items": [],
					"order": 1
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "WatSchema7713dc14Detailf663eee8",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab734ad64bTabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tab7c551261TabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab7c551261TabLabelTabCaption"
					},
					"items": [],
					"order": 2
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "WatSchemaCustomEmailCallDetail56db1802",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab7c551261TabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatSchemaCustomTaskDetail8ba819a4",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab7c551261TabLabel",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "NotesAndFilesTab",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.NotesAndFilesTabCaption"
					},
					"items": [],
					"order": 3
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "DocumentDetailV2604a8e8e",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Files",
				"values": {
					"itemType": 2
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "NotesControlGroup",
				"values": {
					"itemType": 15,
					"caption": {
						"bindTo": "Resources.Strings.NotesGroupCaption"
					},
					"items": []
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "Notes",
				"values": {
					"bindTo": "WatNotes",
					"dataValueType": 1,
					"contentType": 4,
					"layout": {
						"column": 0,
						"row": 0,
						"colSpan": 24
					},
					"labelConfig": {
						"visible": false
					},
					"controlConfig": {
						"imageLoaded": {
							"bindTo": "insertImagesToNotes"
						},
						"images": {
							"bindTo": "NotesImagesCollection"
						}
					}
				},
				"parentName": "NotesControlGroup",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "merge",
				"name": "ESNTab",
				"values": {
					"order": 4
				}
			},
			{
				"operation": "insert",
				"name": "WatSchema94c7e3c8Detail313443d3",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "ESNTab",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tab8de543bfTabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab8de543bfTabLabelTabCaption"
					},
					"items": [],
					"order": 5
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "Tab8de543bfTabLabelGroup8c4e54dc",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab8de543bfTabLabelGroup8c4e54dcGroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tab8de543bfTabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tab8de543bfTabLabelGridLayout68d181ff",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tab8de543bfTabLabelGroup8c4e54dc",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatTravelerFirstName94cfa3d2-0f09-4315-82e4-eb7c96e45f51",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tab8de543bfTabLabelGridLayout68d181ff"
					},
					"bindTo": "WatTravelerFirstName"
				},
				"parentName": "Tab8de543bfTabLabelGridLayout68d181ff",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatWatIAMIde9183765-455c-48d1-9faa-8014dbe2986b",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "Tab8de543bfTabLabelGridLayout68d181ff"
					},
					"bindTo": "WatWatIAMId"
				},
				"parentName": "Tab8de543bfTabLabelGridLayout68d181ff",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "WatTravelerLastNamec0f3bc37-276a-4b04-ae50-246cff74f9aa",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Tab8de543bfTabLabelGridLayout68d181ff"
					},
					"bindTo": "WatTravelerLastName"
				},
				"parentName": "Tab8de543bfTabLabelGridLayout68d181ff",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "WatTravelerEmail34f936b0-7d9c-404e-9202-d0b8bc09a9ff",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "Tab8de543bfTabLabelGridLayout68d181ff"
					},
					"bindTo": "WatTravelerEmail",
					"enabled": true
				},
				"parentName": "Tab8de543bfTabLabelGridLayout68d181ff",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "WatGroupTravelerTypeab024007-c839-4dea-8eb3-b8b2102a4784",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Tab8de543bfTabLabelGridLayout68d181ff"
					},
					"bindTo": "WatGroupTravelerType"
				},
				"parentName": "Tab8de543bfTabLabelGridLayout68d181ff",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "WatTravelerPhone1d5b256d-55ba-4850-be73-4b58e0ac579f",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 2,
						"layoutName": "Tab8de543bfTabLabelGridLayout68d181ff"
					},
					"bindTo": "WatTravelerPhone"
				},
				"parentName": "Tab8de543bfTabLabelGridLayout68d181ff",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "STRING4d849d72-6c39-468c-9439-ed4b1982760c",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Tab8de543bfTabLabelGridLayout68d181ff"
					},
					"bindTo": "WatStudentID",
					"enabled": true
				},
				"parentName": "Tab8de543bfTabLabelGridLayout68d181ff",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "Tabe97b2b47TabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tabe97b2b47TabLabelTabCaption"
					},
					"items": [],
					"order": 6
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "Tabe97b2b47TabLabelGroup16f07943",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tabe97b2b47TabLabelGroup16f07943GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tabe97b2b47TabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tabe97b2b47TabLabelGridLayoute3ca3886",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tabe97b2b47TabLabelGroup16f07943",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatPNR1f78e8af-91fb-4f44-80a3-36b37e04ae69",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tabe97b2b47TabLabelGridLayoute3ca3886"
					},
					"bindTo": "WatPNR"
				},
				"parentName": "Tabe97b2b47TabLabelGridLayoute3ca3886",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CreatedOn228d8730-3ef8-48c2-805c-693bd38dbc5b",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "Tabe97b2b47TabLabelGridLayoute3ca3886"
					},
					"bindTo": "CreatedOn",
					"enabled": false
				},
				"parentName": "Tabe97b2b47TabLabelGridLayoute3ca3886",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "BOOLEANd7ddaef0-90a0-48c7-a3be-5d3ef2b5b700",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Tabe97b2b47TabLabelGridLayoute3ca3886"
					},
					"bindTo": "WatEnrollLearn",
					"enabled": true
				},
				"parentName": "Tabe97b2b47TabLabelGridLayoute3ca3886",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "CreatedBy242efe3b-6b68-4baa-b3b7-df45e684d640",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "Tabe97b2b47TabLabelGridLayoute3ca3886"
					},
					"bindTo": "CreatedBy",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Tabe97b2b47TabLabelGridLayoute3ca3886",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "WatAnvilReady14b74488-5587-47a5-baa3-564c06d42a31",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Tabe97b2b47TabLabelGridLayoute3ca3886"
					},
					"bindTo": "WatAnvilReady"
				},
				"parentName": "Tabe97b2b47TabLabelGridLayoute3ca3886",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "ModifiedOnb8ef21e5-6fab-4511-81e5-b3caf98746a8",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 2,
						"layoutName": "Tabe97b2b47TabLabelGridLayoute3ca3886"
					},
					"bindTo": "ModifiedOn",
					"enabled": false
				},
				"parentName": "Tabe97b2b47TabLabelGridLayoute3ca3886",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "WatAnvilExport07989bfa-afb8-472a-bd7b-b0bd4a6aa87a",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Tabe97b2b47TabLabelGridLayoute3ca3886"
					},
					"bindTo": "WatAnvilExport"
				},
				"parentName": "Tabe97b2b47TabLabelGridLayoute3ca3886",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "ModifiedBy42951f74-66eb-4a01-84d5-bc40e0a95529",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 3,
						"layoutName": "Tabe97b2b47TabLabelGridLayoute3ca3886"
					},
					"bindTo": "ModifiedBy",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Tabe97b2b47TabLabelGridLayoute3ca3886",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "DATETIMEfa71d4cc-703c-4041-aead-1f688319595a",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "Tabe97b2b47TabLabelGridLayoute3ca3886"
					},
					"bindTo": "WatLastInactive",
					"enabled": false
				},
				"parentName": "Tabe97b2b47TabLabelGridLayoute3ca3886",
				"propertyName": "items",
				"index": 8
			}
		]/**SCHEMA_DIFF*/
	};
});
