define("AccountPageV2", [], function() {
	return {
		entitySchemaName: "Account",
		attributes: {},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{
			"WatSchemae083c857Detail0694b455": {
				"schemaName": "WatSchemae083c857Detail",
				"entitySchemaName": "WatAccountRanking",
				"filter": {
					"detailColumn": "WatAccount",
					"masterColumn": "Id"
				}
			},
			"WatSchemab70f4469Detaile2523d56": {
				"schemaName": "WatSchemab70f4469Detail",
				"entitySchemaName": "WatDelegationInstitutions",
				"filter": {
					"detailColumn": "WatAccount",
					"masterColumn": "Id"
				}
			},
			"WatSchema0e472337Detail387ac15b": {
				"schemaName": "WatSchema0e472337Detail",
				"entitySchemaName": "WatConsortiumMember",
				"filter": {
					"detailColumn": "WatAccount",
					"masterColumn": "Id"
				}
			},
			"WatSchema93d4df76Detail66827423": {
				"schemaName": "WatSchema93d4df76Detail",
				"entitySchemaName": "WatInventoryAdjustments",
				"filter": {
					"detailColumn": "WatAccount",
					"masterColumn": "Id"
				}
			},
			"WatSchemaPartnershipRequestsDetail4d869ad6": {
				"schemaName": "WatSchemaPartnershipRequestsDetail",
				"entitySchemaName": "WatPartnershipRequest",
				"filter": {
					"detailColumn": "WatAccount",
					"masterColumn": "Id"
				}
			},
			"WatSchemaAgreementsDetaild7730bd2": {
				"schemaName": "WatSchemaAgreementsDetail",
				"entitySchemaName": "WatAgreement",
				"filter": {
					"detailColumn": "WatInstitution",
					"masterColumn": "Id"
				}
			},
			"WatSchemad43475a2Detail407def9e": {
				"schemaName": "WatSchemad43475a2Detail",
				"entitySchemaName": "WatDelegationGifts",
				"filter": {
					"detailColumn": "WatAccount",
					"masterColumn": "Id"
				}
			},
			"WatSchemaMasterAgreementDetailc8d98514": {
				"schemaName": "WatSchemaMasterAgreementDetail",
				"entitySchemaName": "WatMasterAgreement",
				"filter": {
					"detailColumn": "WatAccount",
					"masterColumn": "Id"
				}
			},
			"WatSchema2815d243Detail7af5e0e3": {
				"schemaName": "WatSchema2815d243Detail",
				"entitySchemaName": "WatReceivedGift",
				"filter": {
					"detailColumn": "WatAccount",
					"masterColumn": "Id"
				}
			},
			"DocumentDetailV2ab359054": {
				"schemaName": "DocumentDetailV2",
				"entitySchemaName": "Document",
				"filter": {
					"detailColumn": "Account",
					"masterColumn": "Id"
				}
			},
			"WatSchema94c7e3c8Detailaf057d96": {
				"schemaName": "WatSchema94c7e3c8Detail",
				"entitySchemaName": "WatDatedNotes",
				"filter": {
					"detailColumn": "WatAccount",
					"masterColumn": "Id"
				}
			},
			"WatSchemaCustomEmailCallDetail44a03732": {
				"schemaName": "WatSchemaCustomEmailCallDetail",
				"entitySchemaName": "Activity",
				"filter": {
					"detailColumn": "Account",
					"masterColumn": "Id"
				}
			},
			"WatSchemaCustomTaskDetailce0d98f9": {
				"schemaName": "WatSchemaCustomTaskDetail",
				"entitySchemaName": "Activity",
				"filter": {
					"detailColumn": "Account",
					"masterColumn": "Id"
				}
			}
		}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{
			"NotesControlGroup": {
				"37ac70bb-83ae-4714-bded-363eeec3175e": {
					"uId": "37ac70bb-83ae-4714-bded-363eeec3175e",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 0,
								"value": 1,
								"dataValueType": 4
							},
							"rightExpression": {
								"type": 0,
								"value": 2,
								"dataValueType": 4
							}
						}
					]
				}
			}
		}/**SCHEMA_BUSINESS_RULES*/,
		methods: {},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "merge",
				"name": "AccountPhotoContainer",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0
					}
				}
			},
			{
				"operation": "merge",
				"name": "AccountName",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 1
					}
				}
			},
			{
				"operation": "merge",
				"name": "AccountType",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 2
					}
				}
			},
			{
				"operation": "merge",
				"name": "AccountOwner",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 3
					}
				}
			},
			{
				"operation": "merge",
				"name": "AccountWeb",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 4
					}
				}
			},
			{
				"operation": "merge",
				"name": "AccountPhone",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 5
					}
				}
			},
			{
				"operation": "merge",
				"name": "NewAccountCategory",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 6
					}
				}
			},
			{
				"operation": "merge",
				"name": "AccountIndustry",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 7
					}
				}
			},
			{
				"operation": "merge",
				"name": "AccountPageGeneralTabContainer",
				"values": {
					"order": 0
				}
			},
			{
				"operation": "merge",
				"name": "AlternativeName",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0
					}
				}
			},
			{
				"operation": "merge",
				"name": "Code",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0
					}
				}
			},
			{
				"operation": "insert",
				"name": "WatSchemae083c857Detail0694b455",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "AccountPageGeneralTabContainer",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "merge",
				"name": "EmployeesNumber",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0
					}
				}
			},
			{
				"operation": "merge",
				"name": "Ownership",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0
					}
				}
			},
			{
				"operation": "merge",
				"name": "AnnualRevenue",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1
					}
				}
			},
			{
				"operation": "merge",
				"name": "ContactsAndStructureTabContainer",
				"values": {
					"order": 1
				}
			},
			{
				"operation": "insert",
				"name": "Tabdcf8d605TabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tabdcf8d605TabLabelTabCaption"
					},
					"items": [],
					"order": 2
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "WatSchemaPartnershipRequestsDetail4d869ad6",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tabdcf8d605TabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatSchemaMasterAgreementDetailc8d98514",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tabdcf8d605TabLabel",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "WatSchemaAgreementsDetaild7730bd2",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tabdcf8d605TabLabel",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "Tab97a8f8ccTabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab97a8f8ccTabLabelTabCaption"
					},
					"items": [],
					"order": 3
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "WatSchema0e472337Detail387ac15b",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab97a8f8ccTabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatSchemab70f4469Detaile2523d56",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab97a8f8ccTabLabel",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "WatSchemad43475a2Detail407def9e",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab97a8f8ccTabLabel",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "WatSchema93d4df76Detail66827423",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab97a8f8ccTabLabel",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "Tab18d66dfcTabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab18d66dfcTabLabelTabCaption"
					},
					"items": [],
					"order": 4
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "WatSchema2815d243Detail7af5e0e3",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab18d66dfcTabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tab1a3755bdTabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab1a3755bdTabLabelTabCaption"
					},
					"items": [],
					"order": 5
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "WatSchemaCustomEmailCallDetail44a03732",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab1a3755bdTabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatSchemaCustomTaskDetailce0d98f9",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab1a3755bdTabLabel",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "merge",
				"name": "NotesTabContainer",
				"values": {
					"order": 6
				}
			},
			{
				"operation": "move",
				"name": "NotesTabContainer",
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "DocumentDetailV2ab359054",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "NotesTabContainer",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "merge",
				"name": "ESNTab",
				"values": {
					"order": 7
				}
			},
			{
				"operation": "move",
				"name": "ESNTab",
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "WatSchema94c7e3c8Detailaf057d96",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "ESNTab",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "merge",
				"name": "RelationshipTabContainer",
				"values": {
					"order": 8
				}
			},
			{
				"operation": "merge",
				"name": "HistoryTabContainer",
				"values": {
					"order": 10
				}
			},
			{
				"operation": "merge",
				"name": "TimelineTab",
				"values": {
					"order": 9
				}
			},
			{
				"operation": "move",
				"name": "CategoriesControlGroup",
				"parentName": "AccountPageGeneralTabContainer",
				"propertyName": "items",
				"index": 4
			}
		]/**SCHEMA_DIFF*/
	};
});
