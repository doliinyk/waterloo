using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Terrasoft.Core;

namespace Terrasoft.Configuration.WatLandingPageHelperNamespace
{
    public class WatLandingPageHelper
    {
        private readonly UserConnection userConnection;
        public WatLandingPageHelper(UserConnection userConnection)
        {
            this.userConnection = userConnection;
        }

        public Guid CreateObject(string entitySchemaName, string body)
        {
            int openningBodyTagIndex = body.IndexOf("<body") == -1 ? 0 : body.IndexOf("<body");
            int closingBodyTagIndex = body.IndexOf("</body>") == -1 ? body.Length - 1 : body.IndexOf("</body>");
            string htmlBody = body.Substring(openningBodyTagIndex, closingBodyTagIndex - openningBodyTagIndex + 1);
            string json = htmlBody.Substring(htmlBody.IndexOf("{"), htmlBody.IndexOf("}") - htmlBody.IndexOf("{") + 1);
            json = Regex.Replace(json, "<.*?>", string.Empty);
            json = System.Net.WebUtility.HtmlDecode(json);
            var fieldValues = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);

            var schema = userConnection.EntitySchemaManager.GetInstanceByName(entitySchemaName);
            var entity = schema.CreateEntity(userConnection);
            entity.SetDefColumnValues();

            foreach (var fieldValue in fieldValues)
            {
                entity.SetColumnValue(fieldValue.Key, fieldValue.Value);
            }
            
            entity.Save();
            return entity.PrimaryColumnValue;
        }
    }
}