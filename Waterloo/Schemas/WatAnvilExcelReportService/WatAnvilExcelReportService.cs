namespace Terrasoft.Configuration {
    using System;
    using System.CodeDom.Compiler;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Data;
    using System.IO;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Security.Cryptography;
    using System.ServiceModel;
    using System.ServiceModel.Web;
    using System.ServiceModel.Activation;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Web;
    using Terrasoft.Core;
    using Terrasoft.Core.DB;
    using Terrasoft.Core.Entities;
    using Terrasoft.Common;
    using OfficeOpenXml;
    using OfficeOpenXml.Style;
    using Newtonsoft.Json;
    using Terrasoft.Common.Json;
    using Terrasoft.Core.Scheduler;
    
    #region Class: AnvilExcelReportService
    
    [ServiceContract]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class AnvilExcelReportService
    {
        #region Properties: Private
        
        private UserConnection _userConnection { get; set; }
        
        #endregion
        
        #region Constructors
        
        public AnvilExcelReportService(){
        	if (HttpContext.Current != null)
        	{
            	_userConnection = HttpContext.Current.Session["UserConnection"] as UserConnection;
        	}
        }
        public AnvilExcelReportService(UserConnection userConnection){
            _userConnection = userConnection;
        }
        
        #endregion
        
        #region Methods: Private
        
        private void Authenticate()
        {
            if (_userConnection == null)
            {
                throw new System.Security.Authentication.AuthenticationException();
            }
        }
        
        private void SetValue(ref ExcelWorksheet Sheet, int Row, int Cell, string Value) {
            if (!string.IsNullOrWhiteSpace(Value)) {
                Sheet.Cells[Row, Cell].Value = Value;
            }
        }
        
        private byte[] BuildExcel(Guid AnvilExportId)
        {
            var FileData = new byte[] { };
            using (System.IO.MemoryStream stream = new MemoryStream()) {
                using (ExcelPackage p = new ExcelPackage(stream)) {
                    var s = p.Workbook.Worksheets.Add("ItineraryImport");
                    try {
                        var sp = (StoredProcedure)new StoredProcedure(_userConnection, "AnvilExport")
                    				.WithParameter("AnvilExportId", AnvilExportId);
                        try {
                            using (DBExecutor dbExecutor = _userConnection.EnsureDBConnection())
                            {
                                using (var reader = sp.ExecuteReader(dbExecutor))
                                {
                                    SetValue(ref s, 1, 1, "Traveller Name");
                                    s.Cells["A1:B1"].Merge = true;
                                    SetValue(ref s, 1, 5, "Traveller Details");
                                    s.Cells["E1:H1"].Merge = true;
                                    SetValue(ref s, 1, 10, "Flight Data");
                                    s.Cells["J1:O1"].Merge = true;
                                    var columnHeaders = new List<string>();
                                    columnHeaders = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                                    foreach (var c in columnHeaders) {
                                        SetValue(ref s, 2, columnHeaders.IndexOf(c) + 1, c);
                                    }
                                    int rowIndex = 3;
                                    while(reader.Read()) {
                                        for (int n = 0; n < reader.FieldCount; n++)
                                        {
                                            SetValue(ref s, rowIndex, n+1, reader.GetValue(n).ToString());
                                        }
                                        rowIndex++;
                                    }
                                }
                            }
                        } catch {}
                    } catch {}
                    FileData = p.GetAsByteArray();
                }
            }
            return FileData;
        }
        
        #endregion Methods: Private
        
        #region Methods: Public
        
        public Guid GenerateAnvilReport()
        {
       		Guid AnvilExportId = Guid.NewGuid();
       		
       		string recordName = string.Format("Anvil Export {0}", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
       		string fileName = recordName + ".xlsx";
       		
        	var excelData = BuildExcel(AnvilExportId);
        	
        	EntitySchema entitySchema = _userConnection.EntitySchemaManager.GetInstanceByName("WatAnvilExportFile");
			Entity entity = entitySchema.CreateEntity(_userConnection);
			entity.SetDefColumnValues();
			
			entity.SetColumnValue("WatAnvilExportId", AnvilExportId);
			entity.SetStreamValue("Data", new MemoryStream(excelData));
			entity.SetColumnValue("Size", excelData.Length);
			entity.SetColumnValue("Name", fileName);
			entity.SetColumnValue("TypeId", new Guid("529bc2f8-0ee0-df11-971b-001d60e938c6"));
			entity.SetColumnValue("Version", 1);
			entity.Save();
			
			var update = new Update(_userConnection, "WatAnvilExport")
			      .Set("WatStatus", Column.Parameter("Completed"))
			      .Set("WatName", Column.Parameter(recordName))
			      .Where("Id").IsEqual(Column.Parameter(AnvilExportId));
			update.Execute(); 
			
			return entity.GetTypedColumnValue<Guid>("Id");
        }
        
        #endregion Methods: Public
    }
    
    #endregion Class: AnvilExcelReportService
}