define("WatSchemaeece2c0bDetail",["ProcessModuleUtilities", "MaskHelper","ConfigurationGrid",
	"ConfigurationGridGenerator", "ConfigurationGridUtilities"], function(ProcessModuleUtilities, MaskHelper) {
	return {
		entitySchemaName: "WatParticipatingFaculties",
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		diff: /**SCHEMA_DIFF*/[]/**SCHEMA_DIFF*/,
		methods: 
		{
			onActiveRowAction: function(buttonTag, primaryColumnValue) 
			{
                if (buttonTag === "notify") 
				{
					MaskHelper.ShowBodyMask();
                    var activeRow = this.getActiveRow();
                    var activeRowId = activeRow.get("Id");
                    var args = {
						sysProcessName: "SMCreateAppointmentProcess",
						parameters: {
							recordId: activeRowId
						}
					};
					ProcessModuleUtilities.executeProcess(args);                    
                }
        	},
			
			ChangeNotifyButtonVisible: function () {
				window.console.log("sup");
				//let activeRow = this.$Id;
				//let lrStatus = this.$ITLRAssessmentStatuses;
				//if (scope.$MultiSelect) {
				//	return false;
				//}
				//if ((activeRow && Ext.isEmpty(this.$TSParent) && (Ext.isEmpty(lrStatus) ||
				//	(!Ext.isEmpty(lrStatus) && lrStatus.value !== "1e55dec6-9305-445c-888f-75b408803247"))) && !fromMeeting) {
				//	return true;
				//} else {
				//	return false;
				//}
				return false;
			},
		}
	};
});
