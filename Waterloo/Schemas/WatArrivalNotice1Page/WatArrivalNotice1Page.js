define("WatArrivalNotice1Page", [], function() {
	return {
		entitySchemaName: "WatArrivalNotice",
		attributes: {},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{
			"Files": {
				"schemaName": "FileDetailV2",
				"entitySchemaName": "WatArrivalNoticeFile",
				"filter": {
					"masterColumn": "Id",
					"detailColumn": "WatArrivalNotice"
				}
			}
		}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{
			"WatTraveler": {
				"4cf6f161-0946-4464-a094-3bf2c7bdee0e": {
					"uId": "4cf6f161-0946-4464-a094-3bf2c7bdee0e",
					"enabled": true,
					"removed": false,
					"ruleType": 1,
					"baseAttributePatch": "WatTrip",
					"comparisonType": 3,
					"autoClean": true,
					"autocomplete": false,
					"type": 1,
					"attribute": "WatTrip"
				},
				"00454ea8-c088-461c-9936-23f677978262": {
					"uId": "00454ea8-c088-461c-9936-23f677978262",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "WatTrip"
							}
						}
					]
				}
			},
			"WatTrip": {
				"e27b8207-b2b3-4b6a-816b-c9eb28435062": {
					"uId": "e27b8207-b2b3-4b6a-816b-c9eb28435062",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "WatTraveler"
							}
						}
					]
				},
				"8d95ef36-887f-4053-aceb-760160b15ebd": {
					"uId": "8d95ef36-887f-4053-aceb-760160b15ebd",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "WatTraveler"
							}
						}
					]
				}
			},
			"WatWatIAMId": {
				"b25ebad4-64e6-47f5-ac88-f81d203f7d98": {
					"uId": "b25ebad4-64e6-47f5-ac88-f81d203f7d98",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "WatTraveler"
							}
						}
					]
				}
			},
			"WatName": {
				"4ac85df8-3fe9-4338-ae9b-31e1876a2e71": {
					"uId": "4ac85df8-3fe9-4338-ae9b-31e1876a2e71",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "WatTrip"
							}
						}
					]
				}
			}
		}/**SCHEMA_BUSINESS_RULES*/,
		methods: {},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"name": "LOOKUP5e1ebcb9-376a-4104-942a-19771bb58c96",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "WatTraveler",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "LOOKUPc73e8858-34e4-45d2-ba23-bf11546d7591",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "WatTrip",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "WatName1163341f-2059-4a90-a427-711c6fd643d0",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "WatName",
					"enabled": true
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "LOOKUPfd58b997-c2e2-48f8-88d1-61bb577087cd",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "WatContact",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "STRING03b972df-5e79-449e-b138-87bceb9a280c",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Header"
					},
					"bindTo": "WatWatIAMId",
					"enabled": true
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "CreatedOn2cc41b69-a7f4-4b4f-9b86-3f88a7ea2f2c",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 2,
						"layoutName": "Header"
					},
					"bindTo": "CreatedOn"
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "DATETIMEee06e123-9f93-4e8d-98e5-9bd7e4450138",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Header"
					},
					"bindTo": "WatArrivalDate",
					"enabled": true
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "Tabc4f88934TabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tabc4f88934TabLabelTabCaption"
					},
					"items": [],
					"order": 0
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tabc4f88934TabLabelGroupa65fbf62",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tabc4f88934TabLabelGroupa65fbf62GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tabc4f88934TabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tabc4f88934TabLabelGridLayoutc15b5bb8",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tabc4f88934TabLabelGroupa65fbf62",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatStreetAddress5a98f085-2021-4201-9995-752c6d74db8d",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tabc4f88934TabLabelGridLayoutc15b5bb8"
					},
					"bindTo": "WatStreetAddress"
				},
				"parentName": "Tabc4f88934TabLabelGridLayoutc15b5bb8",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatLocalPrimaryPhoned6c09093-4165-4646-998b-986207760306",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "Tabc4f88934TabLabelGridLayoutc15b5bb8"
					},
					"bindTo": "WatLocalPrimaryPhone"
				},
				"parentName": "Tabc4f88934TabLabelGridLayoutc15b5bb8",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "WatCityf0e1d9c7-d756-484c-a6e2-c675d506d702",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Tabc4f88934TabLabelGridLayoutc15b5bb8"
					},
					"bindTo": "WatCity"
				},
				"parentName": "Tabc4f88934TabLabelGridLayoutc15b5bb8",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "WatLocalSecondaryPhonedc11dc4e-e6c1-4fd9-b7ee-2c25d75e7219",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "Tabc4f88934TabLabelGridLayoutc15b5bb8"
					},
					"bindTo": "WatLocalSecondaryPhone"
				},
				"parentName": "Tabc4f88934TabLabelGridLayoutc15b5bb8",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "WatZIPPostalCode0a6ce05c-8428-4a96-8cfa-e50f7c80b631",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Tabc4f88934TabLabelGridLayoutc15b5bb8"
					},
					"bindTo": "WatZIPPostalCode"
				},
				"parentName": "Tabc4f88934TabLabelGridLayoutc15b5bb8",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "WatHostUniversityEmployerAgencye2991adc-dfb9-45d9-bf9d-740cf8c8bb2d",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 2,
						"layoutName": "Tabc4f88934TabLabelGridLayoutc15b5bb8"
					},
					"bindTo": "WatHostUniversityEmployerAgency"
				},
				"parentName": "Tabc4f88934TabLabelGridLayoutc15b5bb8",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "WatStateProvince003e0b63-4794-4fa9-bf4e-5d8fec209c78",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Tabc4f88934TabLabelGridLayoutc15b5bb8"
					},
					"bindTo": "WatStateProvince"
				},
				"parentName": "Tabc4f88934TabLabelGridLayoutc15b5bb8",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "WatCountry3a00d3d3-9859-4b97-9e1b-49b19a656695",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "Tabc4f88934TabLabelGridLayoutc15b5bb8"
					},
					"bindTo": "WatCountry"
				},
				"parentName": "Tabc4f88934TabLabelGridLayoutc15b5bb8",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "STRING1c3bf936-43b2-4aa5-9227-26f8a18de45d",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 3,
						"layoutName": "Tabc4f88934TabLabelGridLayoutc15b5bb8"
					},
					"bindTo": "WatLocalEmail",
					"enabled": true
				},
				"parentName": "Tabc4f88934TabLabelGridLayoutc15b5bb8",
				"propertyName": "items",
				"index": 8
			},
			{
				"operation": "insert",
				"name": "NotesAndFilesTab",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.NotesAndFilesTabCaption"
					},
					"items": [],
					"order": 1
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Files",
				"values": {
					"itemType": 2
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "NotesControlGroup",
				"values": {
					"itemType": 15,
					"caption": {
						"bindTo": "Resources.Strings.NotesGroupCaption"
					},
					"items": []
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Notes",
				"values": {
					"bindTo": "WatNotes",
					"dataValueType": 1,
					"contentType": 4,
					"layout": {
						"column": 0,
						"row": 0,
						"colSpan": 24
					},
					"labelConfig": {
						"visible": false
					},
					"controlConfig": {
						"imageLoaded": {
							"bindTo": "insertImagesToNotes"
						},
						"images": {
							"bindTo": "NotesImagesCollection"
						}
					}
				},
				"parentName": "NotesControlGroup",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "merge",
				"name": "ESNTab",
				"values": {
					"order": 2
				}
			}
		]/**SCHEMA_DIFF*/
	};
});
