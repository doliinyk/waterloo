define("DocumentPageV2", ["CommunicationOptionsMixin"], function() {
	return {
		entitySchemaName: "Document",
		attributes: {},
		mixins: {
			CommunicationOptionsMixin: "Terrasoft.CommunicationOptionsMixin"
		},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{
			"EntityConnections": {
				"41679642-0b25-43b6-8ab5-5319337ade49": {
					"uId": "41679642-0b25-43b6-8ab5-5319337ade49",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 0,
								"value": "abc",
								"dataValueType": 1
							},
							"rightExpression": {
								"type": 0,
								"value": "123",
								"dataValueType": 1
							}
						}
					]
				}
			},
			"Account": {
				"ca3b4dbd-fb01-4de4-9ce3-18b70c34ce93": {
					"uId": "ca3b4dbd-fb01-4de4-9ce3-18b70c34ce93",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "CreatedOn"
							}
						}
					]
				}
			}
		}/**SCHEMA_BUSINESS_RULES*/,
		methods: {
			validateAccountOrContactFilling: function(callback, scope) {
				callback.call(scope || this, {
					success: true
				});
			}
		},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "merge",
				"name": "Number",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0
					}
				}
			},
			{
				"operation": "merge",
				"name": "Date",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0
					}
				}
			},
			{
				"operation": "merge",
				"name": "Type",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1
					}
				}
			},
			{
				"operation": "merge",
				"name": "Owner",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1
					}
				}
			},
			{
				"operation": "merge",
				"name": "State",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2
					}
				}
			},
			{
				"operation": "insert",
				"name": "STRING6f260dcc-8a2a-4de4-931c-e7b26ef6fafd",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Header"
					},
					"bindTo": "WatDocumentLocation",
					"enabled": true,
					"showValueAsLink": true,
					"href": {
						"bindTo": "WatDocumentLocation",
						"bindConfig": {
							"converter": "getLinkValue"
						}
					},
					"controlConfig": {
						"className": "Terrasoft.TextEdit",
						"linkclick": {
							"bindTo": "openNewTab"
						}
					},
					"contentType": 0
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "merge",
				"name": "ESNTab",
				"values": {
					"order": 3
				}
			},
			{
				"operation": "merge",
				"name": "GeneralInfoTabContainer",
				"values": {
					"order": 0
				}
			},
			{
				"operation": "merge",
				"name": "GeneralInfoGroup",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.GeneralInfoGroupGroupCaption"
					}
				}
			},
			{
				"operation": "merge",
				"name": "Account",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0
					}
				}
			},
			{
				"operation": "merge",
				"name": "Contact",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0
					}
				}
			},
			{
				"operation": "insert",
				"name": "LOOKUPe732fae1-ee5a-4b8f-828a-885e1fb3a4bd",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "GeneralInfoBlock"
					},
					"bindTo": "WatAgreement",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "GeneralInfoBlock",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "LOOKUPfcecc755-e1aa-43a0-8853-9697c0dd8918",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "GeneralInfoBlock"
					},
					"bindTo": "WatPartnershipRequest",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "GeneralInfoBlock",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "LOOKUP53fac23b-fcdb-47a1-8335-f6eeb8ceab36",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "GeneralInfoBlock"
					},
					"bindTo": "WatDelegation",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "GeneralInfoBlock",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "merge",
				"name": "HistoryTabContainer",
				"values": {
					"order": 1
				}
			},
			{
				"operation": "merge",
				"name": "NotesAndFilesTab",
				"values": {
					"order": 2
				}
			}
		]/**SCHEMA_DIFF*/
	};
});
