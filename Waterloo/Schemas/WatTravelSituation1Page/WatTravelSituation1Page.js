define("WatTravelSituation1Page", [], function() {
	return {
		entitySchemaName: "WatTravelSituation",
		attributes: {},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{
			"Files": {
				"schemaName": "FileDetailV2",
				"entitySchemaName": "WatTravelSituationFile",
				"filter": {
					"masterColumn": "Id",
					"detailColumn": "WatTravelSituation"
				}
			},
			"VisaDetailV2bc363af7": {
				"schemaName": "VisaDetailV2",
				"entitySchemaName": "WatTravelSituationVisa",
				"filter": {
					"masterColumn": "Id",
					"detailColumn": "WatTravelSituation"
				}
			},
			"WatSchema7713dc14Detail48683a0c": {
				"schemaName": "WatSchema7713dc14Detail",
				"entitySchemaName": "WatAssociatedTraveler",
				"filter": {
					"detailColumn": "WatTravelSituation",
					"masterColumn": "Id"
				}
			},
			"DocumentDetailV20246bfef": {
				"schemaName": "DocumentDetailV2",
				"entitySchemaName": "Document",
				"filter": {
					"detailColumn": "WatTravelSituation",
					"masterColumn": "Id"
				}
			},
			"WatSchema94c7e3c8Detailc33aac1a": {
				"schemaName": "WatSchema94c7e3c8Detail",
				"entitySchemaName": "WatDatedNotes",
				"filter": {
					"detailColumn": "WatTravelIncident",
					"masterColumn": "Id"
				}
			},
			"WatSchemaCustomEmailCallDetaile9ce3d54": {
				"schemaName": "WatSchemaCustomEmailCallDetail",
				"entitySchemaName": "Activity",
				"filter": {
					"detailColumn": "WatTravelSituation",
					"masterColumn": "Id"
				}
			},
			"WatSchemaCustomTaskDetailc14592c7": {
				"schemaName": "WatSchemaCustomTaskDetail",
				"entitySchemaName": "Activity",
				"filter": {
					"detailColumn": "WatTravelSituation",
					"masterColumn": "Id"
				}
			}
		}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{
			"WatApprover": {
				"9a8460e2-a9a4-463e-98c2-d8ac3319d214": {
					"uId": "9a8460e2-a9a4-463e-98c2-d8ac3319d214",
					"enabled": true,
					"removed": true,
					"ruleType": 1,
					"baseAttributePatch": "Account.Type",
					"comparisonType": 3,
					"autoClean": false,
					"autocomplete": false,
					"type": 0,
					"value": "57412fad-53e6-df11-971b-001d60e938c6",
					"dataValueType": 10
				}
			},
			"WatOtherSource": {
				"3640a158-2fa5-434c-8d6d-2e7fb4792086": {
					"uId": "3640a158-2fa5-434c-8d6d-2e7fb4792086",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatTravelSituationSource"
							},
							"rightExpression": {
								"type": 0,
								"value": "d3e39763-6521-4019-8b6e-da7db8431c5a",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"WatActionRequired": {
				"2a47505c-9953-4c4a-a64e-742d70592554": {
					"uId": "2a47505c-9953-4c4a-a64e-742d70592554",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatActionable"
							},
							"rightExpression": {
								"type": 0,
								"value": "7ecfa448-377a-41a0-9167-af48f5f19bcd",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"WatApprovalRequired": {
				"1a12288c-931f-4695-a50c-8bcfd7d938a9": {
					"uId": "1a12288c-931f-4695-a50c-8bcfd7d938a9",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatActionable"
							},
							"rightExpression": {
								"type": 0,
								"value": "7ecfa448-377a-41a0-9167-af48f5f19bcd",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"WatActionable": {
				"63d337f1-0b39-4fdc-b19c-5156ca51d8e9": {
					"uId": "63d337f1-0b39-4fdc-b19c-5156ca51d8e9",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "CreatedOn"
							}
						}
					]
				}
			},
			"NotesControlGroup": {
				"70f6f037-e743-441a-a809-2a594178f925": {
					"uId": "70f6f037-e743-441a-a809-2a594178f925",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 0,
								"value": 1,
								"dataValueType": 4
							},
							"rightExpression": {
								"type": 0,
								"value": 2,
								"dataValueType": 4
							}
						}
					]
				}
			},
			"WatApproverUser": {
				"2ac83809-85ff-4882-8f45-5b2375552d26": {
					"uId": "2ac83809-85ff-4882-8f45-5b2375552d26",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatActionable"
							},
							"rightExpression": {
								"type": 0,
								"value": "7ecfa448-377a-41a0-9167-af48f5f19bcd",
								"dataValueType": 10
							}
						}
					]
				},
				"10bd855f-13b2-442c-a02c-c2817d86ae96": {
					"uId": "10bd855f-13b2-442c-a02c-c2817d86ae96",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatApprovalRequired"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				},
				"f59d1afd-f5e1-426a-9bcb-df26546f8eb4": {
					"uId": "f59d1afd-f5e1-426a-9bcb-df26546f8eb4",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatApprovalRequired"
							},
							"rightExpression": {
								"type": 0,
								"value": false,
								"dataValueType": 12
							}
						},
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "WatApproverUser"
							}
						}
					]
				}
			},
			"WatDestination": {
				"00ed1e95-6984-4be8-a8bc-9c45d64c8667": {
					"uId": "00ed1e95-6984-4be8-a8bc-9c45d64c8667",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "CreatedOn"
							}
						}
					]
				}
			},
			"Tabb96e584fTabLabel": {
				"8330ec57-671a-4440-a18f-111bb98485ff": {
					"uId": "8330ec57-671a-4440-a18f-111bb98485ff",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatActionable"
							},
							"rightExpression": {
								"type": 0,
								"value": "7ecfa448-377a-41a0-9167-af48f5f19bcd",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatApprovalRequired"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						},
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "WatApproverUser"
							}
						}
					]
				}
			},
			"WatReporterStaff": {
				"da092d06-a334-43a4-b8d1-401b191b7348": {
					"uId": "da092d06-a334-43a4-b8d1-401b191b7348",
					"enabled": true,
					"removed": false,
					"ruleType": 1,
					"baseAttributePatch": "Account.Type",
					"comparisonType": 3,
					"autoClean": false,
					"autocomplete": false,
					"type": 0,
					"value": "57412fad-53e6-df11-971b-001d60e938c6",
					"dataValueType": 10
				}
			}
		}/**SCHEMA_BUSINESS_RULES*/,
		methods: {},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"name": "WatStatus8060c904-990b-4758-8f35-ab81a9f21ba5",
				"values": {
					"layout": {
						"colSpan": 7,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "WatStatus",
					"enabled": false,
					"contentType": 3
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "LOOKUP3f09833f-80f1-4bc4-9733-0b91ab1bde44",
				"values": {
					"layout": {
						"colSpan": 7,
						"rowSpan": 1,
						"column": 7,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "WatTravelIncident",
					"enabled": true,
					"contentType": 3
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "LOOKUPa15e7cdf-4ac3-4d61-a1ef-6a02a4a48268",
				"values": {
					"layout": {
						"colSpan": 7,
						"rowSpan": 1,
						"column": 14,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "WatReporter",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "DATETIME014d9e21-e177-4e4b-b008-5c4d4dd4675b",
				"values": {
					"layout": {
						"colSpan": 7,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "WatDateReceived",
					"enabled": true
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "LOOKUP1668f1b1-529c-4817-9965-7522bb80f31c",
				"values": {
					"layout": {
						"colSpan": 7,
						"rowSpan": 1,
						"column": 7,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "WatTravelSituationPriority",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.LOOKUP1668f1b1529c481799657522bb80f31cLabelCaption"
						}
					},
					"enabled": true,
					"contentType": 3
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "LOOKUP9e609578-f4e9-4cbd-82ef-1bd839a10976",
				"values": {
					"layout": {
						"colSpan": 7,
						"rowSpan": 1,
						"column": 14,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "WatReporterStaff",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "LOOKUP1058241d-644b-4d29-84b2-9fa4814bfa4a",
				"values": {
					"layout": {
						"colSpan": 7,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Header"
					},
					"bindTo": "WatDestination",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "LOOKUP4ad2f693-64fb-40ab-ac40-99e09ab99b72",
				"values": {
					"layout": {
						"colSpan": 7,
						"rowSpan": 1,
						"column": 7,
						"row": 2,
						"layoutName": "Header"
					},
					"bindTo": "WatTravelSituationSource",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.LOOKUP4ad2f69364fb40abac4099e09ab99b72LabelCaption"
						}
					},
					"enabled": true,
					"contentType": 3
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "BOOLEANc1ff5d61-d339-4252-ab0e-2bbb5afb18d0",
				"values": {
					"layout": {
						"colSpan": 7,
						"rowSpan": 1,
						"column": 14,
						"row": 2,
						"layoutName": "Header"
					},
					"bindTo": "WatFindTravellers",
					"enabled": true
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 8
			},
			{
				"operation": "insert",
				"name": "STRINGbd0d7e69-221f-4fc6-869b-bcc5352ffb7b",
				"values": {
					"layout": {
						"colSpan": 7,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Header"
					},
					"bindTo": "WatCityState",
					"enabled": true
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 9
			},
			{
				"operation": "insert",
				"name": "STRING9ae2b230-c3bf-4c23-9ef5-a0f222077d75",
				"values": {
					"layout": {
						"colSpan": 7,
						"rowSpan": 1,
						"column": 7,
						"row": 3,
						"layoutName": "Header"
					},
					"bindTo": "WatOtherSource",
					"enabled": true
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 10
			},
			{
				"operation": "insert",
				"name": "STRINGe79b3fc0-1383-4a74-b13f-59e85e2356b1",
				"values": {
					"layout": {
						"colSpan": 21,
						"rowSpan": 2,
						"column": 0,
						"row": 4,
						"layoutName": "Header"
					},
					"bindTo": "WatIncidentDescription",
					"enabled": true,
					"contentType": 0
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 11
			},
			{
				"operation": "insert",
				"name": "LOOKUP5b11637a-7592-4f76-97ad-3c62da501683",
				"values": {
					"layout": {
						"colSpan": 4,
						"rowSpan": 1,
						"column": 0,
						"row": 7,
						"layoutName": "Header"
					},
					"bindTo": "WatActionable",
					"enabled": true,
					"contentType": 3,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.LOOKUP5b11637a75924f7697ad3c62da501683LabelCaption"
						}
					}
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 12
			},
			{
				"operation": "insert",
				"name": "STRINGc5f023bd-bbfb-442f-8883-5840437066cc",
				"values": {
					"layout": {
						"colSpan": 21,
						"rowSpan": 2,
						"column": 0,
						"row": 8,
						"layoutName": "Header"
					},
					"bindTo": "WatActionRequired",
					"enabled": true,
					"contentType": 0
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 13
			},
			{
				"operation": "insert",
				"name": "BOOLEANb870525d-8a36-49d8-88f4-97ce016ba878",
				"values": {
					"layout": {
						"colSpan": 7,
						"rowSpan": 1,
						"column": 0,
						"row": 10,
						"layoutName": "Header"
					},
					"bindTo": "WatApprovalRequired",
					"enabled": true
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 14
			},
			{
				"operation": "insert",
				"name": "LOOKUP1caba30c-b420-4a5f-b084-bdbd2749bb52",
				"values": {
					"layout": {
						"colSpan": 7,
						"rowSpan": 1,
						"column": 0,
						"row": 11,
						"layoutName": "Header"
					},
					"bindTo": "WatApproverUser",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 15
			},
			{
				"operation": "insert",
				"name": "Tabb6e341b9TabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tabb6e341b9TabLabelTabCaption"
					},
					"items": [],
					"order": 0
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatSchema7713dc14Detail48683a0c",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tabb6e341b9TabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tabb96e584fTabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.TabVisaCaption"
					},
					"items": [],
					"order": 1
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Tabb96e584fTabLabelGroup9fe6db15",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tabb96e584fTabLabelGroup9fe6db15GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tabb96e584fTabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tabb96e584fTabLabelGridLayoutaefc7f8f",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tabb96e584fTabLabelGroup9fe6db15",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatApprovalRequireda620e158-11ec-409f-9550-c9bbc3bd901d",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tabb96e584fTabLabelGridLayoutaefc7f8f"
					},
					"bindTo": "WatApprovalRequired"
				},
				"parentName": "Tabb96e584fTabLabelGridLayoutaefc7f8f",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatApproverUser81dd492f-d06c-4df5-95ba-3f5229560270",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Tabb96e584fTabLabelGridLayoutaefc7f8f"
					},
					"bindTo": "WatApproverUser"
				},
				"parentName": "Tabb96e584fTabLabelGridLayoutaefc7f8f",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "VisaDetailV2bc363af7",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tabb96e584fTabLabel",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Tab96a2d2d5TabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab96a2d2d5TabLabelTabCaption"
					},
					"items": [],
					"order": 2
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "WatSchemaCustomEmailCallDetaile9ce3d54",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab96a2d2d5TabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatSchemaCustomTaskDetailc14592c7",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab96a2d2d5TabLabel",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "NotesAndFilesTab",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.NotesAndFilesTabCaption"
					},
					"items": [],
					"order": 3
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "DocumentDetailV20246bfef",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Files",
				"values": {
					"itemType": 2
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "NotesControlGroup",
				"values": {
					"itemType": 15,
					"caption": {
						"bindTo": "Resources.Strings.NotesGroupCaption"
					},
					"items": []
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "Notes",
				"values": {
					"bindTo": "WatNotes",
					"dataValueType": 1,
					"contentType": 4,
					"layout": {
						"column": 0,
						"row": 0,
						"colSpan": 24
					},
					"labelConfig": {
						"visible": false
					},
					"controlConfig": {
						"imageLoaded": {
							"bindTo": "insertImagesToNotes"
						},
						"images": {
							"bindTo": "NotesImagesCollection"
						}
					}
				},
				"parentName": "NotesControlGroup",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "merge",
				"name": "ESNTab",
				"values": {
					"order": 4
				}
			},
			{
				"operation": "insert",
				"name": "WatSchema94c7e3c8Detailc33aac1a",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "ESNTab",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tab9862b203TabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab9862b203TabLabelTabCaption"
					},
					"items": [],
					"order": 5
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "Tab9862b203TabLabelGroup48594ea4",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab9862b203TabLabelGroup48594ea4GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tab9862b203TabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tab9862b203TabLabelGridLayout6882a71e",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tab9862b203TabLabelGroup48594ea4",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "BOOLEAN23ab56c9-cdf2-47e4-b05b-c9ec8ee8e167",
				"values": {
					"layout": {
						"colSpan": 5,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tab9862b203TabLabelGridLayout6882a71e"
					},
					"bindTo": "WatRefreshRecipients",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.BOOLEAN23ab56c9cdf247e4b05bc9ec8ee8e167LabelCaption"
						}
					},
					"enabled": true
				},
				"parentName": "Tab9862b203TabLabelGridLayout6882a71e",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "STRING02911006-2c94-4d60-b8ef-74f07558c128",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 4,
						"column": 0,
						"row": 1,
						"layoutName": "Tab9862b203TabLabelGridLayout6882a71e"
					},
					"bindTo": "WatRecipients",
					"enabled": true,
					"contentType": 0
				},
				"parentName": "Tab9862b203TabLabelGridLayout6882a71e",
				"propertyName": "items",
				"index": 1
			}
		]/**SCHEMA_DIFF*/
	};
});
