define("WatHospitalityRequest1Page", [], function() {
	return {
		entitySchemaName: "WatHospitalityRequest",
		attributes: {},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{
			"Files": {
				"schemaName": "FileDetailV2",
				"entitySchemaName": "WatHospitalityRequestFile",
				"filter": {
					"masterColumn": "Id",
					"detailColumn": "WatHospitalityRequest"
				}
			},
			"WatSchemae3d19951Detail650a3f1b": {
				"schemaName": "WatSchemae3d19951Detail",
				"entitySchemaName": "WatHospitalityChange",
				"filter": {
					"detailColumn": "WatHospitalityRequest",
					"masterColumn": "Id"
				}
			}
		}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{
			"WatSubmitRequest": {
				"80d43796-795f-4181-b3a9-63cc28634248": {
					"uId": "80d43796-795f-4181-b3a9-63cc28634248",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatWasSubmitted"
							},
							"rightExpression": {
								"type": 0,
								"value": false,
								"dataValueType": 12
							}
						}
					]
				},
				"8411f702-4309-4c96-b594-00ed059ba3e3": {
					"uId": "8411f702-4309-4c96-b594-00ed059ba3e3",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatHospitalityRequestStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "b017e847-08ae-4142-b7ab-d60445f80092",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatHospitalityRequestStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "724f0c3c-90f7-4d05-bdce-1a33761da7fa",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatHospitalityRequestStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "d64e17a7-c754-41e9-a363-69746cf7ed01",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatHospitalityRequestStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "9b157a42-b3f9-4430-94f4-1337a96b5451",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"WatCancelRequest": {
				"a01337c5-2947-48e0-8cea-1595575b8144": {
					"uId": "a01337c5-2947-48e0-8cea-1595575b8144",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatHospitalityRequestStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "ee30b465-2b83-4d12-8f68-2383ff26669e",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatHospitalityRequestStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "9b157a42-b3f9-4430-94f4-1337a96b5451",
								"dataValueType": 10
							}
						}
					]
				},
				"078fbec8-2458-447c-9ab4-8278e1d072af": {
					"uId": "078fbec8-2458-447c-9ab4-8278e1d072af",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatCancelRequest"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"WatProjectorDisplay": {
				"16e3c10d-42df-46c6-b28f-86401635f252": {
					"uId": "16e3c10d-42df-46c6-b28f-86401635f252",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatHospitalityService"
							},
							"rightExpression": {
								"type": 0,
								"value": "aa458cdd-1683-430b-8708-557ddb1f44b0",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"WatLaptop": {
				"eb258646-3c0a-4a25-907d-1844d0342088": {
					"uId": "eb258646-3c0a-4a25-907d-1844d0342088",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatHospitalityService"
							},
							"rightExpression": {
								"type": 0,
								"value": "aa458cdd-1683-430b-8708-557ddb1f44b0",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"WatConfirmed": {
				"7d933d8d-af32-4450-96de-a8fa13b657ff": {
					"uId": "7d933d8d-af32-4450-96de-a8fa13b657ff",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatHospitalityRequestStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "724f0c3c-90f7-4d05-bdce-1a33761da7fa",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatHospitalityRequestStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "d64e17a7-c754-41e9-a363-69746cf7ed01",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatHospitalityRequestStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "9b157a42-b3f9-4430-94f4-1337a96b5451",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"WatCancelled": {
				"03c0f64e-5e85-4bb4-a7a8-925bb6b326e1": {
					"uId": "03c0f64e-5e85-4bb4-a7a8-925bb6b326e1",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatCancelled"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"WatServiceDate": {
				"91bf6846-a7e8-4929-b9b9-417cf3bd0de2": {
					"uId": "91bf6846-a7e8-4929-b9b9-417cf3bd0de2",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatHospitalityRequestStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "724f0c3c-90f7-4d05-bdce-1a33761da7fa",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatHospitalityRequestStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "d64e17a7-c754-41e9-a363-69746cf7ed01",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatHospitalityRequestStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "9b157a42-b3f9-4430-94f4-1337a96b5451",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"WatServiceTime": {
				"a8b26b3b-5b68-497a-821e-7e7b4b260f3f": {
					"uId": "a8b26b3b-5b68-497a-821e-7e7b4b260f3f",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatHospitalityRequestStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "724f0c3c-90f7-4d05-bdce-1a33761da7fa",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatHospitalityRequestStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "d64e17a7-c754-41e9-a363-69746cf7ed01",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatHospitalityRequestStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "9b157a42-b3f9-4430-94f4-1337a96b5451",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"WatServiceLocation": {
				"20cde245-68bf-4fe4-8980-21fb23c991c9": {
					"uId": "20cde245-68bf-4fe4-8980-21fb23c991c9",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatHospitalityRequestStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "724f0c3c-90f7-4d05-bdce-1a33761da7fa",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatHospitalityRequestStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "d64e17a7-c754-41e9-a363-69746cf7ed01",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatHospitalityRequestStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "9b157a42-b3f9-4430-94f4-1337a96b5451",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"WatHospitalityService": {
				"ed4c132b-b8fd-4351-86d9-36374eb65a93": {
					"uId": "ed4c132b-b8fd-4351-86d9-36374eb65a93",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "CreatedOn"
							}
						}
					]
				}
			},
			"Tabcb7d840fTabLabel": {
				"dac54e50-0f49-4bd3-959f-2a13d8cbf9f3": {
					"uId": "dac54e50-0f49-4bd3-959f-2a13d8cbf9f3",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatHospitalityRequestStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "ee30b465-2b83-4d12-8f68-2383ff26669e",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"Tab49adea4dTabLabel": {
				"7a40eaf4-b93d-43c6-bcd4-aba2abad56bd": {
					"uId": "7a40eaf4-b93d-43c6-bcd4-aba2abad56bd",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatHospitalityRequestStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "ee30b465-2b83-4d12-8f68-2383ff26669e",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CreatedOn": {
				"bda54015-c055-428d-8fcc-3a238d0332ce": {
					"uId": "bda54015-c055-428d-8fcc-3a238d0332ce",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "CreatedOn"
							}
						}
					]
				}
			},
			"CreatedBy": {
				"6daea289-ed43-486a-aad7-f596dcc7981e": {
					"uId": "6daea289-ed43-486a-aad7-f596dcc7981e",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "CreatedOn"
							}
						}
					]
				}
			},
			"WatPax": {
				"2124f788-5669-4bfc-9641-d46e31420880": {
					"uId": "2124f788-5669-4bfc-9641-d46e31420880",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatHospitalityRequestStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "724f0c3c-90f7-4d05-bdce-1a33761da7fa",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatHospitalityRequestStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "d64e17a7-c754-41e9-a363-69746cf7ed01",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatHospitalityRequestStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "9b157a42-b3f9-4430-94f4-1337a96b5451",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"WatServiceEndTime": {
				"2c314230-53ca-465f-a54d-1fa92b824398": {
					"uId": "2c314230-53ca-465f-a54d-1fa92b824398",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatHospitalityRequestStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "724f0c3c-90f7-4d05-bdce-1a33761da7fa",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatHospitalityRequestStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "d64e17a7-c754-41e9-a363-69746cf7ed01",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatHospitalityRequestStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "9b157a42-b3f9-4430-94f4-1337a96b5451",
								"dataValueType": 10
							}
						}
					]
				}
			}
		}/**SCHEMA_BUSINESS_RULES*/,
		methods: {},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"name": "LOOKUPfe014765-c201-4bf3-8e79-4a1e06b49c3f",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "WatHospitalityService",
					"enabled": true,
					"contentType": 3
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "LOOKUP776e6dc5-c629-4fc7-8e6b-c284577237f5",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "WatDelegation",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "DATETIMEc150d2c0-a213-4811-af86-db237a0b201c",
				"values": {
					"layout": {
						"colSpan": 6,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "WatServiceDate",
					"enabled": true
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "CreatedOnf4d50890-829f-47b2-ab17-7101cdfdd4aa",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "CreatedOn",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "DATETIMEd4b3945e-cb43-4c1f-adbb-0d09f6f37f31",
				"values": {
					"layout": {
						"colSpan": 6,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Header"
					},
					"bindTo": "WatServiceTime",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.DATETIMEd4b3945ecb434c1fadbb0d09f6f37f31LabelCaption"
						}
					},
					"enabled": true
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "DATETIME2ec04f62-2449-4d22-a46d-b0202fb3563b",
				"values": {
					"layout": {
						"colSpan": 6,
						"rowSpan": 1,
						"column": 6,
						"row": 2,
						"layoutName": "Header"
					},
					"bindTo": "WatServiceEndTime",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.DATETIME2ec04f6224494d22a46db0202fb3563bLabelCaption"
						}
					},
					"enabled": true
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "CreatedBybe10a2ae-e980-4056-895a-be26570f309b",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 2,
						"layoutName": "Header"
					},
					"bindTo": "CreatedBy",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "STRING11927847-4bf4-4e80-a62c-5b3069592dd4",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Header"
					},
					"bindTo": "WatPax",
					"enabled": true
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "STRING632e4c0c-016d-4221-9e04-2c74495b51fb",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "Header"
					},
					"bindTo": "WatServiceLocation",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.STRING632e4c0c016d42219e042c74495b51fbLabelCaption"
						}
					},
					"enabled": true,
					"contentType": 0
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 8
			},
			{
				"operation": "insert",
				"name": "BOOLEAN3f30ccf6-a0e7-483b-b486-833cc7213e22",
				"values": {
					"layout": {
						"colSpan": 5,
						"rowSpan": 1,
						"column": 0,
						"row": 5,
						"layoutName": "Header"
					},
					"bindTo": "WatLaptop",
					"enabled": true
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 9
			},
			{
				"operation": "insert",
				"name": "BOOLEANc5636ca9-d352-4ceb-a0f2-9add0b2a7197",
				"values": {
					"layout": {
						"colSpan": 5,
						"rowSpan": 1,
						"column": 5,
						"row": 5,
						"layoutName": "Header"
					},
					"bindTo": "WatProjectorDisplay",
					"enabled": true
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 10
			},
			{
				"operation": "insert",
				"name": "STRING3bcf7144-de20-4d81-b32d-e14f78db1a5b",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 2,
						"column": 0,
						"row": 6,
						"layoutName": "Header"
					},
					"bindTo": "WatSpecialInstructions",
					"enabled": true,
					"contentType": 0
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 11
			},
			{
				"operation": "insert",
				"name": "BOOLEAN29b2155f-962a-448a-9b2d-31cdb78dd996",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 8,
						"layoutName": "Header"
					},
					"bindTo": "WatSubmitRequest",
					"enabled": true
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 12
			},
			{
				"operation": "insert",
				"name": "BOOLEAN9eb95abc-1ec1-4d1e-9b3c-456185fd2cd3",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 9,
						"layoutName": "Header"
					},
					"bindTo": "WatCancelRequest",
					"enabled": true
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 13
			},
			{
				"operation": "insert",
				"name": "Tabcb7d840fTabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tabcb7d840fTabLabelTabCaption"
					},
					"items": [],
					"order": 0
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tabcb7d840fTabLabelGroupb676daa7",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tabcb7d840fTabLabelGroupb676daa7GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tabcb7d840fTabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tabcb7d840fTabLabelGridLayout271fd15b",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tabcb7d840fTabLabelGroupb676daa7",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "STRING280ca61b-acdd-41e9-8cdc-dccbf969b0a7",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tabcb7d840fTabLabelGridLayout271fd15b"
					},
					"bindTo": "WatConfirmationNumber",
					"enabled": true
				},
				"parentName": "Tabcb7d840fTabLabelGridLayout271fd15b",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "BOOLEAN0c835ae0-51ba-4ef8-8880-36094ad27e3d",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "Tabcb7d840fTabLabelGridLayout271fd15b"
					},
					"bindTo": "WatConfirmed",
					"enabled": true
				},
				"parentName": "Tabcb7d840fTabLabelGridLayout271fd15b",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "FLOAT1b9c2e87-b78b-4366-9f5c-7068280546ac",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Tabcb7d840fTabLabelGridLayout271fd15b"
					},
					"bindTo": "WatCost",
					"enabled": true
				},
				"parentName": "Tabcb7d840fTabLabelGridLayout271fd15b",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "BOOLEANe24b63b4-c029-4890-932a-21bd1988f019",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "Tabcb7d840fTabLabelGridLayout271fd15b"
					},
					"bindTo": "WatCancelled",
					"enabled": true
				},
				"parentName": "Tabcb7d840fTabLabelGridLayout271fd15b",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "STRINGbc9008e8-de73-4cd0-9814-51d785f5e2c3",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Tabcb7d840fTabLabelGridLayout271fd15b"
					},
					"bindTo": "WatSupplierVendor",
					"enabled": true
				},
				"parentName": "Tabcb7d840fTabLabelGridLayout271fd15b",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "STRING77ed6908-7fec-4d29-a92f-c7bf345d511f",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Tabcb7d840fTabLabelGridLayout271fd15b"
					},
					"bindTo": "WatAdminComments",
					"enabled": true,
					"contentType": 0
				},
				"parentName": "Tabcb7d840fTabLabelGridLayout271fd15b",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "Tab49adea4dTabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab49adea4dTabLabelTabCaption"
					},
					"items": [],
					"order": 1
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "WatSchemae3d19951Detail650a3f1b",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab49adea4dTabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "NotesAndFilesTab",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.NotesAndFilesTabCaption"
					},
					"items": [],
					"order": 2
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "Files",
				"values": {
					"itemType": 2
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "NotesControlGroup",
				"values": {
					"itemType": 15,
					"caption": {
						"bindTo": "Resources.Strings.NotesGroupCaption"
					},
					"items": []
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Notes",
				"values": {
					"bindTo": "WatNotes",
					"dataValueType": 1,
					"contentType": 4,
					"layout": {
						"column": 0,
						"row": 0,
						"colSpan": 24
					},
					"labelConfig": {
						"visible": false
					},
					"controlConfig": {
						"imageLoaded": {
							"bindTo": "insertImagesToNotes"
						},
						"images": {
							"bindTo": "NotesImagesCollection"
						}
					}
				},
				"parentName": "NotesControlGroup",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "merge",
				"name": "ESNTab",
				"values": {
					"order": 3
				}
			}
		]/**SCHEMA_DIFF*/
	};
});
