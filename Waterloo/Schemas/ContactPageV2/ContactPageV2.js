define("ContactPageV2", [], function() {
	return {
		entitySchemaName: "Contact",
		attributes: {},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{
			"WatSchemaTravelerDetailc9de5c0c": {
				"schemaName": "WatSchemaTravelerDetail",
				"entitySchemaName": "WatTraveler",
				"filter": {
					"detailColumn": "WatContact",
					"masterColumn": "Id"
				}
			},
			"WatSchemaTripsDetailfcbe0855": {
				"schemaName": "WatSchemaTripsDetail",
				"entitySchemaName": "WatTrip",
				"filter": {
					"detailColumn": "WatRequesterContact",
					"masterColumn": "Id"
				}
			},
			"WatSchemaf2370fffDetailedab100a": {
				"schemaName": "WatSchemaf2370fffDetail",
				"entitySchemaName": "WatLearnHistory",
				"filter": {
					"detailColumn": "WatContact",
					"masterColumn": "Id"
				}
			},
			"WatSchemaLearnRegistrationDetail5cbaed41": {
				"schemaName": "WatSchemaLearnRegistrationDetail",
				"entitySchemaName": "WatLEARNRegistration",
				"filter": {
					"detailColumn": "WatContact",
					"masterColumn": "Id"
				}
			},
			"WatSchema0efe663cDetail68b43bf9": {
				"schemaName": "WatSchema0efe663cDetail",
				"entitySchemaName": "WatDelegation",
				"filter": {
					"detailColumn": "WatPrimaryContact",
					"masterColumn": "Id"
				}
			},
			"WatSchemafa655ca7Detail9f1ee8dc": {
				"schemaName": "WatSchemafa655ca7Detail",
				"entitySchemaName": "WatDelegationMembers",
				"filter": {
					"detailColumn": "WatDelegationContact",
					"masterColumn": "Id"
				}
			},
			"DocumentDetailV214f1ec9a": {
				"schemaName": "DocumentDetailV2",
				"entitySchemaName": "Document",
				"filter": {
					"detailColumn": "Contact",
					"masterColumn": "Id"
				}
			},
			"WatSchema94c7e3c8Detail13c2ea4a": {
				"schemaName": "WatSchema94c7e3c8Detail",
				"entitySchemaName": "WatDatedNotes",
				"filter": {
					"detailColumn": "WatContact",
					"masterColumn": "Id"
				}
			},
			"WatSchemaCustomEmailCallDetail568506d9": {
				"schemaName": "WatSchemaCustomEmailCallDetail",
				"entitySchemaName": "Activity",
				"filter": {
					"detailColumn": "Contact",
					"masterColumn": "Id"
				}
			},
			"WatSchemaCustomTaskDetaild903e9ec": {
				"schemaName": "WatSchemaCustomTaskDetail",
				"entitySchemaName": "Activity",
				"filter": {
					"detailColumn": "Contact",
					"masterColumn": "Id"
				}
			}
		}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{
			"WatLearn1": {
				"3adfbdce-799e-4566-8db8-9ac847677d32": {
					"uId": "3adfbdce-799e-4566-8db8-9ac847677d32",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "WatLearn2"
							}
						}
					]
				}
			},
			"Tabe8de0020TabLabel": {
				"f90ff9ad-dc7d-4f48-828b-e2c2cd60ea36": {
					"uId": "f90ff9ad-dc7d-4f48-828b-e2c2cd60ea36",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "Type"
							},
							"rightExpression": {
								"type": 0,
								"value": "00783ef6-f36b-1410-a883-16d83cab0980",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"WatWatIAMId": {
				"61f936ff-f886-41f8-8177-4b9f559798f6": {
					"uId": "61f936ff-f886-41f8-8177-4b9f559798f6",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "Type"
							},
							"rightExpression": {
								"type": 0,
								"value": "00783ef6-f36b-1410-a883-16d83cab0980",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"WatTravellerType": {
				"cc5d2056-eb14-466a-b653-0ce6eba7b2ad": {
					"uId": "cc5d2056-eb14-466a-b653-0ce6eba7b2ad",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "Type"
							},
							"rightExpression": {
								"type": 0,
								"value": "00783ef6-f36b-1410-a883-16d83cab0980",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"WatFaculty": {
				"5f656840-9ffb-4e67-b9da-669119f6fb02": {
					"uId": "5f656840-9ffb-4e67-b9da-669119f6fb02",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "Type"
							},
							"rightExpression": {
								"type": 0,
								"value": "00783ef6-f36b-1410-a883-16d83cab0980",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"NotesControlGroup": {
				"7634185c-4fe4-4e78-8db4-b2e6eab387df": {
					"uId": "7634185c-4fe4-4e78-8db4-b2e6eab387df",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 0,
								"value": 1,
								"dataValueType": 4
							},
							"rightExpression": {
								"type": 0,
								"value": 2,
								"dataValueType": 4
							}
						}
					]
				}
			},
			"GeneralInfoTabGroupc3ce40b3": {
				"aef4ea94-36c7-4b36-a4bf-2591c126df1e": {
					"uId": "aef4ea94-36c7-4b36-a4bf-2591c126df1e",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "Type"
							},
							"rightExpression": {
								"type": 0,
								"value": "00783ef6-f36b-1410-a883-16d83cab0980",
								"dataValueType": 10
							}
						}
					]
				}
			}
		}/**SCHEMA_BUSINESS_RULES*/,
		methods: {},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "merge",
				"name": "PhotoTimeZoneContainer",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0
					}
				}
			},
			{
				"operation": "insert",
				"name": "GivenName8251e217-f912-4b9e-b464-a89e210df7f7",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "GivenName",
					"enabled": true
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "MiddleNamef2719dae-0dd9-4d9f-9519-e782c2cd913c",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "MiddleName"
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "Surnamed2b925af-5d15-44f1-92da-59b37429a977",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "Surname",
					"enabled": true
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "merge",
				"name": "JobTitleProfile",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 4
					}
				}
			},
			{
				"operation": "merge",
				"name": "AccountMobilePhone",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 5
					}
				}
			},
			{
				"operation": "merge",
				"name": "AccountPhone",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 6
					}
				}
			},
			{
				"operation": "merge",
				"name": "AccountEmail",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 7
					}
				}
			},
			{
				"operation": "merge",
				"name": "GeneralInfoTab",
				"values": {
					"order": 0
				}
			},
			{
				"operation": "merge",
				"name": "Type",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0
					}
				}
			},
			{
				"operation": "merge",
				"name": "Owner",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0
					}
				}
			},
			{
				"operation": "merge",
				"name": "SalutationType",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1
					}
				}
			},
			{
				"operation": "merge",
				"name": "Gender",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1
					}
				}
			},
			{
				"operation": "insert",
				"name": "BirthDate0d3547b5-1170-4a15-9217-2c44fbd949d1",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "ContactGeneralInfoBlock"
					},
					"bindTo": "BirthDate"
				},
				"parentName": "ContactGeneralInfoBlock",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "merge",
				"name": "Language",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 2
					}
				}
			},
			{
				"operation": "move",
				"name": "Language",
				"parentName": "ContactGeneralInfoBlock",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "merge",
				"name": "Age",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 3
					}
				}
			},
			{
				"operation": "insert",
				"name": "GeneralInfoTabGroupc3ce40b3",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.GeneralInfoTabGroupc3ce40b3GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "GeneralInfoTab",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "GeneralInfoTabGridLayoutad546805",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "GeneralInfoTabGroupc3ce40b3",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatWatIAMId26ea370d-c622-4484-8c83-7d86ddff1001",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "GeneralInfoTabGridLayoutad546805"
					},
					"bindTo": "WatWatIAMId"
				},
				"parentName": "GeneralInfoTabGridLayoutad546805",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatFacultyc5623f0a-63d1-4ea5-9be0-d409eb447573",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "GeneralInfoTabGridLayoutad546805"
					},
					"bindTo": "WatFaculty"
				},
				"parentName": "GeneralInfoTabGridLayoutad546805",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "STRINGf1f2b956-c79f-40c3-9161-04185ed6cd03",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "GeneralInfoTabGridLayoutad546805"
					},
					"bindTo": "WatStudentID",
					"enabled": false
				},
				"parentName": "GeneralInfoTabGridLayoutad546805",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "WatTravellerType29e1da7d-86bc-4c81-89ce-07a910c8bf1d",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "GeneralInfoTabGridLayoutad546805"
					},
					"bindTo": "WatTravellerType"
				},
				"parentName": "GeneralInfoTabGridLayoutad546805",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "Tabb417090eTabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tabb417090eTabLabelTabCaption"
					},
					"items": [],
					"order": 1
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "WatSchema0efe663cDetail68b43bf9",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tabb417090eTabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatSchemafa655ca7Detail9f1ee8dc",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tabb417090eTabLabel",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Tabe8de0020TabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tabe8de0020TabLabelTabCaption"
					},
					"items": [],
					"order": 2
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "Tabe8de0020TabLabelGroup1fc0d7f3",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tabe8de0020TabLabelGroup1fc0d7f3GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tabe8de0020TabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tabe8de0020TabLabelGridLayout8d44187b",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tabe8de0020TabLabelGroup1fc0d7f3",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "STRING6ad668ab-976c-4974-bf8d-9ba7a4b3666b",
				"values": {
					"layout": {
						"colSpan": 7,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tabe8de0020TabLabelGridLayout8d44187b"
					},
					"bindTo": "WatLearn1",
					"enabled": false,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.STRING6ad668ab976c4974bf8d9ba7a4b3666bLabelCaption"
						}
					}
				},
				"parentName": "Tabe8de0020TabLabelGridLayout8d44187b",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "DATETIME21a11305-df39-4ec6-9626-0548a6173ab1",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 16,
						"row": 0,
						"layoutName": "Tabe8de0020TabLabelGridLayout8d44187b"
					},
					"bindTo": "WatLearn1Completed",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.DATETIME21a11305df394ec696260548a6173ab1LabelCaption"
						}
					},
					"enabled": true
				},
				"parentName": "Tabe8de0020TabLabelGridLayout8d44187b",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "DATETIME6b131c1d-d50f-4ea2-a350-fe6b9b9dc371",
				"values": {
					"layout": {
						"colSpan": 9,
						"rowSpan": 1,
						"column": 7,
						"row": 0,
						"layoutName": "Tabe8de0020TabLabelGridLayout8d44187b"
					},
					"bindTo": "WatLearn1Registered",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.DATETIME6b131c1dd50f4ea2a350fe6b9b9dc371LabelCaption"
						}
					},
					"enabled": false
				},
				"parentName": "Tabe8de0020TabLabelGridLayout8d44187b",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "WatSchemaTravelerDetailc9de5c0c",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tabe8de0020TabLabel",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "WatSchemaTripsDetailfcbe0855",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tabe8de0020TabLabel",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "WatSchemaLearnRegistrationDetail5cbaed41",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tabe8de0020TabLabel",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "WatSchemaf2370fffDetailedab100a",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tabe8de0020TabLabel",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "merge",
				"name": "JobTabContainer",
				"values": {
					"order": 3
				}
			},
			{
				"operation": "merge",
				"name": "Job",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0
					}
				}
			},
			{
				"operation": "merge",
				"name": "JobTitle",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0
					}
				}
			},
			{
				"operation": "merge",
				"name": "Department",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1
					}
				}
			},
			{
				"operation": "merge",
				"name": "DecisionRole",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1
					}
				}
			},
			{
				"operation": "merge",
				"name": "TimelineTab",
				"values": {
					"order": 4
				}
			},
			{
				"operation": "move",
				"name": "TimelineTab",
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 4
			},
			{
				"operation": "merge",
				"name": "HistoryTab",
				"values": {
					"order": 5
				}
			},
			{
				"operation": "insert",
				"name": "WatSchemaCustomEmailCallDetail568506d9",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "HistoryTab",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatSchemaCustomTaskDetaild903e9ec",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "HistoryTab",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "merge",
				"name": "NotesAndFilesTab",
				"values": {
					"order": 6
				}
			},
			{
				"operation": "insert",
				"name": "DocumentDetailV214f1ec9a",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "merge",
				"name": "ESNTab",
				"values": {
					"order": 7
				}
			},
			{
				"operation": "insert",
				"name": "WatSchema94c7e3c8Detail13c2ea4a",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "ESNTab",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tabc36fe4a9TabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tabc36fe4a9TabLabelTabCaption"
					},
					"items": [],
					"order": 8
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 8
			},
			{
				"operation": "insert",
				"name": "Tabc36fe4a9TabLabelGroupc1443d0b",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tabc36fe4a9TabLabelGroupc1443d0bGroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tabc36fe4a9TabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tabc36fe4a9TabLabelGridLayout3a9c461a",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tabc36fe4a9TabLabelGroupc1443d0b",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatWatIAMId066c8138-c3b6-4cb7-ae33-7dfde7e2934d",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tabc36fe4a9TabLabelGridLayout3a9c461a"
					},
					"bindTo": "WatWatIAMId"
				},
				"parentName": "Tabc36fe4a9TabLabelGridLayout3a9c461a",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatStudentIDab4c0bb6-d375-4cd7-8b0b-cc67bc10b4b8",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Tabc36fe4a9TabLabelGridLayout3a9c461a"
					},
					"bindTo": "WatStudentID"
				},
				"parentName": "Tabc36fe4a9TabLabelGridLayout3a9c461a",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "remove",
				"name": "AccountName"
			},
			{
				"operation": "remove",
				"name": "Activities"
			},
			{
				"operation": "remove",
				"name": "Contract"
			},
			{
				"operation": "remove",
				"name": "Calls"
			},
			{
				"operation": "remove",
				"name": "EmailDetailV2"
			},
			{
				"operation": "remove",
				"name": "Leads"
			},
			{
				"operation": "remove",
				"name": "Opportunities"
			},
			{
				"operation": "remove",
				"name": "Invoice"
			},
			{
				"operation": "remove",
				"name": "Documents"
			},
			{
				"operation": "remove",
				"name": "Project"
			},
			{
				"operation": "remove",
				"name": "Order"
			},
			{
				"operation": "remove",
				"name": "EducationAndCertificate"
			},
			{
				"operation": "remove",
				"name": "Certificate"
			}
		]/**SCHEMA_DIFF*/
	};
});
