define("WatDelegationGifts1MiniPage", [], function() {
	return {
		entitySchemaName: "WatDelegationGifts",
		attributes: {},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{
			"WatDelegation": {
				"87ee8210-072d-4356-9fde-da1655a5ad2f": {
					"uId": "87ee8210-072d-4356-9fde-da1655a5ad2f",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "WatDelegation"
							}
						}
					]
				}
			},
			"WatDelegationInstitution": {
				"1ff61476-640d-438c-9630-4a526efa9357": {
					"uId": "1ff61476-640d-438c-9630-4a526efa9357",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "WatDelegationMember"
							}
						}
					]
				},
				"8e80cc6c-77f2-4f59-bdd0-ee4e56edace6": {
					"uId": "8e80cc6c-77f2-4f59-bdd0-ee4e56edace6",
					"enabled": true,
					"removed": false,
					"ruleType": 1,
					"baseAttributePatch": "WatDelegation",
					"comparisonType": 3,
					"autoClean": true,
					"autocomplete": true,
					"type": 1,
					"attribute": "WatDelegation"
				},
				"3b2cacc0-544d-4929-870a-c58b9e850b27": {
					"uId": "3b2cacc0-544d-4929-870a-c58b9e850b27",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "WatDelegationMember"
							}
						}
					]
				}
			},
			"WatDelegationMember": {
				"791a3237-3445-4678-bc24-352d57b6410c": {
					"uId": "791a3237-3445-4678-bc24-352d57b6410c",
					"enabled": true,
					"removed": false,
					"ruleType": 1,
					"baseAttributePatch": "WatDelegation",
					"comparisonType": 3,
					"autoClean": true,
					"autocomplete": true,
					"type": 1,
					"attribute": "WatDelegation"
				},
				"c78f0ecb-54ba-442d-988d-5253f07157e0": {
					"uId": "c78f0ecb-54ba-442d-988d-5253f07157e0",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "WatDelegationInstitution"
							}
						}
					]
				},
				"56460830-b2d2-48a5-824a-4e41000b8d32": {
					"uId": "56460830-b2d2-48a5-824a-4e41000b8d32",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "WatDelegationInstitution"
							}
						}
					]
				}
			},
			"WatQuantity": {
				"0333fead-4f27-4b26-854f-8865ddfee258": {
					"uId": "0333fead-4f27-4b26-854f-8865ddfee258",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 0,
								"value": "a",
								"dataValueType": 1
							},
							"rightExpression": {
								"type": 0,
								"value": "a",
								"dataValueType": 1
							}
						}
					]
				}
			}
		}/**SCHEMA_BUSINESS_RULES*/,
		methods: {},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "merge",
				"name": "HeaderContainer",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0
					}
				}
			},
			{
				"operation": "insert",
				"name": "HeaderColumnContainer",
				"values": {
					"itemType": 6,
					"caption": {
						"bindTo": "getPrimaryDisplayColumnValue"
					},
					"labelClass": [
						"label-in-header-container"
					],
					"visible": {
						"bindTo": "isNotAddMode"
					}
				},
				"parentName": "HeaderContainer",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "WatGift9703a923-44a0-4360-9508-a164a7c0891f",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "MiniPage"
					},
					"isMiniPageModelItem": true,
					"visible": {
						"bindTo": "isAddMode"
					},
					"bindTo": "WatGift",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "MiniPage",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "WatDelegationMember63ffd884-226d-45d3-b5b1-514170f881d7",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "MiniPage"
					},
					"isMiniPageModelItem": true,
					"visible": {
						"bindTo": "isAddMode"
					},
					"bindTo": "WatDelegationMember",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "MiniPage",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "WatDelegationInstitutionf8082973-9bab-425c-a55e-8687c7656203",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "MiniPage"
					},
					"isMiniPageModelItem": true,
					"visible": {
						"bindTo": "isAddMode"
					},
					"bindTo": "WatDelegationInstitution",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "MiniPage",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "WatQuantity412c2533-05b2-4ccc-965f-1e68c6b5c42b",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "MiniPage"
					},
					"isMiniPageModelItem": true,
					"visible": {
						"bindTo": "isAddMode"
					},
					"bindTo": "WatQuantity"
				},
				"parentName": "MiniPage",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "WatDelegationGiftStatus70e6b0cf-6fbf-45fd-a452-d637c106a29b",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 5,
						"layoutName": "MiniPage"
					},
					"isMiniPageModelItem": true,
					"visible": {
						"bindTo": "isAddMode"
					},
					"bindTo": "WatDelegationGiftStatus",
					"enabled": true,
					"contentType": 3
				},
				"parentName": "MiniPage",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "WatNotesbc071b2e-08bc-4b44-a8e3-eb80c0c82c68",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 6,
						"layoutName": "MiniPage"
					},
					"isMiniPageModelItem": true,
					"visible": {
						"bindTo": "isAddMode"
					},
					"bindTo": "WatNotes"
				},
				"parentName": "MiniPage",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "WatDelegation32e4152f-0249-48a5-b7bb-f66d532817b9",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 7,
						"layoutName": "MiniPage"
					},
					"isMiniPageModelItem": true,
					"visible": {
						"bindTo": "isAddMode"
					},
					"bindTo": "WatDelegation"
				},
				"parentName": "MiniPage",
				"propertyName": "items",
				"index": 7
			}
		]/**SCHEMA_DIFF*/
	};
});
