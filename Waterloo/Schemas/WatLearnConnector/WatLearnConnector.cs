using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Terrasoft.Core;
using Terrasoft.Core.Configuration;

namespace LearnIntegration
{
	public class LEARN
	{
		private static readonly HttpClient client = new HttpClient();
		private static readonly object padlock = new object();
		private static LEARN instance = null;
		private readonly UserConnection userConnection = null;
		private readonly string url = null;
		
		private LEARN(UserConnection userConnection)
		{
			this.userConnection = userConnection;
			this.url = SysSettings.GetValue<string>(userConnection, "LearnBaseUrl", "");
		}

		public static LEARN Instance(UserConnection userConnection)
		{
			lock (padlock)
			{
				if (instance == null)
				{
					instance = new LEARN(userConnection);
				}
				return instance;
			}
		}

		private static async Task<string> DoPost<T>(string url, T content)
		{
			var response = await client.PostAsync(url, content.AsJson());

			var responseString = await response.Content.ReadAsStringAsync();

			return responseString;
		}

		public T LearnRequest<T>(string WatIAMId, string CourseId)
		{
			string extension = typeof(T).Equals(typeof(FinalGradeResponse)) ? "finalgrade" : "enrollment";
			var data = new BaseRequest(userConnection)
			{
				CourseId = CourseId,
				UserName = WatIAMId
			};
			var request = DoPost<BaseRequest>(string.Format("{0}{1}", url, extension), data);
			request.Wait();
			string response = request.Result;
			return JsonConvert.DeserializeObject<T>(response);
		}
	}

	public static class Extensions
	{
		public static StringContent AsJson(this object o)
			=> new StringContent(JsonConvert.SerializeObject(o), Encoding.UTF8, "application/json");
	}

	public class BaseRequest
	{
		public BaseRequest(UserConnection userConnection)
		{
			this.userConnection = userConnection;
			this.SessionId = SysSettings.GetValue<string>(userConnection, "LearnSessionId", "");
		}
		
		[JsonIgnore]
		private readonly UserConnection userConnection = null;
		
		[JsonProperty("sessionid")]
		private readonly string SessionId;

		[JsonProperty("outputtype")]
		private const string OutputType = "Raw";

		[JsonProperty("username")]
		public string UserName { get; set; }

		[JsonProperty("courseid")]
		public string CourseId { get; set; }
	}

	public class BaseResponse
	{
		[JsonProperty("message")]
		public string ErrorMessage { get; set; }
	}

	public class FinalGradeResponse : BaseResponse
	{
		public int? PointsNumerator { get; set; }
		public int? PointsDenominator { get; set; }
		public string DisplayedGrade { get; set; }
		public DateTime LastModified { get; set; }
	}

	public class EnrollmentResponse : BaseResponse
	{
		public string Enrollment { get; set; }
	}
}