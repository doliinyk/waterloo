define("WatAgreementStakeholders1Page", [], function() {
	return {
		entitySchemaName: "WatAgreementStakeholders",
		attributes: {},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{
			"Files": {
				"schemaName": "FileDetailV2",
				"entitySchemaName": "WatAgreementStakeholdersFile",
				"filter": {
					"masterColumn": "Id",
					"detailColumn": "WatAgreementStakeholders"
				}
			}
		}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{
			"WatContact": {
				"afea8636-a735-4931-a84f-97eb23185e57": {
					"uId": "afea8636-a735-4931-a84f-97eb23185e57",
					"enabled": true,
					"removed": false,
					"ruleType": 1,
					"baseAttributePatch": "Account.Type",
					"comparisonType": 3,
					"autoClean": false,
					"autocomplete": false,
					"type": 0,
					"value": "57412fad-53e6-df11-971b-001d60e938c6",
					"dataValueType": 10
				}
			},
			"NotesAndFilesTab": {
				"b9d34b32-fc92-4782-827b-50867077fbe4": {
					"uId": "b9d34b32-fc92-4782-827b-50867077fbe4",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreement"
							}
						}
					]
				}
			},
			"ESNTab": {
				"e629d5ea-631c-40cc-975a-0f741c127e30": {
					"uId": "e629d5ea-631c-40cc-975a-0f741c127e30",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreement"
							}
						}
					]
				}
			},
			"WatFacultyDepartment": {
				"d413d28d-4458-4c66-8db3-d7c74551cd52": {
					"uId": "d413d28d-4458-4c66-8db3-d7c74551cd52",
					"enabled": true,
					"removed": false,
					"ruleType": 1,
					"baseAttributePatch": "WatFaculty",
					"comparisonType": 3,
					"autoClean": true,
					"autocomplete": true,
					"type": 1,
					"attribute": "WatFaculty"
				},
				"2a993f6e-a981-461e-badc-4683db659033": {
					"uId": "2a993f6e-a981-461e-badc-4683db659033",
					"enabled": true,
					"removed": false,
					"ruleType": 1,
					"baseAttributePatch": "WatFaculty",
					"comparisonType": 3,
					"autoClean": true,
					"autocomplete": true,
					"type": 1,
					"attribute": "WatFaculty"
				}
			}
		}/**SCHEMA_BUSINESS_RULES*/,
		methods: {},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"name": "LOOKUPf8c16ebc-34de-4f50-9ead-fe0358a96e57",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "WatContact",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "LOOKUPd096fb51-5e9e-43b5-a2eb-1e8d66b3f7e4",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "WatAgreement",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "LOOKUP2da3716f-685d-4f02-9add-31d0abc02771",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "WatFaculty",
					"enabled": true,
					"contentType": 3
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "LOOKUP513f7c7c-d998-49da-b432-0e33611aeffb",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Header"
					},
					"bindTo": "WatFacultyDepartment",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.LOOKUP513f7c7cd99849dab4320e33611aeffbLabelCaption"
						}
					},
					"enabled": true,
					"contentType": 3
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "LOOKUP159f0f91-9bea-49e5-b423-dea003ec74cc",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Header"
					},
					"bindTo": "WatFeasible",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.LOOKUP159f0f919bea49e5b423dea003ec74ccLabelCaption"
						}
					},
					"enabled": true,
					"contentType": 3
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "LOOKUPd65053e6-f4cc-4697-99d9-d98afcd1a1f0",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "Header"
					},
					"bindTo": "WatApproved",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.LOOKUPd65053e6f4cc469799d9d98afcd1a1f0LabelCaption"
						}
					},
					"enabled": true,
					"contentType": 3
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "STRING7155005c-f482-422c-a7b0-4727182ae2dc",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 5,
						"layoutName": "Header"
					},
					"bindTo": "WatAgreementNotes",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.STRING7155005cf482422ca7b04727182ae2dcLabelCaption"
						}
					},
					"enabled": true,
					"contentType": 0
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "NotesAndFilesTab",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.NotesAndFilesTabCaption"
					},
					"items": [],
					"order": 0
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Files",
				"values": {
					"itemType": 2
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "NotesControlGroup",
				"values": {
					"itemType": 15,
					"caption": {
						"bindTo": "Resources.Strings.NotesGroupCaption"
					},
					"items": []
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Notes",
				"values": {
					"bindTo": "WatNotes",
					"dataValueType": 1,
					"contentType": 4,
					"layout": {
						"column": 0,
						"row": 0,
						"colSpan": 24
					},
					"labelConfig": {
						"visible": false
					},
					"controlConfig": {
						"imageLoaded": {
							"bindTo": "insertImagesToNotes"
						},
						"images": {
							"bindTo": "NotesImagesCollection"
						}
					}
				},
				"parentName": "NotesControlGroup",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "merge",
				"name": "ESNTab",
				"values": {
					"order": 1
				}
			}
		]/**SCHEMA_DIFF*/
	};
});
