define("WatAgreement1Page", [], function() {
	return {
		entitySchemaName: "WatAgreement",
		attributes: {},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{
			"Files": {
				"schemaName": "FileDetailV2",
				"entitySchemaName": "WatAgreementFile",
				"filter": {
					"masterColumn": "Id",
					"detailColumn": "WatAgreement"
				}
			},
			"VisaDetailV2e20af99d": {
				"schemaName": "VisaDetailV2",
				"entitySchemaName": "WatAgreementVisa",
				"filter": {
					"masterColumn": "Id",
					"detailColumn": "WatAgreement"
				}
			},
			"WatSchemaeece2c0bDetailc4ca703b": {
				"schemaName": "WatSchemaeece2c0bDetail",
				"entitySchemaName": "WatParticipatingFaculties",
				"filter": {
					"detailColumn": "WatAgreement",
					"masterColumn": "Id"
				}
			},
			"WatSchemacc896fb3Detail66abcb51": {
				"schemaName": "WatSchemacc896fb3Detail",
				"entitySchemaName": "WatAgreementStakeholders",
				"filter": {
					"detailColumn": "WatAgreement",
					"masterColumn": "Id"
				}
			},
			"WatSchemac79b46ecDetail34e2424f": {
				"schemaName": "WatSchemac79b46ecDetail",
				"entitySchemaName": "WatAgreementStage",
				"filter": {
					"detailColumn": "WatAgreement",
					"masterColumn": "Id"
				}
			},
			"DocumentDetailV2153423e0": {
				"schemaName": "DocumentDetailV2",
				"entitySchemaName": "Document",
				"filter": {
					"detailColumn": "WatAgreement",
					"masterColumn": "Id"
				}
			},
			"WatSchemaTimerDetaila653d9ba": {
				"schemaName": "WatSchemaTimerDetail",
				"entitySchemaName": "WatTimer",
				"filter": {
					"detailColumn": "WatAgreement",
					"masterColumn": "Id"
				}
			},
			"EmailDetailV20d169b6f": {
				"schemaName": "EmailDetailV2",
				"entitySchemaName": "Activity",
				"filter": {
					"detailColumn": "WatWatAgreement",
					"masterColumn": "Id"
				}
			},
			"WatSchemaf0d78a31Detail0b7a83bf": {
				"schemaName": "WatSchemaf0d78a31Detail",
				"entitySchemaName": "WatFinalizationTasks",
				"filter": {
					"detailColumn": "WatAgreement",
					"masterColumn": "Id"
				}
			},
			"DocumentDetailV26bdbd890": {
				"schemaName": "DocumentDetailV2",
				"entitySchemaName": "Document",
				"filter": {
					"detailColumn": "WatAgreement",
					"masterColumn": "Id"
				}
			},
			"WatSchema94c7e3c8Detailed376edb": {
				"schemaName": "WatSchema94c7e3c8Detail",
				"entitySchemaName": "WatDatedNotes",
				"filter": {
					"detailColumn": "WatAgreement",
					"masterColumn": "Id"
				}
			},
			"WatSchemaCustomEmailCallDetail0131bf20": {
				"schemaName": "WatSchemaCustomEmailCallDetail",
				"entitySchemaName": "Activity",
				"filter": {
					"detailColumn": "WatWatAgreement",
					"masterColumn": "Id"
				}
			},
			"WatSchemaCustomTaskDetail637b94e2": {
				"schemaName": "WatSchemaCustomTaskDetail",
				"entitySchemaName": "Activity",
				"filter": {
					"detailColumn": "WatWatAgreement",
					"masterColumn": "Id"
				}
			}
		}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{
			"WatAgreementType": {
				"f15b3c7b-e7bd-4a69-9948-b82474bc0f0b": {
					"uId": "f15b3c7b-e7bd-4a69-9948-b82474bc0f0b",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreementStage"
							},
							"rightExpression": {
								"type": 0,
								"value": "71691e40-3686-425a-93ca-610e30747614",
								"dataValueType": 10
							}
						}
					]
				},
				"3e771e5d-fc0e-43fd-9886-758c65041d85": {
					"uId": "3e771e5d-fc0e-43fd-9886-758c65041d85",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreementStage"
							},
							"rightExpression": {
								"type": 0,
								"value": "0ce04f24-bb38-454a-ac3a-9ddc03493986",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreementStage"
							},
							"rightExpression": {
								"type": 0,
								"value": "e1944ea7-5526-4dd7-88ba-0848ba51db13",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreementStage"
							},
							"rightExpression": {
								"type": 0,
								"value": "419c6ed8-f682-4a5a-833c-f3f6de035b90",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreementStage"
							},
							"rightExpression": {
								"type": 0,
								"value": "2d278946-1df0-42d3-8c6b-810d0f2975d5",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreementStage"
							},
							"rightExpression": {
								"type": 0,
								"value": "71691e40-3686-425a-93ca-610e30747614",
								"dataValueType": 10
							}
						}
					]
				},
				"cbc976a3-f51d-4432-9f1a-65a4ffc30b41": {
					"uId": "cbc976a3-f51d-4432-9f1a-65a4ffc30b41",
					"enabled": true,
					"removed": true,
					"ruleType": 1,
					"baseAttributePatch": "WatAgreementClassification",
					"comparisonType": 3,
					"autoClean": true,
					"autocomplete": true,
					"type": 1,
					"attribute": "WatAgreementClassification"
				}
			},
			"WatTermStartDate": {
				"8edb9d8d-1611-49f7-b0a0-9d7bd5499ded": {
					"uId": "8edb9d8d-1611-49f7-b0a0-9d7bd5499ded",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreementSigned"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreementStage"
							},
							"rightExpression": {
								"type": 0,
								"value": "e1944ea7-5526-4dd7-88ba-0848ba51db13",
								"dataValueType": 10
							}
						}
					]
				},
				"bb6b83d0-a0a7-4960-8e25-107c431477c4": {
					"uId": "bb6b83d0-a0a7-4960-8e25-107c431477c4",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 2,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreementStage"
							},
							"rightExpression": {
								"type": 0,
								"value": "0ce04f24-bb38-454a-ac3a-9ddc03493986",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreementStage"
							},
							"rightExpression": {
								"type": 0,
								"value": "e1944ea7-5526-4dd7-88ba-0848ba51db13",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreementStage"
							},
							"rightExpression": {
								"type": 0,
								"value": "419c6ed8-f682-4a5a-833c-f3f6de035b90",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreementStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "4e4cda8f-efe9-4388-bbe5-3f6336a580ef",
								"dataValueType": 10
							}
						}
					]
				},
				"466a6840-8466-4edb-8a04-98cafebeadc0": {
					"uId": "466a6840-8466-4edb-8a04-98cafebeadc0",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreementSigned"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"WatTermEndDate": {
				"da80297d-e652-43ff-9a4a-76cc12c0fb0d": {
					"uId": "da80297d-e652-43ff-9a4a-76cc12c0fb0d",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreementStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "effd2081-f8d4-4a1d-9237-edb542f5582a",
								"dataValueType": 10
							}
						}
					]
				},
				"f1c2cca7-fc35-482d-b2d3-d8dc7af40f6e": {
					"uId": "f1c2cca7-fc35-482d-b2d3-d8dc7af40f6e",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 2,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreementStage"
							},
							"rightExpression": {
								"type": 0,
								"value": "0ce04f24-bb38-454a-ac3a-9ddc03493986",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreementStage"
							},
							"rightExpression": {
								"type": 0,
								"value": "e1944ea7-5526-4dd7-88ba-0848ba51db13",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreementStage"
							},
							"rightExpression": {
								"type": 0,
								"value": "419c6ed8-f682-4a5a-833c-f3f6de035b90",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreementStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "4e4cda8f-efe9-4388-bbe5-3f6336a580ef",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"WatContact": {
				"88d12400-e263-4205-af92-b7573fca32c3": {
					"uId": "88d12400-e263-4205-af92-b7573fca32c3",
					"enabled": true,
					"removed": false,
					"ruleType": 1,
					"baseAttributePatch": "Account",
					"comparisonType": 3,
					"autoClean": true,
					"autocomplete": true,
					"type": 1,
					"attribute": "WatInstitution"
				}
			},
			"WatLeadUnitDepartment": {
				"cde06df9-d5b0-4b69-9fd8-fe08cf45f10f": {
					"uId": "cde06df9-d5b0-4b69-9fd8-fe08cf45f10f",
					"enabled": true,
					"removed": false,
					"ruleType": 1,
					"baseAttributePatch": "WatFaculty",
					"comparisonType": 3,
					"autoClean": true,
					"autocomplete": true,
					"type": 1,
					"attribute": "WatLeadUnit"
				}
			},
			"NotesControlGroup": {
				"cfe6039c-ffe2-4df0-8513-05b5045da62c": {
					"uId": "cfe6039c-ffe2-4df0-8513-05b5045da62c",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 0,
								"value": "abc",
								"dataValueType": 1
							},
							"rightExpression": {
								"type": 0,
								"value": "123",
								"dataValueType": 1
							}
						}
					]
				},
				"9079e068-a932-4007-818f-078ea3fe088e": {
					"uId": "9079e068-a932-4007-818f-078ea3fe088e",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 0,
								"value": 1,
								"dataValueType": 4
							},
							"rightExpression": {
								"type": 0,
								"value": 2,
								"dataValueType": 4
							}
						}
					]
				}
			},
			"WatRenewalTerms": {
				"3ecfa79d-d181-4279-a2d3-114ce115fc57": {
					"uId": "3ecfa79d-d181-4279-a2d3-114ce115fc57",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatEOLTreatment"
							},
							"rightExpression": {
								"type": 0,
								"value": "bab57a00-fa8e-4331-82a4-fb0d41b51bd6",
								"dataValueType": 10
							}
						}
					]
				},
				"e83db338-eb7c-480c-99e6-89c1fdeac812": {
					"uId": "e83db338-eb7c-480c-99e6-89c1fdeac812",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatEOLTreatment"
							},
							"rightExpression": {
								"type": 0,
								"value": "bab57a00-fa8e-4331-82a4-fb0d41b51bd6",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"WatName": {
				"d357092f-aa2f-4549-b6ed-f8d2bc0c6264": {
					"uId": "d357092f-aa2f-4549-b6ed-f8d2bc0c6264",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatRequestType"
							},
							"rightExpression": {
								"type": 0,
								"value": "7c6d730d-eecb-41c0-8e5d-3e9cc4167da9",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatRequestType"
							},
							"rightExpression": {
								"type": 0,
								"value": "73dda689-7c0c-4612-8d07-763141100ae9",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatRequestType"
							},
							"rightExpression": {
								"type": 0,
								"value": "d43c349a-63d0-43d9-900e-6fc9f69f4d70",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatRequestType"
							},
							"rightExpression": {
								"type": 0,
								"value": "27a01368-f1c0-4051-b26d-21b3b213fe65",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "WatName"
							}
						}
					]
				}
			},
			"WatTermDuration": {
				"29566d2f-baac-4e49-ae82-a92317d98609": {
					"uId": "29566d2f-baac-4e49-ae82-a92317d98609",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "WatTermStartDate"
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreementStage"
							},
							"rightExpression": {
								"type": 0,
								"value": "e1944ea7-5526-4dd7-88ba-0848ba51db13",
								"dataValueType": 10
							}
						}
					]
				},
				"e1a97f71-4a25-46aa-bcef-a5eefcc47cf5": {
					"uId": "e1a97f71-4a25-46aa-bcef-a5eefcc47cf5",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "WatTermStartDate"
							}
						}
					]
				}
			},
			"WatAgreementClassification": {
				"5ff371c9-9f44-4185-8676-650bde7a2ed1": {
					"uId": "5ff371c9-9f44-4185-8676-650bde7a2ed1",
					"enabled": true,
					"removed": false,
					"ruleType": 1,
					"baseAttributePatch": "WatAgreementType",
					"comparisonType": 3,
					"autoClean": true,
					"autocomplete": false,
					"type": 1,
					"attribute": "WatAgreementType"
				},
				"eea0855f-de51-4206-b1a1-40a301c8d6f7": {
					"uId": "eea0855f-de51-4206-b1a1-40a301c8d6f7",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreementType"
							}
						}
					]
				}
			},
			"WatAgreementSubClassification": {
				"c5864e1e-3e6e-4259-9d81-93b4595ee780": {
					"uId": "c5864e1e-3e6e-4259-9d81-93b4595ee780",
					"enabled": true,
					"removed": false,
					"ruleType": 1,
					"baseAttributePatch": "WatClassification",
					"comparisonType": 3,
					"autoClean": true,
					"autocomplete": false,
					"type": 1,
					"attribute": "WatAgreementClassification"
				},
				"9c06fcc7-fdf7-4dec-8dba-f8f12fb7626b": {
					"uId": "9c06fcc7-fdf7-4dec-8dba-f8f12fb7626b",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreementClassification"
							}
						}
					]
				}
			},
			"WatRequestType": {
				"f76840da-b7d0-492b-a746-e4f27eedfa42": {
					"uId": "f76840da-b7d0-492b-a746-e4f27eedfa42",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreementStage"
							},
							"rightExpression": {
								"type": 0,
								"value": "5dd674ae-5347-476d-9262-13d419467127",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreementStage"
							},
							"rightExpression": {
								"type": 0,
								"value": "181fc291-f2cf-43da-8db6-5edc0e3c562a",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"WatEOLTreatment": {
				"17f50943-d3f6-433c-b62e-908d268115f9": {
					"uId": "17f50943-d3f6-433c-b62e-908d268115f9",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 7,
							"leftExpression": {
								"type": 1,
								"attribute": "WatTermDuration"
							},
							"rightExpression": {
								"type": 0,
								"value": 0,
								"dataValueType": 4
							}
						}
					]
				}
			},
			"WatAgreementSigned": {
				"868b7bab-d778-430a-8736-72d3218eb2fe": {
					"uId": "868b7bab-d778-430a-8736-72d3218eb2fe",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreementStage"
							},
							"rightExpression": {
								"type": 0,
								"value": "e1944ea7-5526-4dd7-88ba-0848ba51db13",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreementStage"
							},
							"rightExpression": {
								"type": 0,
								"value": "419c6ed8-f682-4a5a-833c-f3f6de035b90",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreementSigned"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				},
				"e8f85bd6-754b-47e2-a8ab-ce27cdb0f6c6": {
					"uId": "e8f85bd6-754b-47e2-a8ab-ce27cdb0f6c6",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreementStage"
							},
							"rightExpression": {
								"type": 0,
								"value": "e1944ea7-5526-4dd7-88ba-0848ba51db13",
								"dataValueType": 10
							}
						}
					]
				}
			}
		}/**SCHEMA_BUSINESS_RULES*/,
		methods: {},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"name": "WatName6ede102b-f52e-491a-9b45-8955f24099cf",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "WatName"
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatRequestType00b978db-2dfe-4b01-b4e1-46dc2bb4a1c4",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "WatRequestType",
					"enabled": true,
					"contentType": 3
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "WatAgreementType6d5606e4-2637-43ed-a8b4-ed8ff2c3de5e",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "WatAgreementType",
					"enabled": true,
					"contentType": 3
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "WatAgreementClassificationa7edd901-9474-4540-9d06-50eb77ca005d",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "WatAgreementClassification",
					"enabled": true,
					"contentType": 3,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatAgreementClassificationa7edd901947445409d0650eb77ca005dLabelCaption"
						}
					}
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "LOOKUP3c188d98-41d1-40e5-9559-bb634b6bf683",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "WatAgreementSubClassification",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.LOOKUP3c188d9841d140e59559bb634b6bf683LabelCaption"
						}
					},
					"enabled": true,
					"contentType": 3
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "LOOKUP3fba4343-4d21-4a5f-82dd-94430a22cc0a",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 5,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "WatMasterAgreement",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "WatTermStartDate058bdbc1-0f75-4fc4-9b8e-55e2b0e70f58",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 6,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "WatTermStartDate"
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "WatTermDuration198eb676-75a8-46fc-9f70-352317f50799",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 7,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "WatTermDuration",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatTermDuration198eb67675a846fc9f70352317f50799LabelCaption"
						}
					},
					"enabled": true
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "WatTermEndDatef8d7117c-cc04-4dec-a4cf-05a243360965",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 8,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "WatTermEndDate",
					"enabled": false
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 8
			},
			{
				"operation": "insert",
				"name": "WatAgreementStatusff39432e-d7db-43ab-8154-1a16721d85f2",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 9,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "WatAgreementStatus",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 9
			},
			{
				"operation": "insert",
				"name": "BOOLEAN04301d67-7129-4eb5-98ab-a49b1348fee9",
				"values": {
					"layout": {
						"colSpan": 5,
						"rowSpan": 1,
						"column": 19,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "WatAgreementSigned",
					"enabled": true
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "LOOKUP6a5dd930-5114-4639-b1a1-abd559fc7b5b",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "WatInstitution",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "BOOLEAN8325ea9c-8e1f-4c96-a518-b1c37c999f25",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 9,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "WatEnglishLanguageRequirements",
					"enabled": true,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.BOOLEAN8325ea9c8e1f4c96a518b1c37c999f25LabelCaption"
						}
					}
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "LOOKUP0d76e491-ff4d-41c2-941c-79d83c6c1512",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "WatContact",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "LOOKUPaca9a2c1-cb2e-4c54-9ef6-1d99d1e6456c",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 9,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "WatEFASELAS",
					"enabled": true,
					"contentType": 3
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "LOOKUP30f5f61e-72a9-4e85-b8a2-946aab9b0f84",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Header"
					},
					"bindTo": "WatLeadUnit",
					"enabled": true,
					"contentType": 3
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "WatFundingArrangementsa6e05fd2-9c30-4fcb-8d23-de60918d9150",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 9,
						"row": 2,
						"layoutName": "Header"
					},
					"bindTo": "WatFundingArrangements",
					"enabled": true,
					"contentType": 0
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "LOOKUP709e4954-60c9-4747-8392-4af1905d5ef2",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Header"
					},
					"bindTo": "WatLeadUnitDepartment",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "STRINGabf5aee8-a906-422a-abbf-b87067fae21d",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 9,
						"row": 3,
						"layoutName": "Header"
					},
					"bindTo": "WatQuota",
					"enabled": true,
					"contentType": 0
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 8
			},
			{
				"operation": "insert",
				"name": "LOOKUPedba324d-9a9d-4c84-8f8a-c89103f2e15d",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "Header"
					},
					"bindTo": "WatAcademicLevel",
					"enabled": true,
					"contentType": 3
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 9
			},
			{
				"operation": "insert",
				"name": "STRING1ebc8af5-29cd-499e-8bd9-e98a8de69e6a",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 9,
						"row": 4,
						"layoutName": "Header"
					},
					"bindTo": "WatLisNumber",
					"enabled": true
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 10
			},
			{
				"operation": "insert",
				"name": "STRING70ca3d11-e456-4880-909b-4f76b72ae009",
				"values": {
					"layout": {
						"colSpan": 17,
						"rowSpan": 2,
						"column": 0,
						"row": 5,
						"layoutName": "Header"
					},
					"bindTo": "WatGeneralNotes",
					"enabled": true,
					"contentType": 0
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 11
			},
			{
				"operation": "insert",
				"name": "Tab4fc9f4eaTabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab4fc9f4eaTabLabelTabCaption"
					},
					"items": [],
					"order": 0
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tab4fc9f4eaTabLabelGroupb7db383e",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab4fc9f4eaTabLabelGroupb7db383eGroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tab4fc9f4eaTabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tab4fc9f4eaTabLabelGridLayout794bf4d5",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tab4fc9f4eaTabLabelGroupb7db383e",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "INTEGER0489268e-c93f-4a44-802d-5b80185cc2aa",
				"values": {
					"layout": {
						"colSpan": 6,
						"rowSpan": 1,
						"column": 9,
						"row": 0,
						"layoutName": "Tab4fc9f4eaTabLabelGridLayout794bf4d5"
					},
					"bindTo": "WatRenewalTerms",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.INTEGER0489268ec93f4a44802d5b80185cc2aaLabelCaption"
						}
					},
					"enabled": true
				},
				"parentName": "Tab4fc9f4eaTabLabelGridLayout794bf4d5",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatPartnershipRequest027561ae-74b6-432a-a126-806b550418a7",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 16,
						"row": 0,
						"layoutName": "Tab4fc9f4eaTabLabelGridLayout794bf4d5"
					},
					"bindTo": "WatPartnershipRequest",
					"enabled": false,
					"contentType": 5,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatPartnershipRequest027561ae74b6432aa126806b550418a7LabelCaption"
						}
					}
				},
				"parentName": "Tab4fc9f4eaTabLabelGridLayout794bf4d5",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "WatEOLTreatmenta1ee8e67-e658-4bbc-8bc6-d8ae4abc208c",
				"values": {
					"layout": {
						"colSpan": 9,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tab4fc9f4eaTabLabelGridLayout794bf4d5"
					},
					"bindTo": "WatEOLTreatment"
				},
				"parentName": "Tab4fc9f4eaTabLabelGridLayout794bf4d5",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "WatInitialAgreementDated08accc0-65c8-4a6a-979f-318e81240054",
				"values": {
					"layout": {
						"colSpan": 9,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Tab4fc9f4eaTabLabelGridLayout794bf4d5"
					},
					"bindTo": "WatInitialAgreementDate",
					"enabled": false
				},
				"parentName": "Tab4fc9f4eaTabLabelGridLayout794bf4d5",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "WatActualAgreementEndDate72286745-e88a-4a29-8447-87ae576ecbbd",
				"values": {
					"layout": {
						"colSpan": 9,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Tab4fc9f4eaTabLabelGridLayout794bf4d5"
					},
					"bindTo": "WatActualAgreementEndDate",
					"enabled": false
				},
				"parentName": "Tab4fc9f4eaTabLabelGridLayout794bf4d5",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "WatSchemaeece2c0bDetailc4ca703b",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab4fc9f4eaTabLabel",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "WatSchemacc896fb3Detail66abcb51",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab4fc9f4eaTabLabel",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "Tab2b976856TabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.TabVisaCaption"
					},
					"items": [],
					"order": 1
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "VisaDetailV2e20af99d",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab2b976856TabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tabd87b5dffTabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tabd87b5dffTabLabelTabCaption"
					},
					"items": [],
					"order": 2
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "WatSchemaf0d78a31Detail0b7a83bf",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tabd87b5dffTabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tab3a3d3409TabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab3a3d3409TabLabelTabCaption"
					},
					"items": [],
					"order": 3
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "WatSchemaCustomEmailCallDetail0131bf20",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab3a3d3409TabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatSchemaCustomTaskDetail637b94e2",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab3a3d3409TabLabel",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "NotesAndFilesTab",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.NotesAndFilesTabCaption"
					},
					"items": [],
					"order": 4
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "DocumentDetailV26bdbd890",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Files",
				"values": {
					"itemType": 2
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "NotesControlGroup",
				"values": {
					"itemType": 15,
					"caption": {
						"bindTo": "Resources.Strings.NotesGroupCaption"
					},
					"items": []
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "Notes",
				"values": {
					"bindTo": "WatNotes",
					"dataValueType": 1,
					"contentType": 4,
					"layout": {
						"column": 0,
						"row": 0,
						"colSpan": 24
					},
					"labelConfig": {
						"visible": false
					},
					"controlConfig": {
						"imageLoaded": {
							"bindTo": "insertImagesToNotes"
						},
						"images": {
							"bindTo": "NotesImagesCollection"
						}
					}
				},
				"parentName": "NotesControlGroup",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "merge",
				"name": "ESNTab",
				"values": {
					"order": 5
				}
			},
			{
				"operation": "insert",
				"name": "WatSchema94c7e3c8Detailed376edb",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "ESNTab",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "ESNTabGroupea20b857",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.ESNTabGroupea20b857GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "ESNTab",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "ESNTabGridLayout8250138b",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "ESNTabGroupea20b857",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatNotesf1aca89b-c64e-481e-860b-01aebdb8ea85",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 3,
						"column": 0,
						"row": 0,
						"layoutName": "ESNTabGridLayout8250138b"
					},
					"bindTo": "WatNotes",
					"enabled": true,
					"contentType": 0,
					"labelConfig": {
						"visible": false
					}
				},
				"parentName": "ESNTabGridLayout8250138b",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tabcc28cb4fTabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tabcc28cb4fTabLabelTabCaption"
					},
					"items": [],
					"order": 6
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "Tabcc28cb4fTabLabelGroup24289dcf",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tabcc28cb4fTabLabelGroup24289dcfGroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tabcc28cb4fTabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tabcc28cb4fTabLabelGridLayout4e740f4e",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tabcc28cb4fTabLabelGroup24289dcf",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatLastReachedStage78626fa1-96b3-4974-8c39-70bed81336a4",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tabcc28cb4fTabLabelGridLayout4e740f4e"
					},
					"bindTo": "WatLastReachedStage"
				},
				"parentName": "Tabcc28cb4fTabLabelGridLayout4e740f4e",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CreatedOn738e6780-f71a-48a9-896a-2cec9f192331",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "Tabcc28cb4fTabLabelGridLayout4e740f4e"
					},
					"bindTo": "CreatedOn"
				},
				"parentName": "Tabcc28cb4fTabLabelGridLayout4e740f4e",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "WatStageChangeDatef6bb4921-1e52-4ebf-8046-8a55ec6222cc",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Tabcc28cb4fTabLabelGridLayout4e740f4e"
					},
					"bindTo": "WatStageChangeDate"
				},
				"parentName": "Tabcc28cb4fTabLabelGridLayout4e740f4e",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "CreatedBy0e41185a-0138-4e00-9785-2cfa0fc7e169",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "Tabcc28cb4fTabLabelGridLayout4e740f4e"
					},
					"bindTo": "CreatedBy"
				},
				"parentName": "Tabcc28cb4fTabLabelGridLayout4e740f4e",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "WatMasterAgreement8a2e989b-ea8e-49c3-966a-66aea123679a",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Tabcc28cb4fTabLabelGridLayout4e740f4e"
					},
					"bindTo": "WatMasterAgreement"
				},
				"parentName": "Tabcc28cb4fTabLabelGridLayout4e740f4e",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "ModifiedOnc2c9743a-5b6f-4eeb-846b-55e38a0a2b28",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 2,
						"layoutName": "Tabcc28cb4fTabLabelGridLayout4e740f4e"
					},
					"bindTo": "ModifiedOn"
				},
				"parentName": "Tabcc28cb4fTabLabelGridLayout4e740f4e",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "WatSysFieldAllowLapseedb4af7f-fcb0-493a-b962-fb4fa9581812",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Tabcc28cb4fTabLabelGridLayout4e740f4e"
					},
					"bindTo": "WatSysFieldAllowLapse",
					"enabled": false
				},
				"parentName": "Tabcc28cb4fTabLabelGridLayout4e740f4e",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "ModifiedBy8cbc1a80-dbc2-4772-bb41-e872248f5490",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 3,
						"layoutName": "Tabcc28cb4fTabLabelGridLayout4e740f4e"
					},
					"bindTo": "ModifiedBy"
				},
				"parentName": "Tabcc28cb4fTabLabelGridLayout4e740f4e",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "WatSchemac79b46ecDetail34e2424f",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tabcc28cb4fTabLabel",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "WatSchemaTimerDetaila653d9ba",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tabcc28cb4fTabLabel",
				"propertyName": "items",
				"index": 2
			}
		]/**SCHEMA_DIFF*/
	};
});
