define("WatPartnershipRequest1Page", [], function() {
	return {
		entitySchemaName: "WatPartnershipRequest",
		attributes: {},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{
			"Files": {
				"schemaName": "FileDetailV2",
				"entitySchemaName": "WatPartnershipRequestFile",
				"filter": {
					"masterColumn": "Id",
					"detailColumn": "WatPartnershipRequest"
				}
			},
			"VisaDetailV204b7de63": {
				"schemaName": "VisaDetailV2",
				"entitySchemaName": "WatPartnershipRequestVisa",
				"filter": {
					"masterColumn": "Id",
					"detailColumn": "WatPartnershipRequest"
				}
			},
			"DocumentDetailV2b299bf8c": {
				"schemaName": "DocumentDetailV2",
				"entitySchemaName": "Document",
				"filter": {
					"detailColumn": "WatPartnershipRequest",
					"masterColumn": "Id"
				}
			},
			"DocumentDetailV2650b2338": {
				"schemaName": "DocumentDetailV2",
				"entitySchemaName": "Document",
				"filter": {
					"detailColumn": "WatPartnershipRequest",
					"masterColumn": "Id"
				}
			},
			"WatSchema94c7e3c8Detailffcfbc7e": {
				"schemaName": "WatSchema94c7e3c8Detail",
				"entitySchemaName": "WatDatedNotes",
				"filter": {
					"detailColumn": "WatPartnershipRequest",
					"masterColumn": "Id"
				}
			},
			"WatSchemaCustomEmailCallDetail98411da5": {
				"schemaName": "WatSchemaCustomEmailCallDetail",
				"entitySchemaName": "Activity",
				"filter": {
					"detailColumn": "WatWatPartnershipRequest",
					"masterColumn": "Id"
				}
			},
			"WatSchemaCustomTaskDetail19d256e2": {
				"schemaName": "WatSchemaCustomTaskDetail",
				"entitySchemaName": "Activity",
				"filter": {
					"detailColumn": "WatWatPartnershipRequest",
					"masterColumn": "Id"
				}
			}
		}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{
			"WatLead": {
				"96f442e6-02d4-4b04-8fb7-819364261d85": {
					"uId": "96f442e6-02d4-4b04-8fb7-819364261d85",
					"enabled": true,
					"removed": false,
					"ruleType": 1,
					"baseAttributePatch": "Account",
					"comparisonType": 3,
					"autoClean": false,
					"autocomplete": false,
					"type": 0,
					"value": "e308b781-3c5b-4ecb-89ef-5c1ed4da488e",
					"dataValueType": 10
				},
				"101c3e88-6cfd-464c-a1a9-98c1cce26d39": {
					"uId": "101c3e88-6cfd-464c-a1a9-98c1cce26d39",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatSysFieldLeadMandatory"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				},
				"53367dff-d3f1-4f98-ac3a-27008bd3c25a": {
					"uId": "53367dff-d3f1-4f98-ac3a-27008bd3c25a",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatSysFieldLeadMandatory"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"WatRequestApproval": {
				"fa62ce5b-ece2-475c-aa72-99c84e11883c": {
					"uId": "fa62ce5b-ece2-475c-aa72-99c84e11883c",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatPartnershipRequestStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "fb8197f3-a384-485d-bf80-5bc059bc085f",
								"dataValueType": 10
							}
						}
					]
				},
				"12a5e1df-147a-4edf-866f-2c43d11ec273": {
					"uId": "12a5e1df-147a-4edf-866f-2c43d11ec273",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatPartnershipRequestStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "c4faf7d9-dfff-47b4-9157-42236a072e30",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"WatCreateAgreement": {
				"83bdf7fe-e0c8-4746-a1f7-b0c7cc441720": {
					"uId": "83bdf7fe-e0c8-4746-a1f7-b0c7cc441720",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatPartnershipRequestStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "0f8ffeef-3f2f-40f3-9a50-79ac01aa590a",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatPartnershipRequestStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "4e823080-19a6-48a7-95e7-f244fca76d31",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"ModifiedBy": {
				"5a9ce059-599e-445c-95c4-ec059a210a38": {
					"uId": "5a9ce059-599e-445c-95c4-ec059a210a38",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CreatedOn"
							},
							"rightExpression": {
								"type": 0,
								"value": "2009-09-07T14:58:30.326Z",
								"dataValueType": 7
							}
						}
					]
				}
			},
			"WatRequestType": {
				"fdb68992-62ad-4032-8bab-9241a6819d71": {
					"uId": "fdb68992-62ad-4032-8bab-9241a6819d71",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "CreatedOn"
							}
						}
					]
				},
				"00724a04-66bf-4f79-bbac-0ec60ca5717c": {
					"uId": "00724a04-66bf-4f79-bbac-0ec60ca5717c",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "CreatedOn"
							}
						},
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "WatRequestType"
							}
						}
					]
				},
				"573f2f4f-127f-4b2d-a267-1a2beaa0ca90": {
					"uId": "573f2f4f-127f-4b2d-a267-1a2beaa0ca90",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 0,
								"value": "A",
								"dataValueType": 1
							},
							"rightExpression": {
								"type": 0,
								"value": "A",
								"dataValueType": 1
							}
						}
					]
				}
			},
			"WatAgreementForRenewal": {
				"36f9e919-936a-48c6-a3b8-ee07544b6871": {
					"uId": "36f9e919-936a-48c6-a3b8-ee07544b6871",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatRequestType"
							},
							"rightExpression": {
								"type": 0,
								"value": "445e5c6f-8b13-4a3d-b2fc-7e350d828005",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatRequestType"
							},
							"rightExpression": {
								"type": 0,
								"value": "0e0e61e1-c12c-49fb-90be-f4e20a3c4fc7",
								"dataValueType": 10
							}
						}
					]
				},
				"c35c0743-0e81-44c7-b4ae-848be655b8e7": {
					"uId": "c35c0743-0e81-44c7-b4ae-848be655b8e7",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 2,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatRequestType"
							},
							"rightExpression": {
								"type": 0,
								"value": "7c6d730d-eecb-41c0-8e5d-3e9cc4167da9",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatRequestType"
							},
							"rightExpression": {
								"type": 0,
								"value": "0e0e61e1-c12c-49fb-90be-f4e20a3c4fc7",
								"dataValueType": 10
							}
						}
					]
				},
				"1c5fba3a-6a8f-41b9-82b5-787a7974c5fb": {
					"uId": "1c5fba3a-6a8f-41b9-82b5-787a7974c5fb",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatRequestType"
							},
							"rightExpression": {
								"type": 0,
								"value": "445e5c6f-8b13-4a3d-b2fc-7e350d828005",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatRequestType"
							},
							"rightExpression": {
								"type": 0,
								"value": "0e0e61e1-c12c-49fb-90be-f4e20a3c4fc7",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"WatAssessmentComment": {
				"3a37ed15-2704-4bd3-85ed-a2e0121cf4d8": {
					"uId": "3a37ed15-2704-4bd3-85ed-a2e0121cf4d8",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "WatRequestApproval"
							}
						}
					]
				}
			},
			"WatAccount": {
				"54134619-ced8-4091-afd0-9da9c50fca6b": {
					"uId": "54134619-ced8-4091-afd0-9da9c50fca6b",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatPartnershipRequestStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "fb8197f3-a384-485d-bf80-5bc059bc085f",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatRequestType"
							},
							"rightExpression": {
								"type": 0,
								"value": "27a01368-f1c0-4051-b26d-21b3b213fe65",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatRequestType"
							},
							"rightExpression": {
								"type": 0,
								"value": "73dda689-7c0c-4612-8d07-763141100ae9",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatRequestType"
							},
							"rightExpression": {
								"type": 0,
								"value": "d43c349a-63d0-43d9-900e-6fc9f69f4d70",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"WatContact": {
				"00c1f1f9-d33c-4860-a168-671a9b0ecf88": {
					"uId": "00c1f1f9-d33c-4860-a168-671a9b0ecf88",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatPartnershipRequestStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "fb8197f3-a384-485d-bf80-5bc059bc085f",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatRequestType"
							},
							"rightExpression": {
								"type": 0,
								"value": "73dda689-7c0c-4612-8d07-763141100ae9",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatRequestType"
							},
							"rightExpression": {
								"type": 0,
								"value": "27a01368-f1c0-4051-b26d-21b3b213fe65",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatRequestType"
							},
							"rightExpression": {
								"type": 0,
								"value": "d43c349a-63d0-43d9-900e-6fc9f69f4d70",
								"dataValueType": 10
							}
						}
					]
				},
				"34c81692-bd67-4efe-9484-e44a998f9ddf": {
					"uId": "34c81692-bd67-4efe-9484-e44a998f9ddf",
					"enabled": true,
					"removed": false,
					"ruleType": 1,
					"baseAttributePatch": "Account",
					"comparisonType": 3,
					"autoClean": true,
					"autocomplete": true,
					"type": 1,
					"attribute": "WatAccount"
				}
			},
			"WatName": {
				"eed2fa7f-a362-489b-8888-7738a61edf9c": {
					"uId": "eed2fa7f-a362-489b-8888-7738a61edf9c",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatRequestType"
							},
							"rightExpression": {
								"type": 0,
								"value": "445e5c6f-8b13-4a3d-b2fc-7e350d828005",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatRequestType"
							},
							"rightExpression": {
								"type": 0,
								"value": "0e0e61e1-c12c-49fb-90be-f4e20a3c4fc7",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "WatRequestType"
							}
						}
					]
				},
				"4bca3660-45be-4677-bb5c-dba7a011ab62": {
					"uId": "4bca3660-45be-4677-bb5c-dba7a011ab62",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatRequestType"
							},
							"rightExpression": {
								"type": 0,
								"value": "7c6d730d-eecb-41c0-8e5d-3e9cc4167da9",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatRequestType"
							},
							"rightExpression": {
								"type": 0,
								"value": "73dda689-7c0c-4612-8d07-763141100ae9",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatRequestType"
							},
							"rightExpression": {
								"type": 0,
								"value": "27a01368-f1c0-4051-b26d-21b3b213fe65",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatRequestType"
							},
							"rightExpression": {
								"type": 0,
								"value": "d43c349a-63d0-43d9-900e-6fc9f69f4d70",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "WatName"
							}
						}
					]
				},
				"fa53cc06-0a53-4765-976d-1688ef832fec": {
					"uId": "fa53cc06-0a53-4765-976d-1688ef832fec",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatRequestType"
							},
							"rightExpression": {
								"type": 0,
								"value": "27a01368-f1c0-4051-b26d-21b3b213fe65",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatRequestType"
							},
							"rightExpression": {
								"type": 0,
								"value": "d43c349a-63d0-43d9-900e-6fc9f69f4d70",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatRequestType"
							},
							"rightExpression": {
								"type": 0,
								"value": "73dda689-7c0c-4612-8d07-763141100ae9",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatRequestType"
							},
							"rightExpression": {
								"type": 0,
								"value": "7c6d730d-eecb-41c0-8e5d-3e9cc4167da9",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "WatName"
							}
						}
					]
				},
				"1a79705b-4448-4228-925a-7ffd486a24ce": {
					"uId": "1a79705b-4448-4228-925a-7ffd486a24ce",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatRequestType"
							},
							"rightExpression": {
								"type": 0,
								"value": "445e5c6f-8b13-4a3d-b2fc-7e350d828005",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatRequestType"
							},
							"rightExpression": {
								"type": 0,
								"value": "0e0e61e1-c12c-49fb-90be-f4e20a3c4fc7",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatPartnershipRequestStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "4e823080-19a6-48a7-95e7-f244fca76d31",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatPartnershipRequestStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "0f8ffeef-3f2f-40f3-9a50-79ac01aa590a",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"NotesControlGroup": {
				"8734e2ed-4fb1-4e31-b108-eba81a61d56b": {
					"uId": "8734e2ed-4fb1-4e31-b108-eba81a61d56b",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 0,
								"value": "abc",
								"dataValueType": 1
							},
							"rightExpression": {
								"type": 0,
								"value": "123",
								"dataValueType": 1
							}
						}
					]
				},
				"4e59eff4-030c-48dd-a452-a74a136047cf": {
					"uId": "4e59eff4-030c-48dd-a452-a74a136047cf",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 0,
								"value": 1,
								"dataValueType": 4
							},
							"rightExpression": {
								"type": 0,
								"value": 2,
								"dataValueType": 4
							}
						}
					]
				}
			},
			"WatContactEmail": {
				"dcc6756a-e65f-4ef3-b279-cdcaa4625299": {
					"uId": "dcc6756a-e65f-4ef3-b279-cdcaa4625299",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatRequestType"
							},
							"rightExpression": {
								"type": 0,
								"value": "27a01368-f1c0-4051-b26d-21b3b213fe65",
								"dataValueType": 10
							}
						}
					]
				}
			}
		}/**SCHEMA_BUSINESS_RULES*/,
		methods: {},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"name": "WatRequestType93ceed42-09f3-45f8-8d63-573465dae48a",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "WatRequestType",
					"enabled": true,
					"contentType": 3
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatNameadb68bc7-ac44-4539-80fc-dfec91c51f9c",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "WatName",
					"enabled": true,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatNameadb68bc7ac44453980fcdfec91c51f9cLabelCaption"
						}
					}
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "WatRequestPrioritye7039004-7c0b-4382-8cb3-869f63834b7a",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "WatRequestPriority",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatRequestPrioritye70390047c0b43828cb3869f63834b7aLabelCaption"
						}
					},
					"enabled": true,
					"contentType": 3
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "WatLead92db215d-9043-4221-8e31-af1693164535",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "WatLead"
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "WatAgreementForRenewal9cc12b67-c266-4487-93f6-e323cc5d937f",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "WatAgreementForRenewal",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatAgreementForRenewal9cc12b67c266448793f6e323cc5d937fLabelCaption"
						}
					},
					"enabled": true,
					"contentType": 5
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "LOOKUP1faed81e-8b92-450f-a62e-550a7a34a28c",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "WatAccount",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatSubmissionDate8e65b36a-875a-4eda-aa48-b7608c26c4d3",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "WatSubmissionDate",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "LOOKUP0af9eb48-0e7b-4b5e-96c2-641acab237b9",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "WatContact",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "BOOLEAN482301e5-b395-4349-b558-d5b76f6ef9da",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "WatCancelRequest",
					"enabled": true
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "STRING835f536d-c109-4396-b986-989c66ab5e87",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Header"
					},
					"bindTo": "WatAssessmentComment",
					"enabled": true,
					"contentType": 0
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "ModifiedByd2275bff-5adc-4174-9160-254308d88bed",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Header"
					},
					"bindTo": "ModifiedBy",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.ModifiedByd2275bff5adc41749160254308d88bedLabelCaption"
						}
					},
					"enabled": true,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "LOOKUPd6e2d4ad-8324-48ee-8b88-7f5aff4433a3",
				"values": {
					"layout": {
						"colSpan": 9,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "Header"
					},
					"bindTo": "WatRequestApproval",
					"enabled": true,
					"contentType": 3
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "Tab6d4081bfTabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.TabVisaCaption"
					},
					"items": [],
					"order": 0
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tab6d4081bfTabLabelGroupf55de778",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab6d4081bfTabLabelGroupf55de778GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tab6d4081bfTabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tab6d4081bfTabLabelGridLayout6597aee1",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tab6d4081bfTabLabelGroupf55de778",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatDiscussedWithChair6abd5f42-9c70-440a-98b4-add4664140e9",
				"values": {
					"layout": {
						"colSpan": 5,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tab6d4081bfTabLabelGridLayout6597aee1"
					},
					"bindTo": "WatDiscussedWithChair",
					"enabled": false
				},
				"parentName": "Tab6d4081bfTabLabelGridLayout6597aee1",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatDiscussedWithAssociateDeanc5a5fbe9-c655-41ce-949e-7052413d6778",
				"values": {
					"layout": {
						"colSpan": 5,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Tab6d4081bfTabLabelGridLayout6597aee1"
					},
					"bindTo": "WatDiscussedWithAssociateDean",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatDiscussedWithAssociateDeanc5a5fbe9c65541ce949e7052413d6778LabelCaption"
						}
					},
					"enabled": false
				},
				"parentName": "Tab6d4081bfTabLabelGridLayout6597aee1",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "WatAssociateDeanNameb03cb64c-d68c-48a2-b891-5a5dcc6487e4",
				"values": {
					"layout": {
						"colSpan": 13,
						"rowSpan": 1,
						"column": 5,
						"row": 1,
						"layoutName": "Tab6d4081bfTabLabelGridLayout6597aee1"
					},
					"bindTo": "WatAssociateDeanName",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatAssociateDeanNameb03cb64cd68c48a2b8915a5dcc6487e4LabelCaption"
						}
					},
					"enabled": false
				},
				"parentName": "Tab6d4081bfTabLabelGridLayout6597aee1",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "WatApproverCommente8686ad5-6203-430d-a153-c5f047630c30",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Tab6d4081bfTabLabelGridLayout6597aee1"
					},
					"bindTo": "WatApproverComment",
					"enabled": true,
					"contentType": 0
				},
				"parentName": "Tab6d4081bfTabLabelGridLayout6597aee1",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "VisaDetailV204b7de63",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab6d4081bfTabLabel",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Tab31bd56b2TabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab31bd56b2TabLabelTabCaption"
					},
					"items": [],
					"order": 1
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "WatSchemaCustomEmailCallDetail98411da5",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab31bd56b2TabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatSchemaCustomTaskDetail19d256e2",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab31bd56b2TabLabel",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "NotesAndFilesTab",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.NotesAndFilesTabCaption"
					},
					"items": [],
					"order": 2
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "DocumentDetailV2650b2338",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Files",
				"values": {
					"itemType": 2
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "NotesControlGroup",
				"values": {
					"itemType": 15,
					"caption": {
						"bindTo": "Resources.Strings.NotesGroupCaption"
					},
					"items": []
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "Notes",
				"values": {
					"bindTo": "WatNotes",
					"dataValueType": 1,
					"contentType": 4,
					"layout": {
						"column": 0,
						"row": 0,
						"colSpan": 24
					},
					"labelConfig": {
						"visible": false
					},
					"controlConfig": {
						"imageLoaded": {
							"bindTo": "insertImagesToNotes"
						},
						"images": {
							"bindTo": "NotesImagesCollection"
						}
					}
				},
				"parentName": "NotesControlGroup",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "merge",
				"name": "ESNTab",
				"values": {
					"order": 3
				}
			},
			{
				"operation": "insert",
				"name": "WatSchema94c7e3c8Detailffcfbc7e",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "ESNTab",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tab4f1c9074TabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab4f1c9074TabLabelTabCaption"
					},
					"items": [],
					"order": 4
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "Tab4f1c9074TabLabelGroup67228383",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab4f1c9074TabLabelGroup67228383GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tab4f1c9074TabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tab4f1c9074TabLabelGridLayout30d68aa9",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tab4f1c9074TabLabelGroup67228383",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "STRING253cabd2-cbd8-4676-b674-86514a2df647",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tab4f1c9074TabLabelGridLayout30d68aa9"
					},
					"bindTo": "WatInstitutionName",
					"enabled": true
				},
				"parentName": "Tab4f1c9074TabLabelGridLayout30d68aa9",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "STRINGbd6f3082-b62a-41f4-a4a9-f21a0e17b123",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Tab4f1c9074TabLabelGridLayout30d68aa9"
					},
					"bindTo": "WatContactFirstName",
					"enabled": true
				},
				"parentName": "Tab4f1c9074TabLabelGridLayout30d68aa9",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "STRING0f0c9a52-aaf7-47a6-a0d6-c984efd5e01b",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "Tab4f1c9074TabLabelGridLayout30d68aa9"
					},
					"bindTo": "WatContactEmail",
					"enabled": true
				},
				"parentName": "Tab4f1c9074TabLabelGridLayout30d68aa9",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "STRING9c97c3b6-8a75-452a-8e83-604f27ac95e9",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Tab4f1c9074TabLabelGridLayout30d68aa9"
					},
					"bindTo": "WatContactLastName",
					"enabled": true
				},
				"parentName": "Tab4f1c9074TabLabelGridLayout30d68aa9",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "STRINGc5673eb7-829c-4588-b502-7bd0e535d3aa",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 2,
						"layoutName": "Tab4f1c9074TabLabelGridLayout30d68aa9"
					},
					"bindTo": "WatContactPhoneNumber",
					"enabled": true
				},
				"parentName": "Tab4f1c9074TabLabelGridLayout30d68aa9",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "STRING2f2b5a0a-840d-4ba6-805a-fdb2d7bb2146",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Tab4f1c9074TabLabelGridLayout30d68aa9"
					},
					"bindTo": "WatContactJobTitle",
					"enabled": true
				},
				"parentName": "Tab4f1c9074TabLabelGridLayout30d68aa9",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "STRING93fa96f8-9c20-482d-b37a-148efb0a602e",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "Tab4f1c9074TabLabelGridLayout30d68aa9"
					},
					"bindTo": "WatProposedActivityDescription",
					"enabled": true,
					"contentType": 0
				},
				"parentName": "Tab4f1c9074TabLabelGridLayout30d68aa9",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "STRINGdf84548e-d3fa-4f5e-b234-f9ba6137a2f7",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 5,
						"layoutName": "Tab4f1c9074TabLabelGridLayout30d68aa9"
					},
					"bindTo": "WatExpectedOutcomes",
					"enabled": true,
					"contentType": 0
				},
				"parentName": "Tab4f1c9074TabLabelGridLayout30d68aa9",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "STRING24598b54-b5af-4195-bd9f-408fcee49e31",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 6,
						"layoutName": "Tab4f1c9074TabLabelGridLayout30d68aa9"
					},
					"bindTo": "WatContributionToGoals",
					"enabled": true,
					"contentType": 0
				},
				"parentName": "Tab4f1c9074TabLabelGridLayout30d68aa9",
				"propertyName": "items",
				"index": 8
			},
			{
				"operation": "insert",
				"name": "WatCommentsbfb8ab8a-70dd-43d8-98bf-6c6a16f53c09",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 7,
						"layoutName": "Tab4f1c9074TabLabelGridLayout30d68aa9"
					},
					"bindTo": "WatComments",
					"enabled": true,
					"contentType": 0
				},
				"parentName": "Tab4f1c9074TabLabelGridLayout30d68aa9",
				"propertyName": "items",
				"index": 9
			},
			{
				"operation": "insert",
				"name": "WatDiscussedWithChair9a654b99-71dd-4ff7-8996-8875deb7d700",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 8,
						"layoutName": "Tab4f1c9074TabLabelGridLayout30d68aa9"
					},
					"bindTo": "WatDiscussedWithChair"
				},
				"parentName": "Tab4f1c9074TabLabelGridLayout30d68aa9",
				"propertyName": "items",
				"index": 10
			},
			{
				"operation": "insert",
				"name": "WatDiscussedWithAssociateDean28a8a966-ebc3-4956-9bc1-a68f3d153773",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 9,
						"layoutName": "Tab4f1c9074TabLabelGridLayout30d68aa9"
					},
					"bindTo": "WatDiscussedWithAssociateDean",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatDiscussedWithAssociateDean28a8a966ebc349569bc1a68f3d153773LabelCaption"
						}
					},
					"enabled": true
				},
				"parentName": "Tab4f1c9074TabLabelGridLayout30d68aa9",
				"propertyName": "items",
				"index": 11
			},
			{
				"operation": "insert",
				"name": "WatAssociateDeanNamefc78414f-c5c2-45bb-9630-b40defe9174f",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 10,
						"layoutName": "Tab4f1c9074TabLabelGridLayout30d68aa9"
					},
					"bindTo": "WatAssociateDeanName",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatAssociateDeanNamefc78414fc5c245bb9630b40defe9174fLabelCaption"
						}
					},
					"enabled": true
				},
				"parentName": "Tab4f1c9074TabLabelGridLayout30d68aa9",
				"propertyName": "items",
				"index": 12
			}
		]/**SCHEMA_DIFF*/
	};
});
