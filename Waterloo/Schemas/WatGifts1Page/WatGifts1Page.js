define("WatGifts1Page", [], function() {
	return {
		entitySchemaName: "WatGifts",
		attributes: {},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{
			"Files": {
				"schemaName": "FileDetailV2",
				"entitySchemaName": "WatGiftsFile",
				"filter": {
					"masterColumn": "Id",
					"detailColumn": "WatGifts"
				}
			},
			"WatSchema93d4df76Detail6e1d69bc": {
				"schemaName": "WatSchema93d4df76Detail",
				"entitySchemaName": "WatInventoryAdjustments",
				"filter": {
					"detailColumn": "WatGift",
					"masterColumn": "Id"
				}
			},
			"WatSchemad43475a2Detaile4c8825e": {
				"schemaName": "WatSchemad43475a2Detail",
				"entitySchemaName": "WatDelegationGifts",
				"filter": {
					"detailColumn": "WatGift",
					"masterColumn": "Id"
				}
			}
		}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{
			"WatGiftInventoryCount": {
				"658ac2e8-7272-4344-aa20-c3d4d27ff913": {
					"uId": "658ac2e8-7272-4344-aa20-c3d4d27ff913",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "WatGiftInventoryCount"
							}
						}
					]
				}
			}
		}/**SCHEMA_BUSINESS_RULES*/,
		methods: {},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"name": "WatName95e3f75c-72d1-4e7c-925c-f8c1c3193a7f",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "WatName"
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "LOOKUPb57108a3-4f73-4131-a3ce-3cd8d83a4909",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "WatGiftLevel",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.LOOKUPb57108a34f734131a3ce3cd8d83a4909LabelCaption"
						}
					},
					"enabled": true,
					"contentType": 3
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "LOOKUP62167366-4c20-414e-a185-7236a33537e0",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Header"
					},
					"bindTo": "WatGiftCategory",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.LOOKUP621673664c20414ea1857236a33537e0LabelCaption"
						}
					},
					"enabled": true,
					"contentType": 3
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "FLOAT60436171-6f53-437a-af90-ccc5141518af",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Header"
					},
					"bindTo": "WatGiftInventoryCount",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.FLOAT604361716f53437aaf90ccc5141518afLabelCaption"
						}
					},
					"enabled": true
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "FLOAT7c29604d-6e6c-46e4-bf3b-71db2b576ac7",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "Header"
					},
					"bindTo": "WatGiftPrice",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.FLOAT7c29604d6e6c46e4bf3b71db2b576ac7LabelCaption"
						}
					},
					"enabled": true
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "Tab0939ec53TabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab0939ec53TabLabelTabCaption"
					},
					"items": [],
					"order": 0
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatSchema93d4df76Detail6e1d69bc",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab0939ec53TabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tab1738ddb8TabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab1738ddb8TabLabelTabCaption"
					},
					"items": [],
					"order": 1
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "WatSchemad43475a2Detaile4c8825e",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab1738ddb8TabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "NotesAndFilesTab",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.NotesAndFilesTabCaption"
					},
					"items": [],
					"order": 2
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "Files",
				"values": {
					"itemType": 2
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "NotesControlGroup",
				"values": {
					"itemType": 15,
					"caption": {
						"bindTo": "Resources.Strings.NotesGroupCaption"
					},
					"items": []
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Notes",
				"values": {
					"bindTo": "WatNotes",
					"dataValueType": 1,
					"contentType": 4,
					"layout": {
						"column": 0,
						"row": 0,
						"colSpan": 24
					},
					"labelConfig": {
						"visible": false
					},
					"controlConfig": {
						"imageLoaded": {
							"bindTo": "insertImagesToNotes"
						},
						"images": {
							"bindTo": "NotesImagesCollection"
						}
					}
				},
				"parentName": "NotesControlGroup",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "merge",
				"name": "ESNTab",
				"values": {
					"order": 3
				}
			}
		]/**SCHEMA_DIFF*/
	};
});
