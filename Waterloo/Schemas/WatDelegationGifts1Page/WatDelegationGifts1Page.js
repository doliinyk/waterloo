define("WatDelegationGifts1Page", [], function() {
	return {
		entitySchemaName: "WatDelegationGifts",
		attributes: {},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{
			"Files": {
				"schemaName": "FileDetailV2",
				"entitySchemaName": "WatDelegationGiftsFile",
				"filter": {
					"masterColumn": "Id",
					"detailColumn": "WatDelegationGifts"
				}
			}
		}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{
			"WatQuantity": {
				"a9828d95-b854-48a8-9e41-8552e65ee5f1": {
					"uId": "a9828d95-b854-48a8-9e41-8552e65ee5f1",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "WatQuantity"
							}
						}
					]
				}
			},
			"WatDelegationGiftStatus": {
				"65926ad8-c7fc-4c73-bc6b-434e4acce706": {
					"uId": "65926ad8-c7fc-4c73-bc6b-434e4acce706",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatDelegationGiftStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "ada0cb2f-c45a-4df0-9ec5-a2ad9883bf02",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatTakenOffInventory"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatReturnToInventory"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"WatReturnToInventory": {
				"63185208-c84a-4fa0-ab82-cad8adaee18a": {
					"uId": "63185208-c84a-4fa0-ab82-cad8adaee18a",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatDelegationGiftStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "ada0cb2f-c45a-4df0-9ec5-a2ad9883bf02",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatReturnToInventory"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				},
				"d4c51ac4-2bb4-4377-8882-46eef0211c63": {
					"uId": "d4c51ac4-2bb4-4377-8882-46eef0211c63",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatReturnToInventory"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatDelegationGiftStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "ada0cb2f-c45a-4df0-9ec5-a2ad9883bf02",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"WatDelegationInstitution": {
				"2eaac913-445e-4885-8ae1-542bf311caae": {
					"uId": "2eaac913-445e-4885-8ae1-542bf311caae",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "WatDelegationMember"
							}
						}
					]
				},
				"493f2280-e2fc-41fa-ba49-b7b3beddec55": {
					"uId": "493f2280-e2fc-41fa-ba49-b7b3beddec55",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "WatDelegationMember"
							}
						}
					]
				}
			},
			"WatDelegationMember": {
				"c7b2e35a-7225-42cc-b8c5-c28ca862e4ca": {
					"uId": "c7b2e35a-7225-42cc-b8c5-c28ca862e4ca",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "WatDelegationInstitution"
							}
						}
					]
				},
				"5223096b-ba6c-4b03-b75b-1579f289e08d": {
					"uId": "5223096b-ba6c-4b03-b75b-1579f289e08d",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "WatDelegationInstitution"
							}
						}
					]
				}
			}
		}/**SCHEMA_BUSINESS_RULES*/,
		methods: {},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"name": "LOOKUP740775e6-6868-4648-8cf7-5028d715c6e8",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "WatGift",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "BOOLEAN23584ffa-46db-49da-9b76-6528bb3a053b",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "WatTakenOffInventory",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "FLOATd84408d4-4a92-4519-b915-1904ab558ff4",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "WatQuantity",
					"enabled": true
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "BOOLEAN8e88f127-3808-4849-b553-4e625349eace",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "WatReturnToInventory",
					"enabled": true
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "LOOKUPf7ac2724-a330-44cb-aaa3-3ab75f453430",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Header"
					},
					"bindTo": "WatDelegationGiftStatus",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.LOOKUPf7ac2724a33044cbaaa33ab75f453430LabelCaption"
						}
					},
					"enabled": true,
					"contentType": 3
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "Tab9f48726cTabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab9f48726cTabLabelTabCaption"
					},
					"items": [],
					"order": 0
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tab9f48726cTabLabelGroup5842029d",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab9f48726cTabLabelGroup5842029dGroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tab9f48726cTabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tab9f48726cTabLabelGridLayout28df0a76",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tab9f48726cTabLabelGroup5842029d",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatDelegationMember5ce89eab-e80e-4672-a972-4df7448037e6",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tab9f48726cTabLabelGridLayout28df0a76"
					},
					"bindTo": "WatDelegationMember"
				},
				"parentName": "Tab9f48726cTabLabelGridLayout28df0a76",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatDelegation25aeb31a-c3f8-4768-809a-90c93846666d",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "Tab9f48726cTabLabelGridLayout28df0a76"
					},
					"bindTo": "WatDelegation",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Tab9f48726cTabLabelGridLayout28df0a76",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "WatDelegationInstitutionecb88ab7-f0ad-486e-b33c-01a0b6ab8477",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Tab9f48726cTabLabelGridLayout28df0a76"
					},
					"bindTo": "WatDelegationInstitution"
				},
				"parentName": "Tab9f48726cTabLabelGridLayout28df0a76",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "LOOKUP06b75818-326d-493b-bad4-f9f9f35fa26a",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "Tab9f48726cTabLabelGridLayout28df0a76"
					},
					"bindTo": "WatAccount",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "Tab9f48726cTabLabelGridLayout28df0a76",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "Tab9f48726cTabLabelGroup2426ead2",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab9f48726cTabLabelGroup2426ead2GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tab9f48726cTabLabel",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Tab9f48726cTabLabelGridLayout1453bc7a",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tab9f48726cTabLabelGroup2426ead2",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatPricePerItem4e20b9cf-da9a-4716-90dd-ef6146a22eff",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tab9f48726cTabLabelGridLayout1453bc7a"
					},
					"bindTo": "WatPricePerItem",
					"enabled": false
				},
				"parentName": "Tab9f48726cTabLabelGridLayout1453bc7a",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatQuantity25746d84-068a-4dde-829b-a8324f4002b2",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Tab9f48726cTabLabelGridLayout1453bc7a"
					},
					"bindTo": "WatQuantity",
					"enabled": false
				},
				"parentName": "Tab9f48726cTabLabelGridLayout1453bc7a",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "WatTotalCost4c493a3c-e0c5-45ac-9541-38a16ab772f1",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Tab9f48726cTabLabelGridLayout1453bc7a"
					},
					"bindTo": "WatTotalCost",
					"enabled": false
				},
				"parentName": "Tab9f48726cTabLabelGridLayout1453bc7a",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "NotesAndFilesTab",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.NotesAndFilesTabCaption"
					},
					"items": [],
					"order": 1
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Files",
				"values": {
					"itemType": 2
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "NotesControlGroup",
				"values": {
					"itemType": 15,
					"caption": {
						"bindTo": "Resources.Strings.NotesGroupCaption"
					},
					"items": []
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Notes",
				"values": {
					"bindTo": "WatNotes",
					"dataValueType": 1,
					"contentType": 4,
					"layout": {
						"column": 0,
						"row": 0,
						"colSpan": 24
					},
					"labelConfig": {
						"visible": false
					},
					"controlConfig": {
						"imageLoaded": {
							"bindTo": "insertImagesToNotes"
						},
						"images": {
							"bindTo": "NotesImagesCollection"
						}
					}
				},
				"parentName": "NotesControlGroup",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "merge",
				"name": "ESNTab",
				"values": {
					"order": 2
				}
			}
		]/**SCHEMA_DIFF*/
	};
});
