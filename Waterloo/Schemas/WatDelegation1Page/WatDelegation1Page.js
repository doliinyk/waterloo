define("WatDelegation1Page", [], function() {
	return {
		entitySchemaName: "WatDelegation",
		attributes: {},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{
			"Files": {
				"schemaName": "FileDetailV2",
				"entitySchemaName": "WatDelegationFile",
				"filter": {
					"masterColumn": "Id",
					"detailColumn": "WatDelegation"
				}
			},
			"VisaDetailV219dca577": {
				"schemaName": "VisaDetailV2",
				"entitySchemaName": "WatDelegationVisa",
				"filter": {
					"masterColumn": "Id",
					"detailColumn": "WatDelegation"
				}
			},
			"WatSchemab33cd46aDetail2a1974fe": {
				"schemaName": "WatSchemab33cd46aDetail",
				"entitySchemaName": "WatDelegationPurpose",
				"filter": {
					"detailColumn": "WatDelegation",
					"masterColumn": "Id"
				}
			},
			"WatSchemafa655ca7Detail6a06906d": {
				"schemaName": "WatSchemafa655ca7Detail",
				"entitySchemaName": "WatDelegationMembers",
				"filter": {
					"detailColumn": "WatDelegation",
					"masterColumn": "Id"
				}
			},
			"WatSchemab70f4469Detaile6224498": {
				"schemaName": "WatSchemab70f4469Detail",
				"entitySchemaName": "WatDelegationInstitutions",
				"filter": {
					"detailColumn": "WatDelegation",
					"masterColumn": "Id"
				}
			},
			"WatSchemad43475a2Detail6adc6e2c": {
				"schemaName": "WatSchemad43475a2Detail",
				"entitySchemaName": "WatDelegationGifts",
				"filter": {
					"detailColumn": "WatDelegation",
					"masterColumn": "Id"
				}
			},
			"WatSchema0cb90f63Detaila68dcc58": {
				"schemaName": "WatSchema0cb90f63Detail",
				"entitySchemaName": "WatHospitalityRequest",
				"filter": {
					"detailColumn": "WatDelegation",
					"masterColumn": "Id"
				}
			},
			"DocumentDetailV2546c981a": {
				"schemaName": "DocumentDetailV2",
				"entitySchemaName": "Document",
				"filter": {
					"detailColumn": "WatDelegation",
					"masterColumn": "Id"
				}
			},
			"WatSchema94c7e3c8Detailba865f5c": {
				"schemaName": "WatSchema94c7e3c8Detail",
				"entitySchemaName": "WatDatedNotes",
				"filter": {
					"detailColumn": "WatDelegation",
					"masterColumn": "Id"
				}
			},
			"WatSchemaCustomEmailCallDetailc9f6325c": {
				"schemaName": "WatSchemaCustomEmailCallDetail",
				"entitySchemaName": "Activity",
				"filter": {
					"detailColumn": "WatWatDelegation",
					"masterColumn": "Id"
				}
			},
			"WatSchemaCustomTaskDetail2a1b310e": {
				"schemaName": "WatSchemaCustomTaskDetail",
				"entitySchemaName": "Activity",
				"filter": {
					"detailColumn": "WatWatDelegation",
					"masterColumn": "Id"
				}
			}
		}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{
			"WatOwner": {
				"0086619e-930a-47e3-8883-2cf2e360c516": {
					"uId": "0086619e-930a-47e3-8883-2cf2e360c516",
					"enabled": true,
					"removed": false,
					"ruleType": 1,
					"baseAttributePatch": "Account",
					"comparisonType": 3,
					"autoClean": false,
					"autocomplete": false,
					"type": 0,
					"value": "e308b781-3c5b-4ecb-89ef-5c1ed4da488e",
					"dataValueType": 10
				},
				"6caf2a43-afdc-4104-874c-9cafd191792d": {
					"uId": "6caf2a43-afdc-4104-874c-9cafd191792d",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatDelegationStage"
							},
							"rightExpression": {
								"type": 0,
								"value": "f1107ff3-a206-44bb-bf02-8a4f82bf9f2c",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"WatCancellationReason": {
				"baec52bf-75fb-4c77-80c5-a363461c5bf8": {
					"uId": "baec52bf-75fb-4c77-80c5-a363461c5bf8",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatCancelDelegation"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"WatGiftLevel": {
				"cffa3f18-1bee-4bdc-a533-07eb2523b048": {
					"uId": "cffa3f18-1bee-4bdc-a533-07eb2523b048",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatGiftsRequired"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				},
				"6cd8ff06-f09e-4e27-a3e9-7a7195f27209": {
					"uId": "6cd8ff06-f09e-4e27-a3e9-7a7195f27209",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatGiftsRequired"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				},
				"73243689-a79a-4310-8ebb-90ffa7160603": {
					"uId": "73243689-a79a-4310-8ebb-90ffa7160603",
					"enabled": false,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatDelegationStage"
							},
							"rightExpression": {
								"type": 0,
								"value": "f1107ff3-a206-44bb-bf02-8a4f82bf9f2c",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatDelegationStage"
							},
							"rightExpression": {
								"type": 0,
								"value": "06130f04-e80e-49ec-91bc-eb6b6f699161",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"WatGiftComments": {
				"e7c142f3-5f30-435b-963a-5729c95aeb1f": {
					"uId": "e7c142f3-5f30-435b-963a-5729c95aeb1f",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatGiftsRequired"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						},
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "WatGiftComments"
							}
						}
					]
				}
			},
			"WatPastDelegation": {
				"df771a87-3527-4219-927d-c6a85a69dad0": {
					"uId": "df771a87-3527-4219-927d-c6a85a69dad0",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatPastDelegation"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"WatOnHoldUntil": {
				"7c8ec38d-602e-4154-bab1-1f28a56e7f04": {
					"uId": "7c8ec38d-602e-4154-bab1-1f28a56e7f04",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatOnHold"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				},
				"015955a5-412f-47ff-92bc-071fd48a8551": {
					"uId": "015955a5-412f-47ff-92bc-071fd48a8551",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatOnHold"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"WatRequestNumber": {
				"53f15f68-a236-4643-8164-8ea46922ceb6": {
					"uId": "53f15f68-a236-4643-8164-8ea46922ceb6",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "WatRequestNumber"
							}
						}
					]
				}
			},
			"WatArrivalDate": {
				"c0396bf0-408a-433b-8bd4-a25f9a0dde1e": {
					"uId": "c0396bf0-408a-433b-8bd4-a25f9a0dde1e",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatDatesMandatory"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"WatDepartureDate": {
				"757f399f-2ee4-4277-b5ec-73a1c94cbb8f": {
					"uId": "757f399f-2ee4-4277-b5ec-73a1c94cbb8f",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatDatesMandatory"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"WatTotalDelegates": {
				"2d008917-5675-4a87-8dcf-2b008d8e5ebf": {
					"uId": "2d008917-5675-4a87-8dcf-2b008d8e5ebf",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatDatesMandatory"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"WatAssessmentNotes": {
				"4536a075-d9d9-4414-affd-cbb2b41d98ec": {
					"uId": "4536a075-d9d9-4414-affd-cbb2b41d98ec",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "WatProceedWithDelegation"
							}
						}
					]
				},
				"2c04e451-5358-49fd-904d-73f777029f84": {
					"uId": "2c04e451-5358-49fd-904d-73f777029f84",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "WatProceedWithDelegation"
							}
						}
					]
				}
			},
			"WatLockProceedField": {
				"ff55d452-c232-41dc-ad94-762fa0308ed8": {
					"uId": "ff55d452-c232-41dc-ad94-762fa0308ed8",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 0,
								"value": "123",
								"dataValueType": 1
							},
							"rightExpression": {
								"type": 0,
								"value": "abc",
								"dataValueType": 1
							}
						}
					]
				}
			},
			"WatProceedWithDelegation": {
				"598888cb-c516-450b-a662-b9c185fd86ad": {
					"uId": "598888cb-c516-450b-a662-b9c185fd86ad",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatSysFieldMandatoryProceed"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				},
				"fa5640b2-1015-4a30-9901-f357500b63d2": {
					"uId": "fa5640b2-1015-4a30-9901-f357500b63d2",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatSysFieldLockProceed"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				},
				"7cd31205-3136-44de-a1a7-86e9f8c4a9df": {
					"uId": "7cd31205-3136-44de-a1a7-86e9f8c4a9df",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatDelegationStage"
							},
							"rightExpression": {
								"type": 0,
								"value": "f1107ff3-a206-44bb-bf02-8a4f82bf9f2c",
								"dataValueType": 10
							}
						}
					]
				},
				"98440b1f-a8b5-4bb9-8d48-8e5099cfec9c": {
					"uId": "98440b1f-a8b5-4bb9-8d48-8e5099cfec9c",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatStartAssessment"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"WatSysFieldLockProceed": {
				"189d4595-ad40-42f5-8e0c-3106e3202693": {
					"uId": "189d4595-ad40-42f5-8e0c-3106e3202693",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 0,
								"value": "abc",
								"dataValueType": 1
							},
							"rightExpression": {
								"type": 0,
								"value": "123",
								"dataValueType": 1
							}
						}
					]
				}
			},
			"WatSysFieldMandatoryProceed": {
				"13dc5638-3703-4499-bfd8-40354f96c99e": {
					"uId": "13dc5638-3703-4499-bfd8-40354f96c99e",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 0,
								"value": "abc",
								"dataValueType": 1
							},
							"rightExpression": {
								"type": 0,
								"value": "123",
								"dataValueType": 1
							}
						}
					]
				}
			},
			"WatStartAssessment": {
				"b9fa3901-ecfd-4f0d-b6e3-a3659a35c108": {
					"uId": "b9fa3901-ecfd-4f0d-b6e3-a3659a35c108",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatStartAssessment"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatDelegationStage"
							},
							"rightExpression": {
								"type": 0,
								"value": "f1107ff3-a206-44bb-bf02-8a4f82bf9f2c",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"NotesControlGroup": {
				"94ef6401-5f2f-490d-be85-7b6bef4d0230": {
					"uId": "94ef6401-5f2f-490d-be85-7b6bef4d0230",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 0,
								"value": 1,
								"dataValueType": 4
							},
							"rightExpression": {
								"type": 0,
								"value": 2,
								"dataValueType": 4
							}
						}
					]
				}
			}
		}/**SCHEMA_BUSINESS_RULES*/,
		methods: {			
			emailDetailFilter: function() {
				var filterGroup = new Terrasoft.createFilterGroup();
				filterGroup.logicalOperation = Terrasoft.LogicalOperatorType.AND;
				filterGroup.add(
					"DelegationFilter",
					Terrasoft.createColumnFilterWithParameter(
						Terrasoft.ComparisonType.EQUAL,
						"WatWatDelegation.Id",
						this.get("Id")
					)
				);
				filterGroup.add(
					"EmailFilter",
					Terrasoft.createColumnFilterWithParameter(
						Terrasoft.ComparisonType.EQUAL,
						"Type",
						"e2831dec-cfc0-df11-b00f-001d60e938c6"
					)
				);
				return filterGroup;
			}
		},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"name": "WatName0a93b6b6-3e39-48bd-b1da-4ae91b7e736c",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "WatName",
					"enabled": true
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "STRINGfb69f1df-2556-448c-bd83-5bfdc24e57f2",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "WatRequestNumber",
					"enabled": false
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "LOOKUP8e553c27-b11f-4538-8fc9-243eb9897298",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "WatDelegationType",
					"enabled": true,
					"contentType": 3
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "LOOKUP4a018b0d-5d4e-433c-be35-091dc5caccd7",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "WatOwner",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.LOOKUP4a018b0d5d4e433cbe35091dc5caccd7LabelCaption"
						}
					},
					"enabled": true,
					"contentType": 5
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "LOOKUP2638e577-ebf7-478b-b3cf-e0e47d047949",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "WatDelegationRequestStatus",
					"enabled": false,
					"contentType": 3,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.LOOKUP2638e577ebf7478bb3cfe0e47d047949LabelCaption"
						}
					}
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "WatArrivalDated4a48c1f-c254-4e07-ba47-955e4e04cc17",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 5,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "WatArrivalDate",
					"enabled": false
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "WatDepartureDated9376b92-f3ea-4852-992e-094919487a22",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 6,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "WatDepartureDate",
					"enabled": false
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "BOOLEAN1624a054-796d-4ee2-aab8-9108ad64c699",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 7,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "WatPastDelegation",
					"enabled": false
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "BOOLEAN3e2b4e8e-3773-4dd9-acbf-7fec069eb81a",
				"values": {
					"layout": {
						"colSpan": 5,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "WatCancelDelegation",
					"enabled": true
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "STRING43d7abf3-e263-42d3-b49a-72b7c897a5ed",
				"values": {
					"layout": {
						"colSpan": 14,
						"rowSpan": 1,
						"column": 5,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "WatCancellationReason",
					"enabled": true,
					"contentType": 0
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "BOOLEAN2ca72ed8-d67e-4d65-b39c-0d574d932d68",
				"values": {
					"layout": {
						"colSpan": 5,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "WatOnHold",
					"enabled": true
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "WatOnHoldUntil1a7de67b-ef68-4d8c-9679-828da507647d",
				"values": {
					"layout": {
						"colSpan": 5,
						"rowSpan": 1,
						"column": 5,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "WatOnHoldUntil"
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "Taba8721c93TabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Taba8721c93TabLabelTabCaption"
					},
					"items": [],
					"order": 0
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Taba8721c93TabLabelGroup580d72e2",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Taba8721c93TabLabelGroup580d72e2GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Taba8721c93TabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Taba8721c93TabLabelGridLayout5737f607",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Taba8721c93TabLabelGroup580d72e2",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "DATETIME03891c38-910e-4bc1-92bc-c901d6e51508",
				"values": {
					"layout": {
						"colSpan": 6,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Taba8721c93TabLabelGridLayout5737f607"
					},
					"bindTo": "WatArrivalDate",
					"enabled": true
				},
				"parentName": "Taba8721c93TabLabelGridLayout5737f607",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "STRING24e2e6a7-fec2-4465-ae58-71de06afef78",
				"values": {
					"layout": {
						"colSpan": 6,
						"rowSpan": 1,
						"column": 6,
						"row": 0,
						"layoutName": "Taba8721c93TabLabelGridLayout5737f607"
					},
					"bindTo": "WatArrivalTime",
					"enabled": true
				},
				"parentName": "Taba8721c93TabLabelGridLayout5737f607",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "LOOKUP9631d91c-c1c7-4d60-bfe1-7896609c6d1b",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "Taba8721c93TabLabelGridLayout5737f607"
					},
					"bindTo": "WatGiftLevel",
					"enabled": true,
					"contentType": 3
				},
				"parentName": "Taba8721c93TabLabelGridLayout5737f607",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "DATETIMEbb1db362-8ac6-4ccd-8713-46d04cb2e735",
				"values": {
					"layout": {
						"colSpan": 6,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Taba8721c93TabLabelGridLayout5737f607"
					},
					"bindTo": "WatDepartureDate",
					"enabled": true
				},
				"parentName": "Taba8721c93TabLabelGridLayout5737f607",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "STRINGe8d01ef1-f1dc-47dc-bbae-75a7e5ceb14b",
				"values": {
					"layout": {
						"colSpan": 6,
						"rowSpan": 1,
						"column": 6,
						"row": 1,
						"layoutName": "Taba8721c93TabLabelGridLayout5737f607"
					},
					"bindTo": "WatDepartureTime",
					"enabled": true
				},
				"parentName": "Taba8721c93TabLabelGridLayout5737f607",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "STRING8d7ba300-d918-4aee-9520-e76bea54ad65",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "Taba8721c93TabLabelGridLayout5737f607"
					},
					"bindTo": "WatGeneralComments",
					"enabled": true,
					"contentType": 0
				},
				"parentName": "Taba8721c93TabLabelGridLayout5737f607",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "STRING8d7d6c90-097f-4aea-a46b-df651a0d55ba",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Taba8721c93TabLabelGridLayout5737f607"
					},
					"bindTo": "WatAccessibility",
					"enabled": true,
					"contentType": 0
				},
				"parentName": "Taba8721c93TabLabelGridLayout5737f607",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "INTEGER2bcb0b96-ed4c-4bef-8ff3-f63016d9892f",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Taba8721c93TabLabelGridLayout5737f607"
					},
					"bindTo": "WatTotalDelegates",
					"enabled": true
				},
				"parentName": "Taba8721c93TabLabelGridLayout5737f607",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "WatSchemab33cd46aDetail2a1974fe",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Taba8721c93TabLabel",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Taba8721c93TabLabelGroup63a6a1d4",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Taba8721c93TabLabelGroup63a6a1d4GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Taba8721c93TabLabel",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "Taba8721c93TabLabelGridLayout5a65e398",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Taba8721c93TabLabelGroup63a6a1d4",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatTotalGiftCost91639cfc-e78e-47d6-9bd6-9fbf7082f26c",
				"values": {
					"layout": {
						"colSpan": 7,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Taba8721c93TabLabelGridLayout5a65e398"
					},
					"bindTo": "WatTotalGiftCost",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatTotalGiftCost91639cfce78e47d69bd69fbf7082f26cLabelCaption"
						}
					},
					"enabled": false
				},
				"parentName": "Taba8721c93TabLabelGridLayout5a65e398",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatTotalHospitalityCost46bbe8b2-de20-4388-8166-7ff06045c859",
				"values": {
					"layout": {
						"colSpan": 7,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Taba8721c93TabLabelGridLayout5a65e398"
					},
					"bindTo": "WatTotalHospitalityCost",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatTotalHospitalityCost46bbe8b2de20438881667ff06045c859LabelCaption"
						}
					},
					"enabled": false
				},
				"parentName": "Taba8721c93TabLabelGridLayout5a65e398",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "FLOATff714eeb-4931-4e01-93a8-95871f5cdee6",
				"values": {
					"layout": {
						"colSpan": 7,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Taba8721c93TabLabelGridLayout5a65e398"
					},
					"bindTo": "WatMiscellaneousCost",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.FLOATff714eeb49314e0193a895871f5cdee6LabelCaption"
						}
					},
					"enabled": true
				},
				"parentName": "Taba8721c93TabLabelGridLayout5a65e398",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "FLOAT12c7b30c-2e27-4604-b9bf-4ca7d8431946",
				"values": {
					"layout": {
						"colSpan": 7,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Taba8721c93TabLabelGridLayout5a65e398"
					},
					"bindTo": "WatTotalDelegationCost",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.FLOAT12c7b30c2e274604b9bf4ca7d8431946LabelCaption"
						}
					},
					"enabled": true
				},
				"parentName": "Taba8721c93TabLabelGridLayout5a65e398",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "Tab4c58f731TabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.TabVisaCaption"
					},
					"items": [],
					"order": 1
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Tab4c58f731TabLabelGroup66850641",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab4c58f731TabLabelGroup66850641GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tab4c58f731TabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tab4c58f731TabLabelGridLayout95d10266",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tab4c58f731TabLabelGroup66850641",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatAssessmentNotes6cebe56a-8f41-43e5-a1c1-5b4b82ddf011",
				"values": {
					"layout": {
						"colSpan": 16,
						"rowSpan": 2,
						"column": 0,
						"row": 0,
						"layoutName": "Tab4c58f731TabLabelGridLayout95d10266"
					},
					"bindTo": "WatAssessmentNotes",
					"enabled": true,
					"contentType": 0
				},
				"parentName": "Tab4c58f731TabLabelGridLayout95d10266",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatProceedWithDelegation12cb406d-247e-44e1-b373-af028b4e9b97",
				"values": {
					"layout": {
						"colSpan": 7,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Tab4c58f731TabLabelGridLayout95d10266"
					},
					"bindTo": "WatProceedWithDelegation",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatProceedWithDelegation12cb406d247e44e1b373af028b4e9b97LabelCaption"
						}
					},
					"enabled": true,
					"contentType": 3
				},
				"parentName": "Tab4c58f731TabLabelGridLayout95d10266",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "VisaDetailV219dca577",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab4c58f731TabLabel",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Tab4c58f731TabLabelGroup5ed3629d",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab4c58f731TabLabelGroup5ed3629dGroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tab4c58f731TabLabel",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "Tab4c58f731TabLabelGridLayout49753280",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tab4c58f731TabLabelGroup5ed3629d",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "STRING49419387-914c-437a-9b6d-1bb391542f3c",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tab4c58f731TabLabelGridLayout49753280"
					},
					"bindTo": "WatApprovalNotes",
					"enabled": true,
					"contentType": 0,
					"labelConfig": {
						"visible": false
					}
				},
				"parentName": "Tab4c58f731TabLabelGridLayout49753280",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tab15f8a459TabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab15f8a459TabLabelTabCaption"
					},
					"items": [],
					"order": 2
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "Tab15f8a459TabLabelGroupaac363f1",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab15f8a459TabLabelGroupaac363f1GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tab15f8a459TabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tab15f8a459TabLabelGridLayout8f72a3f2",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tab15f8a459TabLabelGroupaac363f1",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatConsortium13965b5a-b65f-412d-970c-fa584498415b",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tab15f8a459TabLabelGridLayout8f72a3f2"
					},
					"bindTo": "WatConsortium",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "Tab15f8a459TabLabelGridLayout8f72a3f2",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatTotalDelegates039194da-f871-4442-89b1-ca04a4b008e6",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "Tab15f8a459TabLabelGridLayout8f72a3f2"
					},
					"bindTo": "WatTotalDelegates"
				},
				"parentName": "Tab15f8a459TabLabelGridLayout8f72a3f2",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "WatPrimaryContactbf53a6df-2eac-4948-8d9c-4fc59f3f37c5",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Tab15f8a459TabLabelGridLayout8f72a3f2"
					},
					"bindTo": "WatPrimaryContact"
				},
				"parentName": "Tab15f8a459TabLabelGridLayout8f72a3f2",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "WatSchemab70f4469Detaile6224498",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab15f8a459TabLabel",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "WatSchemafa655ca7Detail6a06906d",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab15f8a459TabLabel",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "Tab0c141700TabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab0c141700TabLabelTabCaption"
					},
					"items": [],
					"order": 3
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "Tab0c141700TabLabelGroup897d9f8c",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab0c141700TabLabelGroup897d9f8cGroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tab0c141700TabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tab0c141700TabLabelGridLayout093c524a",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tab0c141700TabLabelGroup897d9f8c",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "BOOLEAN025742ea-6f56-4341-afbe-8161f60e0db4",
				"values": {
					"layout": {
						"colSpan": 5,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tab0c141700TabLabelGridLayout093c524a"
					},
					"bindTo": "WatRoomBooking",
					"enabled": true
				},
				"parentName": "Tab0c141700TabLabelGridLayout093c524a",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "BOOLEANd3873c51-f71b-484e-a11a-88677330ca7a",
				"values": {
					"layout": {
						"colSpan": 5,
						"rowSpan": 1,
						"column": 5,
						"row": 0,
						"layoutName": "Tab0c141700TabLabelGridLayout093c524a"
					},
					"bindTo": "WatTentCards",
					"enabled": true
				},
				"parentName": "Tab0c141700TabLabelGridLayout093c524a",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "BOOLEANca88364f-7024-4bec-b723-54d5b9b21d0a",
				"values": {
					"layout": {
						"colSpan": 5,
						"rowSpan": 1,
						"column": 10,
						"row": 0,
						"layoutName": "Tab0c141700TabLabelGridLayout093c524a"
					},
					"bindTo": "WatNameTags",
					"enabled": true
				},
				"parentName": "Tab0c141700TabLabelGridLayout093c524a",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "BOOLEAN46d54b73-0cc0-43da-8872-7add85d9ea63",
				"values": {
					"layout": {
						"colSpan": 5,
						"rowSpan": 1,
						"column": 15,
						"row": 0,
						"layoutName": "Tab0c141700TabLabelGridLayout093c524a"
					},
					"bindTo": "WatSignage",
					"enabled": true
				},
				"parentName": "Tab0c141700TabLabelGridLayout093c524a",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "BOOLEAN9ab9cbc4-30ab-4854-aba5-42cc3a4e14d3",
				"values": {
					"layout": {
						"colSpan": 5,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Tab0c141700TabLabelGridLayout093c524a"
					},
					"bindTo": "WatFlag",
					"enabled": true
				},
				"parentName": "Tab0c141700TabLabelGridLayout093c524a",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "BOOLEAN9354cc76-0aa3-4ec0-a817-600fc833ca7c",
				"values": {
					"layout": {
						"colSpan": 5,
						"rowSpan": 1,
						"column": 5,
						"row": 1,
						"layoutName": "Tab0c141700TabLabelGridLayout093c524a"
					},
					"bindTo": "WatTableFlags",
					"enabled": true
				},
				"parentName": "Tab0c141700TabLabelGridLayout093c524a",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "BOOLEAN6c5c825d-f771-4584-820e-b8c8fa45e32d",
				"values": {
					"layout": {
						"colSpan": 5,
						"rowSpan": 1,
						"column": 10,
						"row": 1,
						"layoutName": "Tab0c141700TabLabelGridLayout093c524a"
					},
					"bindTo": "WatCoatRack",
					"enabled": true
				},
				"parentName": "Tab0c141700TabLabelGridLayout093c524a",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "BOOLEAN13963945-2785-45cb-bfcf-676fd6203022",
				"values": {
					"layout": {
						"colSpan": 5,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Tab0c141700TabLabelGridLayout093c524a"
					},
					"bindTo": "WatLaptop",
					"enabled": true
				},
				"parentName": "Tab0c141700TabLabelGridLayout093c524a",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "BOOLEAN414631ea-ea56-4b07-9e1b-47e2f3d85bb1",
				"values": {
					"layout": {
						"colSpan": 5,
						"rowSpan": 1,
						"column": 5,
						"row": 2,
						"layoutName": "Tab0c141700TabLabelGridLayout093c524a"
					},
					"bindTo": "WatProjectorDisplay",
					"enabled": true
				},
				"parentName": "Tab0c141700TabLabelGridLayout093c524a",
				"propertyName": "items",
				"index": 8
			},
			{
				"operation": "insert",
				"name": "BOOLEAN3cc2696d-673e-4966-9012-596dd328727e",
				"values": {
					"layout": {
						"colSpan": 5,
						"rowSpan": 1,
						"column": 10,
						"row": 2,
						"layoutName": "Tab0c141700TabLabelGridLayout093c524a"
					},
					"bindTo": "WatPresentation",
					"enabled": true
				},
				"parentName": "Tab0c141700TabLabelGridLayout093c524a",
				"propertyName": "items",
				"index": 9
			},
			{
				"operation": "insert",
				"name": "BOOLEAN32f85908-b60a-4cd6-b454-ac371abed5b5",
				"values": {
					"layout": {
						"colSpan": 5,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Tab0c141700TabLabelGridLayout093c524a"
					},
					"bindTo": "WatFolders",
					"enabled": true
				},
				"parentName": "Tab0c141700TabLabelGridLayout093c524a",
				"propertyName": "items",
				"index": 10
			},
			{
				"operation": "insert",
				"name": "BOOLEANb6882505-01b9-4bee-95d5-c579f12a416e",
				"values": {
					"layout": {
						"colSpan": 5,
						"rowSpan": 1,
						"column": 5,
						"row": 3,
						"layoutName": "Tab0c141700TabLabelGridLayout093c524a"
					},
					"bindTo": "WatBrochures",
					"enabled": true
				},
				"parentName": "Tab0c141700TabLabelGridLayout093c524a",
				"propertyName": "items",
				"index": 11
			},
			{
				"operation": "insert",
				"name": "BOOLEANbee8a4bf-b311-4f48-a11f-c771265dcf1b",
				"values": {
					"layout": {
						"colSpan": 5,
						"rowSpan": 1,
						"column": 10,
						"row": 3,
						"layoutName": "Tab0c141700TabLabelGridLayout093c524a"
					},
					"bindTo": "WatPrintAgenda",
					"enabled": true
				},
				"parentName": "Tab0c141700TabLabelGridLayout093c524a",
				"propertyName": "items",
				"index": 12
			},
			{
				"operation": "insert",
				"name": "BOOLEAN42f515f0-c4c7-46e7-aafe-1350a9e7a8ec",
				"values": {
					"layout": {
						"colSpan": 5,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "Tab0c141700TabLabelGridLayout093c524a"
					},
					"bindTo": "WatRefreshments",
					"enabled": true
				},
				"parentName": "Tab0c141700TabLabelGridLayout093c524a",
				"propertyName": "items",
				"index": 13
			},
			{
				"operation": "insert",
				"name": "BOOLEAN0ad7ec6a-3907-4422-97fd-75464ae662bc",
				"values": {
					"layout": {
						"colSpan": 5,
						"rowSpan": 1,
						"column": 5,
						"row": 4,
						"layoutName": "Tab0c141700TabLabelGridLayout093c524a"
					},
					"bindTo": "WatReservations",
					"enabled": true
				},
				"parentName": "Tab0c141700TabLabelGridLayout093c524a",
				"propertyName": "items",
				"index": 14
			},
			{
				"operation": "insert",
				"name": "BOOLEANa96c8014-671e-492a-8408-c88c6bd96748",
				"values": {
					"layout": {
						"colSpan": 5,
						"rowSpan": 1,
						"column": 10,
						"row": 4,
						"layoutName": "Tab0c141700TabLabelGridLayout093c524a"
					},
					"bindTo": "WatCatering",
					"enabled": true
				},
				"parentName": "Tab0c141700TabLabelGridLayout093c524a",
				"propertyName": "items",
				"index": 15
			},
			{
				"operation": "insert",
				"name": "BOOLEAN0bf2a136-7bd6-4fea-b8d3-40b6d0018c8c",
				"values": {
					"layout": {
						"colSpan": 5,
						"rowSpan": 1,
						"column": 0,
						"row": 5,
						"layoutName": "Tab0c141700TabLabelGridLayout093c524a"
					},
					"bindTo": "WatParking",
					"enabled": true
				},
				"parentName": "Tab0c141700TabLabelGridLayout093c524a",
				"propertyName": "items",
				"index": 16
			},
			{
				"operation": "insert",
				"name": "BOOLEAN14b2c353-8197-4ea2-98e1-4688fbb39134",
				"values": {
					"layout": {
						"colSpan": 5,
						"rowSpan": 1,
						"column": 5,
						"row": 5,
						"layoutName": "Tab0c141700TabLabelGridLayout093c524a"
					},
					"bindTo": "WatCarServiceRental",
					"enabled": true
				},
				"parentName": "Tab0c141700TabLabelGridLayout093c524a",
				"propertyName": "items",
				"index": 17
			},
			{
				"operation": "insert",
				"name": "BOOLEAN97070592-f3b3-43d5-95f5-34be6ae4e1c7",
				"values": {
					"layout": {
						"colSpan": 5,
						"rowSpan": 1,
						"column": 10,
						"row": 5,
						"layoutName": "Tab0c141700TabLabelGridLayout093c524a"
					},
					"bindTo": "WatHotelReservation",
					"enabled": true
				},
				"parentName": "Tab0c141700TabLabelGridLayout093c524a",
				"propertyName": "items",
				"index": 18
			},
			{
				"operation": "insert",
				"name": "WatGiftsRequired12a4a2b1-3b7b-4ef4-8bb5-230f6968c967",
				"values": {
					"layout": {
						"colSpan": 5,
						"rowSpan": 1,
						"column": 0,
						"row": 6,
						"layoutName": "Tab0c141700TabLabelGridLayout093c524a"
					},
					"bindTo": "WatGiftsRequired"
				},
				"parentName": "Tab0c141700TabLabelGridLayout093c524a",
				"propertyName": "items",
				"index": 19
			},
			{
				"operation": "insert",
				"name": "WatGiftCommentsff0b038e-4f8b-4cbb-a2d0-6b31536e99bb",
				"values": {
					"layout": {
						"colSpan": 23,
						"rowSpan": 2,
						"column": 0,
						"row": 7,
						"layoutName": "Tab0c141700TabLabelGridLayout093c524a"
					},
					"bindTo": "WatGiftComments",
					"enabled": true,
					"contentType": 0
				},
				"parentName": "Tab0c141700TabLabelGridLayout093c524a",
				"propertyName": "items",
				"index": 20
			},
			{
				"operation": "insert",
				"name": "Tab0c141700TabLabelGroup5bd4984d",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab0c141700TabLabelGroup5bd4984dGroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tab0c141700TabLabel",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Tab0c141700TabLabelGridLayout0bbe4fdb",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tab0c141700TabLabelGroup5bd4984d",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "FLOAT1207973f-a229-48fa-9cf5-d5725896bf7a",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tab0c141700TabLabelGridLayout0bbe4fdb"
					},
					"bindTo": "WatTotalHospitalityCost",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.FLOAT1207973fa22948fa9cf5d5725896bf7aLabelCaption"
						}
					},
					"enabled": false
				},
				"parentName": "Tab0c141700TabLabelGridLayout0bbe4fdb",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatSchema0cb90f63Detaila68dcc58",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab0c141700TabLabel",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "Tab5fa16d4aTabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab5fa16d4aTabLabelTabCaption"
					},
					"items": [],
					"order": 4
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "Tab5fa16d4aTabLabelGroup4736f56d",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab5fa16d4aTabLabelGroup4736f56dGroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tab5fa16d4aTabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tab5fa16d4aTabLabelGridLayoutc003f2c1",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tab5fa16d4aTabLabelGroup4736f56d",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "FLOATc06597e3-f2aa-4fca-83b9-69ed586bd516",
				"values": {
					"layout": {
						"colSpan": 7,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tab5fa16d4aTabLabelGridLayoutc003f2c1"
					},
					"bindTo": "WatTotalGiftCost",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.FLOATc06597e3f2aa4fca83b969ed586bd516LabelCaption"
						}
					},
					"enabled": false
				},
				"parentName": "Tab5fa16d4aTabLabelGridLayoutc003f2c1",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatGiftComments8fc13fba-0afc-4464-a875-2cec7e5a4257",
				"values": {
					"layout": {
						"colSpan": 17,
						"rowSpan": 1,
						"column": 7,
						"row": 0,
						"layoutName": "Tab5fa16d4aTabLabelGridLayoutc003f2c1"
					},
					"bindTo": "WatGiftComments",
					"enabled": true,
					"contentType": 0
				},
				"parentName": "Tab5fa16d4aTabLabelGridLayoutc003f2c1",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "WatSchemad43475a2Detail6adc6e2c",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab5fa16d4aTabLabel",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Tab29c74f6cTabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab29c74f6cTabLabelTabCaption"
					},
					"items": [],
					"order": 5
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "WatSchemaCustomEmailCallDetailc9f6325c",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab29c74f6cTabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatSchemaCustomTaskDetail2a1b310e",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab29c74f6cTabLabel",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "NotesAndFilesTab",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.NotesAndFilesTabCaption"
					},
					"items": [],
					"order": 6
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "DocumentDetailV2546c981a",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Files",
				"values": {
					"itemType": 2
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "NotesControlGroup",
				"values": {
					"itemType": 15,
					"caption": {
						"bindTo": "Resources.Strings.NotesGroupCaption"
					},
					"items": []
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "Notes",
				"values": {
					"bindTo": "WatNotes",
					"dataValueType": 1,
					"contentType": 4,
					"layout": {
						"column": 0,
						"row": 0,
						"colSpan": 24
					},
					"labelConfig": {
						"visible": false
					},
					"controlConfig": {
						"imageLoaded": {
							"bindTo": "insertImagesToNotes"
						},
						"images": {
							"bindTo": "NotesImagesCollection"
						}
					}
				},
				"parentName": "NotesControlGroup",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "merge",
				"name": "ESNTab",
				"values": {
					"order": 7
				}
			},
			{
				"operation": "insert",
				"name": "WatSchema94c7e3c8Detailba865f5c",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "ESNTab",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tab3cc61684TabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab3cc61684TabLabelTabCaption"
					},
					"items": [],
					"order": 8
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 8
			},
			{
				"operation": "insert",
				"name": "Tab3cc61684TabLabelGroup967a47ce",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab3cc61684TabLabelGroup967a47ceGroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tab3cc61684TabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tab3cc61684TabLabelGridLayout771caa06",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tab3cc61684TabLabelGroup967a47ce",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "STRING8936187b-37b9-4e16-85f0-a7a313181e03",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tab3cc61684TabLabelGridLayout771caa06"
					},
					"bindTo": "WatConsortiumName",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.STRING8936187b37b94e1685f0a7a313181e03LabelCaption"
						}
					},
					"enabled": true
				},
				"parentName": "Tab3cc61684TabLabelGridLayout771caa06",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "STRINGb1ef1b45-b5b9-4091-88f3-727661452e20",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "Tab3cc61684TabLabelGridLayout771caa06"
					},
					"bindTo": "WatPrimaryContactFirstName",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.STRINGb1ef1b45b5b9409188f3727661452e20LabelCaption"
						}
					},
					"enabled": true
				},
				"parentName": "Tab3cc61684TabLabelGridLayout771caa06",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "STRING798cbb8c-b07e-4751-af33-f8be473c4420",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Tab3cc61684TabLabelGridLayout771caa06"
					},
					"bindTo": "WatOrganizationName",
					"enabled": true
				},
				"parentName": "Tab3cc61684TabLabelGridLayout771caa06",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "STRING980a243b-5136-4903-977b-bab53f578d98",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "Tab3cc61684TabLabelGridLayout771caa06"
					},
					"bindTo": "WatPrimaryContactLastName",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.STRING980a243b51364903977bbab53f578d98LabelCaption"
						}
					},
					"enabled": true
				},
				"parentName": "Tab3cc61684TabLabelGridLayout771caa06",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "STRING4d9cc1c1-e6d4-45a7-a7fc-b66a1b49ffd5",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Tab3cc61684TabLabelGridLayout771caa06"
					},
					"bindTo": "WatAddressWebsite",
					"enabled": true,
					"contentType": 0
				},
				"parentName": "Tab3cc61684TabLabelGridLayout771caa06",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "STRING3950ccb3-d2cc-4132-8a88-4c262c86e23e",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 2,
						"layoutName": "Tab3cc61684TabLabelGridLayout771caa06"
					},
					"bindTo": "WatPrimaryContactEmail",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.STRING3950ccb3d2cc41328a884c262c86e23eLabelCaption"
						}
					},
					"enabled": true
				},
				"parentName": "Tab3cc61684TabLabelGridLayout771caa06",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "DATETIME426d1993-7e46-42ec-a34f-589dd37eb6e7",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Tab3cc61684TabLabelGridLayout771caa06"
					},
					"bindTo": "WatPreferredDate",
					"enabled": true
				},
				"parentName": "Tab3cc61684TabLabelGridLayout771caa06",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "STRINGfd205c94-b9ec-49c7-84ac-05ccb2c39841",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 3,
						"layoutName": "Tab3cc61684TabLabelGridLayout771caa06"
					},
					"bindTo": "WatPreferredDuration",
					"enabled": true
				},
				"parentName": "Tab3cc61684TabLabelGridLayout771caa06",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "DATETIMEc9313e79-4670-440f-b424-0f77fac426ab",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "Tab3cc61684TabLabelGridLayout771caa06"
					},
					"bindTo": "WatAlternateDate",
					"enabled": true
				},
				"parentName": "Tab3cc61684TabLabelGridLayout771caa06",
				"propertyName": "items",
				"index": 8
			},
			{
				"operation": "insert",
				"name": "STRINGd1995a01-feb1-4cd0-92e9-90fe41ae1317",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 4,
						"layoutName": "Tab3cc61684TabLabelGridLayout771caa06"
					},
					"bindTo": "WatAlternateDuration",
					"enabled": true
				},
				"parentName": "Tab3cc61684TabLabelGridLayout771caa06",
				"propertyName": "items",
				"index": 9
			},
			{
				"operation": "insert",
				"name": "STRING83394d1c-332e-4849-8c72-76517b22eb69",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 5,
						"layoutName": "Tab3cc61684TabLabelGridLayout771caa06"
					},
					"bindTo": "WatPurpose",
					"enabled": true,
					"contentType": 0
				},
				"parentName": "Tab3cc61684TabLabelGridLayout771caa06",
				"propertyName": "items",
				"index": 10
			},
			{
				"operation": "insert",
				"name": "STRING7c7b4245-89b5-44a6-8c61-9ce4c24bebf7",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 6,
						"layoutName": "Tab3cc61684TabLabelGridLayout771caa06"
					},
					"bindTo": "WatPreviousRelations",
					"enabled": true,
					"contentType": 0
				},
				"parentName": "Tab3cc61684TabLabelGridLayout771caa06",
				"propertyName": "items",
				"index": 11
			},
			{
				"operation": "insert",
				"name": "STRINGb360667e-ac18-4a44-a475-d5a859f2bdcf",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 7,
						"layoutName": "Tab3cc61684TabLabelGridLayout771caa06"
					},
					"bindTo": "WatOtherComments",
					"enabled": true,
					"contentType": 0
				},
				"parentName": "Tab3cc61684TabLabelGridLayout771caa06",
				"propertyName": "items",
				"index": 12
			},
			{
				"operation": "insert",
				"name": "Tab3cc61684TabLabelGroup4c5b9094",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab3cc61684TabLabelGroup4c5b9094GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tab3cc61684TabLabel",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Tab3cc61684TabLabelGridLayoutf715e678",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tab3cc61684TabLabelGroup4c5b9094",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "LOOKUPbd74d138-512f-4336-b8e8-f4a2d583d3ee",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tab3cc61684TabLabelGridLayoutf715e678"
					},
					"bindTo": "WatConsortium",
					"enabled": true,
					"contentType": 3
				},
				"parentName": "Tab3cc61684TabLabelGridLayoutf715e678",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "LOOKUP74af7530-3b26-475d-aa32-3478d8a51845",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Tab3cc61684TabLabelGridLayoutf715e678"
					},
					"bindTo": "WatPrimaryContact",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "Tab3cc61684TabLabelGridLayoutf715e678",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Tabeafa1ed2TabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tabeafa1ed2TabLabelTabCaption"
					},
					"items": [],
					"order": 9
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 9
			},
			{
				"operation": "insert",
				"name": "Tabeafa1ed2TabLabelGroupf168b59d",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tabeafa1ed2TabLabelGroupf168b59dGroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tabeafa1ed2TabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tabeafa1ed2TabLabelGridLayout5c7c11e1",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tabeafa1ed2TabLabelGroupf168b59d",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CreatedOn36f5f9d3-7939-4043-b1f2-fe0121161f4b",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tabeafa1ed2TabLabelGridLayout5c7c11e1"
					},
					"bindTo": "CreatedOn",
					"enabled": false
				},
				"parentName": "Tabeafa1ed2TabLabelGridLayout5c7c11e1",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "ModifiedOn4c5bbacc-184b-4935-817e-81d0ab9ce822",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "Tabeafa1ed2TabLabelGridLayout5c7c11e1"
					},
					"bindTo": "ModifiedOn",
					"enabled": false
				},
				"parentName": "Tabeafa1ed2TabLabelGridLayout5c7c11e1",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "CreatedBy93d77500-1d30-418a-bd3e-0b3518b798fc",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Tabeafa1ed2TabLabelGridLayout5c7c11e1"
					},
					"bindTo": "CreatedBy",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Tabeafa1ed2TabLabelGridLayout5c7c11e1",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "ModifiedBy3e6d509c-9849-4f88-86f3-8b31fb361d76",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "Tabeafa1ed2TabLabelGridLayout5c7c11e1"
					},
					"bindTo": "ModifiedBy",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Tabeafa1ed2TabLabelGridLayout5c7c11e1",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "WatPreArrivalReachedf33c4aa9-c369-46b8-b34a-d39c7443f282",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Tabeafa1ed2TabLabelGridLayout5c7c11e1"
					},
					"bindTo": "WatPreArrivalReached"
				},
				"parentName": "Tabeafa1ed2TabLabelGridLayout5c7c11e1",
				"propertyName": "items",
				"index": 4
			}
		]/**SCHEMA_DIFF*/
	};
});
