define("ActivityPageV2", [], function() {
	return {
		entitySchemaName: "Activity",
		attributes: {},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{
			"CallDetail1f52fc88": {
				"schemaName": "CallDetail",
				"entitySchemaName": "Call",
				"filter": {
					"detailColumn": "Activity",
					"masterColumn": "Id"
				}
			}
		}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{
			"EntityConnections": {
				"296d460f-ce31-49bd-b5b1-3b2d15e36f22": {
					"uId": "296d460f-ce31-49bd-b5b1-3b2d15e36f22",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "DueDate"
							},
							"rightExpression": {
								"type": 0,
								"value": "2009-09-07T22:49:04.288Z",
								"dataValueType": 7
							}
						}
					]
				}
			},
			"Account": {
				"6aba0c35-03f2-4bcb-bdc1-404330382cb6": {
					"uId": "6aba0c35-03f2-4bcb-bdc1-404330382cb6",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatSystemGenerated"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"Contact": {
				"73af980f-56d5-4c32-96b6-184d6c528018": {
					"uId": "73af980f-56d5-4c32-96b6-184d6c528018",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatSystemGenerated"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"WatWatDelegation": {
				"6f5f4734-5a9c-4304-9b45-b8274d06e7f4": {
					"uId": "6f5f4734-5a9c-4304-9b45-b8274d06e7f4",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatSystemGenerated"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"WatWatHospitalityRequest": {
				"2c6d2f97-9777-4917-afdf-0319e4ffb2f2": {
					"uId": "2c6d2f97-9777-4917-afdf-0319e4ffb2f2",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatSystemGenerated"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"Result": {
				"BindParameterEnabledResultToStatus": {
					"uId": "3bd83cdc-26e0-40e2-a3b0-306a6585729a",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "Status",
								"attributePath": "Finish"
							},
							"rightExpression": {
								"type": 0,
								"value": true
							}
						}
					]
				},
				"BindParameterRequiredResultToStatus": {
					"uId": "495537b7-ed36-4c12-a599-39cfab553ddd",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "Status",
								"attributePath": "Finish"
							},
							"rightExpression": {
								"type": 0,
								"value": true
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "IsProcessMode"
							},
							"rightExpression": {
								"type": 0,
								"value": true
							}
						}
					]
				},
				"3ec219ae-bba4-4a0e-afe6-bc5455c9307c": {
					"uId": "3ec219ae-bba4-4a0e-afe6-bc5455c9307c",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "Status"
							},
							"rightExpression": {
								"type": 0,
								"value": "4bdbb88f-58e6-df11-971b-001d60e938c6",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "Status"
							},
							"rightExpression": {
								"type": 0,
								"value": "201cfba8-58e6-df11-971b-001d60e938c6",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"DetailedResult": {
				"BindParameterEnabledDetailedResultToStatus": {
					"uId": "15411984-9d9a-4f3c-8b45-36a0e1a00fbf",
					"enabled": true,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "Status"
							},
							"rightExpression": {
								"type": 0,
								"value": "4bdbb88f-58e6-df11-971b-001d60e938c6",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "Status"
							},
							"rightExpression": {
								"type": 0,
								"value": "201cfba8-58e6-df11-971b-001d60e938c6",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"ActivityCategory": {
				"259730de-da7a-42d4-969a-de789489cb0c": {
					"uId": "259730de-da7a-42d4-969a-de789489cb0c",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatSystemGenerated"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"Title": {
				"1a78f0c3-7637-492d-8d18-333648aa93a8": {
					"uId": "1a78f0c3-7637-492d-8d18-333648aa93a8",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatSystemGenerated"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"WatWatAgreement": {
				"517e35dd-227d-4ef0-9a14-50adef3c787b": {
					"uId": "517e35dd-227d-4ef0-9a14-50adef3c787b",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatSystemGenerated"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"WatWatPartnershipRequest": {
				"dcfb692b-9c8b-473b-89ef-f4e4bd8879ca": {
					"uId": "dcfb692b-9c8b-473b-89ef-f4e4bd8879ca",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatSystemGenerated"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"Owner": {
				"FiltrationContactByAccount": {
					"uId": "53d2fec4-9d43-4128-9ada-5e926f10a2be",
					"enabled": true,
					"ruleType": 1,
					"baseAttributePatch": "Account",
					"comparisonType": 3,
					"autoClean": true,
					"autocomplete": true,
					"type": 0,
					"value": "e308b781-3c5b-4ecb-89ef-5c1ed4da488e",
					"dataValueType": 10
				}
			},
			"WatTeam": {
				"cd64bfbb-42b3-44a0-8871-5421d873dbab": {
					"uId": "cd64bfbb-42b3-44a0-8871-5421d873dbab",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatSystemGenerated"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			}
		}/**SCHEMA_BUSINESS_RULES*/,
		methods: {},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"name": "Type450ad6a4-5865-4941-8ba9-fa38ed17a21d",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "Type",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "ActivityCategory101fd759-151d-4402-a824-beab0271addc",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "ActivityCategory"
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "merge",
				"name": "StartDate",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1
					}
				}
			},
			{
				"operation": "merge",
				"name": "Owner",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1
					},
					"enabled": true,
					"contentType": 5
				}
			},
			{
				"operation": "move",
				"name": "Owner",
				"parentName": "Header",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "merge",
				"name": "DueDate",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2
					}
				}
			},
			{
				"operation": "insert",
				"name": "LOOKUPf2d2c98d-316a-4ece-bc75-4eec7ad4f586",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 2,
						"layoutName": "Header"
					},
					"bindTo": "WatTeam",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "merge",
				"name": "Status",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 3
					},
					"enabled": false
				}
			},
			{
				"operation": "merge",
				"name": "Author",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 3
					}
				}
			},
			{
				"operation": "merge",
				"name": "ShowInScheduler",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 4
					}
				}
			},
			{
				"operation": "move",
				"name": "ShowInScheduler",
				"parentName": "Header",
				"propertyName": "items",
				"index": 8
			},
			{
				"operation": "merge",
				"name": "Priority",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 4
					}
				}
			},
			{
				"operation": "move",
				"name": "Priority",
				"parentName": "Header",
				"propertyName": "items",
				"index": 9
			},
			{
				"operation": "merge",
				"name": "CallDirection",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 5
					}
				}
			},
			{
				"operation": "merge",
				"name": "ESNTab",
				"values": {
					"order": 4
				}
			},
			{
				"operation": "merge",
				"name": "GeneralInfoTab",
				"values": {
					"order": 0
				}
			},
			{
				"operation": "merge",
				"name": "CustomActionSelectedResultControlGroup",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0
					}
				}
			},
			{
				"operation": "merge",
				"name": "Result",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0
					}
				}
			},
			{
				"operation": "insert",
				"name": "DATETIMEeaf728da-9ddf-4d85-9cc0-cf5848b72786",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "ResultControlBlock"
					},
					"bindTo": "WatCompletedOn",
					"enabled": false
				},
				"parentName": "ResultControlBlock",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "LOOKUPc9b692bb-246f-4655-a502-d48940a8b6d5",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 4,
						"layoutName": "ResultControlBlock"
					},
					"bindTo": "WatCompletedBy",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "ResultControlBlock",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "merge",
				"name": "RemindToOwner",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0
					}
				}
			},
			{
				"operation": "merge",
				"name": "RemindToOwnerDate",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0
					}
				}
			},
			{
				"operation": "merge",
				"name": "RemindToAuthor",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1
					}
				}
			},
			{
				"operation": "merge",
				"name": "RemindToAuthorDate",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1
					}
				}
			},
			{
				"operation": "insert",
				"name": "GeneralInfoTabGroup9bc7654d",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.GeneralInfoTabGroup9bc7654dGroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "GeneralInfoTab",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "GeneralInfoTabGridLayoutc4cd3b85",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "GeneralInfoTabGroup9bc7654d",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Contact83c11d09-d630-42ee-a972-29a62e8df15f",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "GeneralInfoTabGridLayoutc4cd3b85"
					},
					"bindTo": "Contact"
				},
				"parentName": "GeneralInfoTabGridLayoutc4cd3b85",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Accountb718b3c4-f23f-4e9d-bb25-a714f2529c89",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "GeneralInfoTabGridLayoutc4cd3b85"
					},
					"bindTo": "Account"
				},
				"parentName": "GeneralInfoTabGridLayoutc4cd3b85",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "WatWatDelegation080043dc-b6e0-4a8f-90f5-a6e0408e426c",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "GeneralInfoTabGridLayoutc4cd3b85"
					},
					"bindTo": "WatWatDelegation"
				},
				"parentName": "GeneralInfoTabGridLayoutc4cd3b85",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "WatWatHospitalityRequesta1c4173f-2cd0-4e9d-a38a-cbdef5c7977b",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "GeneralInfoTabGridLayoutc4cd3b85"
					},
					"bindTo": "WatWatHospitalityRequest"
				},
				"parentName": "GeneralInfoTabGridLayoutc4cd3b85",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "WatWatAgreement05c8ac99-c07b-49d9-a8a7-41f708944b37",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "GeneralInfoTabGridLayoutc4cd3b85"
					},
					"bindTo": "WatWatAgreement"
				},
				"parentName": "GeneralInfoTabGridLayoutc4cd3b85",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "WatWatPartnershipRequestbc696148-0aac-4609-9184-bc96a4962b79",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 2,
						"layoutName": "GeneralInfoTabGridLayoutc4cd3b85"
					},
					"bindTo": "WatWatPartnershipRequest"
				},
				"parentName": "GeneralInfoTabGridLayoutc4cd3b85",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "LOOKUP3133571e-7ef3-4052-8c6b-ac3a124b6739",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "GeneralInfoTabGridLayoutc4cd3b85"
					},
					"bindTo": "WatArrivalNotice",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "GeneralInfoTabGridLayoutc4cd3b85",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "LOOKUPa4ccba4c-b35c-446a-9917-006982ea31da",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 3,
						"layoutName": "GeneralInfoTabGridLayoutc4cd3b85"
					},
					"bindTo": "WatTraveller",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "GeneralInfoTabGridLayoutc4cd3b85",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "LOOKUP481cbcbc-0cea-4f18-9ba2-e99dff9cbc72",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "GeneralInfoTabGridLayoutc4cd3b85"
					},
					"bindTo": "WatTravelSituation",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "GeneralInfoTabGridLayoutc4cd3b85",
				"propertyName": "items",
				"index": 8
			},
			{
				"operation": "merge",
				"name": "ActivityFileNotesTab",
				"values": {
					"order": 3
				}
			},
			{
				"operation": "move",
				"name": "ActivityFileNotesTab",
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 3
			},
			{
				"operation": "merge",
				"name": "ActivityParticipantTab",
				"values": {
					"order": 1
				}
			},
			{
				"operation": "merge",
				"name": "EmailTab",
				"values": {
					"order": 2
				}
			},
			{
				"operation": "insert",
				"name": "CallDetail1f52fc88",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "EmailTab",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Tabf7322a1aTabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tabf7322a1aTabLabelTabCaption"
					},
					"items": [],
					"order": 5
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "Tabf7322a1aTabLabelGroup58eca3c6",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tabf7322a1aTabLabelGroup58eca3c6GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tabf7322a1aTabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tabf7322a1aTabLabelGridLayout7757d97c",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tabf7322a1aTabLabelGroup58eca3c6",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "BOOLEAN0a414f6c-639d-45de-95be-bbec07902731",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tabf7322a1aTabLabelGridLayout7757d97c"
					},
					"bindTo": "WatSystemGenerated",
					"enabled": false
				},
				"parentName": "Tabf7322a1aTabLabelGridLayout7757d97c",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "remove",
				"name": "ActivityCategory"
			},
			{
				"operation": "remove",
				"name": "ConnectionWithProjectControlGroup"
			},
			{
				"operation": "remove",
				"name": "FullProjectName"
			},
			{
				"operation": "remove",
				"name": "CallTab"
			},
			{
				"operation": "remove",
				"name": "Calls"
			},
			{
				"operation": "move",
				"name": "InformationOnStepButtonContainer",
				"parentName": "Header",
				"propertyName": "items",
				"index": 0
			}
		]/**SCHEMA_DIFF*/
	};
});
