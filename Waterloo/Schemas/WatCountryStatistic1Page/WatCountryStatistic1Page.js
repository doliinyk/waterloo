define("WatCountryStatistic1Page", [], function() {
	return {
		entitySchemaName: "WatCountryStatistic",
		attributes: {},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{
			"Files": {
				"schemaName": "FileDetailV2",
				"entitySchemaName": "WatCountryStatisticFile",
				"filter": {
					"masterColumn": "Id",
					"detailColumn": "WatCountryStatistic"
				}
			}
		}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{
			"WatCountry": {
				"7afdec4d-ee5e-4121-967f-9be2c7ac9dba": {
					"uId": "7afdec4d-ee5e-4121-967f-9be2c7ac9dba",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "WatName"
							}
						}
					]
				}
			},
			"WatYear": {
				"be6d450a-65c5-4a45-bb49-35890ec3a63c": {
					"uId": "be6d450a-65c5-4a45-bb49-35890ec3a63c",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "WatName"
							}
						}
					]
				}
			}
		}/**SCHEMA_BUSINESS_RULES*/,
		methods: {},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"name": "WatCountryf7499b64-c88b-46ad-8176-c5bf80ae27ce",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "WatCountry"
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatYearf2a10977-8cb4-4cea-8136-9e22d71016b6",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "WatYear"
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Tab8ffbe07aTabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab8ffbe07aTabLabelTabCaption"
					},
					"items": [],
					"order": 0
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tab8ffbe07aTabLabelGroup08662f67",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab8ffbe07aTabLabelGroup08662f67GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tab8ffbe07aTabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tab8ffbe07aTabLabelGridLayout5216600c",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tab8ffbe07aTabLabelGroup08662f67",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "STRING0d1b20c9-7639-4542-9f17-90b4f90f0dd3",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tab8ffbe07aTabLabelGridLayout5216600c"
					},
					"bindTo": "WatAlumniNumber",
					"enabled": true
				},
				"parentName": "Tab8ffbe07aTabLabelGridLayout5216600c",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "STRING1f9bac71-e6a8-4640-8a1f-38901fcc1c3b",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Tab8ffbe07aTabLabelGridLayout5216600c"
					},
					"bindTo": "WatAlumniRank",
					"enabled": true
				},
				"parentName": "Tab8ffbe07aTabLabelGridLayout5216600c",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "FLOAT8054c3c1-1030-41ab-872e-1ab82219b4c7",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Tab8ffbe07aTabLabelGridLayout5216600c"
					},
					"bindTo": "WatAlumniPercent",
					"enabled": true
				},
				"parentName": "Tab8ffbe07aTabLabelGridLayout5216600c",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "Tab8ffbe07aTabLabelGroup6782bd14",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab8ffbe07aTabLabelGroup6782bd14GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tab8ffbe07aTabLabel",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Tab8ffbe07aTabLabelGridLayoutdc648184",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tab8ffbe07aTabLabelGroup6782bd14",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "INTEGER762fe81d-d18e-4762-96cb-74810c451436",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tab8ffbe07aTabLabelGridLayoutdc648184"
					},
					"bindTo": "WatUndergrad",
					"enabled": true
				},
				"parentName": "Tab8ffbe07aTabLabelGridLayoutdc648184",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "INTEGER5e25f9a9-9f12-4897-ac76-42ccc850f0b3",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Tab8ffbe07aTabLabelGridLayoutdc648184"
					},
					"bindTo": "WatGrad",
					"enabled": true
				},
				"parentName": "Tab8ffbe07aTabLabelGridLayoutdc648184",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "INTEGER64cf3ef2-40dc-4b71-adc3-32182f91ca62",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Tab8ffbe07aTabLabelGridLayoutdc648184"
					},
					"bindTo": "WatNewUndergrad",
					"enabled": true
				},
				"parentName": "Tab8ffbe07aTabLabelGridLayoutdc648184",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "INTEGERfb811b9e-93f8-4232-95dd-1dd5294d7469",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Tab8ffbe07aTabLabelGridLayoutdc648184"
					},
					"bindTo": "WatNewGrad",
					"enabled": true
				},
				"parentName": "Tab8ffbe07aTabLabelGridLayoutdc648184",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "INTEGERc9f53fbd-1259-4e33-ae5d-e112f66c6787",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "Tab8ffbe07aTabLabelGridLayoutdc648184"
					},
					"bindTo": "WatUndergradRank",
					"enabled": true
				},
				"parentName": "Tab8ffbe07aTabLabelGridLayoutdc648184",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "INTEGER18705153-2954-4e88-93f2-51dce0698813",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 5,
						"layoutName": "Tab8ffbe07aTabLabelGridLayoutdc648184"
					},
					"bindTo": "WatGradRank",
					"enabled": true
				},
				"parentName": "Tab8ffbe07aTabLabelGridLayoutdc648184",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "FLOAT24b2be1d-9860-426d-b81b-01862b8dc488",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 6,
						"layoutName": "Tab8ffbe07aTabLabelGridLayoutdc648184"
					},
					"bindTo": "WatUndergradPercent",
					"enabled": true
				},
				"parentName": "Tab8ffbe07aTabLabelGridLayoutdc648184",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "FLOAT3375faa9-068b-485b-808d-ea968d63cbee",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 7,
						"layoutName": "Tab8ffbe07aTabLabelGridLayoutdc648184"
					},
					"bindTo": "WatGradPercent",
					"enabled": true
				},
				"parentName": "Tab8ffbe07aTabLabelGridLayoutdc648184",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "Tab8ffbe07aTabLabelGroupf4acf715",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab8ffbe07aTabLabelGroupf4acf715GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tab8ffbe07aTabLabel",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "Tab8ffbe07aTabLabelGridLayout59136e9b",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tab8ffbe07aTabLabelGroupf4acf715",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "INTEGER35c825fc-dd8e-4483-8bc6-0a35c1c9af17",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tab8ffbe07aTabLabelGridLayout59136e9b"
					},
					"bindTo": "WatCoopNumber",
					"enabled": true
				},
				"parentName": "Tab8ffbe07aTabLabelGridLayout59136e9b",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "INTEGERb8a001ef-823c-48c9-bb43-d2c99dcd1c46",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Tab8ffbe07aTabLabelGridLayout59136e9b"
					},
					"bindTo": "WatCoopRank",
					"enabled": true
				},
				"parentName": "Tab8ffbe07aTabLabelGridLayout59136e9b",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "FLOAT5deb9af7-2a63-43a6-94fd-6d494b450f4d",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Tab8ffbe07aTabLabelGridLayout59136e9b"
					},
					"bindTo": "WatCoopPercent",
					"enabled": true
				},
				"parentName": "Tab8ffbe07aTabLabelGridLayout59136e9b",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "Tab8ffbe07aTabLabelGroup707bdbc9",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab8ffbe07aTabLabelGroup707bdbc9GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tab8ffbe07aTabLabel",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "Tab8ffbe07aTabLabelGridLayout4b8e5e17",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tab8ffbe07aTabLabelGroup707bdbc9",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "INTEGER44de53a6-dadb-458b-b2e9-c32754bc18b0",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tab8ffbe07aTabLabelGridLayout4b8e5e17"
					},
					"bindTo": "WatFacultyNumber",
					"enabled": true
				},
				"parentName": "Tab8ffbe07aTabLabelGridLayout4b8e5e17",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "INTEGER6ada1170-a7c2-4d9e-b0d5-a736759e517c",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Tab8ffbe07aTabLabelGridLayout4b8e5e17"
					},
					"bindTo": "WatFacultyRank",
					"enabled": true
				},
				"parentName": "Tab8ffbe07aTabLabelGridLayout4b8e5e17",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "NotesAndFilesTab",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.NotesAndFilesTabCaption"
					},
					"items": [],
					"order": 1
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Files",
				"values": {
					"itemType": 2
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "NotesControlGroup",
				"values": {
					"itemType": 15,
					"caption": {
						"bindTo": "Resources.Strings.NotesGroupCaption"
					},
					"items": []
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Notes",
				"values": {
					"bindTo": "WatNotes",
					"dataValueType": 1,
					"contentType": 4,
					"layout": {
						"column": 0,
						"row": 0,
						"colSpan": 24
					},
					"labelConfig": {
						"visible": false
					},
					"controlConfig": {
						"imageLoaded": {
							"bindTo": "insertImagesToNotes"
						},
						"images": {
							"bindTo": "NotesImagesCollection"
						}
					}
				},
				"parentName": "NotesControlGroup",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "merge",
				"name": "ESNTab",
				"values": {
					"order": 2
				}
			}
		]/**SCHEMA_DIFF*/
	};
});
