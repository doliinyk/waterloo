define("WatDelegationMembers1MiniPage", [], function() {
	return {
		entitySchemaName: "WatDelegationMembers",
		attributes: {},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{
			"WatDelegationContact": {
				"97f6e8b5-88c6-4bc2-8104-18fdd5bd1d11": {
					"uId": "97f6e8b5-88c6-4bc2-8104-18fdd5bd1d11",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "WatLastName"
							}
						}
					]
				},
				"7cbcff5b-1e74-4bb9-93c2-65f3b4cc7fd5": {
					"uId": "7cbcff5b-1e74-4bb9-93c2-65f3b4cc7fd5",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "WatFirstName"
							}
						}
					]
				}
			},
			"WatFirstName": {
				"de31e054-c6aa-4d03-bfeb-58f5acdebbd6": {
					"uId": "de31e054-c6aa-4d03-bfeb-58f5acdebbd6",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "WatDelegationContact"
							}
						}
					]
				},
				"0946eeca-850c-42e1-8bde-401d2c188e05": {
					"uId": "0946eeca-850c-42e1-8bde-401d2c188e05",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "WatDelegationContact"
							}
						}
					]
				}
			},
			"WatLastName": {
				"66930518-ff94-4313-a86f-f2f018793b3d": {
					"uId": "66930518-ff94-4313-a86f-f2f018793b3d",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "WatDelegationContact"
							}
						}
					]
				},
				"f2a3f8a4-36e2-4b1e-aa16-2916af9042fb": {
					"uId": "f2a3f8a4-36e2-4b1e-aa16-2916af9042fb",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "WatDelegationContact"
							}
						}
					]
				}
			},
			"WatAccount": {
				"de5fa24a-d6b1-4067-8b96-523ba94acbe0": {
					"uId": "de5fa24a-d6b1-4067-8b96-523ba94acbe0",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "WatDelegationContact"
							}
						},
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "WatLastName"
							}
						}
					]
				}
			},
			"WatEmail": {
				"72fcfe89-37d7-4290-84d8-e53dd309b0f0": {
					"uId": "72fcfe89-37d7-4290-84d8-e53dd309b0f0",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "WatDelegationContact"
							}
						}
					]
				},
				"be36e343-cf0b-44d6-a16f-a98bb624830d": {
					"uId": "be36e343-cf0b-44d6-a16f-a98bb624830d",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "WatDelegationContact"
							}
						}
					]
				}
			},
			"WatTitle": {
				"5ec877ab-060a-4094-a37f-04358f2a1494": {
					"uId": "5ec877ab-060a-4094-a37f-04358f2a1494",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "WatDelegationContact"
							}
						}
					]
				}
			}
		}/**SCHEMA_BUSINESS_RULES*/,
		methods: {},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "merge",
				"name": "HeaderContainer",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0
					}
				}
			},
			{
				"operation": "insert",
				"name": "HeaderColumnContainer",
				"values": {
					"itemType": 6,
					"caption": {
						"bindTo": "getPrimaryDisplayColumnValue"
					},
					"labelClass": [
						"label-in-header-container"
					],
					"visible": {
						"bindTo": "isNotAddMode"
					}
				},
				"parentName": "HeaderContainer",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "WatContactCategorya9d417d6-7698-4ce7-968f-a4b1e6004be3",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "MiniPage"
					},
					"isMiniPageModelItem": true,
					"visible": {
						"bindTo": "isAddMode"
					},
					"bindTo": "WatContactCategory"
				},
				"parentName": "MiniPage",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "WatDelegationContact4491ce90-ecd2-4a19-9dc6-72c77e0fa9e0",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "MiniPage"
					},
					"isMiniPageModelItem": true,
					"visible": {
						"bindTo": "isAddMode"
					},
					"bindTo": "WatDelegationContact",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "MiniPage",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "WatFirstName57cf9d34-9c4e-4675-8244-43968e264382",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "MiniPage"
					},
					"isMiniPageModelItem": true,
					"visible": {
						"bindTo": "isAddMode"
					},
					"bindTo": "WatFirstName"
				},
				"parentName": "MiniPage",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "WatLastNamee24e1183-dd34-42c7-8de2-6dc84e2fdd8e",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "MiniPage"
					},
					"isMiniPageModelItem": true,
					"visible": {
						"bindTo": "isAddMode"
					},
					"bindTo": "WatLastName"
				},
				"parentName": "MiniPage",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "WatTitle6e9d02ff-4fd0-4c62-b71f-29713876eec9",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 6,
						"layoutName": "MiniPage"
					},
					"isMiniPageModelItem": true,
					"visible": {
						"bindTo": "isAddMode"
					},
					"bindTo": "WatTitle",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatTitle6e9d02ff4fd04c62b71f29713876eec9LabelCaption"
						}
					},
					"enabled": true
				},
				"parentName": "MiniPage",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "WatEmail965fea73-40f7-472d-8814-5ad1f576026f",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 5,
						"layoutName": "MiniPage"
					},
					"isMiniPageModelItem": true,
					"visible": {
						"bindTo": "isAddMode"
					},
					"bindTo": "WatEmail"
				},
				"parentName": "MiniPage",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "WatAccount941601c3-973e-4705-87d4-134711223a7b",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 7,
						"layoutName": "MiniPage"
					},
					"isMiniPageModelItem": true,
					"visible": {
						"bindTo": "isAddMode"
					},
					"bindTo": "WatAccount",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "MiniPage",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "WatContactCategory2482fc48-636a-4230-aa7f-535853a8e9e5",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "MiniPage"
					},
					"isMiniPageModelItem": true,
					"visible": {
						"bindTo": "isViewMode"
					},
					"bindTo": "WatContactCategory",
					"enabled": true,
					"contentType": 3
				},
				"parentName": "MiniPage",
				"propertyName": "items",
				"index": 8
			},
			{
				"operation": "insert",
				"name": "WatDelegationContactfed2aec1-5ebb-4270-92f6-01539f086d1e",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "MiniPage"
					},
					"isMiniPageModelItem": true,
					"visible": {
						"bindTo": "isViewMode"
					},
					"bindTo": "WatDelegationContact"
				},
				"parentName": "MiniPage",
				"propertyName": "items",
				"index": 9
			},
			{
				"operation": "insert",
				"name": "WatFirstNamee40a7ea5-82d5-403a-a618-4d18fbe8b48e",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "MiniPage"
					},
					"isMiniPageModelItem": true,
					"visible": {
						"bindTo": "isViewMode"
					},
					"bindTo": "WatFirstName"
				},
				"parentName": "MiniPage",
				"propertyName": "items",
				"index": 10
			},
			{
				"operation": "insert",
				"name": "WatLastNamef3d72c91-ba6b-4334-ac7a-998b22d1d51a",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "MiniPage"
					},
					"isMiniPageModelItem": true,
					"visible": {
						"bindTo": "isViewMode"
					},
					"bindTo": "WatLastName"
				},
				"parentName": "MiniPage",
				"propertyName": "items",
				"index": 11
			},
			{
				"operation": "insert",
				"name": "WatTitle96ab9230-2aa5-4452-9e1f-fe5e70fb4055",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 5,
						"layoutName": "MiniPage"
					},
					"isMiniPageModelItem": true,
					"visible": {
						"bindTo": "isViewMode"
					},
					"bindTo": "WatTitle"
				},
				"parentName": "MiniPage",
				"propertyName": "items",
				"index": 12
			},
			{
				"operation": "insert",
				"name": "WatEmail782d6be6-cc4f-4d99-b111-da0951410371",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 6,
						"layoutName": "MiniPage"
					},
					"isMiniPageModelItem": true,
					"visible": {
						"bindTo": "isViewMode"
					},
					"bindTo": "WatEmail"
				},
				"parentName": "MiniPage",
				"propertyName": "items",
				"index": 13
			}
		]/**SCHEMA_DIFF*/
	};
});
