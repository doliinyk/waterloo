define("WatSchemaCustomTaskDetail", [], function() {
	return {
		entitySchemaName: "Activity",
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		diff: /**SCHEMA_DIFF*/[]/**SCHEMA_DIFF*/,
		methods: {
			getFilters: function() {
				var filterGroup = this.callParent(arguments);
				var additionalFilterGroup = this.Ext.create("Terrasoft.FilterGroup");
				
				additionalFilterGroup.add("DocumentAFilter", this.Terrasoft.createColumnFilterWithParameter(
					this.Terrasoft.ComparisonType.EQUAL, "Type", "fbe0acdc-cfc0-df11-b00f-001d60e938c6"));
				additionalFilterGroup.add("DocumentBFilter", this.Terrasoft.createColumnFilterWithParameter(
					this.Terrasoft.ComparisonType.NOT_EQUAL, "ActivityCategory", "03df85bf-6b19-4dea-8463-d5d49b80bb28"));
				additionalFilterGroup.add("DocumentCFilter", this.Terrasoft.createColumnFilterWithParameter(
					this.Terrasoft.ComparisonType.NOT_EQUAL, "Type", "e1831dec-cfc0-df11-b00f-001d60e938c6"));
					
				filterGroup.add("ExtraFilter", additionalFilterGroup);	
				return  filterGroup;
			}
		}
	};
});
