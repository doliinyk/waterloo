define("WatAgreementStakeholders1MiniPage", [], function() {
	return {
		entitySchemaName: "WatAgreementStakeholders",
		attributes: {},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{
			"WatAgreement": {
				"05a1d399-f36f-49d9-bf9c-e3172a1768fa": {
					"uId": "05a1d399-f36f-49d9-bf9c-e3172a1768fa",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreement"
							}
						}
					]
				}
			},
			"WatFaculty": {
				"d947e53b-b8b0-4e5f-8809-bb5affe9ea9e": {
					"uId": "d947e53b-b8b0-4e5f-8809-bb5affe9ea9e",
					"enabled": true,
					"removed": true,
					"ruleType": 1,
					"baseAttributePatch": "WatFacultyDepartment",
					"comparisonType": 3,
					"autoClean": true,
					"autocomplete": true,
					"type": 1,
					"attribute": "WatFacultyDepartment"
				}
			},
			"WatContact": {
				"5fce48f7-55f7-4e4e-a94d-0047241c90cf": {
					"uId": "5fce48f7-55f7-4e4e-a94d-0047241c90cf",
					"enabled": true,
					"removed": false,
					"ruleType": 1,
					"baseAttributePatch": "Account.Type",
					"comparisonType": 3,
					"autoClean": false,
					"autocomplete": false,
					"type": 0,
					"value": "57412fad-53e6-df11-971b-001d60e938c6",
					"dataValueType": 10
				}
			},
			"WatFacultyDepartment": {
				"1fddbb40-a4b3-4566-9fa5-d1b36e5d9210": {
					"uId": "1fddbb40-a4b3-4566-9fa5-d1b36e5d9210",
					"enabled": true,
					"removed": false,
					"ruleType": 1,
					"baseAttributePatch": "WatFaculty",
					"comparisonType": 3,
					"autoClean": true,
					"autocomplete": true,
					"type": 1,
					"attribute": "WatFaculty"
				}
			}
		}/**SCHEMA_BUSINESS_RULES*/,
		methods: {},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "merge",
				"name": "HeaderContainer",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0
					}
				}
			},
			{
				"operation": "insert",
				"name": "HeaderColumnContainer",
				"values": {
					"itemType": 6,
					"caption": {
						"bindTo": "getPrimaryDisplayColumnValue"
					},
					"labelClass": [
						"label-in-header-container"
					],
					"visible": {
						"bindTo": "isNotAddMode"
					}
				},
				"parentName": "HeaderContainer",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "WatContactbdfd7006-e50d-46a6-abed-bd9203463d18",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "MiniPage"
					},
					"isMiniPageModelItem": true,
					"visible": {
						"bindTo": "isAddMode"
					},
					"bindTo": "WatContact",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "MiniPage",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "WatFaculty23f38de1-443a-4db8-a919-2a6ff92ac324",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "MiniPage"
					},
					"isMiniPageModelItem": true,
					"visible": {
						"bindTo": "isAddMode"
					},
					"bindTo": "WatFaculty",
					"enabled": true,
					"contentType": 3
				},
				"parentName": "MiniPage",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "WatFacultyDepartment476e18c8-2b5d-4e3f-9ed7-4a656940b1f7",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "MiniPage"
					},
					"isMiniPageModelItem": true,
					"visible": {
						"bindTo": "isAddMode"
					},
					"bindTo": "WatFacultyDepartment",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatFacultyDepartment476e18c82b5d4e3f9ed74a656940b1f7LabelCaption"
						}
					},
					"enabled": true,
					"contentType": 3
				},
				"parentName": "MiniPage",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "WatFeasible7626c98c-546a-4e83-9952-cad97af49171",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "MiniPage"
					},
					"isMiniPageModelItem": true,
					"visible": {
						"bindTo": "isAddMode"
					},
					"bindTo": "WatFeasible",
					"enabled": true,
					"contentType": 3
				},
				"parentName": "MiniPage",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "WatApproved46805d70-3c2b-4934-98d9-8d5e73e8573c",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 5,
						"layoutName": "MiniPage"
					},
					"isMiniPageModelItem": true,
					"visible": {
						"bindTo": "isAddMode"
					},
					"bindTo": "WatApproved",
					"enabled": true,
					"contentType": 3
				},
				"parentName": "MiniPage",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "WatAgreementNotes74f1ba21-f938-4a56-bda4-11207575fa68",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 6,
						"layoutName": "MiniPage"
					},
					"isMiniPageModelItem": true,
					"visible": {
						"bindTo": "isAddMode"
					},
					"bindTo": "WatAgreementNotes",
					"enabled": true,
					"contentType": 0
				},
				"parentName": "MiniPage",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "WatAgreement7ed9c816-26ba-4601-ae99-ca53bfa339f5",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 7,
						"layoutName": "MiniPage"
					},
					"isMiniPageModelItem": true,
					"visible": {
						"bindTo": "isAddMode"
					},
					"bindTo": "WatAgreement"
				},
				"parentName": "MiniPage",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "WatContactf1b7e2a4-af3b-481f-a67b-afca0c572177",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "MiniPage"
					},
					"isMiniPageModelItem": true,
					"visible": {
						"bindTo": "isEditMode"
					},
					"bindTo": "WatContact",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "MiniPage",
				"propertyName": "items",
				"index": 8
			},
			{
				"operation": "insert",
				"name": "WatFaculty53337db5-44a4-41ad-9b92-6b43e2a9c190",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "MiniPage"
					},
					"isMiniPageModelItem": true,
					"visible": {
						"bindTo": "isEditMode"
					},
					"bindTo": "WatFaculty",
					"enabled": true,
					"contentType": 3
				},
				"parentName": "MiniPage",
				"propertyName": "items",
				"index": 9
			},
			{
				"operation": "insert",
				"name": "WatFacultyDepartmentbba1aa45-fe19-4282-b158-5f3ec6680ae3",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "MiniPage"
					},
					"isMiniPageModelItem": true,
					"visible": {
						"bindTo": "isEditMode"
					},
					"bindTo": "WatFacultyDepartment",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatFacultyDepartmentbba1aa45fe194282b1585f3ec6680ae3LabelCaption"
						}
					},
					"enabled": true,
					"contentType": 3
				},
				"parentName": "MiniPage",
				"propertyName": "items",
				"index": 10
			},
			{
				"operation": "insert",
				"name": "WatFeasibled80173e8-75db-4134-b853-13d13f8e8868",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "MiniPage"
					},
					"isMiniPageModelItem": true,
					"visible": {
						"bindTo": "isEditMode"
					},
					"bindTo": "WatFeasible",
					"enabled": true,
					"contentType": 3
				},
				"parentName": "MiniPage",
				"propertyName": "items",
				"index": 11
			},
			{
				"operation": "insert",
				"name": "WatApproved99a32984-5264-4804-8835-3d5f66dc8495",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 5,
						"layoutName": "MiniPage"
					},
					"isMiniPageModelItem": true,
					"visible": {
						"bindTo": "isEditMode"
					},
					"bindTo": "WatApproved",
					"enabled": true,
					"contentType": 3
				},
				"parentName": "MiniPage",
				"propertyName": "items",
				"index": 12
			},
			{
				"operation": "insert",
				"name": "WatAgreementNotesdeaa0896-2669-4fb6-9dd2-6bc0fe0a5a84",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 6,
						"layoutName": "MiniPage"
					},
					"isMiniPageModelItem": true,
					"visible": {
						"bindTo": "isEditMode"
					},
					"bindTo": "WatAgreementNotes",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatAgreementNotesdeaa089626694fb69dd26bc0fe0a5a84LabelCaption"
						}
					},
					"enabled": true
				},
				"parentName": "MiniPage",
				"propertyName": "items",
				"index": 13
			},
			{
				"operation": "insert",
				"name": "WatAgreement4cd4f7e1-5f23-4da9-8ea6-8b8c37a3b3d6",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 7,
						"layoutName": "MiniPage"
					},
					"isMiniPageModelItem": true,
					"visible": {
						"bindTo": "isEditMode"
					},
					"bindTo": "WatAgreement"
				},
				"parentName": "MiniPage",
				"propertyName": "items",
				"index": 14
			},
			{
				"operation": "insert",
				"name": "WatContactb4fff130-4a81-4a6c-904b-195235d4f1c0",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "MiniPage"
					},
					"isMiniPageModelItem": true,
					"visible": {
						"bindTo": "isViewMode"
					},
					"bindTo": "WatContact",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "MiniPage",
				"propertyName": "items",
				"index": 15
			},
			{
				"operation": "insert",
				"name": "WatFaculty7fc482c3-f266-49c7-9af5-d163cf1ba0ab",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "MiniPage"
					},
					"isMiniPageModelItem": true,
					"visible": {
						"bindTo": "isViewMode"
					},
					"bindTo": "WatFaculty",
					"enabled": true,
					"contentType": 3
				},
				"parentName": "MiniPage",
				"propertyName": "items",
				"index": 16
			},
			{
				"operation": "insert",
				"name": "WatFacultyDepartment8a8358b1-1a04-40b1-9a76-a29c70e98d8f",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "MiniPage"
					},
					"isMiniPageModelItem": true,
					"visible": {
						"bindTo": "isViewMode"
					},
					"bindTo": "WatFacultyDepartment",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.WatFacultyDepartment8a8358b11a0440b19a76a29c70e98d8fLabelCaption"
						}
					},
					"enabled": true,
					"contentType": 3
				},
				"parentName": "MiniPage",
				"propertyName": "items",
				"index": 17
			},
			{
				"operation": "insert",
				"name": "WatFeasible13d668bb-06ac-41c5-86ab-6a65243801d2",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "MiniPage"
					},
					"isMiniPageModelItem": true,
					"visible": {
						"bindTo": "isViewMode"
					},
					"bindTo": "WatFeasible",
					"enabled": true,
					"contentType": 3
				},
				"parentName": "MiniPage",
				"propertyName": "items",
				"index": 18
			},
			{
				"operation": "insert",
				"name": "WatApproved81db0ba0-f005-48b9-98ef-20c27157cf20",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 5,
						"layoutName": "MiniPage"
					},
					"isMiniPageModelItem": true,
					"visible": {
						"bindTo": "isViewMode"
					},
					"bindTo": "WatApproved",
					"enabled": true,
					"contentType": 3
				},
				"parentName": "MiniPage",
				"propertyName": "items",
				"index": 19
			},
			{
				"operation": "insert",
				"name": "WatAgreementNotesb418fa64-8b33-404d-8c3a-2b18ad7fc608",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 6,
						"layoutName": "MiniPage"
					},
					"isMiniPageModelItem": true,
					"visible": {
						"bindTo": "isViewMode"
					},
					"bindTo": "WatAgreementNotes",
					"enabled": true,
					"contentType": 0
				},
				"parentName": "MiniPage",
				"propertyName": "items",
				"index": 20
			}
		]/**SCHEMA_DIFF*/
	};
});
