define("WatHospitalityChange1Page", [], function() {
	return {
		entitySchemaName: "WatHospitalityChange",
		attributes: {},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{
			"Files": {
				"schemaName": "FileDetailV2",
				"entitySchemaName": "WatHospitalityChangeFile",
				"filter": {
					"masterColumn": "Id",
					"detailColumn": "WatHospitalityChange"
				}
			}
		}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{
			"WatConfirmNewDate": {
				"dc63abaa-59d8-40f2-ba58-17b91ee601db": {
					"uId": "dc63abaa-59d8-40f2-ba58-17b91ee601db",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "WatNewDate"
							}
						},
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "CreatedOn"
							}
						}
					]
				},
				"531d8a2c-a33c-4241-a7fc-90a3d592d1ea": {
					"uId": "531d8a2c-a33c-4241-a7fc-90a3d592d1ea",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "WatNewDate"
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatConfirmChange"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				},
				"debfdde2-6715-4625-84a4-1680395e2ffc": {
					"uId": "debfdde2-6715-4625-84a4-1680395e2ffc",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatChangeRequestStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "724f0c3c-90f7-4d05-bdce-1a33761da7fa",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatChangeRequestStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "d64e17a7-c754-41e9-a363-69746cf7ed01",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatChangeRequestStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "9b157a42-b3f9-4430-94f4-1337a96b5451",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"WatConfirmNewTime": {
				"e3d6ceef-2fa8-4da9-81b6-7ca0e8e61904": {
					"uId": "e3d6ceef-2fa8-4da9-81b6-7ca0e8e61904",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "WatNewTime"
							}
						},
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "CreatedOn"
							}
						}
					]
				},
				"98f6d083-0e5f-4ae0-ae55-cbac12c21db6": {
					"uId": "98f6d083-0e5f-4ae0-ae55-cbac12c21db6",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "WatNewTime"
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatConfirmChange"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				},
				"5e5c354d-7027-4cf2-bcce-ad2f55eeb55e": {
					"uId": "5e5c354d-7027-4cf2-bcce-ad2f55eeb55e",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatChangeRequestStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "724f0c3c-90f7-4d05-bdce-1a33761da7fa",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatChangeRequestStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "d64e17a7-c754-41e9-a363-69746cf7ed01",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatChangeRequestStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "9b157a42-b3f9-4430-94f4-1337a96b5451",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"WatConfirmNewLocation": {
				"12293c5c-3f47-4780-94c1-559169dbf528": {
					"uId": "12293c5c-3f47-4780-94c1-559169dbf528",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "WatNewLocation"
							}
						},
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "CreatedOn"
							}
						}
					]
				},
				"4506fd62-26f5-43ed-9241-d1e725f5ecee": {
					"uId": "4506fd62-26f5-43ed-9241-d1e725f5ecee",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "WatNewLocation"
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatConfirmChange"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				},
				"10e62e79-3e03-4c58-bc8a-cc26e05bb555": {
					"uId": "10e62e79-3e03-4c58-bc8a-cc26e05bb555",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatChangeRequestStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "724f0c3c-90f7-4d05-bdce-1a33761da7fa",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatChangeRequestStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "d64e17a7-c754-41e9-a363-69746cf7ed01",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatChangeRequestStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "9b157a42-b3f9-4430-94f4-1337a96b5451",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"WatConfirmNewPax": {
				"34df9238-6588-4844-8fa9-f5e6dd4f02c2": {
					"uId": "34df9238-6588-4844-8fa9-f5e6dd4f02c2",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "WatNewPaxCount"
							}
						},
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "CreatedOn"
							}
						}
					]
				},
				"1e51dbaf-23d9-4d96-bf9b-0d32c4fb3e25": {
					"uId": "1e51dbaf-23d9-4d96-bf9b-0d32c4fb3e25",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "WatNewPaxCount"
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatConfirmChange"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				},
				"3f1a97e4-7cc3-4ba6-97bb-9b8356f2546d": {
					"uId": "3f1a97e4-7cc3-4ba6-97bb-9b8356f2546d",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatChangeRequestStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "724f0c3c-90f7-4d05-bdce-1a33761da7fa",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatChangeRequestStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "d64e17a7-c754-41e9-a363-69746cf7ed01",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatChangeRequestStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "9b157a42-b3f9-4430-94f4-1337a96b5451",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"WatCancelChangeRequest": {
				"62f44275-2976-4a54-a766-73ee2be00c20": {
					"uId": "62f44275-2976-4a54-a766-73ee2be00c20",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "CreatedOn"
							}
						}
					]
				},
				"871ce5c8-5ddf-4ef0-a653-9060f3278e4f": {
					"uId": "871ce5c8-5ddf-4ef0-a653-9060f3278e4f",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatConfirmChange"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatChangeRequestStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "ee30b465-2b83-4d12-8f68-2383ff26669e",
								"dataValueType": 10
							}
						}
					]
				},
				"5e57711a-1486-4b3b-853c-dc010f17c7be": {
					"uId": "5e57711a-1486-4b3b-853c-dc010f17c7be",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 1,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "WatConfirmChange"
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatChangeRequestStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "ee30b465-2b83-4d12-8f68-2383ff26669e",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"WatSubmitChangeRequest": {
				"1692a592-966d-4c5e-8149-badc6579b989": {
					"uId": "1692a592-966d-4c5e-8149-badc6579b989",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatSubmitChangeRequest"
							},
							"rightExpression": {
								"type": 0,
								"value": false,
								"dataValueType": 12
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatChangeRequestStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "ee30b465-2b83-4d12-8f68-2383ff26669e",
								"dataValueType": 10
							}
						}
					]
				},
				"783e690d-702e-4079-9cf3-0fc5f17946f0": {
					"uId": "783e690d-702e-4079-9cf3-0fc5f17946f0",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "CreatedOn"
							}
						}
					]
				}
			},
			"WatConfirmChange": {
				"1aeb474f-50fd-4446-bbbb-b08ae67e8af5": {
					"uId": "1aeb474f-50fd-4446-bbbb-b08ae67e8af5",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 1,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "WatConfirmNewDate"
							}
						},
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "WatConfirmNewLocation"
							}
						},
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "WatConfirmNewPax"
							}
						},
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "WatConfirmNewTime"
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatCancelChangeRequest"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatChangeRequestStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "ee30b465-2b83-4d12-8f68-2383ff26669e",
								"dataValueType": 10
							}
						}
					]
				},
				"778412c6-2bd1-422b-b985-db18cfeb9786": {
					"uId": "778412c6-2bd1-422b-b985-db18cfeb9786",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "WatCancelChangeRequest"
							}
						}
					]
				},
				"55be1499-8210-4d9f-86f2-0eeb17f17d3d": {
					"uId": "55be1499-8210-4d9f-86f2-0eeb17f17d3d",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 2,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "WatConfirmNewDate"
							}
						},
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "WatConfirmNewLocation"
							}
						},
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "WatConfirmNewPax"
							}
						},
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "WatConfirmNewTime"
							}
						}
					]
				}
			},
			"WatNewEndTime": {
				"931be5f8-a70d-43c6-8315-c045af3ab6ee": {
					"uId": "931be5f8-a70d-43c6-8315-c045af3ab6ee",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "WatNewTime"
							}
						}
					]
				}
			},
			"WatNewTime": {
				"7dc51f01-69a3-40ca-8a60-68ae958e74f9": {
					"uId": "7dc51f01-69a3-40ca-8a60-68ae958e74f9",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "WatNewEndTime"
							}
						}
					]
				}
			}
		}/**SCHEMA_BUSINESS_RULES*/,
		methods: {},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"name": "LOOKUPfb99fa59-49d5-47b4-a6d9-2ea1f9d40c25",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "WatChangeRequestStatus",
					"enabled": false,
					"contentType": 3
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "LOOKUP3711f421-25a9-43e0-b9c7-41b9666d6eb8",
				"values": {
					"layout": {
						"colSpan": 5,
						"rowSpan": 1,
						"column": 6,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "WatConfirmNewDate",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.LOOKUP3711f42125a943e0b9c741b9666d6eb8LabelCaption"
						}
					},
					"enabled": true,
					"contentType": 3
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "DATETIMEfe3ea89f-1b7f-48d5-9e4f-8a043bc787bb",
				"values": {
					"layout": {
						"colSpan": 6,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "WatNewDate",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "WatHospitalityRequest044d53eb-e4f5-41da-a8f5-b56344baeb5d",
				"values": {
					"layout": {
						"colSpan": 7,
						"rowSpan": 1,
						"column": 17,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "WatHospitalityRequest",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "DATETIMEf81350a9-b7bc-42b6-99c6-314c9d8a1fce",
				"values": {
					"layout": {
						"colSpan": 4,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "WatNewTime",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "STRINGa6e9fb2b-8ad9-412e-bf9b-7e294d65b729",
				"values": {
					"layout": {
						"colSpan": 4,
						"rowSpan": 1,
						"column": 4,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "WatNewEndTime",
					"enabled": true,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.STRINGa6e9fb2b8ad9412ebf9b7e294d65b729LabelCaption"
						}
					}
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "LOOKUP43471298-8c6c-4490-a8d7-6c076370e441",
				"values": {
					"layout": {
						"colSpan": 5,
						"rowSpan": 1,
						"column": 8,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "WatConfirmNewTime",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.LOOKUP434712988c6c4490a8d76c076370e441LabelCaption"
						}
					},
					"enabled": true,
					"contentType": 3
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "LOOKUP0ef78f16-11ef-469a-a91f-1d7d988bad48",
				"values": {
					"layout": {
						"colSpan": 5,
						"rowSpan": 1,
						"column": 6,
						"row": 2,
						"layoutName": "Header"
					},
					"bindTo": "WatConfirmNewPax",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.LOOKUP0ef78f1611ef469aa91f1d7d988bad48LabelCaption"
						}
					},
					"enabled": true,
					"contentType": 3
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "STRING85c6d23d-7bec-4f46-98df-15758e7aac81",
				"values": {
					"layout": {
						"colSpan": 6,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Header"
					},
					"bindTo": "WatNewPaxCount",
					"enabled": true
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "LOOKUPe5b90da2-f40f-4db4-8922-42da762c001b",
				"values": {
					"layout": {
						"colSpan": 5,
						"rowSpan": 1,
						"column": 6,
						"row": 3,
						"layoutName": "Header"
					},
					"bindTo": "WatConfirmNewLocation",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.LOOKUPe5b90da2f40f4db4892242da762c001bLabelCaption"
						}
					},
					"enabled": true,
					"contentType": 3
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 8
			},
			{
				"operation": "insert",
				"name": "STRING463cac73-a813-477b-9657-acbad37608c1",
				"values": {
					"layout": {
						"colSpan": 6,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Header"
					},
					"bindTo": "WatNewLocation",
					"enabled": false,
					"contentType": 0
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 9
			},
			{
				"operation": "insert",
				"name": "WatNotesa51bece5-f3d1-4c04-af68-ef0f3898788c",
				"values": {
					"layout": {
						"colSpan": 11,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "Header"
					},
					"bindTo": "WatNotes",
					"enabled": true,
					"contentType": 0
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 10
			},
			{
				"operation": "insert",
				"name": "BOOLEANa71c2545-9200-46cd-a8be-cc41c98e38ea",
				"values": {
					"layout": {
						"colSpan": 6,
						"rowSpan": 1,
						"column": 0,
						"row": 5,
						"layoutName": "Header"
					},
					"bindTo": "WatCancelChangeRequest",
					"enabled": true
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 11
			},
			{
				"operation": "insert",
				"name": "Tab920baf5fTabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab920baf5fTabLabelTabCaption"
					},
					"items": [],
					"order": 0
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tab920baf5fTabLabelGroup22421ab9",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab920baf5fTabLabelGroup22421ab9GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tab920baf5fTabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tab920baf5fTabLabelGridLayout20a7a526",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tab920baf5fTabLabelGroup22421ab9",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CreatedOn3499f472-1d34-4001-871f-93c09e2afbae",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tab920baf5fTabLabelGridLayout20a7a526"
					},
					"bindTo": "CreatedOn",
					"enabled": false
				},
				"parentName": "Tab920baf5fTabLabelGridLayout20a7a526",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CreatedByd1fde6cf-6c1c-453c-90c5-64bfd291c0cf",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "Tab920baf5fTabLabelGridLayout20a7a526"
					},
					"bindTo": "CreatedBy",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Tab920baf5fTabLabelGridLayout20a7a526",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "ModifiedOn617c6746-36e7-4b05-a261-ef982dbea7bf",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Tab920baf5fTabLabelGridLayout20a7a526"
					},
					"bindTo": "ModifiedOn",
					"enabled": false
				},
				"parentName": "Tab920baf5fTabLabelGridLayout20a7a526",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "ModifiedByc823c00f-72e4-4035-9709-ee3f21a6c655",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "Tab920baf5fTabLabelGridLayout20a7a526"
					},
					"bindTo": "ModifiedBy",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Tab920baf5fTabLabelGridLayout20a7a526",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "DATETIME9c2d074b-1ae6-4880-96f2-245b1da3f7a0",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Tab920baf5fTabLabelGridLayout20a7a526"
					},
					"bindTo": "WatSubmittedOn",
					"enabled": false
				},
				"parentName": "Tab920baf5fTabLabelGridLayout20a7a526",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "LOOKUP8e454cf3-1239-441d-aa65-0712f08d14c9",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 2,
						"layoutName": "Tab920baf5fTabLabelGridLayout20a7a526"
					},
					"bindTo": "WatSubmittedBy",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Tab920baf5fTabLabelGridLayout20a7a526",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "DATETIMEf768e0a7-0359-446a-a746-7d9bd93183e4",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Tab920baf5fTabLabelGridLayout20a7a526"
					},
					"bindTo": "WatConfirmedOn",
					"enabled": false
				},
				"parentName": "Tab920baf5fTabLabelGridLayout20a7a526",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "LOOKUPc8596516-1b8a-40ba-8b70-9355d0775e89",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 3,
						"layoutName": "Tab920baf5fTabLabelGridLayout20a7a526"
					},
					"bindTo": "WatConfirmedBy",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Tab920baf5fTabLabelGridLayout20a7a526",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "NotesAndFilesTab",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.NotesAndFilesTabCaption"
					},
					"items": [],
					"order": 1
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Files",
				"values": {
					"itemType": 2
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "NotesControlGroup",
				"values": {
					"itemType": 15,
					"caption": {
						"bindTo": "Resources.Strings.NotesGroupCaption"
					},
					"items": []
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Notes",
				"values": {
					"bindTo": "WatNotes",
					"dataValueType": 1,
					"contentType": 4,
					"layout": {
						"column": 0,
						"row": 0,
						"colSpan": 24
					},
					"labelConfig": {
						"visible": false
					},
					"controlConfig": {
						"imageLoaded": {
							"bindTo": "insertImagesToNotes"
						},
						"images": {
							"bindTo": "NotesImagesCollection"
						}
					}
				},
				"parentName": "NotesControlGroup",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "merge",
				"name": "ESNTab",
				"values": {
					"order": 2
				}
			}
		]/**SCHEMA_DIFF*/
	};
});
