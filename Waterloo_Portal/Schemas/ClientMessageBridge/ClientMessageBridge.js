  define("ClientMessageBridge", ["ConfigurationConstants"],
    function(ConfigurationConstants) {
        return {
            // Сообщения.
            messages: {
                //Имя сообщения.
                "ReloadDetailASU": {
                    // Тип сообщения — широковещательное, без указания конкретного подписчика.
                    "mode": Terrasoft.MessageMode.BROADCAST,
                    // Направление сообщения — публикация.
                    "direction": Terrasoft.MessageDirectionType.PUBLISH
                },
				"ReloadDetailFaculties": {
                    // Тип сообщения — широковещательное, без указания конкретного подписчика.
                    "mode": Terrasoft.MessageMode.BROADCAST,
                    // Направление сообщения — публикация.
                    "direction": Terrasoft.MessageDirectionType.PUBLISH
                },
				"ReloadQuestions": {
                    // Тип сообщения — широковещательное, без указания конкретного подписчика.
                    "mode": Terrasoft.MessageMode.BROADCAST,
                    // Направление сообщения — публикация.
                    "direction": Terrasoft.MessageDirectionType.PUBLISH
                },
            },
            methods: {
                // Инициализация схемы.
                init: function() {
                    this.callParent(arguments);
					this.addMessageConfig({
						sender: "ReloadDetailASU",
						messageName: "ReloadDetailASU"
					});
					this.addMessageConfig({
						sender: "ReloadDetailFaculties",
						messageName: "ReloadDetailFaculties"
					});
					this.addMessageConfig({
						sender: "ReloadQuestions",
						messageName: "ReloadQuestions"
					});
                },
            }
        };
    });