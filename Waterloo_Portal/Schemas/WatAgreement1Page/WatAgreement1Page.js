define("WatAgreement1Page", [], function() {
	return {
		entitySchemaName: "WatAgreement",
		attributes: {},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{
			"SchemaConsultedASUs": {
				"schemaName": "Schemab7181e75Detail",
				"entitySchemaName": "SMConsultedASUs",
				"filter": {
					"detailColumn": "WatAgreement",
					"masterColumn": "Id"
				}
			},
			"Schemaa7878603Detaildd8dc162": {
				"schemaName": "Schemaa7878603Detail",
				"entitySchemaName": "SMNewConsultedFaculties",
				"filter": {
					"detailColumn": "Agreement",
					"masterColumn": "Id"
				}
			}
		}/**SCHEMA_DETAILS*/,
		messages: {
			"ReloadDetailASU" : {
                    "mode": Terrasoft.MessageMode.BROADCAST,
                    "direction": Terrasoft.MessageDirectionType.SUBSCRIBE
                },
			"ReloadDetailFaculties" : {
                    "mode": Terrasoft.MessageMode.BROADCAST,
                    "direction": Terrasoft.MessageDirectionType.SUBSCRIBE
                },
		},
		businessRules: /**SCHEMA_BUSINESS_RULES*/{}/**SCHEMA_BUSINESS_RULES*/,
		methods: {
			subscribeSandboxEvents: function() {
                this.callParent(arguments);
				this.sandbox.subscribe("ReloadDetailASU",this.reloadDetailASU,this);
				this.sandbox.subscribe("ReloadDetailFaculties",this.reloadFaculties,this);
			},
			
			reloadDetailASU: function(){
				window.console.log("ASU");
				this.updateDetail({
                        detail: "SchemaConsultedASUs"
                    });
			},
			
			reloadFaculties: function(){
				window.console.log("Faculties");
				this.updateDetail({
                        detail: "Schemaa7878603Detaildd8dc162"
                    });
			},
		},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"name": "WatRequestType7e796ee8-545e-485f-a614-0d110745b98f",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "WatRequestType"
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "WatAgreementClassificationbd35db38-5a47-405f-b09a-8799a6d0ca7b",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "WatAgreementClassification"
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "WatAgreementSubClassification002a40c1-0d8b-49db-a888-c4a8d960f4ee",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "WatAgreementSubClassification"
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "WatMasterAgreement36b1ba6b-e54a-491d-bec4-f06df3464dcb",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 5,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "WatMasterAgreement"
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "remove",
				"name": "WatTermDuration198eb676-75a8-46fc-9f70-352317f50799",
				"properties": [
					"labelConfig"
				]
			},
			{
				"operation": "remove",
				"name": "BOOLEAN8325ea9c-8e1f-4c96-a518-b1c37c999f25",
				"properties": [
					"labelConfig"
				]
			},
			{
				"operation": "remove",
				"name": "INTEGER0489268e-c93f-4a44-802d-5b80185cc2aa",
				"properties": [
					"labelConfig"
				]
			},
			{
				"operation": "remove",
				"name": "WatPartnershipRequest027561ae-74b6-432a-a126-806b550418a7",
				"properties": [
					"labelConfig"
				]
			},
			{
				"operation": "insert",
				"name": "TabConsultationLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.TabConsultationLabelTabCaption"
					},
					"items": [],
					"order": 1
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "TabDeadLineTableGroup",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.TabDeadLineTableGroupGroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "TabConsultationLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "TabConsultationLabelGridLayout4f5eba70",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "TabDeadLineTableGroup",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "DATETIME21130cf4-ff5b-45c7-9668-a722fc568924",
				"values": {
					"layout": {
						"colSpan": 10,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "TabConsultationLabelGridLayout4f5eba70"
					},
					"bindTo": "SMFaultyDeadlineDate",
					"enabled": true
				},
				"parentName": "TabConsultationLabelGridLayout4f5eba70",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "DATETIMEc83a3bd3-49f8-40ca-8191-04b8250fd374",
				"values": {
					"layout": {
						"colSpan": 11,
						"rowSpan": 1,
						"column": 11,
						"row": 0,
						"layoutName": "TabConsultationLabelGridLayout4f5eba70"
					},
					"bindTo": "SMAcademicSupportUnitDeadlineDate",
					"enabled": true,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.DATETIMEc83a3bd349f840ca819104b8250fd374LabelCaption"
						}
					}
				},
				"parentName": "TabConsultationLabelGridLayout4f5eba70",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "SchemaConsultedASUs",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "TabConsultationLabel",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Schemaa7878603Detaildd8dc162",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "TabConsultationLabel",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "merge",
				"name": "Tab2b976856TabLabel",
				"values": {
					"order": 2
				}
			},
			{
				"operation": "merge",
				"name": "Tabd87b5dffTabLabel",
				"values": {
					"order": 3
				}
			},
			{
				"operation": "merge",
				"name": "Tab3a3d3409TabLabel",
				"values": {
					"order": 4
				}
			},
			{
				"operation": "merge",
				"name": "NotesAndFilesTab",
				"values": {
					"order": 5
				}
			},
			{
				"operation": "merge",
				"name": "ESNTab",
				"values": {
					"order": 6
				}
			},
			{
				"operation": "merge",
				"name": "Tabcc28cb4fTabLabel",
				"values": {
					"order": 7
				}
			},
			{
				"operation": "remove",
				"name": "WatRequestType00b978db-2dfe-4b01-b4e1-46dc2bb4a1c4"
			},
			{
				"operation": "remove",
				"name": "WatAgreementClassificationa7edd901-9474-4540-9d06-50eb77ca005d"
			},
			{
				"operation": "remove",
				"name": "LOOKUP3c188d98-41d1-40e5-9559-bb634b6bf683"
			},
			{
				"operation": "remove",
				"name": "LOOKUP3fba4343-4d21-4a5f-82dd-94430a22cc0a"
			}
		]/**SCHEMA_DIFF*/
	};
});
