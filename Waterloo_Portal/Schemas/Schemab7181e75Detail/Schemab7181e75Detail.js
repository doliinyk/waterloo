define("Schemab7181e75Detail", ["ProcessModuleUtilities", "MaskHelper", "SMBaseSectionGridRowViewModel"], function(ProcessModuleUtilities, MaskHelper) {
	return {
		entitySchemaName: "SMConsultedASUs",
		attributes: {
			"ButtonClicked": {
				 "dataValueType": Terrasoft.DataValueType.BOOLEAN,
				 "type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				 "value": false
			}
		},
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		diff: /**SCHEMA_DIFF*/[
			
			{
				"operation": "merge",
				"name": "DataGrid",
				"values": {
					"activeRowAction": {"bindTo": "onActiveRowAction"},
					"activeRowActions": [
						{
							"className": "Terrasoft.Button",
							"style":this.Terrasoft.controls.ButtonEnums.style.BLUE,
							"markerValue": "schedule",
							"tag": "notify",
							"caption": "Notify",
							"visible": {
								"bindTo": "isNotifyButtonVisible",
							}
						}
					]
				}
			}
		]/**SCHEMA_DIFF*/,
		methods: {
			onActiveRowAction: function(buttonTag, primaryColumnValue) 
			{
				var scope = this;
                if (buttonTag === "notify") 
				{
					//if(scope.get("ButtonClicked") == false){
						scope.set("ButtonClicked", true);
						
						var activeRow = this.getActiveRow();
						var activeRowId = activeRow.get("Id");
						var notifiedDate = activeRow.get("SMNotifiedDate");
						if(!notifiedDate){
							MaskHelper.ShowBodyMask();
							var args = {
								sysProcessName: "SMNotifyResposibleGroup",
								parameters: {
									recordId: activeRowId
								}
							};
							ProcessModuleUtilities.executeProcess(args); 
						} else {
							scope.showInformationDialog("The department has already been notified");
						}
						 
					//} else {
					//	scope.showInformationDialog("The notification was already send");
					//}
                }
        	},
			
			smOpenView: function(){
				MaskHelper.ShowBodyMask();
                var activeRow = this.getActiveRow();
                var activeRowId = this.get("MasterRecordId");
                var args = {
					sysProcessName: "ChangeSMConsultedASUsDetailColumn",
						parameters: {
							recordId: activeRowId,
							state: true
						}
					};
				ProcessModuleUtilities.executeProcess(args);    
			},
			
			smLockView: function(){
				MaskHelper.ShowBodyMask();
                var activeRow = this.getActiveRow();
                var activeRowId = this.get("MasterRecordId");
                var args = {
					sysProcessName: "ChangeSMConsultedASUsDetailColumn",
						parameters: {
							recordId: activeRowId,
							state: false
						}
					};
				ProcessModuleUtilities.executeProcess(args);   
			},
			/**
			 * ######## ######## ###### ###### ############# ############# ####### ## ########### #######
			 * @protected
			 * @return {String} ########## ########
			 */
			getGridRowViewModelClassName: function() {
				return "Terrasoft.SMBaseSectionGridRowViewModel";
			}
		}
	};
});
