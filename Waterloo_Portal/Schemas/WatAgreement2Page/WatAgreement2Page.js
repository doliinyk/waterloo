define("WatAgreement2Page", [], function() {
	return {
		entitySchemaName: "WatAgreement",
		attributes: {},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{
			"Files": {
				"schemaName": "FileDetailV2",
				"entitySchemaName": "WatAgreementFile",
				"filter": {
					"masterColumn": "Id",
					"detailColumn": "WatAgreement"
				}
			},
			"VisaDetailV2e20af99d": {
				"schemaName": "VisaDetailV2",
				"entitySchemaName": "WatAgreementVisa",
				"filter": {
					"masterColumn": "Id",
					"detailColumn": "WatAgreement"
				}
			},
			"WatSchemaeece2c0bDetailc4ca703b": {
				"schemaName": "WatSchemaeece2c0bDetail",
				"entitySchemaName": "WatParticipatingFaculties",
				"filter": {
					"detailColumn": "WatAgreement",
					"masterColumn": "Id"
				}
			},
			"WatSchemacc896fb3Detail66abcb51": {
				"schemaName": "WatSchemacc896fb3Detail",
				"entitySchemaName": "WatAgreementStakeholders",
				"filter": {
					"detailColumn": "WatAgreement",
					"masterColumn": "Id"
				}
			},
			"WatSchemac79b46ecDetail34e2424f": {
				"schemaName": "WatSchemac79b46ecDetail",
				"entitySchemaName": "WatAgreementStage",
				"filter": {
					"detailColumn": "WatAgreement",
					"masterColumn": "Id"
				}
			},
			"DocumentDetailV2153423e0": {
				"schemaName": "DocumentDetailV2",
				"entitySchemaName": "Document",
				"filter": {
					"detailColumn": "WatAgreement",
					"masterColumn": "Id"
				}
			},
			"WatSchemaTimerDetaila653d9ba": {
				"schemaName": "WatSchemaTimerDetail",
				"entitySchemaName": "WatTimer",
				"filter": {
					"detailColumn": "WatAgreement",
					"masterColumn": "Id"
				}
			},
			"EmailDetailV20d169b6f": {
				"schemaName": "EmailDetailV2",
				"entitySchemaName": "Activity",
				"filter": {
					"detailColumn": "WatWatAgreement",
					"masterColumn": "Id"
				}
			},
			"WatSchemaf0d78a31Detail0b7a83bf": {
				"schemaName": "WatSchemaf0d78a31Detail",
				"entitySchemaName": "WatFinalizationTasks",
				"filter": {
					"detailColumn": "WatAgreement",
					"masterColumn": "Id"
				}
			},
			"DocumentDetailV26bdbd890": {
				"schemaName": "DocumentDetailV2",
				"entitySchemaName": "Document",
				"filter": {
					"detailColumn": "WatAgreement",
					"masterColumn": "Id"
				}
			},
			"WatSchema94c7e3c8Detailed376edb": {
				"schemaName": "WatSchema94c7e3c8Detail",
				"entitySchemaName": "WatDatedNotes",
				"filter": {
					"detailColumn": "WatAgreement",
					"masterColumn": "Id"
				}
			},
			"WatSchemaCustomEmailCallDetail0131bf20": {
				"schemaName": "WatSchemaCustomEmailCallDetail",
				"entitySchemaName": "Activity",
				"filter": {
					"detailColumn": "WatWatAgreement",
					"masterColumn": "Id"
				}
			},
			"WatSchemaCustomTaskDetail637b94e2": {
				"schemaName": "WatSchemaCustomTaskDetail",
				"entitySchemaName": "Activity",
				"filter": {
					"detailColumn": "WatWatAgreement",
					"masterColumn": "Id"
				}
			},
			"Schemab7181e75Detail0a4a0e09": {
				"schemaName": "Schemab7181e75Detail",
				"entitySchemaName": "SMConsultedASUs",
				"filter": {
					"detailColumn": "WatAgreement",
					"masterColumn": "Id"
				}
			},
			"Schema948d6649Detail44906143": {
				"schemaName": "Schema948d6649Detail",
				"entitySchemaName": "SMNewConsultedFaculties",
				"filter": {
					"detailColumn": "Agreement",
					"masterColumn": "Id"
				}
			}
		}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{
			"WatAgreementType": {
				"f15b3c7b-e7bd-4a69-9948-b82474bc0f0b": {
					"uId": "f15b3c7b-e7bd-4a69-9948-b82474bc0f0b",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreementStage"
							},
							"rightExpression": {
								"type": 0,
								"value": "71691e40-3686-425a-93ca-610e30747614",
								"dataValueType": 10
							}
						}
					]
				},
				"3e771e5d-fc0e-43fd-9886-758c65041d85": {
					"uId": "3e771e5d-fc0e-43fd-9886-758c65041d85",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreementStage"
							},
							"rightExpression": {
								"type": 0,
								"value": "0ce04f24-bb38-454a-ac3a-9ddc03493986",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreementStage"
							},
							"rightExpression": {
								"type": 0,
								"value": "e1944ea7-5526-4dd7-88ba-0848ba51db13",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreementStage"
							},
							"rightExpression": {
								"type": 0,
								"value": "419c6ed8-f682-4a5a-833c-f3f6de035b90",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreementStage"
							},
							"rightExpression": {
								"type": 0,
								"value": "2d278946-1df0-42d3-8c6b-810d0f2975d5",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreementStage"
							},
							"rightExpression": {
								"type": 0,
								"value": "71691e40-3686-425a-93ca-610e30747614",
								"dataValueType": 10
							}
						}
					]
				},
				"cbc976a3-f51d-4432-9f1a-65a4ffc30b41": {
					"uId": "cbc976a3-f51d-4432-9f1a-65a4ffc30b41",
					"enabled": true,
					"removed": true,
					"ruleType": 1,
					"baseAttributePatch": "WatAgreementClassification",
					"comparisonType": 3,
					"autoClean": true,
					"autocomplete": true,
					"type": 1,
					"attribute": "WatAgreementClassification"
				}
			},
			"WatTermStartDate": {
				"8edb9d8d-1611-49f7-b0a0-9d7bd5499ded": {
					"uId": "8edb9d8d-1611-49f7-b0a0-9d7bd5499ded",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreementSigned"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreementStage"
							},
							"rightExpression": {
								"type": 0,
								"value": "e1944ea7-5526-4dd7-88ba-0848ba51db13",
								"dataValueType": 10
							}
						}
					]
				},
				"bb6b83d0-a0a7-4960-8e25-107c431477c4": {
					"uId": "bb6b83d0-a0a7-4960-8e25-107c431477c4",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 2,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreementStage"
							},
							"rightExpression": {
								"type": 0,
								"value": "0ce04f24-bb38-454a-ac3a-9ddc03493986",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreementStage"
							},
							"rightExpression": {
								"type": 0,
								"value": "e1944ea7-5526-4dd7-88ba-0848ba51db13",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreementStage"
							},
							"rightExpression": {
								"type": 0,
								"value": "419c6ed8-f682-4a5a-833c-f3f6de035b90",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreementStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "4e4cda8f-efe9-4388-bbe5-3f6336a580ef",
								"dataValueType": 10
							}
						}
					]
				},
				"466a6840-8466-4edb-8a04-98cafebeadc0": {
					"uId": "466a6840-8466-4edb-8a04-98cafebeadc0",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreementSigned"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"WatTermEndDate": {
				"da80297d-e652-43ff-9a4a-76cc12c0fb0d": {
					"uId": "da80297d-e652-43ff-9a4a-76cc12c0fb0d",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreementStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "effd2081-f8d4-4a1d-9237-edb542f5582a",
								"dataValueType": 10
							}
						}
					]
				},
				"f1c2cca7-fc35-482d-b2d3-d8dc7af40f6e": {
					"uId": "f1c2cca7-fc35-482d-b2d3-d8dc7af40f6e",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 2,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreementStage"
							},
							"rightExpression": {
								"type": 0,
								"value": "0ce04f24-bb38-454a-ac3a-9ddc03493986",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreementStage"
							},
							"rightExpression": {
								"type": 0,
								"value": "e1944ea7-5526-4dd7-88ba-0848ba51db13",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreementStage"
							},
							"rightExpression": {
								"type": 0,
								"value": "419c6ed8-f682-4a5a-833c-f3f6de035b90",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreementStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "4e4cda8f-efe9-4388-bbe5-3f6336a580ef",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"WatContact": {
				"88d12400-e263-4205-af92-b7573fca32c3": {
					"uId": "88d12400-e263-4205-af92-b7573fca32c3",
					"enabled": true,
					"removed": false,
					"ruleType": 1,
					"baseAttributePatch": "Account",
					"comparisonType": 3,
					"autoClean": true,
					"autocomplete": true,
					"type": 1,
					"attribute": "WatInstitution"
				}
			},
			"WatLeadUnitDepartment": {
				"cde06df9-d5b0-4b69-9fd8-fe08cf45f10f": {
					"uId": "cde06df9-d5b0-4b69-9fd8-fe08cf45f10f",
					"enabled": true,
					"removed": false,
					"ruleType": 1,
					"baseAttributePatch": "WatFaculty",
					"comparisonType": 3,
					"autoClean": true,
					"autocomplete": true,
					"type": 1,
					"attribute": "WatLeadUnit"
				}
			},
			"NotesControlGroup": {
				"cfe6039c-ffe2-4df0-8513-05b5045da62c": {
					"uId": "cfe6039c-ffe2-4df0-8513-05b5045da62c",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 0,
								"value": "abc",
								"dataValueType": 1
							},
							"rightExpression": {
								"type": 0,
								"value": "123",
								"dataValueType": 1
							}
						}
					]
				},
				"9079e068-a932-4007-818f-078ea3fe088e": {
					"uId": "9079e068-a932-4007-818f-078ea3fe088e",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 0,
								"value": 1,
								"dataValueType": 4
							},
							"rightExpression": {
								"type": 0,
								"value": 2,
								"dataValueType": 4
							}
						}
					]
				}
			},
			"WatRenewalTerms": {
				"3ecfa79d-d181-4279-a2d3-114ce115fc57": {
					"uId": "3ecfa79d-d181-4279-a2d3-114ce115fc57",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatEOLTreatment"
							},
							"rightExpression": {
								"type": 0,
								"value": "bab57a00-fa8e-4331-82a4-fb0d41b51bd6",
								"dataValueType": 10
							}
						}
					]
				},
				"e83db338-eb7c-480c-99e6-89c1fdeac812": {
					"uId": "e83db338-eb7c-480c-99e6-89c1fdeac812",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatEOLTreatment"
							},
							"rightExpression": {
								"type": 0,
								"value": "bab57a00-fa8e-4331-82a4-fb0d41b51bd6",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"WatName": {
				"d357092f-aa2f-4549-b6ed-f8d2bc0c6264": {
					"uId": "d357092f-aa2f-4549-b6ed-f8d2bc0c6264",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatRequestType"
							},
							"rightExpression": {
								"type": 0,
								"value": "7c6d730d-eecb-41c0-8e5d-3e9cc4167da9",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatRequestType"
							},
							"rightExpression": {
								"type": 0,
								"value": "73dda689-7c0c-4612-8d07-763141100ae9",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatRequestType"
							},
							"rightExpression": {
								"type": 0,
								"value": "d43c349a-63d0-43d9-900e-6fc9f69f4d70",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatRequestType"
							},
							"rightExpression": {
								"type": 0,
								"value": "27a01368-f1c0-4051-b26d-21b3b213fe65",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "WatName"
							}
						}
					]
				}
			},
			"WatTermDuration": {
				"29566d2f-baac-4e49-ae82-a92317d98609": {
					"uId": "29566d2f-baac-4e49-ae82-a92317d98609",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "WatTermStartDate"
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreementStage"
							},
							"rightExpression": {
								"type": 0,
								"value": "e1944ea7-5526-4dd7-88ba-0848ba51db13",
								"dataValueType": 10
							}
						}
					]
				},
				"e1a97f71-4a25-46aa-bcef-a5eefcc47cf5": {
					"uId": "e1a97f71-4a25-46aa-bcef-a5eefcc47cf5",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "WatTermStartDate"
							}
						}
					]
				}
			},
			"WatAgreementClassification": {
				"5ff371c9-9f44-4185-8676-650bde7a2ed1": {
					"uId": "5ff371c9-9f44-4185-8676-650bde7a2ed1",
					"enabled": true,
					"removed": false,
					"ruleType": 1,
					"baseAttributePatch": "WatAgreementType",
					"comparisonType": 3,
					"autoClean": true,
					"autocomplete": false,
					"type": 1,
					"attribute": "WatAgreementType"
				},
				"eea0855f-de51-4206-b1a1-40a301c8d6f7": {
					"uId": "eea0855f-de51-4206-b1a1-40a301c8d6f7",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreementType"
							}
						}
					]
				}
			},
			"WatAgreementSubClassification": {
				"c5864e1e-3e6e-4259-9d81-93b4595ee780": {
					"uId": "c5864e1e-3e6e-4259-9d81-93b4595ee780",
					"enabled": true,
					"removed": false,
					"ruleType": 1,
					"baseAttributePatch": "WatClassification",
					"comparisonType": 3,
					"autoClean": true,
					"autocomplete": false,
					"type": 1,
					"attribute": "WatAgreementClassification"
				},
				"9c06fcc7-fdf7-4dec-8dba-f8f12fb7626b": {
					"uId": "9c06fcc7-fdf7-4dec-8dba-f8f12fb7626b",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreementClassification"
							}
						}
					]
				}
			},
			"WatRequestType": {
				"f76840da-b7d0-492b-a746-e4f27eedfa42": {
					"uId": "f76840da-b7d0-492b-a746-e4f27eedfa42",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreementStage"
							},
							"rightExpression": {
								"type": 0,
								"value": "5dd674ae-5347-476d-9262-13d419467127",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreementStage"
							},
							"rightExpression": {
								"type": 0,
								"value": "181fc291-f2cf-43da-8db6-5edc0e3c562a",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"WatEOLTreatment": {
				"17f50943-d3f6-433c-b62e-908d268115f9": {
					"uId": "17f50943-d3f6-433c-b62e-908d268115f9",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 7,
							"leftExpression": {
								"type": 1,
								"attribute": "WatTermDuration"
							},
							"rightExpression": {
								"type": 0,
								"value": 0,
								"dataValueType": 4
							}
						}
					]
				}
			},
			"WatAgreementSigned": {
				"868b7bab-d778-430a-8736-72d3218eb2fe": {
					"uId": "868b7bab-d778-430a-8736-72d3218eb2fe",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreementStage"
							},
							"rightExpression": {
								"type": 0,
								"value": "e1944ea7-5526-4dd7-88ba-0848ba51db13",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreementStage"
							},
							"rightExpression": {
								"type": 0,
								"value": "419c6ed8-f682-4a5a-833c-f3f6de035b90",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreementSigned"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				},
				"e8f85bd6-754b-47e2-a8ab-ce27cdb0f6c6": {
					"uId": "e8f85bd6-754b-47e2-a8ab-ce27cdb0f6c6",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "WatAgreementStage"
							},
							"rightExpression": {
								"type": 0,
								"value": "e1944ea7-5526-4dd7-88ba-0848ba51db13",
								"dataValueType": 10
							}
						}
					]
				}
			}
		}/**SCHEMA_BUSINESS_RULES*/,
		methods: {},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"name": "WatName6ede102b-f52e-491a-9b45-8955f24099cf",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "WatName",
					"enabled": false
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatRequestType82291ca8-10ec-4593-a296-be1a68699894",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "WatRequestType",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "WatAgreementType8ab5efb7-b58a-40de-af2f-d2e683aba589",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "WatAgreementType",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "WatAgreementClassificationed9621fa-e34e-40ce-8e1d-962586e74fa3",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "WatAgreementClassification",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "WatAgreementSubClassification7911e348-c866-4a21-89bf-efa732f9b81f",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "WatAgreementSubClassification",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "WatMasterAgreement78bd6ca4-530d-4dd1-bb85-ca46e0e1caf4",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 5,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "WatMasterAgreement",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "WatTermStartDate058bdbc1-0f75-4fc4-9b8e-55e2b0e70f58",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 6,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "WatTermStartDate",
					"enabled": false
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "WatTermDuration198eb676-75a8-46fc-9f70-352317f50799",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 7,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "WatTermDuration",
					"enabled": false
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "WatTermEndDatef8d7117c-cc04-4dec-a4cf-05a243360965",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 8,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "WatTermEndDate",
					"enabled": false
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 8
			},
			{
				"operation": "insert",
				"name": "WatAgreementStatus44fcf9cb-4f48-4e0e-9de9-e6a047bf9b96",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 9,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "WatAgreementStatus",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 9
			},
			{
				"operation": "insert",
				"name": "BOOLEAN04301d67-7129-4eb5-98ab-a49b1348fee9",
				"values": {
					"layout": {
						"colSpan": 5,
						"rowSpan": 1,
						"column": 19,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "WatAgreementSigned",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "WatInstitution1b454cec-422c-47e4-9949-07c86ae12a70",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "WatInstitution",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "BOOLEAN8325ea9c-8e1f-4c96-a518-b1c37c999f25",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 9,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "WatEnglishLanguageRequirements",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "WatContact2cfcb954-47b0-42b1-83ff-866c00a323fd",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "WatContact",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "WatEFASELASdf0dbc70-09a0-4a93-a0da-715f96185ee1",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 9,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "WatEFASELAS",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "WatLeadUnit77a86ba5-915f-4a45-b804-2f188a0e3115",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Header"
					},
					"bindTo": "WatLeadUnit",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "WatFundingArrangementsa6e05fd2-9c30-4fcb-8d23-de60918d9150",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 9,
						"row": 2,
						"layoutName": "Header"
					},
					"bindTo": "WatFundingArrangements",
					"enabled": false,
					"contentType": 0
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "WatLeadUnitDepartmentf0fafde2-803f-402d-b36b-0dd00c7ec9e9",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Header"
					},
					"bindTo": "WatLeadUnitDepartment",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "STRINGabf5aee8-a906-422a-abbf-b87067fae21d",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 9,
						"row": 3,
						"layoutName": "Header"
					},
					"bindTo": "WatQuota",
					"enabled": false,
					"contentType": 0
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 8
			},
			{
				"operation": "insert",
				"name": "WatAcademicLevel34a117b2-1b8a-4128-a9f2-70b99d66c9eb",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "Header"
					},
					"bindTo": "WatAcademicLevel",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 9
			},
			{
				"operation": "insert",
				"name": "STRING1ebc8af5-29cd-499e-8bd9-e98a8de69e6a",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 9,
						"row": 4,
						"layoutName": "Header"
					},
					"bindTo": "WatLisNumber",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 10
			},
			{
				"operation": "insert",
				"name": "STRING70ca3d11-e456-4880-909b-4f76b72ae009",
				"values": {
					"layout": {
						"colSpan": 17,
						"rowSpan": 2,
						"column": 0,
						"row": 5,
						"layoutName": "Header"
					},
					"bindTo": "WatGeneralNotes",
					"enabled": false,
					"contentType": 0
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 11
			},
			{
				"operation": "insert",
				"name": "Tab2bbaa09dTabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab2bbaa09dTabLabelTabCaption"
					},
					"items": [],
					"order": 0
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Schemab7181e75Detail0a4a0e09",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab2bbaa09dTabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Schema948d6649Detail44906143",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab2bbaa09dTabLabel",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "remove",
				"name": "ESNTab"
			},
			{
				"operation": "remove",
				"name": "ESNFeedContainer"
			},
			{
				"operation": "remove",
				"name": "ESNFeed"
			}
		]/**SCHEMA_DIFF*/
	};
});
