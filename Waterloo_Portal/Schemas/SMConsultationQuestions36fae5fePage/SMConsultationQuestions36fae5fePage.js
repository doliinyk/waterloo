define("SMConsultationQuestions36fae5fePage", [], function() {
	return {
		entitySchemaName: "SMConsultationQuestions",
		attributes: {},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{
			"Name": {
				"a78479e5-ba6b-47b7-b181-821bd714d904": {
					"uId": "a78479e5-ba6b-47b7-b181-821bd714d904",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMOpenForEditing"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"SMConsultedASUs": {
				"33b63f2c-ab7b-45bc-955d-a59c00525101": {
					"uId": "33b63f2c-ab7b-45bc-955d-a59c00525101",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMOpenForEditing"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"Question": {
				"f3892e5e-1eeb-48ac-976a-2672fade2f0d": {
					"uId": "f3892e5e-1eeb-48ac-976a-2672fade2f0d",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 1,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "SMConsultedASUs"
							}
						},
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "SMConsultedFaculties"
							}
						}
					]
				},
				"75b210dd-d805-411b-92f8-4c7a5717c8b7": {
					"uId": "75b210dd-d805-411b-92f8-4c7a5717c8b7",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMOpenForEditing"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"SMOption": {
				"dad65355-663a-488e-8760-8445f40edd7f": {
					"uId": "dad65355-663a-488e-8760-8445f40edd7f",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "Question"
							},
							"rightExpression": {
								"type": 0,
								"value": "Is your Faculty Interested in participating in this Agreement",
								"dataValueType": 1
							}
						}
					]
				},
				"51ceaf43-0a01-46d2-863e-32e1c2ccdf6e": {
					"uId": "51ceaf43-0a01-46d2-863e-32e1c2ccdf6e",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMOpenForEditing"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				},
				"f8973452-5d25-40cf-8999-da970b327e21": {
					"uId": "f8973452-5d25-40cf-8999-da970b327e21",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "Question"
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "Question"
							},
							"rightExpression": {
								"type": 0,
								"value": "Is your Faculty Interested in participating in this Agreement",
								"dataValueType": 1
							}
						}
					]
				}
			}
		}/**SCHEMA_BUSINESS_RULES*/,
		methods: {},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[]/**SCHEMA_DIFF*/
	};
});
