define("SMConsultedASUs2Page", ["ProcessModuleUtilities", "MaskHelper", "css!ConsultedASUCSS"], function(ProcessModuleUtilities, MaskHelper) {
	return {
		entitySchemaName: "SMConsultedASUs",
		attributes: {
			"MemberOfStudentSuccessOffice": {
				 "dataValueType": Terrasoft.DataValueType.BOOLEAN,
				 "type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				 "value": false
			},
			"MemberOfCoop": {
				 "dataValueType": Terrasoft.DataValueType.BOOLEAN,
				 "type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				 "value": false
			},
			"MemberOfRegistarOffice": {
				 "dataValueType": Terrasoft.DataValueType.BOOLEAN,
				 "type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				 "value": false
			},
			"MemberOfGraduteStudies": {
				 "dataValueType": Terrasoft.DataValueType.BOOLEAN,
				 "type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				 "value": false
			},
			"MemberOfOfficeOfRessearch": {
				 "dataValueType": Terrasoft.DataValueType.BOOLEAN,
				 "type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				 "value": false
			},
			"MemberOfFaculties": {
				 "dataValueType": Terrasoft.DataValueType.BOOLEAN,
				 "type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				 "value": false
			},
			"OpenForEditingAvailable": {
				 "dataValueType": Terrasoft.DataValueType.BOOLEAN,
				 "type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				 "value": false
			},
			"CloseForEditingAvailable": {
				 "dataValueType": Terrasoft.DataValueType.BOOLEAN,
				 "type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				 "value": false
			},
			"ButtonsAreVisibleForTheGroup": {
				 "dataValueType": Terrasoft.DataValueType.BOOLEAN,
				 "type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				 "value": false
			},
			"QuestionQ2Availability": {
				 "dataValueType": Terrasoft.DataValueType.BOOLEAN,
				 "type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				 "value": false
			},
			"QuestionQ3Availability": {
				 "dataValueType": Terrasoft.DataValueType.BOOLEAN,
				 "type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				 "value": false
			},
			"QuestionQ4Availability": {
				 "dataValueType": Terrasoft.DataValueType.BOOLEAN,
				 "type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				 "value": false
			},
			"QuestionQ5Availability": {
				 "dataValueType": Terrasoft.DataValueType.BOOLEAN,
				 "type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				 "value": false
			},
			"QuestionQ6Availability": {
				 "dataValueType": Terrasoft.DataValueType.BOOLEAN,
				 "type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				 "value": false
			},
			"QuestionQ7Availability": {
				 "dataValueType": Terrasoft.DataValueType.BOOLEAN,
				 "type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				 "value": false
			},
			"QuestionQ1Availability": {
				 "dataValueType": Terrasoft.DataValueType.BOOLEAN,
				 "type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				 "value": false
			},
			"QuestionQ8Availability": {
				 "dataValueType": Terrasoft.DataValueType.BOOLEAN,
				 "type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				 "value": false
			},
			"RecommendationQ1Availability": {
				 "dataValueType": Terrasoft.DataValueType.BOOLEAN,
				 "type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				 "value": false
			},
			
			"RecommendationQ2Availability": {
				 "dataValueType": Terrasoft.DataValueType.BOOLEAN,
				 "type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				 "value": false
			},
			"RecommendationQ3Availability": {
				 "dataValueType": Terrasoft.DataValueType.BOOLEAN,
				 "type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				 "value": false
			},
			"RecommendationQ4Availability": {
				 "dataValueType": Terrasoft.DataValueType.BOOLEAN,
				 "type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				 "value": false
			},
			"IsModelItemsEnabled": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"value": true,
				"dependencies": [{
					"columns": ["SMResponseSubmitted", "SMOpenForEditing"],
					"methodName": "setCardLockoutStatus"
				}]
			},
			"FacilityParticipationConfirmation": {
				 "dataValueType": Terrasoft.DataValueType.BOOLEAN,
				 "type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				 "value": false
			},
			"FacilityFeedback": {
				 "dataValueType": Terrasoft.DataValueType.BOOLEAN,
				 "type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				 "value": false
			}
		},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{
			"Files": {
				"schemaName": "FileDetailV2",
				"entitySchemaName": "SMConsultedASUsFile",
				"filter": {
					"masterColumn": "Id",
					"detailColumn": "SMConsultedASUs"
				}
			},
			"SMAdditionalQuestionDetail": {
				"schemaName": "SMSchema148cf4faDetail",
				"entitySchemaName": "SMAdditionalQuestion",
				"filter": {
					"detailColumn": "SMConsultedASU",
					"masterColumn": "Id"
				}
			}
		}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{
			"SMAgreementType": {
				"9878f419-b33a-4b1a-8cdc-fb046ff2b9d5": {
					"uId": "9878f419-b33a-4b1a-8cdc-fb046ff2b9d5",
					"enabled": false,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMOpenForEditing"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"WatAgreement": {
				"c87f47e7-52e6-46ff-a5c2-97b0ac028339": {
					"uId": "c87f47e7-52e6-46ff-a5c2-97b0ac028339",
					"enabled": false,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMOpenForEditing"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"SMDeadlineDate": {
				"5bbbc092-f532-47d6-b1ef-b168a380ea75": {
					"uId": "5bbbc092-f532-47d6-b1ef-b168a380ea75",
					"enabled": false,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMOpenForEditing"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"SMFaculty": {
				"90715475-bb38-4a11-b9fb-f2e0581a4e41": {
					"uId": "90715475-bb38-4a11-b9fb-f2e0581a4e41",
					"enabled": false,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMOpenForEditing"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"SMFacultyDepartment": {
				"7627ae86-b31e-4320-9013-b0488553de1d": {
					"uId": "7627ae86-b31e-4320-9013-b0488553de1d",
					"enabled": false,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMOpenForEditing"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"SMNotifiedDate": {
				"9eebd62f-7e31-46ee-b050-78cdf1c4de08": {
					"uId": "9eebd62f-7e31-46ee-b050-78cdf1c4de08",
					"enabled": false,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMOpenForEditing"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"SMRequestType": {
				"48ea32a5-4d16-450d-b3e3-a9f0b3b176a1": {
					"uId": "48ea32a5-4d16-450d-b3e3-a9f0b3b176a1",
					"enabled": false,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMOpenForEditing"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"QuestionQ1OperationalFeasibility": {
				"e3af6e83-5844-43d4-aa64-fe9c5c9d296e": {
					"uId": "e3af6e83-5844-43d4-aa64-fe9c5c9d296e",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMOpenForEditing"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"QuestionQ2AnticipatedStudentInterest": {
				"0aa436fb-525f-4fda-b726-0e3a792fd446": {
					"uId": "0aa436fb-525f-4fda-b726-0e3a792fd446",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMOpenForEditing"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"QuestionQ3AnticipatedFacultyMember": {
				"f40da776-c728-4022-b385-b654f95ecfc0": {
					"uId": "f40da776-c728-4022-b385-b654f95ecfc0",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "SMFacultyDepartment"
							}
						}
					]
				}
			},
			"QuestionQ4AnticipatedInterestfromCoOp": {
				"c195bdbd-f979-44b4-a842-418e5eded48c": {
					"uId": "c195bdbd-f979-44b4-a842-418e5eded48c",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMOpenForEditing"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"QuestionQ5QualityAcademicAgreementandPartner": {
				"8d468736-e7cb-44bd-90ec-98258e450e9c": {
					"uId": "8d468736-e7cb-44bd-90ec-98258e450e9c",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMOpenForEditing"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"QuestionQ6ResearchAgreementandPartner": {
				"9728b43e-b1a6-493e-b31b-334f290f5ae1": {
					"uId": "9728b43e-b1a6-493e-b31b-334f290f5ae1",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMOpenForEditing"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"QuestionQ7CurrentandExpectedOutcomes": {
				"e23a8d36-dfd2-4a58-a84d-d7bbaed6d3f8": {
					"uId": "e23a8d36-dfd2-4a58-a84d-d7bbaed6d3f8",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMOpenForEditing"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				},
				"f0aeebda-400a-4783-b46b-ddbbc03bb29a": {
					"uId": "f0aeebda-400a-4783-b46b-ddbbc03bb29a",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMFacultyDepartment"
							},
							"rightExpression": {
								"type": 0,
								"value": "1b9eaca5-6c29-45c6-b98a-bf87312eb2a0",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMFacultyDepartment"
							},
							"rightExpression": {
								"type": 0,
								"value": "5e123412-113b-4fcf-b5d9-8ba9927df06c",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMFacultyDepartment"
							},
							"rightExpression": {
								"type": 0,
								"value": "2c5ec53d-738e-4faa-8db1-06ddfe07a0ac",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"QuestionQ8RelevantFactors": {
				"aedbe118-1279-4e87-b374-8982ab03d03d": {
					"uId": "aedbe118-1279-4e87-b374-8982ab03d03d",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMOpenForEditing"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"FacultyFeedbackforConcluding": {
				"3bb1f9c1-c355-4b94-aa4d-2900f21cec60": {
					"uId": "3bb1f9c1-c355-4b94-aa4d-2900f21cec60",
					"enabled": false,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMOpenForEditing"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"ASUAgreementConcluding": {
				"5e397c30-c4d8-403d-86ec-9ea8d8a49c69": {
					"uId": "5e397c30-c4d8-403d-86ec-9ea8d8a49c69",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "SMFacultyDepartment"
							}
						}
					]
				},
				"439a8bbb-12ed-4b5a-9df5-9a7904052166": {
					"uId": "439a8bbb-12ed-4b5a-9df5-9a7904052166",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMOpenForEditing"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"FacultyAgreementforConcluding": {
				"72c909ec-4c90-420d-a381-d21b56c7a273": {
					"uId": "72c909ec-4c90-420d-a381-d21b56c7a273",
					"enabled": false,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMOpenForEditing"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"ConsultationTabLabelGroupdd3cc404": {
				"0a8d8ece-983e-43ac-9d53-c583a8327de9": {
					"uId": "0a8d8ece-983e-43ac-9d53-c583a8327de9",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "QuestionQ4Availability"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				},
				"a56fa00e-96ee-406f-b3a7-d644743787b7": {
					"uId": "a56fa00e-96ee-406f-b3a7-d644743787b7",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMFacultyDepartment"
							},
							"rightExpression": {
								"type": 0,
								"value": "769c870b-8f57-4a54-916e-ccc0a3c047d2",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"ConsultationTabLabelGroupfae98f37": {
				"9b0c26a5-34ed-419d-a9ef-ee15278cf374": {
					"uId": "9b0c26a5-34ed-419d-a9ef-ee15278cf374",
					"enabled": false,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMFacultyDepartment"
							},
							"rightExpression": {
								"type": 0,
								"value": "1b9eaca5-6c29-45c6-b98a-bf87312eb2a0",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMFacultyDepartment"
							},
							"rightExpression": {
								"type": 0,
								"value": "5e123412-113b-4fcf-b5d9-8ba9927df06c",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMFacultyDepartment"
							},
							"rightExpression": {
								"type": 0,
								"value": "2c5ec53d-738e-4faa-8db1-06ddfe07a0ac",
								"dataValueType": 10
							}
						}
					]
				},
				"3e750f81-aa60-4e58-bbe4-dc07229326e2": {
					"uId": "3e750f81-aa60-4e58-bbe4-dc07229326e2",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "QuestionQ1Availability"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"ConsultationTabLabelGroup47fe93fa": {
				"d9c1cfd0-d182-4d68-97d9-18e129270649": {
					"uId": "d9c1cfd0-d182-4d68-97d9-18e129270649",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "QuestionQ2Availability"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"ConsultationTabLabelGroupfe5b1a5c": {
				"d344e17e-0bb8-4b3f-a512-a767cb7b7fb0": {
					"uId": "d344e17e-0bb8-4b3f-a512-a767cb7b7fb0",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "QuestionQ5Availability"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"ConsultationTabLabelGroupc0acde0a": {
				"f7f0f90c-5a32-4344-9f30-57f126068aa6": {
					"uId": "f7f0f90c-5a32-4344-9f30-57f126068aa6",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMFacultyDepartment"
							},
							"rightExpression": {
								"type": 0,
								"value": "769c870b-8f57-4a54-916e-ccc0a3c047d2",
								"dataValueType": 10
							}
						}
					]
				},
				"0ae9b8fd-19ba-4728-8daa-af42db3d19f3": {
					"uId": "0ae9b8fd-19ba-4728-8daa-af42db3d19f3",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "QuestionQ8Availability"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"ConsultationTabLabelGroup395facc9": {
				"33dfee63-92e6-4623-bd81-672f655b11ba": {
					"uId": "33dfee63-92e6-4623-bd81-672f655b11ba",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "QuestionQ6Availability"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"ConsultationTabLabelGroup93925aa1": {
				"8a178c77-8a8d-4b3a-9d58-54f8e51b23be": {
					"uId": "8a178c77-8a8d-4b3a-9d58-54f8e51b23be",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "QuestionQ7Availability"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"ConsultationTabLabelGroup61538809": {
				"8c245eb6-7b19-4993-899c-dfc308a2d939": {
					"uId": "8c245eb6-7b19-4993-899c-dfc308a2d939",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "RecommendationQ3Availability"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				},
				"f7c0c665-68fa-472e-9ce9-53a796869ea4": {
					"uId": "f7c0c665-68fa-472e-9ce9-53a796869ea4",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMFacultyDepartment"
							},
							"rightExpression": {
								"type": 0,
								"value": "5e123412-113b-4fcf-b5d9-8ba9927df06c",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMRequestType"
							},
							"rightExpression": {
								"type": 0,
								"value": "48db38d2-0edc-45ca-930f-f102d0b160cb",
								"dataValueType": 10
							}
						}
					]
				},
				"2bbf7eac-e739-44d6-a183-ac6bd1ed9b80": {
					"uId": "2bbf7eac-e739-44d6-a183-ac6bd1ed9b80",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMRequestType"
							},
							"rightExpression": {
								"type": 0,
								"value": "48db38d2-0edc-45ca-930f-f102d0b160cb",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMFacultyDepartment"
							},
							"rightExpression": {
								"type": 0,
								"value": "20dd8015-77f4-488a-902a-428e7534df5a",
								"dataValueType": 10
							}
						}
					]
				},
				"4cc57842-353d-4a0d-ba4a-02426fabe728": {
					"uId": "4cc57842-353d-4a0d-ba4a-02426fabe728",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMFacultyDepartment"
							},
							"rightExpression": {
								"type": 0,
								"value": "769c870b-8f57-4a54-916e-ccc0a3c047d2",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMRequestType"
							},
							"rightExpression": {
								"type": 0,
								"value": "48db38d2-0edc-45ca-930f-f102d0b160cb",
								"dataValueType": 10
							}
						}
					]
				},
				"57f3ea47-437e-4d52-b9c1-77e60390a165": {
					"uId": "57f3ea47-437e-4d52-b9c1-77e60390a165",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMFacultyDepartment"
							},
							"rightExpression": {
								"type": 0,
								"value": "2c5ec53d-738e-4faa-8db1-06ddfe07a0ac",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMRequestType"
							},
							"rightExpression": {
								"type": 0,
								"value": "48db38d2-0edc-45ca-930f-f102d0b160cb",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"ConsultationTabLabelGroup268c1849": {
				"ecf60582-2e40-4ee3-aa48-06392bc1d782": {
					"uId": "ecf60582-2e40-4ee3-aa48-06392bc1d782",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "RecommendationQ4Availability"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				},
				"f2b77c8f-1b71-43cd-84c0-4235f74dff65": {
					"uId": "f2b77c8f-1b71-43cd-84c0-4235f74dff65",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMRequestType"
							},
							"rightExpression": {
								"type": 0,
								"value": "48db38d2-0edc-45ca-930f-f102d0b160cb",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMFacultyDepartment"
							},
							"rightExpression": {
								"type": 0,
								"value": "2c5ec53d-738e-4faa-8db1-06ddfe07a0ac",
								"dataValueType": 10
							}
						}
					]
				},
				"2bb8198b-c5bc-4f77-a7e0-39ced7859724": {
					"uId": "2bb8198b-c5bc-4f77-a7e0-39ced7859724",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMRequestType"
							},
							"rightExpression": {
								"type": 0,
								"value": "48db38d2-0edc-45ca-930f-f102d0b160cb",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMFacultyDepartment"
							},
							"rightExpression": {
								"type": 0,
								"value": "20dd8015-77f4-488a-902a-428e7534df5a",
								"dataValueType": 10
							}
						}
					]
				},
				"99b9f3c5-2ecf-4759-bc55-919beaf14dcb": {
					"uId": "99b9f3c5-2ecf-4759-bc55-919beaf14dcb",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMRequestType"
							},
							"rightExpression": {
								"type": 0,
								"value": "48db38d2-0edc-45ca-930f-f102d0b160cb",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMFacultyDepartment"
							},
							"rightExpression": {
								"type": 0,
								"value": "769c870b-8f57-4a54-916e-ccc0a3c047d2",
								"dataValueType": 10
							}
						}
					]
				},
				"70bfb29e-9976-462e-b460-a3f09c2e1036": {
					"uId": "70bfb29e-9976-462e-b460-a3f09c2e1036",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMRequestType"
							},
							"rightExpression": {
								"type": 0,
								"value": "48db38d2-0edc-45ca-930f-f102d0b160cb",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMFacultyDepartment"
							},
							"rightExpression": {
								"type": 0,
								"value": "5e123412-113b-4fcf-b5d9-8ba9927df06c",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"ConsultationTabLabelGroup7d73c054": {
				"d1b6bf09-730e-48ce-aae6-2b997ed8e15e": {
					"uId": "d1b6bf09-730e-48ce-aae6-2b997ed8e15e",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "RecommendationQ1Availability"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"ConsultationTabLabelGroup4f98e555": {
				"16a49e72-ef9c-43ec-a924-cd03e29c9822": {
					"uId": "16a49e72-ef9c-43ec-a924-cd03e29c9822",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "RecommendationQ2Availability"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"ASUFeedbackforConcluding": {
				"e2b87683-4446-45e1-9957-8d6f2e730573": {
					"uId": "e2b87683-4446-45e1-9957-8d6f2e730573",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMOpenForEditing"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"Questions": {
				"8472635b-2c2a-4b46-a80e-67fb4e03ba62": {
					"uId": "8472635b-2c2a-4b46-a80e-67fb4e03ba62",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "QuestionQ1Availability"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "QuestionQ2Availability"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "QuestionQ3Availability"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "QuestionQ4Availability"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "QuestionQ5Availability"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "QuestionQ6Availability"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "QuestionQ7Availability"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "QuestionQ8Availability"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "RecommendationQ1Availability"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "RecommendationQ2Availability"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "RecommendationQ3Availability"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "RecommendationQ4Availability"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			}
		}/**SCHEMA_BUSINESS_RULES*/,
		methods: {
			checkQuestionQ1Availability: function(){
				//Please provide any comment on the operational feasibility (programming type, academic level, proposed faculties, term alignment, etc.) of the proposed partnership
				var agreementType = this.get("SMAgreementType");
				var requestType = this.get("SMRequestType");
				var facDept = this.get("SMFacultyDepartment");
				if(!!requestType && (requestType.value === '7c6d730d-eecb-41c0-8e5d-3e9cc4167da9' || requestType.value ===  '445e5c6f-8b13-4a3d-b2fc-7e350d828005'))
					{
						if(!!agreementType && (agreementType.value==='22f47e79-2f60-404a-a4de-20ee3e9834fb' || agreementType.value==='ab7596e6-5575-454d-bef6-57be8fa484bd' || agreementType.value==='9da17d88-7f99-4cab-a609-9928c0a69261' || agreementType.value==='2f6032a4-58ae-470e-967d-2af7578165f5')){
							if(!!facDept && (facDept.value === '1b9eaca5-6c29-45c6-b98a-bf87312eb2a0' || facDept.value === '5e123412-113b-4fcf-b5d9-8ba9927df06c' || facDept.value === '2c5ec53d-738e-4faa-8db1-06ddfe07a0ac')){
								this.set("QuestionQ1Availability", true);
							}
							
						   }
					}
				
			},
			
			checkQuestionQ2Availability: function(){
				//Please advise on the anticipated student interest and engagement in proposed partnership
				var agreementType = this.get("SMAgreementType");
				var requestType = this.get("SMRequestType");
				var facDept = this.get("SMFacultyDepartment");
				if(!!requestType && (requestType.value === '7c6d730d-eecb-41c0-8e5d-3e9cc4167da9' || requestType.value ===  '445e5c6f-8b13-4a3d-b2fc-7e350d828005')) {
					if(!!agreementType && (agreementType.value==='22f47e79-2f60-404a-a4de-20ee3e9834fb' || agreementType.value==='9da17d88-7f99-4cab-a609-9928c0a69261' || agreementType.value==='2f6032a4-58ae-470e-967d-2af7578165f5')){
						if(!!facDept && (facDept.value === '1b9eaca5-6c29-45c6-b98a-bf87312eb2a0' || facDept.value === '5e123412-113b-4fcf-b5d9-8ba9927df06c' || facDept.value === '2c5ec53d-738e-4faa-8db1-06ddfe07a0ac')){
							this.set("QuestionQ2Availability", true);
						}
						
					   }
				}
				
			},
			
			checkQuestionQ3Availability: function(){
				//Advise on the anticipated faculty member interest and engagement in the proposed agreement
				this.set("QuestionQ3Availability", false);
			},
			
			checkQuestionQ4Availability: function(){
				//Advise on the anticipated interest from Co-op students to participate in the proposed hybrid student mobility agreement
				var agreementType = this.get("SMAgreementType");
				var requestType = this.get("SMRequestType");
				var facDept = this.get("SMFacultyDepartment");
				if(!!requestType && (requestType.value === '7c6d730d-eecb-41c0-8e5d-3e9cc4167da9' || requestType.value ===  '445e5c6f-8b13-4a3d-b2fc-7e350d828005')) {
					if(!!agreementType && agreementType.value==='9da17d88-7f99-4cab-a609-9928c0a69261'){
					var subCualification = this.get("SMSubClassification");
					if(!!subCualification && subCualification.displayValue.includes("Hybrid")){
						if(!!facDept && (facDept.value === '769c870b-8f57-4a54-916e-ccc0a3c047d2' || facDept.value === '9b31767f-3bb0-4556-8149-d107cab1efd0')){
							this.set("QuestionQ4Availability", true);
						}
					}
				   }
				}
				
			},
			
			checkQuestionQ5Availability: function(){
				//Comment on the academic alignment and quality of the proposed agreement and partner.
				var agreementType = this.get("SMAgreementType");
				var requestType = this.get("SMRequestType");
				var facDept = this.get("SMFacultyDepartment");
				if(!!requestType &&  (requestType.value === '7c6d730d-eecb-41c0-8e5d-3e9cc4167da9' || requestType.value ===  '445e5c6f-8b13-4a3d-b2fc-7e350d828005'))
					{
						if(!!agreementType && (agreementType.value==='22f47e79-2f60-404a-a4de-20ee3e9834fb' || agreementType.value=== '16257282-5967-4b4e-ab6c-9805cf300fdb' || agreementType.value=== 'ab7596e6-5575-454d-bef6-57be8fa484bd' || agreementType.value==='9da17d88-7f99-4cab-a609-9928c0a69261')){
							if(!!facDept && (facDept.value === '1b9eaca5-6c29-45c6-b98a-bf87312eb2a0' || facDept.value === '5e123412-113b-4fcf-b5d9-8ba9927df06c' || facDept.value === '2c5ec53d-738e-4faa-8db1-06ddfe07a0ac')){
								this.set("QuestionQ5Availability", true);
							}
						   }
					}
			},
			
			checkQuestionQ6Availability: function(){
				//Comment on the research alignment and quality of the proposed agreement and partner.
				var agreementType = this.get("SMAgreementType");
				var requestType = this.get("SMRequestType");
				var facDept = this.get("SMFacultyDepartment");
				if(!!requestType && (requestType.value === '7c6d730d-eecb-41c0-8e5d-3e9cc4167da9' || requestType.value ===  '445e5c6f-8b13-4a3d-b2fc-7e350d828005'))
					{
						if(!!agreementType && (agreementType.value==='ab7596e6-5575-454d-bef6-57be8fa484bd' || agreementType.value=== '16257282-5967-4b4e-ab6c-9805cf300fdb')){
							if(!!facDept && (facDept.value === '2c5ec53d-738e-4faa-8db1-06ddfe07a0ac' || facDept.value === '20dd8015-77f4-488a-902a-428e7534df5a')){
								this.set("QuestionQ6Availability", true);
							}
							
						   }
					}
			},
			
			checkQuestionQ7Availability: function(){
				//what Current/expected outcomes and strategic alignment
				var agreementType = this.get("SMAgreementType");
				var requestType = this.get("SMRequestType");
				var facDept = this.get("SMFacultyDepartment");
				if(!!requestType && (requestType.value === '7c6d730d-eecb-41c0-8e5d-3e9cc4167da9' || requestType.value ===  '445e5c6f-8b13-4a3d-b2fc-7e350d828005'))
					{
						if(!!agreementType && (agreementType.value==='22f47e79-2f60-404a-a4de-20ee3e9834fb' || agreementType.value==='ab7596e6-5575-454d-bef6-57be8fa484bd' || agreementType.value=== '16257282-5967-4b4e-ab6c-9805cf300fdb' || agreementType.value==='9da17d88-7f99-4cab-a609-9928c0a69261' || agreementType.value==='2f6032a4-58ae-470e-967d-2af7578165f5')){
							if(!!facDept && (facDept.value === '1b9eaca5-6c29-45c6-b98a-bf87312eb2a0' || facDept.value === '5e123412-113b-4fcf-b5d9-8ba9927df06c' || facDept.value === '2c5ec53d-738e-4faa-8db1-06ddfe07a0ac')){
								this.set("QuestionQ7Availability", true);
							}
							
						   }
					}
			},
			
			checkQuestionQ8Availability: function(){
				//Please advise if there are any other relevant factors that Waterloo International should be aware when considering this proposed partnership related to the scope of your department and role.
				var agreementType = this.get("SMAgreementType");
				var requestType = this.get("SMRequestType");
				var facDept = this.get("SMFacultyDepartment");
				if(!!requestType && (requestType.value === '7c6d730d-eecb-41c0-8e5d-3e9cc4167da9' || requestType.value ===  '445e5c6f-8b13-4a3d-b2fc-7e350d828005'))
					{
						if(!!agreementType && (agreementType.value==='22f47e79-2f60-404a-a4de-20ee3e9834fb' || agreementType.value==='ab7596e6-5575-454d-bef6-57be8fa484bd' || agreementType.value=== '16257282-5967-4b4e-ab6c-9805cf300fdb' || agreementType.value==='9da17d88-7f99-4cab-a609-9928c0a69261' || agreementType.value==='2f6032a4-58ae-470e-967d-2af7578165f5')){
							if(!!facDept && (facDept.value === '1b9eaca5-6c29-45c6-b98a-bf87312eb2a0' || facDept.value === '769c870b-8f57-4a54-916e-ccc0a3c047d2' || facDept.value === '9b31767f-3bb0-4556-8149-d107cab1efd0' || facDept.value === '5e123412-113b-4fcf-b5d9-8ba9927df06c' || facDept.value === '2c5ec53d-738e-4faa-8db1-06ddfe07a0ac')){
								this.set("QuestionQ8Availability", true);
							}
							
						   }
					}
			},
			
			checkRecommendationQ1Availability: function(){
				this.set("RecommendationQ1Availability", false);
			},
			
			checkRecommendationQ2Availability: function(){
				this.set("RecommendationQ2Availability", false);
			},
			
			checkRecommendationQ3Availability: function(){
				var agreementType = this.get("SMAgreementType");
				var requestType = this.get("SMRequestType");
				var facDept = this.get("SMFacultyDepartment");
				if(!!requestType && requestType.value === '48db38d2-0edc-45ca-930f-f102d0b160cb')
					{
						if(!!agreementType && (agreementType.value==='22f47e79-2f60-404a-a4de-20ee3e9834fb' || agreementType.value==='ab7596e6-5575-454d-bef6-57be8fa484bd' || agreementType.value=== '16257282-5967-4b4e-ab6c-9805cf300fdb' || agreementType.value==='9da17d88-7f99-4cab-a609-9928c0a69261' || agreementType.value==='2f6032a4-58ae-470e-967d-2af7578165f5')){
							if(!!facDept && (facDept.value === '1b9eaca5-6c29-45c6-b98a-bf87312eb2a0' || facDept.value === '769c870b-8f57-4a54-916e-ccc0a3c047d2' || facDept.value === '9b31767f-3bb0-4556-8149-d107cab1efd0' || facDept.value === '5e123412-113b-4fcf-b5d9-8ba9927df06c' || facDept.value === '2c5ec53d-738e-4faa-8db1-06ddfe07a0ac' || facDept.value === '20dd8015-77f4-488a-902a-428e7534df5a')){
								this.set("RecommendationQ3Availability", true);
							}
							
						   }
					}
			},
			
			checkRecommendationQ4Availability: function(){
				var agreementType = this.get("SMAgreementType");
				var requestType = this.get("SMRequestType");
				var facDept = this.get("SMFacultyDepartment");
				if(!!requestType && requestType.value === '48db38d2-0edc-45ca-930f-f102d0b160cb')
					{
						if(!!agreementType && (agreementType.value==='22f47e79-2f60-404a-a4de-20ee3e9834fb' || agreementType.value==='ab7596e6-5575-454d-bef6-57be8fa484bd' || agreementType.value=== '16257282-5967-4b4e-ab6c-9805cf300fdb' || agreementType.value==='9da17d88-7f99-4cab-a609-9928c0a69261' || agreementType.value==='2f6032a4-58ae-470e-967d-2af7578165f5')){
							if(!!facDept && (facDept.value === '1b9eaca5-6c29-45c6-b98a-bf87312eb2a0' || facDept.value === '769c870b-8f57-4a54-916e-ccc0a3c047d2' || facDept.value === '9b31767f-3bb0-4556-8149-d107cab1efd0' || facDept.value === '5e123412-113b-4fcf-b5d9-8ba9927df06c' || facDept.value === '2c5ec53d-738e-4faa-8db1-06ddfe07a0ac' || facDept.value === '20dd8015-77f4-488a-902a-428e7534df5a')){
								this.set("RecommendationQ4Availability", true);
							}
							
						   }
					}
			},
			checkFacultyConfirmation: function(){
				var agreementType = this.get("SMFacultyDepartment");
				var requestType = this.get("SMRequestType");
				if(!!requestType && (requestType.value !== '14d10e70-49a9-43a7-b83f-658549383b36' || requestType.value !==  '00b95a4b-a60b-419f-86a9-47ee62dc5374'))
					{
						if(!!agreementType && (agreementType.value==='1b9eaca5-6c29-45c6-b98a-bf87312eb2a0' || agreementType.value==='769c870b-8f57-4a54-916e-ccc0a3c047d2' || agreementType.value=== '5e123412-113b-4fcf-b5d9-8ba9927df06c' || agreementType.value=== '2c5ec53d-738e-4faa-8db1-06ddfe07a0ac' || agreementType.value==='20dd8015-77f4-488a-902a-428e7534df5a')){
							this.set("FacilityParticipationConfirmation", true);
						   }
					}
			},
			checkFacilityFeedback: function(){
				var agreementType = this.get("SMFacultyDepartment");
				var requestType = this.get("SMRequestType");
				var facultyConfirmation = this.get("SMFacultyParticipationConfirmation");
				if(!!requestType && (requestType.value !== '14d10e70-49a9-43a7-b83f-658549383b36' || requestType.value !==  '00b95a4b-a60b-419f-86a9-47ee62dc5374'))
					{
						if(!!agreementType && (agreementType.value==='1b9eaca5-6c29-45c6-b98a-bf87312eb2a0' || agreementType.value==='769c870b-8f57-4a54-916e-ccc0a3c047d2' || agreementType.value=== '5e123412-113b-4fcf-b5d9-8ba9927df06c' || agreementType.value=== '2c5ec53d-738e-4faa-8db1-06ddfe07a0ac' || agreementType.value==='20dd8015-77f4-488a-902a-428e7534df5a')){
							if(!!facultyConfirmation && facultyConfirmation.displayValue === "No"){
								this.set("FacilityFeedback", true);
							}
							
						   }
					}
			},
			
			onEntityInitialized: function(){
				this.sendRequestForQuestionAvailability(this);
				this.callParent(arguments);
				this.checkIfTheButtonIsAvailable();	
				this.checkIfTheButtonsIsVisible();
				this.setCardLockoutStatus();
			},
			
			questionModeration: function(){
				var studentSuccessOffice = this.get("MemberOfStudentSuccessOffice");
				var coOp = this.get("MemberOfCoop");
				var registrOffice = this.get("MemberOfRegistarOffice");
				var graduateStudies = this.get("MemberOfGraduteStudies");
				var officeOfRessearch = this.get("MemberOfOfficeOfRessearch");
				var faculties = this.get("MemberOfFaculties");
			},
			
			checkRoleAfterQuestionAvailabilityCheck: function(question, attribute, scope){
				/*
				var esq = Ext.create("Terrasoft.EntitySchemaQuery", {
					rootSchemaName: "SMQestionRequestTypesRoleConnection"
				});
				var recId = esq.addColumn("Id");
				esq.filters.addItem(esq.createColumnFilterWithParameter(
					this.Terrasoft.ComparisonType.EQUAL, "SMQuestions.Name", qestion));
				esq.filters.addItem(esq.createColumnFilterWithParameter(
					this.Terrasoft.ComparisonType.EQUAL, "SMAgreementType.Id", agreementType));
				esq.getEntityCollection(function(result) {
					if (result.success && result.collection.getCount() > 0) {
						scope.set("RecommendationQ4Availability", true);
					} 
				}, this);
				*/
			},
			
			CompleateConsultationFunction: function(){
				var message = this.get("Resources.Strings.SMLabelAfterFinishingConsultation");
				this.showConfirmationDialog(message, function(returnCode) {
					if (returnCode === this.Terrasoft.MessageBoxButtons.NO.returnCode) {
						return;
					} else {
						this.set("SMResponseSubmitted", true);
						this.set("CloseForEditingAvailable", false);
						this.set("OpenForEditingAvailable", true);
						this.set("SMOpenForEditing", false);
						this.save();
					}
					var ii = this.Terrasoft.MessageBoxButtons.NO.returnCode;
				}, [this.Terrasoft.MessageBoxButtons.YES.returnCode, this.Terrasoft.MessageBoxButtons.NO.returnCode]);
			},
			
			sendRequestForQuestionAvailability: function(scope){
				scope.checkQuestionQ1Availability();
				scope.checkQuestionQ2Availability();
				scope.checkQuestionQ3Availability();
				scope.checkQuestionQ4Availability();
				scope.checkQuestionQ5Availability();
				scope.checkQuestionQ6Availability();
				scope.checkQuestionQ7Availability();
				scope.checkQuestionQ8Availability();
				scope.checkRecommendationQ1Availability();
				scope.checkRecommendationQ2Availability();
				scope.checkRecommendationQ3Availability();
				scope.checkRecommendationQ4Availability();
			},
			
			checkUsersRolesSM: function(){
				var scope = this;
				var currentUser = Terrasoft.SysValue.CURRENT_USER.value;
				var esq = Ext.create("Terrasoft.EntitySchemaQuery", {
					rootSchemaName: "SysUserInRole"
				});
				var userIsInTheGroup = false;
				esq.addColumn("SysRole.Name");
				esq.addColumn("SysRole.ParentRole.Name");
				esq.addColumn("SysUser");
				var esqFirstFilter = esq.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL,
				"SysUser", currentUser);
				esq.filters.add("esqFirstFilter", esqFirstFilter);
				esq.getEntityCollection(function (result) {
					if (!result.success) {
						window.console.log("Вы не находитесь в указанной группе");
						return;
					}
					if (result.success){
						if(result.collection.collection.length >= 1){
							result.collection.each(function (item) {
								if(item.get("SysRole.ParentRole.Name") == "Faculties"){
									scope.set("MemberOfFaculties", true);
									scope.sendRequestForQuestionAvailability(scope);
									return;
								}
								if(item.get("SysRole.Name") == "System Administrators"){
									scope.set("MemberOfFaculties", true);
									scope.sendRequestForQuestionAvailability(scope);
									return;
								}
								if(item.get("SysRole.ParentRole.Name") == "ASU"){
									if(item.get("SysRole.Name") == "Co-op"){
										scope.set("MemberOfCoop", true);
										scope.sendRequestForQuestionAvailability(scope);
										return;
										
									}
									if(item.get("SysRole.Name") == "Student Success Office"){
										scope.set("MemberOfStudentSuccessOffice", true);
										scope.sendRequestForQuestionAvailability(scope);
										return;
									}
									if(item.get("SysRole.Name") == "Graduate Studies and Post Doctoral Affairs"){
										scope.set("MemberOfGraduteStudies", true);
										scope.sendRequestForQuestionAvailability(scope);
										return;
									}
									if(item.get("SysRole.Name") == "Office of Research"){
										scope.set("MemberOfOfficeOfRessearch", true);
										scope.sendRequestForQuestionAvailability(scope);
										return;
									}
									else {
										scope.set("MemberOfRegistarOffice", true);
										scope.sendRequestForQuestionAvailability(scope);
										return;
									}
								}
							});
						}
					}
				}, this);
					
			},
			
			checkIfTheButtonsIsVisible: function(){
				var scope = this;
				var currentUser = Terrasoft.SysValue.CURRENT_USER.value;
				var esq = Ext.create("Terrasoft.EntitySchemaQuery", {
					rootSchemaName: "SysUserInRole"
				});
				var userIsInTheGroup = false;
				esq.addColumn("SysRole");
				esq.addColumn("SysUser");
				var esqFirstFilter = esq.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL,
				"SysUser", currentUser);
				var esqSecondFilter = esq.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL,
				"SysRole.Name", "Agreements Team");
				esq.filters.add("esqFirstFilter", esqFirstFilter);
				esq.filters.add("esqSecondFilter", esqSecondFilter);
				esq.getEntityCollection(function (result) {
					if (!result.success) {
						window.console.log("Visible request failed");
						return;
					}
					if (result.success){
						if(result.collection.collection.length === 1){
							scope.set("ButtonsAreVisibleForTheGroup", true);
						}
					}
				}, this);
			},
			
			
			
			checkIfTheButtonIsAvailable: function(){
				if (this.get("SMOpenForEditing")){
					this.set("OpenForEditingAvailable", false);
					this.set("CloseForEditingAvailable", true);
				} else {
					this.set("OpenForEditingAvailable", true);
					this.set("CloseForEditingAvailable", false);
				}
			},
			
			openForEditing: function(){
                this.set("CloseForEditingAvailable", true);
				this.set("OpenForEditingAvailable", false);
				this.set("SMOpenForEditing", true);
			},
			
			lockForEditing: function(){
                this.set("CloseForEditingAvailable", false);
				this.set("OpenForEditingAvailable", true);
				this.set("SMOpenForEditing", false);
			},
			
			setCardLockoutStatus: function() {
				const isResponseSubmitted = this.$SMResponseSubmitted;
				const isOpenForEditing = this.$SMOpenForEditing;
				this.set("IsModelItemsEnabled", !isResponseSubmitted || isOpenForEditing);
			}
		},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "merge",
				"name": "CardContentWrapper",
				"values": {
					"generator": "DisableControlsGenerator.generatePartial"
				}
			},
			{
				"operation": "insert",
				"name": "lockForEditing",
				"values": {
					"itemType": 5,
					"caption": {
						"bindTo": "Resources.Strings.lockForEditing"
					},
					"click": {
						"bindTo": "lockForEditing"
					},
					"enabled": {
						"bindTo": "CloseForEditingAvailable"
					},
					"visible": {
						"bindTo": "ButtonsAreVisibleForTheGroup"
					},
					"styles": {
						"margin-left": "100px"
					},
					"classes": {
						"textClass": [
							"actions-button-margin-right"
						]
					},
					"style": "blue"
				},
				"parentName": "LeftContainer",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "openForEditing",
				"values": {
					"itemType": 5,
					"caption": {
						"bindTo": "Resources.Strings.openForEditing"
					},
					"click": {
						"bindTo": "openForEditing"
					},
					"enabled": {
						"bindTo": "OpenForEditingAvailable"
					},
					"visible": {
						"bindTo": "ButtonsAreVisibleForTheGroup"
					},
					"style": "green"
				},
				"parentName": "LeftContainer",
				"propertyName": "items",
				"index": 8
			},
			{
				"operation": "insert",
				"name": "WatAgreementecd83b32-04b5-45ad-98ef-1f416ab5420e",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "WatAgreement",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "SMAgreementTypeb7b7537f-8b09-4729-bf47-8f9787b38fcc",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "SMAgreementType",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "SMRequestType3f5e81ce-131b-4ea4-b74f-0c27d46352a8",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "SMRequestType",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "SMClassification668c7798-978d-409b-b487-0371a984bffd",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "SMClassification",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "SMSubClassification6569386f-af46-4ead-961c-b941ebbaa195",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "SMSubClassification",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "SMFacultyaaec6b43-5857-4f85-a5f2-ee7fe560cfa9",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "SMFaculty",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "SMFacultyDepartmentb76cb78a-a776-42f3-83d6-2b59594cb0a6",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "SMFacultyDepartment",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "SMDeadlineDatecccface1-369c-48b5-a63f-a13d1d24771b",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "SMDeadlineDate",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "SMNotifiedDate819052b9-6391-4ad8-8806-a0157d2a87aa",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "SMNotifiedDate",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "SMResponseSubmitted5cf49810-b1ee-413f-94a4-3b9e6f2fd5f6",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Header"
					},
					"bindTo": "SMResponseSubmitted",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "ConsultationTabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.ConsultationTabLabelTabCaption"
					},
					"items": [],
					"order": 0
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "ConsultationTabLabelGroupfae98f37",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.ConsultationTabLabelGroupfae98f37GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "ConsultationTabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "ConsultationTabLabelGridLayoute32057b4",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "ConsultationTabLabelGroupfae98f37",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "QuestionQ1OperationalFeasibilityec38a009-5065-4eb9-8219-2f33c7d4799e",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "ConsultationTabLabelGridLayoute32057b4"
					},
					"wrapClass": [
						"wider-label"
					],
					"bindTo": "QuestionQ1OperationalFeasibility",
					"enabled": true,
					"visible": {
						"bindTo": "QuestionQ1Availability"
					},
					"contentType": 0,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.QuestionQ1OperationalFeasibilityec38a00950654eb982192f33c7d4799eLabelCaption"
						}
					}
				},
				"parentName": "ConsultationTabLabelGridLayoute32057b4",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "ConsultationTabLabelGroup47fe93fa",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.ConsultationTabLabelGroup47fe93faGroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "ConsultationTabLabel",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "ConsultationTabLabelGridLayout9c556d07",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "ConsultationTabLabelGroup47fe93fa",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "QuestionQ2AnticipatedStudentInterest350efe7a-238e-4291-9e55-6f9767d792ff",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "ConsultationTabLabelGridLayout9c556d07"
					},
					"wrapClass": [
						"wider-label"
					],
					"bindTo": "QuestionQ2AnticipatedStudentInterest",
					"contentType": 0,
					"enabled": true,
					"visible": {
						"bindTo": "QuestionQ2Availability"
					},
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.QuestionQ2AnticipatedStudentInterest350efe7a238e42919e556f9767d792ffLabelCaption"
						}
					}
				},
				"parentName": "ConsultationTabLabelGridLayout9c556d07",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "ConsultationTabLabelGroupdd3cc404",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.ConsultationTabLabelGroupdd3cc404GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "ConsultationTabLabel",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "ConsultationTabLabelGridLayout1bb1aa81",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "ConsultationTabLabelGroupdd3cc404",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "QuestionQ4AnticipatedInterestfromCoOp3cd5259b-b3f6-4af0-8c55-2257d3119007",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "ConsultationTabLabelGridLayout1bb1aa81"
					},
					"bindTo": "QuestionQ4AnticipatedInterestfromCoOp",
					"wrapClass": [
						"wider-label"
					],
					"enabled": true,
					"visible": {
						"bindTo": "QuestionQ4Availability"
					},
					"contentType": 0,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.QuestionQ4AnticipatedInterestfromCoOp3cd5259bb3f64af08c552257d3119007LabelCaption"
						}
					}
				},
				"parentName": "ConsultationTabLabelGridLayout1bb1aa81",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "ConsultationTabLabelGroupfe5b1a5c",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.ConsultationTabLabelGroupfe5b1a5cGroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "ConsultationTabLabel",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "ConsultationTabLabelGridLayouta1525d02",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "ConsultationTabLabelGroupfe5b1a5c",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "QuestionQ5QualityAcademicAgreementandPartner53e77e81-183c-4d54-97e1-63c07080ff62",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "ConsultationTabLabelGridLayouta1525d02"
					},
					"wrapClass": [
						"wider-label"
					],
					"bindTo": "QuestionQ5QualityAcademicAgreementandPartner",
					"enabled": true,
					"visible": {
						"bindTo": "QuestionQ5Availability"
					},
					"contentType": 0,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.QuestionQ5QualityAcademicAgreementandPartner53e77e81183c4d5497e163c07080ff62LabelCaption"
						}
					}
				},
				"parentName": "ConsultationTabLabelGridLayouta1525d02",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "ConsultationTabLabelGroup395facc9",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.ConsultationTabLabelGroup395facc9GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "ConsultationTabLabel",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "ConsultationTabLabelGridLayout954f8145",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "ConsultationTabLabelGroup395facc9",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "QuestionQ6ResearchAgreementandPartner33828693-896d-4c12-8453-9a85e0b5e10a",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "ConsultationTabLabelGridLayout954f8145"
					},
					"wrapClass": [
						"wider-label"
					],
					"bindTo": "QuestionQ6ResearchAgreementandPartner",
					"enabled": true,
					"visible": {
						"bindTo": "QuestionQ6Availability"
					},
					"contentType": 0,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.QuestionQ6ResearchAgreementandPartner33828693896d4c1284539a85e0b5e10aLabelCaption"
						}
					}
				},
				"parentName": "ConsultationTabLabelGridLayout954f8145",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "ConsultationTabLabelGroup93925aa1",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.ConsultationTabLabelGroup93925aa1GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "ConsultationTabLabel",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "ConsultationTabLabelGridLayout40dc9774",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "ConsultationTabLabelGroup93925aa1",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "QuestionQ7CurrentandExpectedOutcomesb74d520b-1519-46c6-b5bb-1135a3f3b1b7",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "ConsultationTabLabelGridLayout40dc9774"
					},
					"bindTo": "QuestionQ7CurrentandExpectedOutcomes",
					"wrapClass": [
						"wider-label"
					],
					"enabled": true,
					"visible": {
						"bindTo": "QuestionQ7Availability"
					},
					"contentType": 0,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.QuestionQ7CurrentandExpectedOutcomesb74d520b151946c6b5bb1135a3f3b1b7LabelCaption"
						}
					}
				},
				"parentName": "ConsultationTabLabelGridLayout40dc9774",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "ConsultationTabLabelGroupc0acde0a",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.ConsultationTabLabelGroupc0acde0aGroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "ConsultationTabLabel",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "ConsultationTabLabelGridLayout39f9f323",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "ConsultationTabLabelGroupc0acde0a",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "QuestionQ8RelevantFactors044a966d-9370-47a7-b1f0-f8c3ca14c703",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "ConsultationTabLabelGridLayout39f9f323"
					},
					"bindTo": "QuestionQ8RelevantFactors",
					"wrapClass": [
						"wider-label"
					],
					"enabled": true,
					"visible": {
						"bindTo": "QuestionQ8Availability"
					},
					"contentType": 0,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.QuestionQ8RelevantFactors044a966d937047a7b1f0f8c3ca14c703LabelCaption"
						}
					}
				},
				"parentName": "ConsultationTabLabelGridLayout39f9f323",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "ConsultationTabLabelGroup7d73c054",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.ConsultationTabLabelGroup7d73c054GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "ConsultationTabLabel",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "ConsultationTabLabelGridLayout1269f506",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "ConsultationTabLabelGroup7d73c054",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "FacultyAgreementforConcluding74c7afaa-aa11-4672-a1c7-9d503cac0614",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "ConsultationTabLabelGridLayout1269f506"
					},
					"bindTo": "FacultyAgreementforConcluding",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.FacultyAgreementforConcluding74c7afaaaa114672a1c79d503cac0614LabelCaption"
						}
					},
					"enabled": true
				},
				"parentName": "ConsultationTabLabelGridLayout1269f506",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "ConsultationTabLabelGroup4f98e555",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.ConsultationTabLabelGroup4f98e555GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "ConsultationTabLabel",
				"propertyName": "items",
				"index": 8
			},
			{
				"operation": "insert",
				"name": "ConsultationTabLabelGridLayout34d0f334",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "ConsultationTabLabelGroup4f98e555",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "FacultyFeedbackforConcluding873f2e70-df00-4223-9ae4-a0411f185ff7",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "ConsultationTabLabelGridLayout34d0f334"
					},
					"bindTo": "FacultyFeedbackforConcluding",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.FacultyFeedbackforConcluding873f2e70df0042239ae4a0411f185ff7LabelCaption"
						}
					},
					"enabled": true
				},
				"parentName": "ConsultationTabLabelGridLayout34d0f334",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "ConsultationTabLabelGroup61538809",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.ConsultationTabLabelGroup61538809GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "ConsultationTabLabel",
				"propertyName": "items",
				"index": 9
			},
			{
				"operation": "insert",
				"name": "ConsultationTabLabelGridLayoute891c459",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "ConsultationTabLabelGroup61538809",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "ASUAgreementConcluding9506e588-c028-4d77-a0b9-00a751bfaa80",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "ConsultationTabLabelGridLayoute891c459"
					},
					"wrapClass": [
						"wider-label"
					],
					"bindTo": "ASUAgreementConcluding",
					"enabled": true,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.ASUAgreementConcluding9506e588c0284d77a0b900a751bfaa80LabelCaption"
						}
					}
				},
				"parentName": "ConsultationTabLabelGridLayoute891c459",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "ConsultationTabLabelGroup268c1849",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.ConsultationTabLabelGroup268c1849GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "ConsultationTabLabel",
				"propertyName": "items",
				"index": 10
			},
			{
				"operation": "insert",
				"name": "ConsultationTabLabelGridLayout59d3bf40",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "ConsultationTabLabelGroup268c1849",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "ASUFeedbackforConcluding17bfc5ff-c4ff-413b-8317-ff2c17647631",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "ConsultationTabLabelGridLayout59d3bf40"
					},
					"wrapClass": [
						"wider-label"
					],
					"bindTo": "ASUFeedbackforConcluding",
					"enabled": true,
					"contentType": 0,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.ASUFeedbackforConcluding17bfc5ffc4ff413b8317ff2c17647631LabelCaption"
						}
					}
				},
				"parentName": "ConsultationTabLabelGridLayout59d3bf40",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "SMAdditionalQuestionDetail",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "ConsultationTabLabel",
				"propertyName": "items",
				"index": 11
			},
			{
				"operation": "insert",
				"name": "Questions",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.QuestionsGroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "ConsultationTabLabel",
				"propertyName": "items",
				"index": 12
			},
			{
				"operation": "insert",
				"name": "ConsultationTabLabelGridLayoutd537e37c",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Questions",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CompleateFormButton",
				"values": {
					"layout": {
						"colSpan": 10,
						"rowSpan": 1,
						"column": 6,
						"row": 0,
						"layoutName": "ConsultationTabLabelGridLayoutd537e37c"
					},
					"itemType": 5,
					"caption": {
						"bindTo": "Resources.Strings.CompleateConsultation"
					},
					"click": {
						"bindTo": "CompleateConsultationFunction"
					},
					"enabled": {
						"bindTo": "SMResponseSubmitted",
						"bindConfig": {
							"converter": "invertBooleanValue"
						}
					},
					"styles": {
						"margin-left": "100px",
						"margin-right": "0px"
					},
					"style": "green"
				},
				"parentName": "ConsultationTabLabelGridLayoutd537e37c",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "NotesAndFilesTab",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.NotesAndFilesTabCaption"
					},
					"items": [],
					"order": 1
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Files",
				"values": {
					"itemType": 2
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "NotesControlGroup",
				"values": {
					"itemType": 15,
					"caption": {
						"bindTo": "Resources.Strings.NotesGroupCaption"
					},
					"items": []
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Notes",
				"values": {
					"bindTo": "Notes",
					"dataValueType": 1,
					"contentType": 4,
					"layout": {
						"column": 0,
						"row": 0,
						"colSpan": 24
					},
					"labelConfig": {
						"visible": false
					},
					"controlConfig": {
						"imageLoaded": {
							"bindTo": "insertImagesToNotes"
						},
						"images": {
							"bindTo": "NotesImagesCollection"
						}
					}
				},
				"parentName": "NotesControlGroup",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "merge",
				"name": "ESNTab",
				"values": {
					"order": 2
				}
			}
		]/**SCHEMA_DIFF*/
	};
});
