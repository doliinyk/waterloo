define("SMNewConsultedFaculties2Page", ["ProcessModuleUtilities", "MaskHelper", "css!ConsultedASUCSS"], function(ProcessModuleUtilities, MaskHelper) {
	return {
		entitySchemaName: "SMNewConsultedFaculties",
		attributes: {
			"OpenForEditingAvailable": {
				 "dataValueType": Terrasoft.DataValueType.BOOLEAN,
				 "type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				 "value": false
			},
			"CloseForEditingAvailable": {
				 "dataValueType": Terrasoft.DataValueType.BOOLEAN,
				 "type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				 "value": false
			},
			"ButtonsAreVisibleForTheGroup": {
				 "dataValueType": Terrasoft.DataValueType.BOOLEAN,
				 "type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				 "value": false
			},
			"QuestionQ2Availability": {
				 "dataValueType": Terrasoft.DataValueType.BOOLEAN,
				 "type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				 "value": false
			},
			"QuestionQ3Availability": {
				 "dataValueType": Terrasoft.DataValueType.BOOLEAN,
				 "type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				 "value": false
			},
			"QuestionQ4Availability": {
				 "dataValueType": Terrasoft.DataValueType.BOOLEAN,
				 "type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				 "value": false
			},
			"QuestionQ5Availability": {
				 "dataValueType": Terrasoft.DataValueType.BOOLEAN,
				 "type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				 "value": false
			},
			"QuestionQ6Availability": {
				 "dataValueType": Terrasoft.DataValueType.BOOLEAN,
				 "type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				 "value": false
			},
			"QuestionQ7Availability": {
				 "dataValueType": Terrasoft.DataValueType.BOOLEAN,
				 "type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				 "value": false
			},
			"QuestionQ1Availability": {
				 "dataValueType": Terrasoft.DataValueType.BOOLEAN,
				 "type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				 "value": false
			},
			"QuestionQ8Availability": {
				 "dataValueType": Terrasoft.DataValueType.BOOLEAN,
				 "type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				 "value": false
			},
			"RecommendationQ1Availability": {
				 "dataValueType": Terrasoft.DataValueType.BOOLEAN,
				 "type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				 "value": false
			},
			"RecommendationQ2Availability": {
				 "dataValueType": Terrasoft.DataValueType.BOOLEAN,
				 "type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				 "value": false
			},
			"RecommendationQ3Availability": {
				 "dataValueType": Terrasoft.DataValueType.BOOLEAN,
				 "type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				 "value": false
			},
			"RecommendationQ4Availability": {
				 "dataValueType": Terrasoft.DataValueType.BOOLEAN,
				 "type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				 "value": false
			},
			"IsModelItemsEnabled": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"value": true,
				"dependencies": [{
					"columns": ["SMResponseSubmitted", "SMOpenForEditing"],
					"methodName": "setCardLockoutStatus"
				}]
			},
			"FacilityParticipationConfirmation": {
				 "dataValueType": Terrasoft.DataValueType.BOOLEAN,
				 "type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				 "value": false
			},
			"FacilityFeedback": {
				 "dataValueType": Terrasoft.DataValueType.BOOLEAN,
				 "type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				 "value": false
			}
		},
		messages: {
			"ReloadQuestions" : {
                    "mode": Terrasoft.MessageMode.BROADCAST,
                    "direction": Terrasoft.MessageDirectionType.SUBSCRIBE
                },
		},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{
			"Files": {
				"schemaName": "FileDetailV2",
				"entitySchemaName": "SMNewConsultedFacultiesFile",
				"filter": {
					"masterColumn": "Id",
					"detailColumn": "SMNewConsultedFaculties"
				}
			},
			"SMAdditionalQuestionDetail": {
				"schemaName": "SMSchema148cf4faDetail",
				"entitySchemaName": "SMAdditionalQuestion",
				"filter": {
					"detailColumn": "SMConsultedFaculty",
					"masterColumn": "Id"
				}
			}
		}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{
			"SMParticipationQ2": {
				"34cb0247-2f66-4963-ab50-93338449f37f": {
					"uId": "34cb0247-2f66-4963-ab50-93338449f37f",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMParticipationO2"
							},
							"rightExpression": {
								"type": 0,
								"value": "b78f528a-965d-4c10-82cf-9a3968d8bd3d",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"SMParticipationA2": {
				"70f4a94d-c8bd-4821-ade6-1d84cb8d6ad5": {
					"uId": "70f4a94d-c8bd-4821-ade6-1d84cb8d6ad5",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMParticipationO2"
							},
							"rightExpression": {
								"type": 0,
								"value": "b78f528a-965d-4c10-82cf-9a3968d8bd3d",
								"dataValueType": 10
							}
						}
					]
				},
				"35dfc046-62cd-4e29-861f-bd9aab182819": {
					"uId": "35dfc046-62cd-4e29-861f-bd9aab182819",
					"enabled": false,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMParticipationO1"
							},
							"rightExpression": {
								"type": 0,
								"value": "b78f528a-965d-4c10-82cf-9a3968d8bd3d",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"SMQuestions": {
				"1104de91-b07f-472b-96da-99dbc002f878": {
					"uId": "1104de91-b07f-472b-96da-99dbc002f878",
					"enabled": false,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMParticipationO1"
							},
							"rightExpression": {
								"type": 0,
								"value": "37000588-4cf6-4516-8b1d-d73424373e00",
								"dataValueType": 10
							}
						}
					]
				},
				"a7b9b868-c66c-4669-a735-a7ae34482aad": {
					"uId": "a7b9b868-c66c-4669-a735-a7ae34482aad",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMFacultyParticipationConfirmation"
							},
							"rightExpression": {
								"type": 0,
								"value": "37000588-4cf6-4516-8b1d-d73424373e00",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMFacultyParticipationConfirmation"
							},
							"rightExpression": {
								"type": 0,
								"value": "b78f528a-965d-4c10-82cf-9a3968d8bd3d",
								"dataValueType": 10
							}
						}
					]
				},
				"e39c0ec2-8818-4e8e-a7c2-ce4f4cec4bdc": {
					"uId": "e39c0ec2-8818-4e8e-a7c2-ce4f4cec4bdc",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "FacilityParticipationConfirmation"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "QuestionQ1Availability"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "QuestionQ2Availability"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "QuestionQ3Availability"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "QuestionQ4Availability"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "QuestionQ5Availability"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "QuestionQ6Availability"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "QuestionQ7Availability"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "QuestionQ8Availability"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "RecommendationQ1Availability"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "RecommendationQ2Availability"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "RecommendationQ3Availability"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "RecommendationQ4Availability"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"RecoomendationAndFeedback": {
				"1598daaf-f7e4-48e7-8479-b9f2fba2ee5c": {
					"uId": "1598daaf-f7e4-48e7-8479-b9f2fba2ee5c",
					"enabled": false,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMParticipationO1"
							},
							"rightExpression": {
								"type": 0,
								"value": "37000588-4cf6-4516-8b1d-d73424373e00",
								"dataValueType": 10
							}
						}
					]
				},
				"e18527da-5e97-4e3e-a661-bbf7b754eb63": {
					"uId": "e18527da-5e97-4e3e-a661-bbf7b754eb63",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMFacultyParticipationConfirmation"
							},
							"rightExpression": {
								"type": 0,
								"value": "37000588-4cf6-4516-8b1d-d73424373e00",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMRequestType"
							},
							"rightExpression": {
								"type": 0,
								"value": "48db38d2-0edc-45ca-930f-f102d0b160cb",
								"dataValueType": 10
							}
						}
					]
				},
				"dd8d38be-755b-43fd-b044-387dbac628ba": {
					"uId": "dd8d38be-755b-43fd-b044-387dbac628ba",
					"enabled": false,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMFacultyParticipationConfirmation"
							},
							"rightExpression": {
								"type": 0,
								"value": "37000588-4cf6-4516-8b1d-d73424373e00",
								"dataValueType": 10
							}
						}
					]
				},
				"026839e5-e6c1-4f29-b1a2-f983097a88e1": {
					"uId": "026839e5-e6c1-4f29-b1a2-f983097a88e1",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "RecommendationQ3Availability"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"NonParticipationFeedback": {
				"010b51b7-c43c-4092-a0ef-edc737285b92": {
					"uId": "010b51b7-c43c-4092-a0ef-edc737285b92",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMFacultyParticipationConfirmation"
							},
							"rightExpression": {
								"type": 0,
								"value": "b78f528a-965d-4c10-82cf-9a3968d8bd3d",
								"dataValueType": 10
							}
						}
					]
				},
				"adb4ed4d-6846-4612-8fab-fb31f055c049": {
					"uId": "adb4ed4d-6846-4612-8fab-fb31f055c049",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMOpenForEditing"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"QuestionQ1": {
				"50a7eacd-1bae-4835-83e9-a513931af4a4": {
					"uId": "50a7eacd-1bae-4835-83e9-a513931af4a4",
					"enabled": false,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMFacultyParticipationConfirmation"
							},
							"rightExpression": {
								"type": 0,
								"value": "37000588-4cf6-4516-8b1d-d73424373e00",
								"dataValueType": 10
							}
						}
					]
				},
				"8ee35659-133d-42a3-b433-4f6ced689f47": {
					"uId": "8ee35659-133d-42a3-b433-4f6ced689f47",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMOpenForEditing"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"ConsultationTabLabelGroupfae98f37": {
				"576444e1-0c4b-4fd2-81f2-f4dad332eb78": {
					"uId": "576444e1-0c4b-4fd2-81f2-f4dad332eb78",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMFacultyParticipationConfirmation"
							},
							"rightExpression": {
								"type": 0,
								"value": "37000588-4cf6-4516-8b1d-d73424373e00",
								"dataValueType": 10
							}
						}
					]
				},
				"89012a95-3803-4d49-a621-45f7867c0b46": {
					"uId": "89012a95-3803-4d49-a621-45f7867c0b46",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "QuestionQ1Availability"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMFacultyParticipationConfirmation"
							},
							"rightExpression": {
								"type": 0,
								"value": "37000588-4cf6-4516-8b1d-d73424373e00",
								"dataValueType": 10
							}
						}
					]
				},
				"8ac6f4d1-6194-4b8d-b24e-950c7741f4c3": {
					"uId": "8ac6f4d1-6194-4b8d-b24e-950c7741f4c3",
					"enabled": false,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMFacultyParticipationConfirmation"
							},
							"rightExpression": {
								"type": 0,
								"value": "37000588-4cf6-4516-8b1d-d73424373e00",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMRequestType"
							},
							"rightExpression": {
								"type": 0,
								"value": "445e5c6f-8b13-4a3d-b2fc-7e350d828005",
								"dataValueType": 10
							}
						}
					]
				},
				"999ca361-0e63-48c3-8f47-0b69227ad7a5": {
					"uId": "999ca361-0e63-48c3-8f47-0b69227ad7a5",
					"enabled": false,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMAgreementType"
							},
							"rightExpression": {
								"type": 0,
								"value": "22f47e79-2f60-404a-a4de-20ee3e9834fb",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMAgreementType"
							},
							"rightExpression": {
								"type": 0,
								"value": "ab7596e6-5575-454d-bef6-57be8fa484bd",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMAgreementType"
							},
							"rightExpression": {
								"type": 0,
								"value": "9da17d88-7f99-4cab-a609-9928c0a69261",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMAgreementType"
							},
							"rightExpression": {
								"type": 0,
								"value": "2f6032a4-58ae-470e-967d-2af7578165f5",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"ConsultationTabLabelGroup47fe93fa": {
				"5cb75ac5-6340-438c-9339-65bb5b94f8b7": {
					"uId": "5cb75ac5-6340-438c-9339-65bb5b94f8b7",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMFacultyParticipationConfirmation"
							},
							"rightExpression": {
								"type": 0,
								"value": "37000588-4cf6-4516-8b1d-d73424373e00",
								"dataValueType": 10
							}
						}
					]
				},
				"e07602ab-c82f-43da-bc74-a57210dda5a7": {
					"uId": "e07602ab-c82f-43da-bc74-a57210dda5a7",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "QuestionQ2Availability"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMFacultyParticipationConfirmation"
							},
							"rightExpression": {
								"type": 0,
								"value": "37000588-4cf6-4516-8b1d-d73424373e00",
								"dataValueType": 10
							}
						}
					]
				},
				"00d626a8-26c9-4c3d-a0c9-8868dcc55b51": {
					"uId": "00d626a8-26c9-4c3d-a0c9-8868dcc55b51",
					"enabled": false,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMFacultyParticipationConfirmation"
							},
							"rightExpression": {
								"type": 0,
								"value": "37000588-4cf6-4516-8b1d-d73424373e00",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMRequestType"
							},
							"rightExpression": {
								"type": 0,
								"value": "445e5c6f-8b13-4a3d-b2fc-7e350d828005",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"QuestionQ5": {
				"565bc89e-4451-4a56-b016-79e460ed14e6": {
					"uId": "565bc89e-4451-4a56-b016-79e460ed14e6",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMFacultyParticipationConfirmation"
							},
							"rightExpression": {
								"type": 0,
								"value": "37000588-4cf6-4516-8b1d-d73424373e00",
								"dataValueType": 10
							}
						}
					]
				},
				"f94a5839-5bdf-4ef0-a66f-e9d60c448129": {
					"uId": "f94a5839-5bdf-4ef0-a66f-e9d60c448129",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMOpenForEditing"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"ConsultationTabLabelGroupc0acde0a": {
				"b749043a-bbc8-459c-8a4b-a9c6039e6efe": {
					"uId": "b749043a-bbc8-459c-8a4b-a9c6039e6efe",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMFacultyParticipationConfirmation"
							},
							"rightExpression": {
								"type": 0,
								"value": "37000588-4cf6-4516-8b1d-d73424373e00",
								"dataValueType": 10
							}
						}
					]
				},
				"77a9169f-4bb2-4f37-b771-112d29589d3e": {
					"uId": "77a9169f-4bb2-4f37-b771-112d29589d3e",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "QuestionQ6Availability"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMFacultyParticipationConfirmation"
							},
							"rightExpression": {
								"type": 0,
								"value": "37000588-4cf6-4516-8b1d-d73424373e00",
								"dataValueType": 10
							}
						}
					]
				},
				"17805aed-f262-49a7-814e-9fcbfeccb4a0": {
					"uId": "17805aed-f262-49a7-814e-9fcbfeccb4a0",
					"enabled": false,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMFacultyParticipationConfirmation"
							},
							"rightExpression": {
								"type": 0,
								"value": "37000588-4cf6-4516-8b1d-d73424373e00",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMRequestType"
							},
							"rightExpression": {
								"type": 0,
								"value": "445e5c6f-8b13-4a3d-b2fc-7e350d828005",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"ConsultationTabLabelGroupba4e7bbd": {
				"880324a1-6069-4c8e-8479-09bcd1203554": {
					"uId": "880324a1-6069-4c8e-8479-09bcd1203554",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMFacultyParticipationConfirmation"
							},
							"rightExpression": {
								"type": 0,
								"value": "37000588-4cf6-4516-8b1d-d73424373e00",
								"dataValueType": 10
							}
						}
					]
				},
				"2e4d5884-9fac-481c-8420-c870a446fe63": {
					"uId": "2e4d5884-9fac-481c-8420-c870a446fe63",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "QuestionQ8Availability"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMFacultyParticipationConfirmation"
							},
							"rightExpression": {
								"type": 0,
								"value": "37000588-4cf6-4516-8b1d-d73424373e00",
								"dataValueType": 10
							}
						}
					]
				},
				"714425a0-0fe5-47b5-a610-21b780b0903a": {
					"uId": "714425a0-0fe5-47b5-a610-21b780b0903a",
					"enabled": false,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMFacultyParticipationConfirmation"
							},
							"rightExpression": {
								"type": 0,
								"value": "37000588-4cf6-4516-8b1d-d73424373e00",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMRequestType"
							},
							"rightExpression": {
								"type": 0,
								"value": "445e5c6f-8b13-4a3d-b2fc-7e350d828005",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"ConsultationTabLabelGroup61538809": {
				"33d41ce4-63cc-401c-a056-520f13c7e6db": {
					"uId": "33d41ce4-63cc-401c-a056-520f13c7e6db",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMFacultyParticipationConfirmation"
							},
							"rightExpression": {
								"type": 0,
								"value": "37000588-4cf6-4516-8b1d-d73424373e00",
								"dataValueType": 10
							}
						}
					]
				},
				"e24c9ce3-1ecc-41f0-979e-9cfa0f4bbb2d": {
					"uId": "e24c9ce3-1ecc-41f0-979e-9cfa0f4bbb2d",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "QuestionQ7Availability"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMFacultyParticipationConfirmation"
							},
							"rightExpression": {
								"type": 0,
								"value": "37000588-4cf6-4516-8b1d-d73424373e00",
								"dataValueType": 10
							}
						}
					]
				},
				"71d4e40d-ff74-4e96-8944-3b0f4c62ff68": {
					"uId": "71d4e40d-ff74-4e96-8944-3b0f4c62ff68",
					"enabled": false,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMFacultyParticipationConfirmation"
							},
							"rightExpression": {
								"type": 0,
								"value": "37000588-4cf6-4516-8b1d-d73424373e00",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMRequestType"
							},
							"rightExpression": {
								"type": 0,
								"value": "445e5c6f-8b13-4a3d-b2fc-7e350d828005",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"ConsultationTabLabelGroupdd3cc404": {
				"c7f0090a-05df-42d5-b663-102952925b57": {
					"uId": "c7f0090a-05df-42d5-b663-102952925b57",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMFacultyParticipationConfirmation"
							},
							"rightExpression": {
								"type": 0,
								"value": "37000588-4cf6-4516-8b1d-d73424373e00",
								"dataValueType": 10
							}
						}
					]
				},
				"57c30d45-2860-4b38-bd95-e52610486ac7": {
					"uId": "57c30d45-2860-4b38-bd95-e52610486ac7",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "QuestionQ3Availability"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMFacultyParticipationConfirmation"
							},
							"rightExpression": {
								"type": 0,
								"value": "37000588-4cf6-4516-8b1d-d73424373e00",
								"dataValueType": 10
							}
						}
					]
				},
				"4aaf8c00-3b45-4daf-a837-b899f23435e6": {
					"uId": "4aaf8c00-3b45-4daf-a837-b899f23435e6",
					"enabled": false,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMFacultyParticipationConfirmation"
							},
							"rightExpression": {
								"type": 0,
								"value": "37000588-4cf6-4516-8b1d-d73424373e00",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMRequestType"
							},
							"rightExpression": {
								"type": 0,
								"value": "445e5c6f-8b13-4a3d-b2fc-7e350d828005",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"ConsultationTabLabelGroup02833302": {
				"78d1105f-d867-4c5d-ae7c-858dd2a6a7de": {
					"uId": "78d1105f-d867-4c5d-ae7c-858dd2a6a7de",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMFacultyParticipationConfirmation"
							},
							"rightExpression": {
								"type": 0,
								"value": "37000588-4cf6-4516-8b1d-d73424373e00",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMRequestType"
							},
							"rightExpression": {
								"type": 0,
								"value": "48db38d2-0edc-45ca-930f-f102d0b160cb",
								"dataValueType": 10
							}
						}
					]
				},
				"38914c7b-b446-4e4b-844e-ea58ef72d461": {
					"uId": "38914c7b-b446-4e4b-844e-ea58ef72d461",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "RecommendationQ4Availability"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"ConsultationTabLabelGroupfe5b1a5c": {
				"fef5cb45-b563-4d32-aef1-31c36dfb289c": {
					"uId": "fef5cb45-b563-4d32-aef1-31c36dfb289c",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMFacultyParticipationConfirmation"
							},
							"rightExpression": {
								"type": 0,
								"value": "37000588-4cf6-4516-8b1d-d73424373e00",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"QuestionQ2": {
				"e1462a82-a7ea-4eb0-932e-5967f9204af6": {
					"uId": "e1462a82-a7ea-4eb0-932e-5967f9204af6",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMOpenForEditing"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"QuestionQ3": {
				"ba357d77-36d0-4213-8222-1a6d1b9e943d": {
					"uId": "ba357d77-36d0-4213-8222-1a6d1b9e943d",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMOpenForEditing"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"QuestionQ4": {
				"5d39ad81-a195-4516-a35b-a0a53d07c0f0": {
					"uId": "5d39ad81-a195-4516-a35b-a0a53d07c0f0",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMOpenForEditing"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"QuestionQ6": {
				"8dd10395-796d-400f-85a4-e451a363ddc1": {
					"uId": "8dd10395-796d-400f-85a4-e451a363ddc1",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMOpenForEditing"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"QuestionQ7": {
				"73d91b4f-5529-416b-9570-2d95f9a90d65": {
					"uId": "73d91b4f-5529-416b-9570-2d95f9a90d65",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMOpenForEditing"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"QuestionQ8": {
				"97d3d87e-38bf-49f1-b054-f35b7ae83658": {
					"uId": "97d3d87e-38bf-49f1-b054-f35b7ae83658",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMOpenForEditing"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"RecommendationO1": {
				"883bf2a5-6353-415b-bf28-001eda8fb828": {
					"uId": "883bf2a5-6353-415b-bf28-001eda8fb828",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMOpenForEditing"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"RecommendationQ2": {
				"5cb797e6-ed28-4901-aca8-8d2e22fd5abc": {
					"uId": "5cb797e6-ed28-4901-aca8-8d2e22fd5abc",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMOpenForEditing"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"ConsultationTabLabelGroup93925aa1": {
				"3bf42965-7ec7-4f20-a108-f17a78224079": {
					"uId": "3bf42965-7ec7-4f20-a108-f17a78224079",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "QuestionQ5Availability"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMFacultyParticipationConfirmation"
							},
							"rightExpression": {
								"type": 0,
								"value": "37000588-4cf6-4516-8b1d-d73424373e00",
								"dataValueType": 10
							}
						}
					]
				},
				"233b54e3-3c89-406a-a655-69a13b13b260": {
					"uId": "233b54e3-3c89-406a-a655-69a13b13b260",
					"enabled": false,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMFacultyParticipationConfirmation"
							},
							"rightExpression": {
								"type": 0,
								"value": "37000588-4cf6-4516-8b1d-d73424373e00",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMRequestType"
							},
							"rightExpression": {
								"type": 0,
								"value": "445e5c6f-8b13-4a3d-b2fc-7e350d828005",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"SMPartisipation": {
				"6d52aa54-c62b-4796-be44-49aa0e8e94c4": {
					"uId": "6d52aa54-c62b-4796-be44-49aa0e8e94c4",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "FacilityParticipationConfirmation"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"SMFacultyParticipationConfirmation": {
				"d758b6ac-44e4-4a18-8bb7-54e6d5c3aaeb": {
					"uId": "d758b6ac-44e4-4a18-8bb7-54e6d5c3aaeb",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMOpenForEditing"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"ConsultationTabLabelGroupd4419c1f": {
				"59864242-abd3-468c-a50c-af65d7f00398": {
					"uId": "59864242-abd3-468c-a50c-af65d7f00398",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "RecommendationQ1Availability"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"FacultyAgreementforConcluding": {
				"51d86999-6107-4fbb-b85d-90fa0dc56232": {
					"uId": "51d86999-6107-4fbb-b85d-90fa0dc56232",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMOpenForEditing"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"ConsultationTabLabelGroupde2553c0": {
				"bf5d7093-68ea-4174-86a8-f9300f86580b": {
					"uId": "bf5d7093-68ea-4174-86a8-f9300f86580b",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "RecommendationQ2Availability"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"FacultyFeedbackforConcluding": {
				"3f2841d2-b3bf-4eec-8c35-2f51f967c4e4": {
					"uId": "3f2841d2-b3bf-4eec-8c35-2f51f967c4e4",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMOpenForEditing"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"ConsultationTabLabelGroup644b6665": {
				"e3942fb2-59ca-46da-aab7-dd33e7812024": {
					"uId": "e3942fb2-59ca-46da-aab7-dd33e7812024",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "QuestionQ4Availability"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMFacultyParticipationConfirmation"
							},
							"rightExpression": {
								"type": 0,
								"value": "37000588-4cf6-4516-8b1d-d73424373e00",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"ConsultationTabLabelGroup4ae38965": {
				"ab845374-9c34-4b2f-bc18-bcc1ad365827": {
					"uId": "ab845374-9c34-4b2f-bc18-bcc1ad365827",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "SMFacultyParticipationConfirmation"
							},
							"rightExpression": {
								"type": 0,
								"value": "b78f528a-965d-4c10-82cf-9a3968d8bd3d",
								"dataValueType": 10
							}
						}
					]
				}
			}
		}/**SCHEMA_BUSINESS_RULES*/,
		methods: {
			subscribeSandboxEvents: function() {
                this.callParent(arguments);
				this.sandbox.subscribe("ReloadQuestions",this.reloadDetailASU,this);
			},
			
			CompleateConsultationFunction: function(){
				var message = this.get("Resources.Strings.SMLabelAfterFinishingConsultation");
				this.showConfirmationDialog(message, function(returnCode) {
					if (returnCode === this.Terrasoft.MessageBoxButtons.NO.returnCode) {
						return;
					} else {
						this.set("SMResponseSubmitted", true);
						this.set("CloseForEditingAvailable", false);
						this.set("OpenForEditingAvailable", true);
						this.set("SMOpenForEditing", false);
						this.save();
					}
					var ii = this.Terrasoft.MessageBoxButtons.NO.returnCode;
				}, [this.Terrasoft.MessageBoxButtons.YES.returnCode, this.Terrasoft.MessageBoxButtons.NO.returnCode]);
			},
			
			reloadDetailASU: function(){
				window.console.log("ASU");
				this.updateDetail({
                        detail: "SchemaConsultationQuestions"
                    });
			},
			
			checkIfTheButtonIsAvailable: function(){
				if (this.get("SMOpenForEditing")){
					this.set("OpenForEditingAvailable", false);
					this.set("CloseForEditingAvailable", true);
				} else {
					this.set("OpenForEditingAvailable", true);
					this.set("CloseForEditingAvailable", false);
				}
			},
			
			openForEditing: function(){
                this.set("CloseForEditingAvailable", true);
				this.set("OpenForEditingAvailable", false);
				this.set("SMOpenForEditing", true);
			},
			
			lockForEditing: function(){
                this.set("CloseForEditingAvailable", false);
				this.set("OpenForEditingAvailable", true);
				this.set("SMOpenForEditing", false);
			},
			
			checkQuestionQ1Availability: function(){
				//Please provide any comment on the operational feasibility (programming type, academic level, proposed faculties, term alignment, etc.) of the proposed partnership
				var agreementType = this.get("SMAgreementType");
				var requestType = this.get("SMRequestType");
				if(!!requestType && (requestType.value === '7c6d730d-eecb-41c0-8e5d-3e9cc4167da9' || requestType.value ===  '445e5c6f-8b13-4a3d-b2fc-7e350d828005'))
					{
						if(!!agreementType && (agreementType.value==='22f47e79-2f60-404a-a4de-20ee3e9834fb' || agreementType.value==='ab7596e6-5575-454d-bef6-57be8fa484bd' || agreementType.value==='9da17d88-7f99-4cab-a609-9928c0a69261' || agreementType.value==='2f6032a4-58ae-470e-967d-2af7578165f5')){
							this.set("QuestionQ1Availability", true);
							
						   }
					}
				
			},
			
			checkQuestionQ2Availability: function(){
				//Please advise on the anticipated student interest and engagement in proposed partnership
				var agreementType = this.get("SMAgreementType");
				var requestType = this.get("SMRequestType");
				if(!!requestType && (requestType.value === '7c6d730d-eecb-41c0-8e5d-3e9cc4167da9' || requestType.value ===  '445e5c6f-8b13-4a3d-b2fc-7e350d828005')) {
					if(!!agreementType && (agreementType.value==='22f47e79-2f60-404a-a4de-20ee3e9834fb' || agreementType.value==='9da17d88-7f99-4cab-a609-9928c0a69261' || agreementType.value==='2f6032a4-58ae-470e-967d-2af7578165f5')){
						this.set("QuestionQ2Availability", true);
						
					   }
				}
				
			},
			
			checkQuestionQ3Availability: function(){
				//Advise on the anticipated faculty member interest and engagement in the proposed agreement
				
				var agreementType = this.get("SMAgreementType");
				var requestType = this.get("SMRequestType");
				if(!!requestType && (requestType.value === '7c6d730d-eecb-41c0-8e5d-3e9cc4167da9' || requestType.value ===  '445e5c6f-8b13-4a3d-b2fc-7e350d828005')) {
					if(!!agreementType && (agreementType.value==='22f47e79-2f60-404a-a4de-20ee3e9834fb' || agreementType.value==='16257282-5967-4b4e-ab6c-9805cf300fdb')){
						this.set("QuestionQ3Availability", true);
						
					   }
				}
			},
			
			checkQuestionQ4Availability: function(){
				//Advise on the anticipated interest from Co-op students to participate in the proposed hybrid student mobility agreement
				var agreementType = this.get("SMAgreementType");
				var requestType = this.get("SMRequestType");
				if(!!requestType && (requestType.value === '7c6d730d-eecb-41c0-8e5d-3e9cc4167da9' || requestType.value ===  '445e5c6f-8b13-4a3d-b2fc-7e350d828005')) {
					if(!!agreementType && agreementType.value==='9da17d88-7f99-4cab-a609-9928c0a69261'){
					var subCualification = this.get("SMSubClassification");
					if(!!subCualification && subCualification.displayValue.includes("Hybrid")){
						this.set("QuestionQ4Availability", true);
					}
				   }
				}
				
			},
			
			checkQuestionQ5Availability: function(){
				//Comment on the academic alignment and quality of the proposed agreement and partner.
				var agreementType = this.get("SMAgreementType");
				var requestType = this.get("SMRequestType");
				if(!!requestType && (requestType.value === '7c6d730d-eecb-41c0-8e5d-3e9cc4167da9' || requestType.value ===  '445e5c6f-8b13-4a3d-b2fc-7e350d828005'))
					{
						if(!!agreementType && (agreementType.value==='22f47e79-2f60-404a-a4de-20ee3e9834fb' || agreementType.value=== '16257282-5967-4b4e-ab6c-9805cf300fdb' || agreementType.value=== 'ab7596e6-5575-454d-bef6-57be8fa484bd' || agreementType.value==='9da17d88-7f99-4cab-a609-9928c0a69261')){
							this.set("QuestionQ5Availability", true);
						   }
					}
			},
			
			checkQuestionQ6Availability: function(){
				//Comment on the research alignment and quality of the proposed agreement and partner.
				var agreementType = this.get("SMAgreementType");
				var requestType = this.get("SMRequestType");
				var facDept = this.get("SMFacultyDepartment");
				if(!!requestType && (requestType.value === '7c6d730d-eecb-41c0-8e5d-3e9cc4167da9' || requestType.value ===  '445e5c6f-8b13-4a3d-b2fc-7e350d828005'))
					{
						if(!!agreementType && (agreementType.value==='ab7596e6-5575-454d-bef6-57be8fa484bd' || agreementType.value=== '16257282-5967-4b4e-ab6c-9805cf300fdb')){
							this.set("QuestionQ6Availability", true);
							
						   }
					}
			},
			
			checkQuestionQ7Availability: function(){
				//what Current/expected outcomes and strategic alignment
				var agreementType = this.get("SMAgreementType");
				var requestType = this.get("SMRequestType");
				if(!!requestType && (requestType.value === '7c6d730d-eecb-41c0-8e5d-3e9cc4167da9' || requestType.value ===  '445e5c6f-8b13-4a3d-b2fc-7e350d828005'))
					{
						if(!!agreementType && (agreementType.value==='22f47e79-2f60-404a-a4de-20ee3e9834fb' || agreementType.value==='ab7596e6-5575-454d-bef6-57be8fa484bd' || agreementType.value=== '16257282-5967-4b4e-ab6c-9805cf300fdb' || agreementType.value==='9da17d88-7f99-4cab-a609-9928c0a69261' || agreementType.value==='2f6032a4-58ae-470e-967d-2af7578165f5')){
							this.set("QuestionQ7Availability", true);
							
						   }
					}
			},
			
			checkQuestionQ8Availability: function(){
				//Please advise if there are any other relevant factors that Waterloo International should be aware when considering this proposed partnership related to the scope of your department and role.
				var agreementType = this.get("SMAgreementType");
				var requestType = this.get("SMRequestType");
				if(!!requestType && (requestType.value === '7c6d730d-eecb-41c0-8e5d-3e9cc4167da9' || requestType.value ===  '445e5c6f-8b13-4a3d-b2fc-7e350d828005'))
					{
						if(!!agreementType && (agreementType.value==='22f47e79-2f60-404a-a4de-20ee3e9834fb' || agreementType.value==='ab7596e6-5575-454d-bef6-57be8fa484bd' || agreementType.value=== '16257282-5967-4b4e-ab6c-9805cf300fdb' || agreementType.value==='9da17d88-7f99-4cab-a609-9928c0a69261' || agreementType.value==='2f6032a4-58ae-470e-967d-2af7578165f5')){
							this.set("QuestionQ8Availability", true);
							
						   }
					}
			},
			
			checkRecommendationQ1Availability: function(){
				var agreementType = this.get("SMAgreementType");
				var requestType = this.get("SMRequestType");
				if(!!requestType && requestType.value === '48db38d2-0edc-45ca-930f-f102d0b160cb')
					{
						if(!!agreementType && (agreementType.value==='22f47e79-2f60-404a-a4de-20ee3e9834fb' || agreementType.value==='ab7596e6-5575-454d-bef6-57be8fa484bd' || agreementType.value=== '16257282-5967-4b4e-ab6c-9805cf300fdb' || agreementType.value==='9da17d88-7f99-4cab-a609-9928c0a69261' || agreementType.value==='2f6032a4-58ae-470e-967d-2af7578165f5')){
							this.set("RecommendationQ1Availability", true);
							
						   }
					}
				
			},
			
			checkRecommendationQ2Availability: function(){
				var agreementType = this.get("SMAgreementType");
				var requestType = this.get("SMRequestType");
				if(!!requestType && requestType.value === '48db38d2-0edc-45ca-930f-f102d0b160cb')
					{
						if(!!agreementType && (agreementType.value==='22f47e79-2f60-404a-a4de-20ee3e9834fb' || agreementType.value==='ab7596e6-5575-454d-bef6-57be8fa484bd' || agreementType.value=== '16257282-5967-4b4e-ab6c-9805cf300fdb' || agreementType.value==='9da17d88-7f99-4cab-a609-9928c0a69261' || agreementType.value==='2f6032a4-58ae-470e-967d-2af7578165f5')){
							this.set("RecommendationQ2Availability", true);
							
						   }
					}
				
			},
			
			checkRecommendationQ3Availability: function(){
				this.set("RecommendationQ3Availability", false);
			},
			
			checkRecommendationQ4Availability: function(){
				this.set("RecommendationQ4Availability", false);
				
			},
			checkFacultyConfirmation: function(){
				var agreementType = this.get("SMAgreementType");
				var requestType = this.get("SMRequestType");
				if(!!requestType && (requestType.value === '7c6d730d-eecb-41c0-8e5d-3e9cc4167da9' || requestType.value ===  '445e5c6f-8b13-4a3d-b2fc-7e350d828005'))
					{
						if(!!agreementType && (agreementType.value==='22f47e79-2f60-404a-a4de-20ee3e9834fb' || agreementType.value==='16257282-5967-4b4e-ab6c-9805cf300fdb' || agreementType.value=== 'ab7596e6-5575-454d-bef6-57be8fa484bd' || agreementType.value=== '9da17d88-7f99-4cab-a609-9928c0a69261' || agreementType.value==='2f6032a4-58ae-470e-967d-2af7578165f5')){
							this.set("FacilityParticipationConfirmation", true);
						   }
					}
			},
			checkFacilityFeedback: function(){
				var agreementType = this.get("SMAgreementType");
				var requestType = this.get("SMRequestType");
				var facultyConfirmation = this.get('SMFacultyParticipationConfirmation');
				if(!!requestType && (requestType.value === '7c6d730d-eecb-41c0-8e5d-3e9cc4167da9' || requestType.value ===  '445e5c6f-8b13-4a3d-b2fc-7e350d828005'))
					{
						if(!!agreementType && (agreementType.value==='22f47e79-2f60-404a-a4de-20ee3e9834fb' || agreementType.value==='16257282-5967-4b4e-ab6c-9805cf300fdb' || agreementType.value=== 'ab7596e6-5575-454d-bef6-57be8fa484bd' || agreementType.value=== '9da17d88-7f99-4cab-a609-9928c0a69261' || agreementType.value==='2f6032a4-58ae-470e-967d-2af7578165f5')){
							if(!!facultyConfirmation && facultyConfirmation.displayValue === "No"){
								this.set("FacilityFeedback", true);
							}
							
						   }
					}
			},
			
			onEntityInitialized: function(){
				this.callParent(arguments);
				this.checkFacultyConfirmation();
				this.checkFacilityFeedback();
				this.checkIfTheButtonIsAvailable();	
				this.checkIfTheButtonsIsVisible();
				this.checkQuestionQ1Availability();
				this.checkQuestionQ2Availability();
				this.checkQuestionQ3Availability();
				this.checkQuestionQ4Availability();
				this.checkQuestionQ5Availability();
				this.checkQuestionQ6Availability();
				this.checkQuestionQ7Availability();
				this.checkQuestionQ8Availability();
				this.checkRecommendationQ1Availability();
				this.checkRecommendationQ2Availability();
				this.checkRecommendationQ3Availability();
				this.checkRecommendationQ4Availability();
				this.setCardLockoutStatus();
			},
			
			checkIfTheButtonsIsVisible: function(){
				var scope = this;
				var currentUser = Terrasoft.SysValue.CURRENT_USER.value;
				var esq = Ext.create("Terrasoft.EntitySchemaQuery", {
					rootSchemaName: "SysUserInRole"
				});
				var userIsInTheGroup = false;
				esq.addColumn("SysRole");
				esq.addColumn("SysUser");
				var esqFirstFilter = esq.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL,
				"SysUser", currentUser);
				var esqSecondFilter = esq.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL,
				"SysRole.Name", "Agreements Team");
				esq.filters.add("esqFirstFilter", esqFirstFilter);
				esq.filters.add("esqSecondFilter", esqSecondFilter);
				esq.getEntityCollection(function (result) {
					if (!result.success) {
						// обработка/логирование ошибки, например
						window.console.log("Visible request failed");
						return;
					}
					if (result.success){
						if(result.collection.collection.length === 1){
							scope.set("ButtonsAreVisibleForTheGroup", true);
						}
					}
				}, this);
			},
			
		/*	checkIfTheButtonIsAvailable: function(){
				var scope = this;
				var esq = Ext.create("Terrasoft.EntitySchemaQuery", {
					rootSchemaName: "SMConsultationQuestions"
				});
				esq.addColumn("SMOpenForEditing");

				var esqFirstFilter = esq.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL,
					"SMConsultedFaculties", this.get("Id"));
				esq.filters.add("esqFirstFilter", esqFirstFilter);
				esq.getEntityCollection(function (result) {
					if (!result.success) {
						// обработка/логирование ошибки, например
						window.console.log("Request is failed");
						return;
					}
					result.collection.each(function (item) {
						if(item.get("SMOpenForEditing") == true){
							scope.set("CloseForEditingAvailable", true);
							scope.set("OpenForEditingAvailable", false);
						} else {
							scope.set("CloseForEditingAvailable", false);
							scope.set("OpenForEditingAvailable", true);
						}
					});
				}, this);
			},*/
			setCardLockoutStatus: function() {
				const isResponseSubmitted = this.$SMResponseSubmitted;
				const isOpenForEditing = this.$SMOpenForEditing;
				this.set("IsModelItemsEnabled", !isResponseSubmitted || isOpenForEditing);
			}
		},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "merge",
				"name": "CardContentWrapper",
				"values": {
					"generator": "DisableControlsGenerator.generatePartial"
				}
			},
			{
				"operation": "insert",
				"name": "lockForEditing",
				"values": {
					"itemType": 5,
					"caption": {
						"bindTo": "Resources.Strings.lockForEditing"
					},
					"click": {
						"bindTo": "lockForEditing"
					},
					"enabled": {
						"bindTo": "CloseForEditingAvailable"
					},
					"visible": {
						"bindTo": "ButtonsAreVisibleForTheGroup"
					},
					"styles": {
						"margin-left": "100px"
					},
					"classes": {
						"textClass": [
							"actions-button-margin-right"
						]
					},
					"style": "blue"
				},
				"parentName": "LeftContainer",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "openForEditing",
				"values": {
					"itemType": 5,
					"caption": {
						"bindTo": "Resources.Strings.openForEditing"
					},
					"click": {
						"bindTo": "openForEditing"
					},
					"enabled": {
						"bindTo": "OpenForEditingAvailable"
					},
					"visible": {
						"bindTo": "ButtonsAreVisibleForTheGroup"
					},
					"style": "green"
				},
				"parentName": "LeftContainer",
				"propertyName": "items",
				"index": 8
			},
			{
				"operation": "insert",
				"name": "Agreementaf3ede70-a35b-4211-892d-5437c32ef7ae",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "Agreement",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "SMAgreementType2adcb003-aecc-4f64-849f-bfbc94f3299b",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "SMAgreementType",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "SMRequestTypea904aa0e-798c-435d-a9f2-fc203b8e258c",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "SMRequestType",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "SMClassification1ae3c954-9e57-48a4-99cd-5bb2eb1d2bd7",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "SMClassification",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "SMSubClassificationd70d2c88-c6c7-4115-885a-fb7a681fa038",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "SMSubClassification",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "SMDeadlineDate7438c91e-d326-41ab-996b-5a82a1d5ce77",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "SMDeadlineDate",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "SMNotifiedDatecac279ac-1c57-4877-9e42-f62feaf1839b",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "SMNotifiedDate",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Faculty317a9307-ac08-4516-b8a7-275395845e18",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "Faculty",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "SMResponseSubmitted4dbf2b5b-8887-487e-9dae-f126117dedaf",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "SMResponseSubmitted",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "ConsultationTabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.ConsultationTabLabelTabCaption"
					},
					"items": [],
					"order": 0
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "SMPartisipation",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.SMPartisipationGroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "ConsultationTabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "ConsultationTabLabelGridLayoutb4a48946",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "SMPartisipation",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "LOOKUP3b5456d1-76a6-417e-950d-18824b215a37",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "ConsultationTabLabelGridLayoutb4a48946"
					},
					"bindTo": "SMFacultyParticipationConfirmation",
					"enabled": true,
					"contentType": 3,
					"wrapClass": [
						"wider-label"
					],
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.LOOKUP3b5456d176a6417e950d18824b215a37LabelCaption"
						}
					}
				},
				"parentName": "ConsultationTabLabelGridLayoutb4a48946",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "ConsultationTabLabelGroup4ae38965",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.ConsultationTabLabelGroup4ae38965GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "ConsultationTabLabel",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "ConsultationTabLabelGridLayout227f510c",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "ConsultationTabLabelGroup4ae38965",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "NonParticipationFeedback337bb439-9074-45e6-be19-c9aae63ed218",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "ConsultationTabLabelGridLayout227f510c"
					},
					"bindTo": "NonParticipationFeedback",
					"wrapClass": [
						"wider-label"
					],
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.NonParticipationFeedback337bb439907445e6be19c9aae63ed218LabelCaption"
						}
					},
					"enabled": true,
					"contentType": 0
				},
				"parentName": "ConsultationTabLabelGridLayout227f510c",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "ConsultationTabLabelGroupfae98f37",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.ConsultationTabLabelGroupfae98f37GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "ConsultationTabLabel",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "ConsultationTabLabelGridLayout3a1d3296",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "ConsultationTabLabelGroupfae98f37",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "QuestionQ141d3d789-9c23-4b59-a5ff-e8ad6dd1b7bd",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "ConsultationTabLabelGridLayout3a1d3296"
					},
					"bindTo": "QuestionQ1",
					"wrapClass": [
						"wider-label"
					],
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.QuestionQ141d3d7899c234b59a5ffe8ad6dd1b7bdLabelCaption"
						}
					},
					"enabled": true,
					"visible": {
						"bindTo": "QuestionQ1Availability"
					},
					"contentType": 0
				},
				"parentName": "ConsultationTabLabelGridLayout3a1d3296",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "ConsultationTabLabelGroup47fe93fa",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.ConsultationTabLabelGroup47fe93faGroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "ConsultationTabLabel",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "ConsultationTabLabelGridLayout2a7a7138",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "ConsultationTabLabelGroup47fe93fa",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "QuestionQ2bd952a71-44a2-419b-89b4-b605332fd779",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "ConsultationTabLabelGridLayout2a7a7138"
					},
					"bindTo": "QuestionQ2",
					"wrapClass": [
						"wider-label"
					],
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.QuestionQ2bd952a7144a2419b89b4b605332fd779LabelCaption"
						}
					},
					"enabled": true,
					"visible": {
						"bindTo": "QuestionQ2Availability"
					},
					"contentType": 0
				},
				"parentName": "ConsultationTabLabelGridLayout2a7a7138",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "ConsultationTabLabelGroupdd3cc404",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.ConsultationTabLabelGroupdd3cc404GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "ConsultationTabLabel",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "ConsultationTabLabelGridLayout4b9d9b81",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "ConsultationTabLabelGroupdd3cc404",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "QuestionQ36e2c5902-f17e-48dc-872b-1342fc75b50d",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "ConsultationTabLabelGridLayout4b9d9b81"
					},
					"wrapClass": [
						"wider-label"
					],
					"bindTo": "QuestionQ3",
					"enabled": true,
					"visible": {
						"bindTo": "QuestionQ3Availability"
					},
					"contentType": 0,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.QuestionQ36e2c5902f17e48dc872b1342fc75b50dLabelCaption"
						}
					}
				},
				"parentName": "ConsultationTabLabelGridLayout4b9d9b81",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "ConsultationTabLabelGroup93925aa1",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.ConsultationTabLabelGroup93925aa1GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "ConsultationTabLabel",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "ConsultationTabLabelGridLayoutfbd94549",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "ConsultationTabLabelGroup93925aa1",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "QuestionQ520550b33-97c3-4d7a-bb09-eafc532f62ae",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "ConsultationTabLabelGridLayoutfbd94549"
					},
					"wrapClass": [
						"wider-label"
					],
					"bindTo": "QuestionQ5",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.QuestionQ520550b3397c34d7abb09eafc532f62aeLabelCaption"
						}
					},
					"enabled": true,
					"visible": {
						"bindTo": "QuestionQ5Availability"
					},
					"contentType": 0
				},
				"parentName": "ConsultationTabLabelGridLayoutfbd94549",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "ConsultationTabLabelGroupc0acde0a",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.ConsultationTabLabelGroupc0acde0aGroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "ConsultationTabLabel",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "ConsultationTabLabelGridLayout45c0003b",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "ConsultationTabLabelGroupc0acde0a",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "QuestionQ67a1737be-2263-4c45-a63d-a0a305e533ef",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "ConsultationTabLabelGridLayout45c0003b"
					},
					"wrapClass": [
						"wider-label"
					],
					"bindTo": "QuestionQ6",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.QuestionQ67a1737be22634c45a63da0a305e533efLabelCaption"
						}
					},
					"enabled": true,
					"contentType": 0
				},
				"parentName": "ConsultationTabLabelGridLayout45c0003b",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "ConsultationTabLabelGroup61538809",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.ConsultationTabLabelGroup61538809GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "ConsultationTabLabel",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "ConsultationTabLabelGridLayout57037f60",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "ConsultationTabLabelGroup61538809",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "QuestionQ727ae9c21-27d7-4c2a-adad-59d46efccf30",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "ConsultationTabLabelGridLayout57037f60"
					},
					"enabled": true,
					"wrapClass": [
						"wider-label"
					],
					"visible": {
						"bindTo": "QuestionQ7Availability"
					},
					"bindTo": "QuestionQ7",
					"contentType": 0,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.QuestionQ727ae9c2127d74c2aadad59d46efccf30LabelCaption"
						}
					}
				},
				"parentName": "ConsultationTabLabelGridLayout57037f60",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "ConsultationTabLabelGroupba4e7bbd",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.ConsultationTabLabelGroupba4e7bbdGroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "ConsultationTabLabel",
				"propertyName": "items",
				"index": 8
			},
			{
				"operation": "insert",
				"name": "ConsultationTabLabelGridLayout9daed20b",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "ConsultationTabLabelGroupba4e7bbd",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "QuestionQ8543d71ce-4d85-4b90-b1a8-c2a371e600d5",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "ConsultationTabLabelGridLayout9daed20b"
					},
					"wrapClass": [
						"wider-label"
					],
					"bindTo": "QuestionQ8",
					"enabled": true,
					"contentType": 0,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.QuestionQ8543d71ce4d854b90b1a8c2a371e600d5LabelCaption"
						}
					}
				},
				"parentName": "ConsultationTabLabelGridLayout9daed20b",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "ConsultationTabLabelGroupd4419c1f",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.ConsultationTabLabelGroupd4419c1fGroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "ConsultationTabLabel",
				"propertyName": "items",
				"index": 9
			},
			{
				"operation": "insert",
				"name": "ConsultationTabLabelGridLayout25ac15a6",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "ConsultationTabLabelGroupd4419c1f",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "BOOLEAN8889c9f8-3ab4-4ea3-a59a-c250b69e89cc",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "ConsultationTabLabelGridLayout25ac15a6"
					},
					"wrapClass": [
						"wider-label"
					],
					"bindTo": "FacultyAgreementforConcluding",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.BOOLEAN8889c9f83ab44ea3a59ac250b69e89ccLabelCaption"
						}
					},
					"enabled": true
				},
				"parentName": "ConsultationTabLabelGridLayout25ac15a6",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "ConsultationTabLabelGroupde2553c0",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.ConsultationTabLabelGroupde2553c0GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "ConsultationTabLabel",
				"propertyName": "items",
				"index": 10
			},
			{
				"operation": "insert",
				"name": "ConsultationTabLabelGridLayout05154997",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "ConsultationTabLabelGroupde2553c0",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "STRINGd85c3b29-4913-4266-bef2-67332b837f2a",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "ConsultationTabLabelGridLayout05154997"
					},
					"wrapClass": [
						"wider-label"
					],
					"bindTo": "FacultyFeedbackforConcluding",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.STRINGd85c3b2949134266bef267332b837f2aLabelCaption"
						}
					},
					"enabled": true,
					"contentType": 0
				},
				"parentName": "ConsultationTabLabelGridLayout05154997",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "RecoomendationAndFeedback",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.RecoomendationAndFeedbackGroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "ConsultationTabLabel",
				"propertyName": "items",
				"index": 11
			},
			{
				"operation": "insert",
				"name": "ConsultationTabLabelGridLayout44ed3327",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "RecoomendationAndFeedback",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "LOOKUPc8828365-b465-4c96-a40c-2cb556a85506",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "ConsultationTabLabelGridLayout44ed3327"
					},
					"bindTo": "RecommendationO1",
					"wrapClass": [
						"wider-label"
					],
					"visible": {
						"bindTo": "RecommendationQ3Availability"
					},
					"enabled": true,
					"contentType": 3,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.LOOKUPc8828365b4654c96a40c2cb556a85506LabelCaption"
						}
					}
				},
				"parentName": "ConsultationTabLabelGridLayout44ed3327",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "ConsultationTabLabelGroup02833302",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.ConsultationTabLabelGroup02833302GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "ConsultationTabLabel",
				"propertyName": "items",
				"index": 12
			},
			{
				"operation": "insert",
				"name": "ConsultationTabLabelGridLayout9386a2a2",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "ConsultationTabLabelGroup02833302",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "RecommendationQ243507527-c1fc-408c-a046-ab9b7b384da1",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "ConsultationTabLabelGridLayout9386a2a2"
					},
					"wrapClass": [
						"wider-label"
					],
					"bindTo": "RecommendationQ2",
					"enabled": true,
					"contentType": 0,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.RecommendationQ243507527c1fc408ca046ab9b7b384da1LabelCaption"
						}
					}
				},
				"parentName": "ConsultationTabLabelGridLayout9386a2a2",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "SMAdditionalQuestionDetail",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "ConsultationTabLabel",
				"propertyName": "items",
				"index": 13
			},
			{
				"operation": "insert",
				"name": "SMQuestions",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.SMQuestionsGroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "ConsultationTabLabel",
				"propertyName": "items",
				"index": 14
			},
			{
				"operation": "insert",
				"name": "ConsultationTabLabelGridLayout62140086",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "SMQuestions",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CompleateFormButton",
				"values": {
					"layout": {
						"colSpan": 10,
						"rowSpan": 1,
						"column": 7,
						"row": 0,
						"layoutName": "ConsultationTabLabelGridLayout62140086"
					},
					"itemType": 5,
					"caption": {
						"bindTo": "Resources.Strings.CompleateConsultation"
					},
					"click": {
						"bindTo": "CompleateConsultationFunction"
					},
					"enabled": {
						"bindTo": "SMResponseSubmitted",
						"bindConfig": {
							"converter": "invertBooleanValue"
						}
					},
					"styles": {
						"margin-left": "100px",
						"margin-right": "0px"
					},
					"style": "green"
				},
				"parentName": "ConsultationTabLabelGridLayout62140086",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "NotesAndFilesTab",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.NotesAndFilesTabCaption"
					},
					"items": [],
					"order": 1
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Files",
				"values": {
					"itemType": 2
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "NotesControlGroup",
				"values": {
					"itemType": 15,
					"caption": {
						"bindTo": "Resources.Strings.NotesGroupCaption"
					},
					"items": []
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Notes",
				"values": {
					"bindTo": "Notes",
					"dataValueType": 1,
					"contentType": 4,
					"layout": {
						"column": 0,
						"row": 0,
						"colSpan": 24
					},
					"labelConfig": {
						"visible": false
					},
					"controlConfig": {
						"imageLoaded": {
							"bindTo": "insertImagesToNotes"
						},
						"images": {
							"bindTo": "NotesImagesCollection"
						}
					}
				},
				"parentName": "NotesControlGroup",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "merge",
				"name": "ESNTab",
				"values": {
					"order": 2
				}
			}
		]/**SCHEMA_DIFF*/
	};
});
