define("SMBaseSectionGridRowViewModel", ["BaseSectionGridRowViewModel"],
	function() {

		/**
		 * @class Terrasoft.configuration.BaseSectionGridRowViewModel
		 */
		Ext.define("Terrasoft.configuration.SMBaseSectionGridRowViewModel", {
			extend: "Terrasoft.BaseSectionGridRowViewModel",
			alternateClassName: "Terrasoft.SMBaseSectionGridRowViewModel",
			
			isNotifyButtonVisible: function() {
				return Terrasoft.CurrentUser.userType !== Terrasoft.UserType.SSP;
			}
			//endregion
		});

		return Terrasoft.SMBaseSectionGridRowViewModel;
	});