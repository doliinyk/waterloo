define("BasePageV2", [], function() {
	return {
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		messages: {
			"SocketMessageReceived": {
				"mode": Terrasoft.MessageMode.BROADCAST,
				"direction": Terrasoft.MessageDirectionType.SUBSCRIBE
			}
		},
		mixins: {},
		methods: {
			init: function() {
				this.callParent(arguments);
				this.sandbox.subscribe("SocketMessageReceived", this.onSMRefreshPage, this);
			},
			onSMRefreshPage: function(args) {
				var contactId = (args || {}).contactId;
				var eventName = (args || {}).eventName;
				
				if (contactId === Terrasoft.SysValue.CURRENT_USER_CONTACT.value
					&& eventName === "SMRefreshPage") {
					this.reloadEntity();
				}
			}
		},
		diff: /**SCHEMA_DIFF*/[]/**SCHEMA_DIFF*/
	};
});
